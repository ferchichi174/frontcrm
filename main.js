(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _map_french_map_french_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./map-french/map-french.component */ "./src/app/map-french/map-french.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _test_test_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./test/test.component */ "./src/app/test/test.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");









const routes = [
    { path: '', component: _app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"] },
    { path: 'home', component: _home_home_component__WEBPACK_IMPORTED_MODULE_6__["HomeComponent"] },
    { path: 'login', component: _login_login_component__WEBPACK_IMPORTED_MODULE_4__["LoginComponent"] },
    { path: 'test', component: _test_test_component__WEBPACK_IMPORTED_MODULE_5__["TestComponent"] },
    { path: 'map', component: _map_french_map_french_component__WEBPACK_IMPORTED_MODULE_3__["MapFrenchComponent"] },
    { path: 'crm', loadChildren: () => __webpack_require__.e(/*! import() | crm-crm-module */ "crm-crm-module").then(__webpack_require__.bind(null, /*! ./crm/crm.module */ "./src/app/crm/crm.module.ts")).then(m => m.CrmModule) },
    { path: 'cpn', loadChildren: () => __webpack_require__.e(/*! import() | cpn-cpn-module */ "cpn-cpn-module").then(__webpack_require__.bind(null, /*! ./cpn/cpn.module */ "./src/app/cpn/cpn.module.ts")).then(m => m.CpnModule) },
];
class AppRoutingModule {
}
AppRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: AppRoutingModule });
AppRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function AppRoutingModule_Factory(t) { return new (t || AppRoutingModule)(); }, imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AppRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppRoutingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
                exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");




class AppComponent {
    constructor(router) {
        this.title = 'frontCrm';
        this.loading = false;
        router.events.subscribe((event) => {
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationStart"]) {
                this.loading = true;
            }
            else if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"]) {
                this.loading = false;
            }
        });
    }
}
AppComponent.ɵfac = function AppComponent_Factory(t) { return new (t || AppComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"])); };
AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AppComponent, selectors: [["app-root"]], decls: 1, vars: 0, template: function AppComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "router-outlet");
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterOutlet"]], styles: [".loader[_ngcontent-%COMP%] {\r\n    display: inline-block;\r\n    width: 40px;\r\n    height: 40px;\r\n    position: absolute;\r\n    left: 0;\r\n    right: 0;\r\n    margin-left: auto;\r\n    margin-right: auto;\r\n    top: calc(50% - 50px);\r\n    transform: translateY(-50%);\r\n  }\r\n  \r\n  .loader[_ngcontent-%COMP%]:after {\r\n    content: ' ';\r\n    display: block;\r\n    width: 30px;\r\n    height: 30px;\r\n    border-radius: 50%;\r\n    border: 5px solid #fff;\r\n    border-color: #fff transparent #fff transparent;\r\n    animation: loader 1.2s linear infinite;\r\n  }\r\n  \r\n  @keyframes loader {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(360deg);\r\n    }\r\n  }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxxQkFBcUI7SUFDckIsV0FBVztJQUNYLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsT0FBTztJQUNQLFFBQVE7SUFDUixpQkFBaUI7SUFDakIsa0JBQWtCO0lBQ2xCLHFCQUFxQjtJQUNyQiwyQkFBMkI7RUFDN0I7O0VBRUE7SUFDRSxZQUFZO0lBQ1osY0FBYztJQUNkLFdBQVc7SUFDWCxZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLHNCQUFzQjtJQUN0QiwrQ0FBK0M7SUFDL0Msc0NBQXNDO0VBQ3hDOztFQUVBO0lBQ0U7TUFDRSx1QkFBdUI7SUFDekI7SUFDQTtNQUNFLHlCQUF5QjtJQUMzQjtFQUNGIiwiZmlsZSI6InNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubG9hZGVyIHtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIHdpZHRoOiA0MHB4O1xyXG4gICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgbGVmdDogMDtcclxuICAgIHJpZ2h0OiAwO1xyXG4gICAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XHJcbiAgICB0b3A6IGNhbGMoNTAlIC0gNTBweCk7XHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XHJcbiAgfVxyXG4gIFxyXG4gIC5sb2FkZXI6YWZ0ZXIge1xyXG4gICAgY29udGVudDogJyAnO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICB3aWR0aDogMzBweDtcclxuICAgIGhlaWdodDogMzBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIGJvcmRlcjogNXB4IHNvbGlkICNmZmY7XHJcbiAgICBib3JkZXItY29sb3I6ICNmZmYgdHJhbnNwYXJlbnQgI2ZmZiB0cmFuc3BhcmVudDtcclxuICAgIGFuaW1hdGlvbjogbG9hZGVyIDEuMnMgbGluZWFyIGluZmluaXRlO1xyXG4gIH1cclxuICBcclxuICBAa2V5ZnJhbWVzIGxvYWRlciB7XHJcbiAgICAwJSB7XHJcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gICAgfVxyXG4gICAgMTAwJSB7XHJcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDM2MGRlZyk7XHJcbiAgICB9XHJcbiAgfSJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-root',
                templateUrl: './app.component.html',
                styleUrls: ['./app.component.css']
            }]
    }], function () { return [{ type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] }]; }, null); })();


/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/animations.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _material_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./material-module */ "./src/app/material-module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _security_token_interceptor_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./security/token-interceptor.service */ "./src/app/security/token-interceptor.service.ts");
/* harmony import */ var ngx_countdown__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-countdown */ "./node_modules/ngx-countdown/__ivy_ngcc__/fesm2015/ngx-countdown.js");
/* harmony import */ var _test_test_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./test/test.component */ "./src/app/test/test.component.ts");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/icon.js");
/* harmony import */ var _fullcalendar_angular__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @fullcalendar/angular */ "./node_modules/@fullcalendar/angular/__ivy_ngcc__/fesm2015/fullcalendar-angular.js");
/* harmony import */ var _fullcalendar_daygrid__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @fullcalendar/daygrid */ "./node_modules/@fullcalendar/daygrid/main.js");
/* harmony import */ var _fullcalendar_interaction__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @fullcalendar/interaction */ "./node_modules/@fullcalendar/interaction/main.js");
/* harmony import */ var _map_french_map_french_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./map-french/map-french.component */ "./src/app/map-french/map-french.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");


/**************** library      **********************************/





/**************** component      **********************************/












_fullcalendar_angular__WEBPACK_IMPORTED_MODULE_13__["FullCalendarModule"].registerPlugins([
    _fullcalendar_daygrid__WEBPACK_IMPORTED_MODULE_14__["default"],
    _fullcalendar_interaction__WEBPACK_IMPORTED_MODULE_15__["default"]
]);
class AppModule {
}
AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineNgModule"]({ type: AppModule, bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]] });
AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjector"]({ factory: function AppModule_Factory(t) { return new (t || AppModule)(); }, providers: [{
            provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HTTP_INTERCEPTORS"],
            useClass: _security_token_interceptor_service__WEBPACK_IMPORTED_MODULE_9__["TokenInterceptorService"],
            multi: true,
        }], imports: [[
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__["BrowserAnimationsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
            _material_module__WEBPACK_IMPORTED_MODULE_6__["MaterialModule"],
            _angular_material_icon__WEBPACK_IMPORTED_MODULE_12__["MatIconModule"],
            ngx_countdown__WEBPACK_IMPORTED_MODULE_10__["CountdownModule"],
            _fullcalendar_angular__WEBPACK_IMPORTED_MODULE_13__["FullCalendarModule"],
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsetNgModuleScope"](AppModule, { declarations: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"],
        _login_login_component__WEBPACK_IMPORTED_MODULE_8__["LoginComponent"],
        _test_test_component__WEBPACK_IMPORTED_MODULE_11__["TestComponent"],
        _map_french_map_french_component__WEBPACK_IMPORTED_MODULE_16__["MapFrenchComponent"],
        _home_home_component__WEBPACK_IMPORTED_MODULE_17__["HomeComponent"]], imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
        _app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"],
        _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__["BrowserAnimationsModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
        _material_module__WEBPACK_IMPORTED_MODULE_6__["MaterialModule"],
        _angular_material_icon__WEBPACK_IMPORTED_MODULE_12__["MatIconModule"],
        ngx_countdown__WEBPACK_IMPORTED_MODULE_10__["CountdownModule"],
        _fullcalendar_angular__WEBPACK_IMPORTED_MODULE_13__["FullCalendarModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](AppModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"],
        args: [{
                declarations: [
                    _app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"],
                    _login_login_component__WEBPACK_IMPORTED_MODULE_8__["LoginComponent"],
                    _test_test_component__WEBPACK_IMPORTED_MODULE_11__["TestComponent"],
                    _map_french_map_french_component__WEBPACK_IMPORTED_MODULE_16__["MapFrenchComponent"],
                    _home_home_component__WEBPACK_IMPORTED_MODULE_17__["HomeComponent"]
                ],
                imports: [
                    _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                    _app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"],
                    _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__["BrowserAnimationsModule"],
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                    _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
                    _material_module__WEBPACK_IMPORTED_MODULE_6__["MaterialModule"],
                    _angular_material_icon__WEBPACK_IMPORTED_MODULE_12__["MatIconModule"],
                    ngx_countdown__WEBPACK_IMPORTED_MODULE_10__["CountdownModule"],
                    _fullcalendar_angular__WEBPACK_IMPORTED_MODULE_13__["FullCalendarModule"],
                ],
                providers: [{
                        provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HTTP_INTERCEPTORS"],
                        useClass: _security_token_interceptor_service__WEBPACK_IMPORTED_MODULE_9__["TokenInterceptorService"],
                        multi: true,
                    }],
                bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]],
                schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["CUSTOM_ELEMENTS_SCHEMA"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/baseUrl.ts":
/*!****************************!*\
  !*** ./src/app/baseUrl.ts ***!
  \****************************/
/*! exports provided: baseUrl */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "baseUrl", function() { return baseUrl; });
const baseUrl = "http://127.0.0.1:8000";


/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_app_services_token_storage_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/token-storage.service */ "./src/app/services/token-storage.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");





class HomeComponent {
    constructor(tokenStorage, route) {
        this.tokenStorage = tokenStorage;
        this.route = route;
    }
    ngOnInit() {
        $(document).mousemove(function (e) {
            $('#info-box').css('top', e.pageY - $('#info-box').height() - 30);
            $('#info-box').css('left', e.pageX - ($('#info-box').width()) / 2);
        }).mouseover();
        $('.search').mouseenter(function () {
            $(this).addClass('search--show');
            $(this).removeClass('search--hide');
        });
        $('.search').mouseleave(function () {
            $(this).addClass('search--hide');
            $(this).removeClass('search--show');
        });
        $(document).ready(function () {
            $('.item_num').counterUp({
                time: 2000
            });
        });
    }
}
HomeComponent.ɵfac = function HomeComponent_Factory(t) { return new (t || HomeComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_token_storage_service__WEBPACK_IMPORTED_MODULE_1__["TokenStorageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"])); };
HomeComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: HomeComponent, selectors: [["app-home"]], decls: 341, vars: 9, consts: [["rel", "stylesheet", "href", "https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css", "integrity", "sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l", "crossorigin", "anonymous"], [1, "navbar", "navbar-expand-lg", "nav_g"], [1, "navbar-brand", 3, "routerLink"], ["src", "assets/cpnimages/sitecpn/logo-blanc-png.png", "alt", "Logo", 1, "brand_logo", "d-inline-block", "align-text-top", "nav_img"], ["type", "button", "data-toggle", "collapse", "data-target", "#navbarSupportedContent", "aria-controls", "navbarSupportedContent", "aria-expanded", "false", "aria-label", "Toggle navigation", 1, "navbar-toggler", "nav_t"], [1, "navbar-toggler-icon"], ["id", "navbarSupportedContent", 1, "collapse", "navbar-collapse"], [1, "navbar-nav", "ml-auto", "topnav"], [1, "nav-item"], [1, "nav-link", 3, "routerLink"], ["href", "", 1, "nav-link"], [1, "nav-link", "con", 3, "routerLink"], [1, "primary_body", "mb-5"], [1, "home_container"], [1, "section_heading", "mb-3"], [1, "heading_wrapper", "container-fluid", "g-0"], [1, "row", "g-0"], [1, "col-md-8", "col-12"], [1, "row", "g-0", "justify-content-center"], [1, "col-auto"], [1, "img_wrapper", "content"], ["src", "assets/img/33.png", "alt", "", 1, "heading_img"], [1, "col-md-4", "p-3"], [1, "row", "g-0", "justify-content-start"], [1, "col-md-auto", "transtion"], [1, "title_heading"], [1, "desc_heading"], [1, "search_bloc"], [1, "search"], ["placeholder", "Quel type de subvention souhaitez vous", 1, "search__input"], [1, "carre"], [1, "row"], [1, "col", 2, "display", "initial", "flex-direction", "row", "padding", "8px"], ["src", "assets/img/I.png", "alt", "", 2, "width", "10%", "margin", "0 0 0 30px"], [2, "margin-left", "4px", "font-size", "13px"], [2, "text-align", "center", "margin", "0 90px 0 0"], [2, "text-align", "center", "font-size", "12px", "margin", "0 0px 0 -45px"], [2, "color", "#00FF00"], [1, "our_success", "mb-3"], [1, "success_wrapper", "container", "px-4", "g-0"], [1, "col-md-6", "py-2"], [1, "success_txt"], [1, "success_desc"], [1, "col-md-6", "py-2", 2, "display", "flex", "flex-direction", "column", "justify-content", "center", "width", "40%", "align-items", "inherit"], [1, "success_list"], [1, "success_item"], [1, "k"], [1, "item_num"], [1, "item_desc"], [1, "actuality", "mb-5"], [1, "float_actions"], [1, "actions_content"], [1, "action_items"], ["href", "cpn/Home_tpe_pme", 1, "item_href"], [1, "ihref_logo"], ["width", "40%", "src", "assets/img/Entreprise.png", "alt", ""], [1, "ihref_text"], ["href", "/cpn/agence", 1, "item_href"], ["width", "40%", "src", "assets/img/Agence.png"], ["href", "/cpn/Home_collectivite", 1, "item_href"], ["width", "40%", "src", "assets/img/Collectivit\u00E9.png"], [1, "card-blog", "mb-5"], [1, "card_wrapper", "container", "px-4", "g-0"], [1, "row", "py-3"], [1, "col-md-4"], [1, "card", "d-flex", "flex-column", "align-items-center", "justify-content-center", "threeB"], ["src", "assets/cpnimages/sitecpn/agent.png", "alt", "...", 1, "card-img-top", 2, "height", "50%", "width", "50%"], [1, "card-body", "py-0", "d-flex", "flex-column", "justify-content-center", "align-items-center"], [1, "card-title", "home"], [1, "card-text", "home"], [1, "card-footer", "py-0"], ["href", "#", 1, "test-btn3", "btn", "btn-light", 2, "margin-top", "24px", "border-radius", "20px"], ["src", "assets/cpnimages/sitecpn/casque.png", "alt", "...", 1, "card-img-top", 2, "height", "50%", "width", "50%"], [1, "card-text", "home", 2, "text-align", "center"], ["href", "#", 1, "test-btn3", "btn", "btn-danger", 2, "border-radius", "20px", "background-color", "#ce1212", "text-align", "center"], ["src", "assets/cpnimages/sitecpn/money.png", "alt", "...", 1, "card-img-top", 2, "height", "50%", "width", "50%"], ["href", "#", 1, "test-btn3", "btn", "btn-light", 2, "border-radius", "20px"], [1, "text-bloc", "g-0", "mb-5", "block3"], [1, "container", "px-4"], [1, "text_body", 2, "margin-left", "15px"], [1, "text_content", 2, "font-size", "70px", "color", "#111d5e"], [2, "margin-top", "-75px", "margin-left", "-17px", "color", "#111d5e"], [1, "divider", "g-0", "mb-5"], [1, "divider_ligne"], [1, "second-bloc"], [1, "container", "px-4", "block1"], [2, "color", "#111d5e", "font-weight", "bold"], [1, "wavy"], [1, "outer-div", "one"], [1, "inner-div"], [1, "front"], [1, "front__face-photo1"], ["src", "assets/cpnimages/sitecpn/icone-2.png", "alt", "", 2, "width", "100%"], [1, "front__text"], [1, "front__text-header"], [1, "front__text-para"], [1, "outer-div", "two"], [1, "front__face-photo2"], ["src", "assets/cpnimages/sitecpn/icone-1.png", "alt", "", 2, "width", "100%"], [1, "outer-div", "three"], [1, "front__face-photo3"], ["src", "assets/cpnimages/sitecpn/icone-3.png", "alt", "", 2, "width", "100%"], [1, "third-bloc", "g-0", "mb-5"], [1, "container", "px-4", "text-center"], [2, "color", "#111d5e"], [1, "subvention-text", "text-center", 2, "color", "gray"], [1, "col-6", "block5"], ["src", "assets/cpnimages/sitecpn/old.jpg", "alt", "..."], [1, "col-6", "block6"], [1, "third-bloc-border", 2, "border-radius", "20px", "border", "2px solid blue", "width", "90%", "padding", "20px"], [2, "font-size", "26px", "font-weight", "700", "color", "blue"], [1, "third-bloc-border", 2, "border-radius", "20px", "border", "2px solid #C5C5C5", "margin-top", "20px", "width", "90%", "padding", "20px"], [2, "font-size", "26px", "font-weight", "700"], [1, "fourth-bloc", 2, "padding-top", "100px"], [1, "container", "px-4", "lastB"], [1, "text-center"], [1, "row", "row-cols-1", "row-cols-md-3", "g-4", "justify-content-center"], [1, "col-md-3", "card1"], [1, "card", "h-100", "box"], [1, "card-body"], [1, "card-text"], [1, "image"], ["src", "assets/cpnimages/sitecpn/avis3.png", "alt", " avis 1", 1, "img-fluid"], [1, "col-md-3", 2, "margin-right", "50px", "height", "345px", "width", "315px"], [1, "card", "h-100", 2, "box-shadow", "0px 1px 15px grey", "border", "none", "border-radius", "71px 14px 71px 14px", "background-color", "#ffffff", "padding", "40px"], [2, "padding", "44px 0 0 26px", "font-size", "20px", "color", "#00BFFF"], [2, "padding", "0px 0 0px 54px", "font-size", "10px", "color", "#c7c7c7"], [2, "position", "relative", "top", "-132px", "left", "-116px", "height", "100px", "width", "100px", "margin", "0 auto", "border-radius", "50%", "background-size", "contain", "overflow", "hidden"], ["src", "assets/cpnimages/sitecpn/avis1.png", "alt", " avis 2", 1, "img-fluid", 2, "width", "120%", "height", "120%"], [1, "card-text", 2, "margin-right", "-43px"], [2, "padding", "0px 0 0 26px", "font-size", "20px", "color", "#00BFFF"], ["src", "assets/cpnimages/sitecpn/avis2.png", "alt", "avis 3", 1, "img-fluid", 2, "width", "120%", "height", "120%"], [1, "text-lg-start", "text-muted"], [1, "d-flex", "justify-content-center", "justify-content-lg-between"], [1, ""], [1, "container", "text-md-start", "mt-5", "footer", 2, "font-size", "12px"], [1, "row", "mt-3"], [1, "col-md-3", "col-lg-4", "col-xl-3", "mx-auto"], [1, "text-uppercase", "fw-bold", "mb-4"], ["src", "assets/cpnimages/sitecpn/logo-cpn3.png", "alt", "logo", "width", "50px", "height", "50px"], [2, "color", "white", "font-size", "12px"], [2, "font-size", "25px"], ["aria-hidden", "true", 1, "fab", "fa-instagram"], ["aria-hidden", "true", 1, "fab", "fa-youtube", 2, "margin-left", "5px"], ["aria-hidden", "true", 1, "fab", "fa-facebook", 2, "margin-left", "5px"], ["aria-hidden", "true", 1, "fab", "fa-linkedin", 2, "margin-left", "5px"], [1, "col-md-2", "col-lg-2", "col-xl-2", "mx-auto"], [1, "text-uppercase", "fw-bold", "mb-4", 2, "color", "white"], ["href", "#!", 1, "text-reset", 2, "color", "white"], [1, "col-md-3", "col-lg-2", "col-xl-2", "mx-auto"], ["href", "#!", 1, "text-reset", "text-left", 2, "color", "white"], [1, "text-reset", 2, "color", "white", 3, "routerLink"], [1, "col-md-4", "col-lg-3", "col-xl-3", "mx-auto", "mb-md-0"], [2, "color", "white"], [1, "fas", "fa-phone", "me-3"], [2, "color", "white", "width", "max-content"], [1, "fas", "fa-envelope", "me-3"], ["href", "mailto:votreconseiller@cpn-aide-aux-entreprise.com", 2, "color", "#fff", "width", "max-content", "text-decoration", "none"], [1, "text-center", 2, "background-color", "#0c133a", "color", "#fff", "font-size", "13px"], ["href", "https://jobid.fr/", 1, "text-reset", "fw-bold", 2, "color", "white"]], template: function HomeComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "link", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "nav", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "a", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "img", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "button", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "span", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "li", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Acceuil");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "li", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Actualit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "li", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, " Subvention");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "li", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, " Agenda +");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "li", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "a", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "A propos");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "li", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, " Contactez-nous");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "li", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, " Inscription");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "li", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "a", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, " Connexion");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "section", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](41, "img", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "div", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "h2", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, "Transition Num\u00E9rique");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "h4", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](48, "2021");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "form", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](51, "input", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "div", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](55, "img", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "span", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](57, "Followers");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "h4", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](59, "1500K");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "p", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "a", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](62, "2.1%");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](63, "vs last 7 days");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "section", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "div", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "h5", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](69, "Notre succ\u00E9s");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "h3", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](71, "Peut importe votre secteur d'acitivit\u00E9 nous vous offrons une subvention");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "div", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "ul", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "li", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](75, "h3", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "span", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](77, "562");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](78, "K");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](79, "p", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](80, "Entreprises");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "li", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](82, "h3", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "span", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](84, "10");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](85, "K");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "p", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](87, "Subventions");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](88, "li", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](89, "h3", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](90, "span", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](91, "200");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](92, "K+ ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](93, "p", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](94, "Adh\u00E9rants");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](95, "section", 49);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](96, "div", 50);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](97, "ul", 51);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](98, "li", 52);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "a", 53);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](100, "i", 54);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](101, "img", 55);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](102, "p", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](103, "Entreprise");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](104, "li", 52);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](105, "a", 57);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](106, "i", 54);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](107, "img", 58);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](108, "p", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](109, "Agence");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](110, "li", 52);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](111, "a", 59);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](112, "i", 54);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](113, "img", 60);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](114, "p", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](115, "Collectivites");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](116, "section", 61);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](117, "div", 62);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](118, "div", 63);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](119, "div", 64);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](120, "div", 65);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](121, "img", 66);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](122, "div", 67);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](123, "h5", 68);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](124, "Subvention imm\u00E9diate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](125, "p", 69);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](126, "Besoin d'un ch\u00E8que Num\u00E9rique");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](127, "div", 70);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](128, "a", 71);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](129, "Testez Votre \u00E9ligibilit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](130, "div", 64);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](131, "div", 65);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](132, "img", 72);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](133, "div", 67);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](134, "h5", 68);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](135, "Accompagnement");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](136, "p", 73);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](137, "Des Experts \u00E0 votre disposition pour digitaliser votre entreprise");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](138, "div", 70);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](139, "a", 74);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](140, "En savoir plus");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](141, "div", 64);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](142, "div", 65);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](143, "img", 75);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](144, "div", 67);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](145, "h5", 68);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](146, "Financement");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](147, "p", 73);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](148, "Obtenez le imm\u00E9diatement sur votre devis ou facture");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](149, "div", 70);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](150, "a", 76);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](151, "En savoir plus");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](152, "section", 77);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](153, "div", 78);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](154, "ul", 79);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](155, "li", 80);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](156, "h1", 81);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](157, " Conseils et Accompagnement");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](158, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](159, "Le CPN est un acteur majeur dans la transition digital des entreprises nous subventionnons et accompagnons tous type de projet de d\u00E9veloppement informatique nous vous orientons au-pr\u00E8s d\u2019agence de d\u00E9veloppement v\u00E9rifier et d\u00E9sign\u00E9.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](160, "section", 82);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](161, "div", 78);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](162, "span", 83);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](163, "section", 84);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](164, "div", 85);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](165, "h1", 86);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](166, "Facilit\u00E9 & Rapidit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](167, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](168, " Tous les services du CPN Aide aux entreprises sont destin\u00E9e aux petites et moyennes entreprises et Start-up. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](169, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](170, "Nous faisons un suivie et donnons acc\u00E8s a notre r\u00E9seau de partenaires et d\u2019entreprise pour favoris\u00E9 leurs croissance. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](171, "div", 87);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](172, "div", 88);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](173, "div", 89);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](174, "div", 90);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](175, "div", 91);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](176, "img", 92);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](177, "div", 93);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](178, "h3", 94);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](179, "Agence");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](180, "p", 95);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](181, "Grace au Cpn Soyez accompagn\u00E9 au pr\u00E9s d'agence garentie et referenc\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](182, "div", 96);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](183, "div", 89);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](184, "div", 90);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](185, "div", 97);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](186, "img", 98);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](187, "div", 93);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](188, "h3", 94);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](189, "Transformation ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](190, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](191, " digitale");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](192, "p", 95);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](193, "Grace au Cpn Soyez accompagn\u00E9 au pr\u00E9s d'agence garentie et referenc\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](194, "div", 99);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](195, "div", 89);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](196, "div", 90);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](197, "div", 100);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](198, "img", 101);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](199, "div", 93);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](200, "h3", 94);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](201, "Financement ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](202, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](203, " Im\u00E9diat");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](204, "p", 95);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](205, "Grace au Cpn Soyez accompagn\u00E9 au pr\u00E9s d'agence garentie et referenc\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](206, "section", 102);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](207, "div", 103);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](208, "h1", 104);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](209, "Pour obtenir votre subvention");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](210, "p", 105);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](211, " Le CPN s'engage \u00E0 vous mettre en relation avec une agence. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](212, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](213, " Le cabinet vous permet d'obtenir une subvention sur votre ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](214, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](215, " d\u00E9veloppement informatique, et vous met a disposition \u00E9galement ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](216, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](217, " sont r\u00E9seaux d'entreprises et de partenaire international. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](218, "div", 106);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](219, "img", 107);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](220, "div", 108);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](221, "div", 109);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](222, "h3", 110);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](223, "Economiser");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](224, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](225, "nos subventions sont calcul\u00E9es par rapport \u00E0 votre investissement quel que soit le poids de votre projet digital. Nos aides varient de 1000 \u00E0 10 000 euro de subvention imm\u00E9diate.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](226, "div", 111);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](227, "h3", 112);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](228, "Gagner du temps");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](229, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](230, " Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](231, "div", 111);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](232, "h3", 112);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](233, "Service de qualit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](234, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](235, "Tous nos conseillers d\u00E9tiennent un domaine d\u2019expertise qui leurs est propre et pourront vous accompagner dans la r\u00E9alisation de vos projets.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](236, "section", 113);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](237, "div", 114);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](238, "div", 115);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](239, "h1", 104);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](240, "Avis D'entreprises");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](241, "p", 105);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](242, " Ils nous ont fait confiance , ont \u00E9t\u00E9 accompagn\u00E9s par le CPN et ont r\u00E9ussi \u00E0 avoir leur ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](243, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](244, " subventions ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](245, "div", 116);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](246, "div", 117);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](247, "div", 118);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](248, "div", 119);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](249, "p", 120);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](250, "\"J\u2019ai \u00E9t\u00E9 accompagn\u00E9 par le CPN et j\u2019ai eu ma subvention, excellent service.\"");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](251, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](252, "Justin Rhodes");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](253, "h6");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](254, "Marketing officer");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](255, "div", 121);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](256, "img", 122);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](257, "div", 123);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](258, "div", 124);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](259, "div", 119);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](260, "p", 120);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](261, "\"Rapidit\u00E9, efficacit\u00E9 et un tr\u00E8s bon service client, le CPN m\u2019a aid\u00E9 \u00E0 num\u00E9riser mon entreprise.\"");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](262, "h4", 125);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](263, "Justin Rhodes");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](264, "h6", 126);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](265, "Marketing officer");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](266, "div", 127);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](267, "img", 128);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](268, "div", 123);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](269, "div", 124);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](270, "div", 119);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](271, "p", 129);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](272, "\"Gr\u00E2ce \u00E0 la digitalisation de mon entreprise j\u2019ai pu augmenter mon chiffre d\u2019affaire, et c\u2019est gr\u00E2ce au CPN que j\u2019ai pu \u00EAtre bien accompagn\u00E9 et conseill\u00E9.\" ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](273, "h4", 130);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](274, "Justin Rhodes");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](275, "h6", 126);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](276, "Marketing officer");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](277, "div", 127);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](278, "img", 131);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](279, "footer", 132);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](280, "section", 133);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](281, "section", 134);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](282, "div", 135);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](283, "div", 136);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](284, "div", 137);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](285, "h6", 138);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](286, "img", 139);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](287, "p", 140);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](288, " Le Cabinet de Propulsion Num\u00E9rique aide les entreprises \u00E0 se propulser num\u00E9riquement et \u00E0 b\u00E9n\u00E9ficier de financement. CPN est un organisme de financement \u00E0 but non lucratif. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](289, "p", 141);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](290, "i", 142);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](291, "i", 143);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](292, "i", 144);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](293, "i", 145);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](294, "div", 146);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](295, "h6", 147);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](296, " Menu ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](297, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](298, "a", 148);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](299, "Acceuil");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](300, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](301, "a", 148);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](302, "Actualit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](303, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](304, "a", 148);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](305, "Agenda");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](306, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](307, "a", 148);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](308, "A propos");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](309, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](310, "a", 148);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](311, "Contactez-nous");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](312, "div", 149);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](313, "h6", 147);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](314, " Support ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](315, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](316, "a", 150);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](317, "FAQ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](318, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](319, "a", 150);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](320, "Inscription");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](321, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](322, "a", 150);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](323, "Actualit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](324, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](325, "a", 151);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](326, "Contact");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](327, "div", 152);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](328, "h6", 147);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](329, " Contact ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](330, "p", 153);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](331, "i", 154);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](332, "+33 0184142394");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](333, "p", 155);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](334, "i", 156);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](335, "a", 157);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](336, "votreconseiller@cpn-aide-aux-entreprise.com");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](337, "div", 158);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](338, " \u00A9 2021 Copyright:Tous droits r\u00E9serv\u00E9s ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](339, "a", 159);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](340, "Jobid.fr");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/home");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/home");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/cpn/Actualite");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/cpn/Subvention");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/cpn/agenda");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/cpn/Contact");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/cpn/Inscription");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/cpn/Connexion");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](295);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "Contact");
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterLinkWithHref"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgForm"]], styles: ["footer[_ngcontent-%COMP%]{\r\n  background: #111D5E !important;\r\n  color: white !important;\r\n\r\n}\r\n\r\n.navbar-nav[_ngcontent-%COMP%]{\r\n  flex-direction: row;\r\n  justify-content: space-between;\r\n  display: flex;\r\n  font-size: 14px;\r\n  }\r\n\r\n.nav-link[_ngcontent-%COMP%]{\r\n      color: white !important;\r\n  }\r\n\r\n.navlinkwhit[_ngcontent-%COMP%]{\r\n      color: #111D5E !important;\r\n  }\r\n\r\n.con[_ngcontent-%COMP%]{\r\n      color: white !important;\r\n      border: none;\r\n      background: red;\r\n      border-radius: 25px;\r\n      width: 120px;\r\n      text-align: center;\r\n  height: 100%;\r\n  display: flex;\r\n  justify-content: center;\r\n  align-items: center;\r\n  }\r\n\r\n.topnav[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:hover {\r\n      border-bottom: 0.1px solid red;\r\n\r\n  }\r\n\r\n.nav_t[_ngcontent-%COMP%]{\r\n      color: white !important;\r\n  }\r\n\r\n.nav_g[_ngcontent-%COMP%]{\r\n      background-color: #111D5E;\r\n  }\r\n\r\n.navwhit[_ngcontent-%COMP%]{\r\n      background-color: #EBECF0;\r\n\r\n  }\r\n\r\n.nav_img[_ngcontent-%COMP%]{\r\n      width: 80px;\r\n      margin-bottom: 10px;\r\n  }\r\n\r\n.navbar-brand[_ngcontent-%COMP%] {\r\n      display: inline-block;\r\n      padding-top: .3125rem;\r\n      padding-bottom: .3125rem;\r\n      margin-right: 1rem;\r\n      font-size: 1.25rem;\r\n      line-height: inherit;\r\n      white-space: nowrap;\r\n      margin-left: 75px;\r\n      z-index: 5;\r\n  }\r\n\r\n\r\n\r\n.float_actions[_ngcontent-%COMP%] {\r\nposition: fixed;\r\nbackground: red;\r\nborder-radius: 10px;\r\nleft: 0;\r\ntop: 35%;\r\npadding: 10px;\r\ndisplay: flex;\r\njustify-content: center;\r\nalign-items: center;\r\nwidth: 7%;\r\n}\r\n\r\n.float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%] {\r\npadding: 0;\r\nmargin: 0 0 -30px 0;\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: space-between;\r\n}\r\n\r\n.float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%] {\r\npadding: 5px;\r\nwidth: 120px;\r\nheight: 120px;\r\ndisplay: flex;\r\njustify-content: center;\r\nalign-content: center;\r\nposition: relative;\r\n}\r\n\r\n.float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]::before{\r\ncontent: \">\";\r\nposition: absolute;\r\nright: -30px;\r\ntop: 15%;\r\ncolor: white;\r\nfont-size: 20px;\r\nwidth: 40%;\r\nfont-weight: bold;\r\n}\r\n\r\n.float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]:hover:after{\r\ncontent: \"Testez mon \u00E9gibilit\u00E9\";\r\nposition: absolute;\r\nright: -200px;\r\ntop: 15%;\r\ncolor: black;\r\nfont-size: 17px;\r\nwidth: 40%;\r\nbackground: white;\r\nwidth: 180px;\r\nborder-radius: 25px;\r\ntext-align: center;\r\nheight: 40px;\r\ndisplay: flex;\r\njustify-content: center;\r\nalign-items: center;\r\n}\r\n\r\n.float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%] {\r\ntext-decoration: none;\r\ndisplay: block;\r\nflex-direction: column;\r\njustify-content: center;\r\nfont-size: 14px;\r\n}\r\n\r\n.float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%]   .ihref_logo[_ngcontent-%COMP%] {\r\nwidth: 100px;\r\nheight: 100px;\r\nmargin-left: 25px;\r\n}\r\n\r\n.float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%]   .ihref_text[_ngcontent-%COMP%] {\r\ntext-align: center;\r\nmargin: 0;\r\ncolor: white;\r\nmargin-top: 7px;\r\n}\r\n\r\n\r\n\r\n.outer-div[_ngcontent-%COMP%], .inner-div[_ngcontent-%COMP%] {\r\n  height: 378px;\r\n  max-width: 300px;\r\n  margin: 0 auto;\r\n  position: relative;\r\n}\r\n\r\n.outer-div[_ngcontent-%COMP%] {\r\n  perspective: 900px;\r\n  perspective-origin: 50% calc(50% - 18em);\r\n}\r\n\r\n.one[_ngcontent-%COMP%]{\r\nmargin: 11px 0px 0px 556px\r\n}\r\n\r\n.two[_ngcontent-%COMP%]{\r\nmargin: -580px 0 0 1065px\r\n}\r\n\r\n.three[_ngcontent-%COMP%]{\r\nmargin: -12px 0 0 1059px\r\n}\r\n\r\n.inner-div[_ngcontent-%COMP%] {\r\n  margin: 0 auto;\r\n  border-radius: 5px;\r\n  font-weight: 400;\r\n  color: black;\r\n  font-size: 1rem;\r\n  text-align: center;\r\n \r\n}\r\n\r\n[_ngcontent-%COMP%]:hover   .social-icon[_ngcontent-%COMP%] {\r\n  opacity: 1;\r\n  top: 0;\r\n}\r\n\r\n\r\n\r\n.outer-div[_ngcontent-%COMP%]:hover   .inner-div[_ngcontent-%COMP%] {\r\n  \r\n}\r\n\r\n.front[_ngcontent-%COMP%], .back[_ngcontent-%COMP%] {\r\n  position: relative;\r\n  top: 0;\r\n  left: 0;\r\n  -webkit-backface-visibility: hidden;\r\n          backface-visibility: hidden;\r\n}\r\n\r\n.front[_ngcontent-%COMP%] {\r\n  cursor: pointer;\r\n  height: 85%;\r\n  background: white;\r\n  -webkit-backface-visibility: hidden;\r\n          backface-visibility: hidden;\r\n  box-shadow: 0 0 40px rgba(0, 0, 0, 0.1) inset;\r\n  box-shadow: 0px 1px 15px grey;\r\n  border-radius: 25px;\r\n\r\n}\r\n\r\n.front__face-photo1[_ngcontent-%COMP%] {\r\n  position: relative;\r\n  top: 10px;\r\n  height: 120px;\r\n  width: 120px;\r\n  margin: 0 auto;\r\n  border-radius: 50%;\r\n\r\n  background-size: contain;\r\n  overflow: hidden;\r\n  \r\n}\r\n\r\n.front__face-photo2[_ngcontent-%COMP%] {\r\n  position: relative;\r\n  top: 10px;\r\n  height: 120px;\r\n  width: 120px;\r\n  margin: 0 auto;\r\n  border-radius: 50%;\r\n  background-size: contain;\r\n  overflow: hidden;\r\n  \r\n}\r\n\r\n.front__face-photo3[_ngcontent-%COMP%] {\r\n  position: relative;\r\n  top: 10px;\r\n  height: 120px;\r\n  width: 120px;\r\n  margin: 0 auto;\r\n  border-radius: 50%;\r\n\r\n  background-size: contain;\r\n  overflow: hidden;\r\n  \r\n}\r\n\r\n.front__text[_ngcontent-%COMP%] {\r\n  position: relative;\r\n  top: 35px;\r\n  margin: 0 auto;\r\n  font-family: \"Montserrat\";\r\n  font-size: 18px;\r\n  -webkit-backface-visibility: hidden;\r\n          backface-visibility: hidden;\r\n}\r\n\r\n.front__text-header[_ngcontent-%COMP%] {\r\n  font-weight: 700;\r\n  font-family: \"Oswald\";\r\n  text-transform: uppercase;\r\n  font-size: 20px;\r\n}\r\n\r\n.front__text-para[_ngcontent-%COMP%] {\r\n  position: relative;\r\n  top: -5px;\r\n  color: #000;\r\n  font-size: 14px;\r\n  letter-spacing: 0.4px;\r\n  font-weight: 400;\r\n  font-family: \"Montserrat\", sans-serif;\r\n}\r\n\r\n.front-icons[_ngcontent-%COMP%] {\r\n  position: relative;\r\n  top: 0;\r\n  font-size: 14px;\r\n  margin-right: 6px;\r\n  color: gray;\r\n}\r\n\r\n.front__text-hover[_ngcontent-%COMP%] {\r\n  position: relative;\r\n  top: 10px;\r\n  font-size: 10px;\r\n  color: red;\r\n  -webkit-backface-visibility: hidden;\r\n          backface-visibility: hidden;\r\n\r\n  font-weight: 700;\r\n  text-transform: uppercase;\r\n  letter-spacing: .4px;\r\n\r\n  border: 2px solid red;\r\n  padding: 8px 15px;\r\n  border-radius: 30px;\r\n\r\n  background: red;\r\n  color: white;\r\n}\r\n\r\n.back[_ngcontent-%COMP%] {\r\n  transform: rotateY(180deg);\r\n  position: absolute;\r\n  top: 0;\r\n  left: 0;\r\n  height: 85%;\r\n  width: 100%;\r\n  background-color: #fffefe;\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: center;\r\n  align-items: center;\r\n  box-shadow: 0px 1px 15px grey;\r\n  border-radius: 25px;\r\n\r\n}\r\n\r\n.carre[_ngcontent-%COMP%] {\r\nwidth: 200px;\r\nheight: 90px;\r\nbackground: white;\r\nborder-radius: 18px;\r\nmargin-left: -290px;\r\nmargin-top: 335px;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%] {\r\n background:#111d5e;\r\n min-height: -moz-fit-content;\r\n min-height: fit-content;\r\n border-bottom-right-radius: 100px;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .img_wrapper[_ngcontent-%COMP%]   .heading_img[_ngcontent-%COMP%] {\r\n height: 100%;\r\n max-height: 585px;\r\n margin-top: 1;\r\n margin-left: -13px;\r\n width: 1050px;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .title_heading[_ngcontent-%COMP%] {\r\n font-size: 60px;\r\n font-weight: 800;\r\n color:  #ffffff;\r\n width: max-content;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .desc_heading[_ngcontent-%COMP%] {\r\n font-size: 40px;\r\n font-weight: 700;\r\n color:  #ffffff;\r\n width: max-content;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .our_success[_ngcontent-%COMP%]   .success_wrapper[_ngcontent-%COMP%]   .success_txt[_ngcontent-%COMP%] {\r\n text-transform: uppercase;\r\n font-weight: 400;\r\n font-size: 14px;\r\n color: #111d5e;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .our_success[_ngcontent-%COMP%]   .success_wrapper[_ngcontent-%COMP%]   .success_desc[_ngcontent-%COMP%] {\r\n font-size: 28px;\r\n color: #111d5e;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .our_success[_ngcontent-%COMP%]   .success_wrapper[_ngcontent-%COMP%]   .success_list[_ngcontent-%COMP%] {\r\n display: flex;\r\n justify-content: space-between;\r\n margin: 0;\r\n padding: 0;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .our_success[_ngcontent-%COMP%]   .success_wrapper[_ngcontent-%COMP%]   .success_list[_ngcontent-%COMP%]   .success_item[_ngcontent-%COMP%] {\r\n list-style: none;\r\n display: flex;\r\n flex-direction: column;\r\n justify-content: center;\r\n align-items: center;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .our_success[_ngcontent-%COMP%]   .success_wrapper[_ngcontent-%COMP%]   .success_list[_ngcontent-%COMP%]   .success_item[_ngcontent-%COMP%]   .item_num[_ngcontent-%COMP%] {\r\n margin: 0;\r\n font-weight: 700;\r\n color: #111d5e;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .our_success[_ngcontent-%COMP%]   .success_wrapper[_ngcontent-%COMP%]   .success_list[_ngcontent-%COMP%]   .success_item[_ngcontent-%COMP%]   .item_desc[_ngcontent-%COMP%] {\r\n margin: 0;\r\n color: #111d5e;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .card-blog[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%] {\r\n background-color: #111d5e;\r\n border-radius: 30px;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .card-blog[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]   .card-body[_ngcontent-%COMP%] {\r\n margin-bottom: 14px;\r\n margin-top: -22px;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .card-blog[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]   .card-body[_ngcontent-%COMP%]   .test-btn1[_ngcontent-%COMP%] {\r\n border-radius: 20px;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .card-blog[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]   .card-body[_ngcontent-%COMP%]   .test-btn2[_ngcontent-%COMP%] {\r\n border: none;\r\n background: red;\r\n border-radius: 25px;\r\n color: white;\r\n padding: 8px 30px;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .card-blog[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]   .card-body[_ngcontent-%COMP%]   .test-btn3[_ngcontent-%COMP%] {\r\n border-radius: 20px;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .card-blog[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]   .card-group[_ngcontent-%COMP%] {\r\n text-align: center;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .card-blog[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]   .card-group[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%] {\r\n border: none;\r\n position: relative;\r\n display: flex;\r\n flex-direction: column;\r\n min-width: 0;\r\n word-wrap: break-word;\r\n background-color: #111d5e;\r\n border-radius: 12.25rem;\r\n color: white;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .card-blog[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]   .card-group[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%]   .card-img-top[_ngcontent-%COMP%] {\r\n width: 50%;\r\n margin-left: 91px;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .french_map[_ngcontent-%COMP%]   .map_wrapper[_ngcontent-%COMP%]   .map_holder[_ngcontent-%COMP%]   .map_svg[_ngcontent-%COMP%]   g[_ngcontent-%COMP%]   path[_ngcontent-%COMP%] {\r\n fill: gray;\r\n stroke-width: 1px;\r\n stroke: gray;\r\n cursor: pointer;\r\n transition: 0.3s;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .french_map[_ngcontent-%COMP%]   .map_wrapper[_ngcontent-%COMP%]   .map_holder[_ngcontent-%COMP%]   .map_svg[_ngcontent-%COMP%]   g[_ngcontent-%COMP%]   path[_ngcontent-%COMP%]:hover {\r\n fill: red;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .divider[_ngcontent-%COMP%]   .divider_ligne[_ngcontent-%COMP%] {\r\n width: 100%;\r\n height: 5px;\r\n background: #111d5e;\r\n display: block;\r\n position: relative;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .divider[_ngcontent-%COMP%]   .divider_ligne[_ngcontent-%COMP%]::before {\r\n content: \"\";\r\n position: absolute;\r\n width: 30%;\r\n top: 0;\r\n left: 0;\r\n height: 5px;\r\n background: red;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .divider[_ngcontent-%COMP%]   .divider_ligne[_ngcontent-%COMP%]::after {\r\n content: \"\";\r\n position: absolute;\r\n width: 30%;\r\n top: 0;\r\n right: 0;\r\n height: 5px;\r\n background: red;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .actuality[_ngcontent-%COMP%]   .actuality_wrapper[_ngcontent-%COMP%]   .title[_ngcontent-%COMP%]   .title_txt[_ngcontent-%COMP%] {\r\n font-size: 45px;\r\n font-weight: 800;\r\n position: relative;\r\n margin: 0;\r\n color: #111d5e;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .actuality[_ngcontent-%COMP%]   .actuality_wrapper[_ngcontent-%COMP%]   .title[_ngcontent-%COMP%]   .title_txt[_ngcontent-%COMP%]::after {\r\n content: \"\";\r\n width: 170px;\r\n height: 3px;\r\n bottom: 0;\r\n left: 90px;\r\n background: #111d5e;\r\n position: absolute;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .actuality[_ngcontent-%COMP%]   .actuality_wrapper[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%] {\r\n width: 100%;\r\n height: -moz-fit-content;\r\n height: fit-content;\r\n overflow: hidden;\r\n border-radius: 5px;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .actuality[_ngcontent-%COMP%]   .actuality_wrapper[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .card_img[_ngcontent-%COMP%] {\r\n overflow: hidden;\r\n border-radius: 0 0 5px 5px;\r\n height: 150px;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .actuality[_ngcontent-%COMP%]   .actuality_wrapper[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .card_img[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%] {\r\n height: 100%;\r\n width: 100%;\r\n object-fit: cover;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .actuality[_ngcontent-%COMP%]   .actuality_wrapper[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .card_txt[_ngcontent-%COMP%] {\r\n height: 60px;\r\n justify-content: center;\r\n position: relative;\r\n display: flex;\r\n flex-direction: column;\r\n padding: 5px;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .actuality[_ngcontent-%COMP%]   .actuality_wrapper[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .card_txt[_ngcontent-%COMP%]   .title[_ngcontent-%COMP%] {\r\n font-size: 16px;\r\n font-weight: 600;\r\n width: 100%;\r\n white-space: nowrap;\r\n overflow: hidden;\r\n text-overflow: ellipsis;\r\n margin: 0;\r\n color: #111d5e;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .actuality[_ngcontent-%COMP%]   .actuality_wrapper[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .card_txt[_ngcontent-%COMP%]   .desc[_ngcontent-%COMP%] {\r\n font-size: 14px;\r\n font-weight: 500;\r\n overflow: hidden;\r\n width: 80%;\r\n white-space: nowrap;\r\n text-overflow: ellipsis;\r\n margin: 0;\r\n color: #111d5e;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .actuality[_ngcontent-%COMP%]   .actuality_wrapper[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .card_txt[_ngcontent-%COMP%]   .see_more[_ngcontent-%COMP%] {\r\n position: absolute;\r\n right: 5px;\r\n bottom: 10px;\r\n font-size: 12px;\r\n color: gray;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .our_proposal[_ngcontent-%COMP%]   .proposal_wrapper[_ngcontent-%COMP%]   .title[_ngcontent-%COMP%]   .title_txt[_ngcontent-%COMP%] {\r\n font-size: 45px;\r\n font-weight: 800;\r\n position: relative;\r\n margin: 0;\r\n color: #111d5e;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .our_proposal[_ngcontent-%COMP%]   .proposal_wrapper[_ngcontent-%COMP%]   .title[_ngcontent-%COMP%]   .title_txt[_ngcontent-%COMP%]::after {\r\n content: \"\";\r\n width: 170px;\r\n height: 3px;\r\n bottom: 0;\r\n left: 250px;\r\n background: #111d5e;\r\n position: absolute;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .our_proposal[_ngcontent-%COMP%]   .proposal_wrapper[_ngcontent-%COMP%]   .title[_ngcontent-%COMP%]   .desc_txt[_ngcontent-%COMP%] {\r\n margin: 0;\r\n color: #111d5e;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .our_proposal[_ngcontent-%COMP%]   .proposal_wrapper[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%] {\r\n border: 2px solid #65b9b7;\r\n border-radius: 10px;\r\n padding: 10px;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .our_proposal[_ngcontent-%COMP%]   .proposal_wrapper[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .card_header[_ngcontent-%COMP%] {\r\n height: 80px;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .our_proposal[_ngcontent-%COMP%]   .proposal_wrapper[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .card_header[_ngcontent-%COMP%]   .title[_ngcontent-%COMP%] {\r\n margin: 0;\r\n font-weight: 900;\r\n color: #111d5e;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .our_proposal[_ngcontent-%COMP%]   .proposal_wrapper[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .card_header[_ngcontent-%COMP%]   .desc[_ngcontent-%COMP%] {\r\n margin: 0;\r\n font-weight: 600;\r\n color: #111d5e;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .our_proposal[_ngcontent-%COMP%]   .proposal_wrapper[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .card_desc[_ngcontent-%COMP%]   .desc[_ngcontent-%COMP%] {\r\n margin: 0;\r\n color: #111d5e;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   *[_ngcontent-%COMP%], .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   *[_ngcontent-%COMP%]:before, .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   *[_ngcontent-%COMP%]:after {\r\n box-sizing: border-box;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   body[_ngcontent-%COMP%] {\r\nbackground: #f5f5f5;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%] {\r\ndisplay: flex;\r\nflex-direction: row;\r\njustify-content: flex-start;\r\nmargin-left: unset;\r\nwidth: 650px;\r\nmargin-left: 45px;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   main[_ngcontent-%COMP%] {\r\n left: 50%;\r\n position: absolute;\r\n top: 50%;\r\n transform: translateX(-50%) translateY(-50%);\r\n width: 300px;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search[_ngcontent-%COMP%]:before, .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search[_ngcontent-%COMP%]:after {\r\n content: \"\";\r\n display: block;\r\n position: absolute;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search[_ngcontent-%COMP%]:before {\r\n border: 5px solid #ffffff ;\r\n border-radius: 20px;\r\n height: 40px;\r\n transition: all 0.3s ease-out;\r\n transition-delay: 0.3s;\r\n width: 40px;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search[_ngcontent-%COMP%]:after {\r\n background: #ffffff;\r\n border-radius: 3px;\r\n height: 5px;\r\n transform: rotate(-45deg);\r\n transform-origin: 0% 100%;\r\n transition: all 0.3s ease-out;\r\n width: 15px;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\r\n background: transparent;\r\n border: none;\r\n border-radius: 20px;\r\n display: block;\r\n font-size: 20px;\r\n height: 40px;\r\n line-height: 40px;\r\n opacity: 0;\r\n outline: none;\r\n padding: 0 15px;\r\n position: relative;\r\n transition: all 0.3s ease-out;\r\n transition-delay: 0.6s;\r\n width: 40px;\r\n z-index: 1;\r\n color: rgb(223, 223, 223);\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--hide[_ngcontent-%COMP%]:before {\r\n transition-delay: 0.3s;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--hide[_ngcontent-%COMP%]:after {\r\n transition-delay: 0.6s;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--hide[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\r\n transition-delay: 0s;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]:after {\r\n transform: rotate(45deg) translateX(15px) translateY(-2px);\r\n width: 0;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]:before {\r\n border: 5px solid #ffffff;\r\n border-radius: 20px;\r\n height: 40px;\r\n width: 500px;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\r\n opacity: 1;\r\n width: 500px;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%] {\r\n position: fixed;\r\n background: red;\r\n border-radius: 10px;\r\n left: 0;\r\n top: 35%;\r\n padding: 10px;\r\n display: flex;\r\n justify-content: center;\r\n align-items: center;\r\n width: 7%;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%] {\r\n padding: 0;\r\n margin: 0 0 -30px 0;\r\n display: flex;\r\n flex-direction: column;\r\n justify-content: space-between;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%] {\r\n padding: 5px;\r\n width: 120px;\r\n height: 120px;\r\n display: flex;\r\n justify-content: center;\r\n align-content: center;\r\n position: relative;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]::before{\r\ncontent: \">\";\r\nposition: absolute;\r\nright: -10px;\r\ntop: 15%;\r\ncolor: white;\r\nfont-size: 20px;\r\nwidth: 40%;\r\nfont-weight: bold;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]:hover:after{\r\ncontent: \"Testez mon \u00E9gibilit\u00E9\";\r\nposition: absolute;\r\nright: -180px;\r\ntop: 15%;\r\ncolor: black;\r\nfont-size: 17px;\r\nwidth: 40%;\r\nbackground: white;\r\nwidth: 180px;\r\nborder-radius: 25px;\r\ntext-align: center;\r\nheight: 40px;\r\ndisplay: flex;\r\njustify-content: center;\r\nalign-items: center;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%] {\r\n text-decoration: none;\r\n display: block;\r\n flex-direction: column;\r\n justify-content: center;\r\n font-size: 14px;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%]   .ihref_logo[_ngcontent-%COMP%] {\r\n width: 100px;\r\n height: 100px;\r\n margin-left: 25px;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%]   .ihref_text[_ngcontent-%COMP%] {\r\n text-align: center;\r\n margin: 0;\r\n color: white;\r\n margin-top: 7px;\r\n}\r\n\r\n.jumbotron[_ngcontent-%COMP%] {\r\nmargin: -21px -22px 0px -24px;\r\nheight: 800px;\r\ncolor: white !important;\r\nborder-radius: 0 0 15%;\r\n}\r\n\r\n.imgdame[_ngcontent-%COMP%] {\r\nwidth: 1000px;\r\nheight: 500px;\r\nmargin-top: -10%;\r\nmargin-left: -10%;\r\n}\r\n\r\n.text_transition[_ngcontent-%COMP%] {\r\nfont-size: 60px;\r\ncolor: #111d5e;\r\n}\r\n\r\n#wrap[_ngcontent-%COMP%] {\r\nmargin: 50px 100px;\r\ndisplay: inline-block;\r\nposition: relative;\r\nheight: 60px;\r\nfloat: right;\r\npadding: 0;\r\nposition: relative;\r\n}\r\n\r\ninput[type=\"text\"][_ngcontent-%COMP%] {\r\nheight: 50px;\r\nfont-size: 30px;\r\ndisplay: inline-block;\r\n\r\nfont-weight: 100;\r\nborder: none;\r\noutline: none;\r\ncolor: white;\r\npadding: 3px;\r\npadding-right: 60px;\r\nwidth: 0px;\r\nposition: absolute;\r\ntop: 0;\r\nright: 0;\r\nbackground: none;\r\nz-index: 3;\r\ntransition: width 0.4s cubic-bezier(0, 0.795, 0, 1);\r\ncursor: pointer;\r\n}\r\n\r\ninput[type=\"text\"][_ngcontent-%COMP%]:focus:hover {\r\nborder-bottom: 1px solid white;\r\n}\r\n\r\ninput[type=\"text\"][_ngcontent-%COMP%]:focus {\r\nwidth: 700px;\r\nz-index: 1;\r\nborder-bottom: 1px solid white;\r\ncursor: text;\r\n}\r\n\r\ninput[type=\"submit\"][_ngcontent-%COMP%] {\r\nheight: 50px;\r\nwidth: 50px;\r\ndisplay: inline-block;\r\ncolor: white;\r\nfloat: right;\r\nbackground: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAMAAABg3Am1AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAADNQTFRFU1NT9fX1lJSUXl5e1dXVfn5+c3Nz6urqv7+/tLS0iYmJqampn5+fysrK39/faWlp////Vi4ZywAAABF0Uk5T/////////////////////wAlrZliAAABLklEQVR42rSWWRbDIAhFHeOUtN3/ags1zaA4cHrKZ8JFRHwoXkwTvwGP1Qo0bYObAPwiLmbNAHBWFBZlD9j0JxflDViIObNHG/Do8PRHTJk0TezAhv7qloK0JJEBh+F8+U/hopIELOWfiZUCDOZD1RADOQKA75oq4cvVkcT+OdHnqqpQCITWAjnWVgGQUWz12lJuGwGoaWgBKzRVBcCypgUkOAoWgBX/L0CmxN40u6xwcIJ1cOzWYDffp3axsQOyvdkXiH9FKRFwPRHYZUaXMgPLeiW7QhbDRciyLXJaKheCuLbiVoqx1DVRyH26yb0hsuoOFEPsoz+BVE0MRlZNjGZcRQyHYkmMp2hBTIzdkzCTc/pLqOnBrk7/yZdAOq/q5NPBH1f7x7fGP4C3AAMAQrhzX9zhcGsAAAAASUVORK5CYII=)\r\n  center center no-repeat;\r\ntext-indent: -10000px;\r\nborder: none;\r\nposition: absolute;\r\ntop: 0;\r\nright: 0;\r\nz-index: 2;\r\ncursor: pointer;\r\nopacity: 0.4;\r\ncursor: pointer;\r\ntransition: opacity 0.4s ease;\r\n}\r\n\r\ninput[type=\"submit\"][_ngcontent-%COMP%]:hover {\r\nopacity: 0.8;\r\n}\r\n\r\n.item_num[_ngcontent-%COMP%] {\r\n  margin: 0;\r\n  font-weight: 700;\r\n  color: #111d5e;\r\n}\r\n\r\n.success_item[_ngcontent-%COMP%] {\r\n  list-style: none;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items: center;\r\n}\r\n\r\n.item_desc[_ngcontent-%COMP%] {\r\n  margin: 0;\r\n  color: #111d5e;\r\n}\r\n\r\n.success_list[_ngcontent-%COMP%] {\r\n  display: flex;\r\n  justify-content: space-between;\r\n  margin: 0;\r\n  padding: 0;\r\n}\r\n\r\n.success_txt[_ngcontent-%COMP%] {\r\n  text-transform: uppercase;\r\n  font-weight: 400;\r\n  font-size: 14px;\r\n  color: #111d5e;\r\n}\r\n\r\n.success_desc[_ngcontent-%COMP%] {\r\n  font-size: 28px;\r\n  color: #111d5e;\r\n}\r\n\r\n.container_box[_ngcontent-%COMP%] {\r\n  width: 100%;\r\n  height: auto;\r\n  display: flex;\r\n  flex-direction: row;\r\n\r\n}\r\n\r\nh1[_ngcontent-%COMP%]{\r\nfont-weight: bold;\r\ncolor: #111d5e;\r\n\r\n}\r\n\r\n.k[_ngcontent-%COMP%]{\r\nfont-weight: bold;\r\nmargin: inherit;\r\n}\r\n\r\nh5[_ngcontent-%COMP%]{\r\ncolor: #111d5e;\r\nfont-size: 15px;\r\n}\r\n\r\np[_ngcontent-%COMP%]{\r\nfont-size: 12px;\r\ncolor: #111d5e;\r\n\r\n}\r\n\r\n.footer[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n  font-size: 12px;\r\ncolor: #ffffff;\r\n}\r\n\r\n.actu[_ngcontent-%COMP%]{\r\ndisplay: flex;\r\nflex-direction: row;\r\njustify-content: center;\r\nalign-items: center;\r\nwidth: 100%;\r\n}\r\n\r\n.main[_ngcontent-%COMP%] {\r\nwidth: 800px;\r\nheight: 350px;\r\npadding: 3em;\r\nmargin-left: 25%;\r\n}\r\n\r\n.texte[_ngcontent-%COMP%] {\r\nwidth: 800px;\r\nheight: 350px;\r\npadding: 3em;\r\nmargin-left: 25%;\r\n}\r\n\r\n.french_map[_ngcontent-%COMP%], .map_wrapper[_ngcontent-%COMP%], .map_holder[_ngcontent-%COMP%], .map_svg[_ngcontent-%COMP%], g[_ngcontent-%COMP%], path[_ngcontent-%COMP%] {\r\nfill: gainsboro ;\r\nstroke-width: 1px ;\r\nstroke: gray;\r\nwidth: 100%;\r\nheight: 100%;\r\ncursor: pointer;\r\ntransition: .2s ;\r\n}\r\n\r\nh1[_ngcontent-%COMP%]{\r\nfont-weight: bold;\r\nmargin: 50px 0 50px 0;\r\n\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .french_map[_ngcontent-%COMP%]   .map_wrapper[_ngcontent-%COMP%]   .map_holder[_ngcontent-%COMP%]   .map_svg[_ngcontent-%COMP%]   g[_ngcontent-%COMP%]   path[_ngcontent-%COMP%]:hover {\r\nfill: #111D5E ;\r\nstroke: #ffd2d2 ;\r\n}\r\n\r\n#us-map[_ngcontent-%COMP%] {\r\ndisplay: block !important;\r\nposition: absolute !important;\r\ntop: 0 !important;\r\nleft: 0 !important;\r\nwidth: 100% !important;\r\nheight: 100% !important;\r\n}\r\n\r\npath[_ngcontent-%COMP%]:hover, circle[_ngcontent-%COMP%]:hover {\r\nstroke: #002868 ;\r\nstroke-width: 2px ;\r\nstroke-linejoin: round ;\r\nfill: #002868 ;\r\ncursor: pointer ;\r\n}\r\n\r\n#path[_ngcontent-%COMP%] {\r\nfill: none ;\r\nstroke: #A9A9A9 ;\r\ncursor: default ;\r\n}\r\n\r\n#info-box[_ngcontent-%COMP%] {\r\ndisplay: none ;\r\nposition: absolute ;\r\ntop: 0px ;\r\nleft: 0px  ;\r\nz-index: 1;\r\nbackground-color: #ffffff ;\r\nborder: 2px solid #ffffff ;\r\nborder-radius: 5px ;\r\npadding: 5px ;\r\nfont-family: arial ;\r\nwidth: 100px;\r\nheight: 100px;\r\nbox-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px;\r\n\r\n  }\r\n\r\n.round[_ngcontent-%COMP%]{\r\n    border-radius: 30px;\r\n    -webkit-border-radius: 30px;\r\n    -moz-border-radius: 30px;\r\n  }\r\n\r\n.right-in[_ngcontent-%COMP%]:after{\r\n    content: ' ';\r\n    position: absolute;\r\n    width: 0;\r\n    height: 0;\r\n    left: auto;\r\n    right: 35px;\r\n    top: 80px;\r\n    bottom: auto;\r\n    z-index: 0;\r\n    transform: rotate(225deg); \r\n    border: 12px solid;\r\n    border-color: white transparent transparent white;\r\n  }\r\n\r\n.act-content[_ngcontent-%COMP%]{\r\nbackground-color: #EBECF0;\r\n\r\n}\r\n\r\n.act-img[_ngcontent-%COMP%]{\r\n    background-color: #EBECF0;\r\n  }\r\n\r\n.proposons[_ngcontent-%COMP%]{\r\n  height: 300px;\r\n  background: linear-gradient(#ffffff, #e5e6e7);\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: space-around;\r\n  align-items: center;\r\n}\r\n\r\n.proposons[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n  margin: 0;\r\n}\r\n\r\n.SousB[_ngcontent-%COMP%]{\r\n  border: 3px solid rgb(41, 191, 211);\r\n  border-radius: 15px;\r\n  height: 160px;\r\n  margin: 0;\r\n  padding: 20px;\r\n  width: 300px;\r\n}\r\n\r\n.SousB[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n  font-size: 30px;\r\n}\r\n\r\n.SousB[_ngcontent-%COMP%]   h3[_ngcontent-%COMP%]{\r\n  font-size: 15px;\r\nfont-weight: bold;\r\nline-height: normal;\r\n}\r\n\r\n.SousB[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n  font-size: 15px;\r\n}\r\n\r\n.SousB[_ngcontent-%COMP%]   i[_ngcontent-%COMP%]{\r\n  margin-left: 277px;\r\nz-index: auto;\r\nfont-size: 26px;\r\nmargin-top: -50px;\r\n}\r\n\r\n.btntest[_ngcontent-%COMP%]{\r\n  -webkit-text-decoration:auto ;\r\n          text-decoration:auto ;  border: none; background: red;border-radius: 25px;color: white;padding: 5px 15px;\r\n}\r\n\r\n.test[_ngcontent-%COMP%]{\r\nwidth: 100px;\r\nheight: 200px;\r\nmargin-top: 20px;\r\nmargin-right: 5px;\r\n}\r\n\r\n.test[_ngcontent-%COMP%]   i[_ngcontent-%COMP%]{\r\nfont-size: 30px;\r\ncolor: white;\r\n}\r\n\r\n.showtest[_ngcontent-%COMP%]{\r\ndisplay: none;\r\n}\r\n\r\n.showtest[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]{\r\nmargin-top: -30px;\r\n}\r\n\r\n.test[_ngcontent-%COMP%]:hover    + .showtest[_ngcontent-%COMP%] {\r\ndisplay:block;\r\n}\r\n\r\n.action_items[_ngcontent-%COMP%]{\r\n\r\n}\r\n\r\n.content[_ngcontent-%COMP%]{\r\nwidth: max-content;\r\n}\r\n\r\n\r\n\r\n.home[_ngcontent-%COMP%]{\r\ncolor: white;\r\nfont-size: 18px;\r\n}\r\n\r\n.threeB[_ngcontent-%COMP%]{\r\nbackground: transparent;\r\n color: white;\r\n padding: 20px;\r\n}\r\n\r\n.threeB[_ngcontent-%COMP%]:hover{\r\n border: 5px solid white;\r\n}\r\n\r\n.chiffre[_ngcontent-%COMP%]{\r\ndisplay: flex;\r\n            flex-direction: column;\r\n            justify-content: center;\r\n            width: 40%;\r\n            align-items: inherit;\r\n}\r\n\r\n.transtion[_ngcontent-%COMP%]{\r\ndisplay: flex;\r\nposition: absolute;\r\nmargin-left: -350px;\r\nwidth: max-content;\r\nflex-direction: column;\r\n}\r\n\r\n.card1[_ngcontent-%COMP%]{\r\n  margin-right: 50px;height: 345px;width: 315px;\r\n}\r\n\r\n.card1[_ngcontent-%COMP%]   .box[_ngcontent-%COMP%]{\r\n  box-shadow: 0px 1px 15px grey;\r\n  border: none;\r\n  border-radius: 71px 14px 71px 14px;\r\n  background-color: #ffffff;\r\n  padding: 40px;\r\n}\r\n\r\n.card1[_ngcontent-%COMP%]   .box[_ngcontent-%COMP%]   h4[_ngcontent-%COMP%]{\r\n  padding: 69px 0 0 26px;\r\n  font-size: 20px;\r\n  color:#00BFFF\r\n}\r\n\r\n.card1[_ngcontent-%COMP%]   .box[_ngcontent-%COMP%]   h6[_ngcontent-%COMP%]{\r\n  padding: 0px 0 0px 54px;\r\n  font-size: 10px;\r\n  color:#c7c7c7\r\n}\r\n\r\n.card1[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  position: relative;\r\n  top: -132px;\r\n  left: -116px;\r\n  height: 100px;\r\n  width: 100px;\r\n  margin: 0 auto;\r\n  border-radius: 50%;\r\n  background-size: contain;\r\n  overflow: hidden;\r\n}\r\n\r\n.card1[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n  width: 120%;\r\n  height: 120%\r\n}\r\n\r\n.lastB[_ngcontent-%COMP%]{\r\n  width: 100%;\r\n \r\n}\r\n\r\n.second-bloc[_ngcontent-%COMP%]{\r\n  background-image: url('sin.png');\r\n  background-repeat: no-repeat;\r\n  background-position:658px 234px;\r\n  margin-top:100px;\r\n}\r\n\r\n.second-bloc[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]{\r\n \r\n}\r\n\r\n.second-bloc[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{padding-right: 48rem;}\r\n\r\n.block5[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n  margin-left: 190px;\r\n  height:100%;\r\n  width:100%\r\n}\r\n\r\n.block6[_ngcontent-%COMP%]{\r\n  padding-top: 30px; \r\n  padding-left: 110px;\r\n  margin-top: -501px; \r\n  float:right\r\n}\r\n\r\n.block3[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n  padding-right: 25rem;\r\n  width: 800px;\r\n}\r\n\r\n\r\n\r\n@media screen and (max-width: 768px) {\r\n\r\n    .proposons[_ngcontent-%COMP%]{\r\n      height: 800px;\r\n  background: linear-gradient(#ffffff, #e5e6e7);\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: space-around;\r\n  align-items: center;\r\n    }\r\n    .SousB[_ngcontent-%COMP%]   i[_ngcontent-%COMP%]{\r\n      margin-left: 77px;\r\n  z-index: auto;\r\n  font-size: 26px;\r\n  margin-top: 70px;\r\n  transform: rotate(90deg);\r\n  position: absolute;\r\n    }\r\n  \r\n    .chiffre[_ngcontent-%COMP%]{\r\n      display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  width: 100%;\r\n  align-items: center;\r\n    }\r\n    .success_item[_ngcontent-%COMP%]{\r\n      margin-left: 20px;\r\n      list-style: none;\r\n      display: flex;\r\n      flex-direction: column;\r\n      justify-content: center;\r\n      align-items: center;\r\n    }\r\n    .k[_ngcontent-%COMP%]{\r\n      margin: initial;\r\n    }\r\n   \r\n  \r\n    .primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]{\r\n      position: fixed;\r\n  background: red;\r\n  border-radius: 10px;\r\n  left: 0;\r\n  top: 35%;\r\n  padding: 10px;\r\n  display: flex;\r\n  justify-content: center;\r\n  align-items: center;\r\n  width: 3%;\r\n  height: 5%;\r\n    }\r\n    .primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]::after{\r\n      content: \">\";\r\n      color: white;\r\n      position: fixed;\r\n  background: red;\r\n  border-radius: 10px;\r\n  left: 0;\r\n  top: 35%;\r\n  padding: 0px;\r\n  display: flex;\r\n  justify-content: center;\r\n  align-items: center;\r\n  width: 5%;\r\n  height: 5%;\r\n  font-weight: bold;\r\n  font-size: 20px;\r\n  \r\n    }\r\n    .primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]:hover:after{\r\n      content: \">\";\r\n      color: white;\r\n      position: fixed;\r\n  background: red;\r\n  border-radius: 10px;\r\n  left: 0;\r\n  top: 35%;\r\n  padding: 10px;\r\n  display: none;\r\n  justify-content: center;\r\n  align-items: center;\r\n  width: 3%;\r\n  height: 5%;\r\n    }\r\n    .primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%] {\r\n      padding: 0;\r\n      margin: 0 0 -30px 0;\r\n      display: none;\r\n      flex-direction: column;\r\n      justify-content: space-between;\r\n   }\r\n    .primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]:hover{\r\n      position: fixed;\r\n      background: red;\r\n      border-radius: 10px;\r\n      left: 0;\r\n      top: 35%;\r\n      padding: 10px;\r\n      display: flex;\r\n      justify-content: center;\r\n      align-items: center;\r\n      width: 20%;\r\n      height: 23%;\r\n      z-index: 99999;\r\n    }\r\n  \r\n   .primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]:hover   .actions_content[_ngcontent-%COMP%] {\r\n    padding: 0;\r\n    margin: 0 0 -30px 0;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n  }\r\n  \r\n  \r\n  .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]:before {\r\n    border: 5px solid #111d5e;\r\n    border-radius: 20px;\r\n    height: 40px;\r\n    width: 300px;\r\n  }\r\n  .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\r\n    opacity: 1;\r\n    width: 300px;\r\n  }\r\n\r\n  primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .img_wrapper[_ngcontent-%COMP%]   .heading_img[_ngcontent-%COMP%] {\r\n    font-size: 40px;\r\n    font-weight: 800;\r\n    color: #ffffff;\r\n    width: max-content;\r\n    text-align: center;\r\n }\r\n .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%] {\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: flex-start;\r\n    margin-left: unset;\r\n    width: 390px;\r\n}\r\n.transtion[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  width: -moz-fit-content;\r\n  width: fit-content;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items: center;\r\n  margin: 0;\r\n}\r\n.carre[_ngcontent-%COMP%] {\r\n  width: 200px;\r\n  height: 90px;\r\n  background: white;\r\n  border-radius: 18px;\r\n  margin-left: 110px;\r\n  margin-top: 250px;\r\n  align-items: center;\r\n}\r\n.content[_ngcontent-%COMP%]{\r\n  width: -moz-fit-content;\r\n  width: fit-content;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .title_heading[_ngcontent-%COMP%] {\r\n  font-size: 30px;\r\n  font-weight: 800;\r\n  color:  #ffffff;\r\n  width: max-content;\r\n}\r\n.lastB[_ngcontent-%COMP%]{\r\n width: 80%;\r\n margin: 50px;\r\n}\r\n.text-center[_ngcontent-%COMP%]{\r\n margin-bottom: 50px;\r\n}\r\n\r\n.card1[_ngcontent-%COMP%]{\r\nmargin-right: 0px;\r\nheight: 345px;\r\nwidth: 315px;\r\n}\r\n.card1[_ngcontent-%COMP%]   .box[_ngcontent-%COMP%]{\r\nbox-shadow: 0px 1px 15px grey;\r\nborder: none;\r\nborder-radius: 71px 14px 71px 14px;\r\nbackground-color: #ffffff;\r\npadding: 40px;\r\n}\r\n\r\n\r\n.second-bloc[_ngcontent-%COMP%]{\r\nbackground-image: url('sin.png');\r\nbackground-repeat: no-repeat;\r\nbackground-position:658px 234px;\r\nmargin-top:100px;\r\n}\r\n.second-bloc[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]{\r\ndisplay: flex;\r\nflex-direction: column;\r\nalign-items: flex-start;\r\n}\r\n.second-bloc[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\npadding-right: 0;\r\n}\r\n\r\n .one[_ngcontent-%COMP%]{\r\nmargin: 40px 0 0 0 ;\r\n}\r\n .two[_ngcontent-%COMP%]{\r\nmargin: 0;\r\n}\r\n .three[_ngcontent-%COMP%]{\r\nmargin: 0;\r\n}\r\n.outer-div[_ngcontent-%COMP%]{\r\n  margin-left: 14%;\r\n}\r\n.block5[_ngcontent-%COMP%]{\r\nmax-width: 100%;\r\nwidth: 100%;\r\n}\r\n.block5[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\nmargin-left: 0;\r\nheight:100%;\r\nwidth:100%\r\n}\r\n\r\n.block6[_ngcontent-%COMP%]{\r\npadding-top: 30px; \r\npadding-left:0;\r\nmargin-top: 0; \r\nmax-width: 100%;\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: center;\r\nalign-items: center;\r\n}\r\n.block3[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\npadding-right: 0;\r\nwidth: 500px;\r\n}\r\n\r\n\r\n  .navbar-brand[_ngcontent-%COMP%] {\r\n      display: inline-block;\r\n      padding-top: .3125rem;\r\n      padding-bottom: .3125rem;\r\n      margin-right: 1rem;\r\n      font-size: 1.25rem;\r\n      line-height: inherit;\r\n      white-space: nowrap;\r\n      margin-left: 0;\r\n      z-index: 5;\r\n}\r\n\r\n  }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9ob21lLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUNBO0VBQ0UsOEJBQThCO0VBQzlCLHVCQUF1Qjs7QUFFekI7O0FBRUE7RUFDRSxtQkFBbUI7RUFDbkIsOEJBQThCO0VBQzlCLGFBQWE7RUFDYixlQUFlO0VBQ2Y7O0FBQ0E7TUFDSSx1QkFBdUI7RUFDM0I7O0FBQ0E7TUFDSSx5QkFBeUI7RUFDN0I7O0FBQ0E7TUFDSSx1QkFBdUI7TUFDdkIsWUFBWTtNQUNaLGVBQWU7TUFDZixtQkFBbUI7TUFDbkIsWUFBWTtNQUNaLGtCQUFrQjtFQUN0QixZQUFZO0VBQ1osYUFBYTtFQUNiLHVCQUF1QjtFQUN2QixtQkFBbUI7RUFDbkI7O0FBQ0E7TUFDSSw4QkFBOEI7O0VBRWxDOztBQUNBO01BQ0ksdUJBQXVCO0VBQzNCOztBQUNBO01BQ0kseUJBQXlCO0VBQzdCOztBQUNBO01BQ0kseUJBQXlCOztFQUU3Qjs7QUFDQTtNQUNJLFdBQVc7TUFDWCxtQkFBbUI7RUFDdkI7O0FBQ0c7TUFDQyxxQkFBcUI7TUFDckIscUJBQXFCO01BQ3JCLHdCQUF3QjtNQUN4QixrQkFBa0I7TUFDbEIsa0JBQWtCO01BQ2xCLG9CQUFvQjtNQUNwQixtQkFBbUI7TUFDbkIsaUJBQWlCO01BQ2pCLFVBQVU7RUFDZDs7QUFHRiw0R0FBNEc7O0FBQzVHO0FBQ0EsZUFBZTtBQUNmLGVBQWU7QUFDZixtQkFBbUI7QUFDbkIsT0FBTztBQUNQLFFBQVE7QUFDUixhQUFhO0FBQ2IsYUFBYTtBQUNiLHVCQUF1QjtBQUN2QixtQkFBbUI7QUFDbkIsU0FBUztBQUNUOztBQUNBO0FBQ0EsVUFBVTtBQUNWLG1CQUFtQjtBQUNuQixhQUFhO0FBQ2Isc0JBQXNCO0FBQ3RCLDhCQUE4QjtBQUM5Qjs7QUFDQTtBQUNBLFlBQVk7QUFDWixZQUFZO0FBQ1osYUFBYTtBQUNiLGFBQWE7QUFDYix1QkFBdUI7QUFDdkIscUJBQXFCO0FBQ3JCLGtCQUFrQjtBQUNsQjs7QUFDQTtBQUNBLFlBQVk7QUFDWixrQkFBa0I7QUFDbEIsWUFBWTtBQUNaLFFBQVE7QUFDUixZQUFZO0FBQ1osZUFBZTtBQUNmLFVBQVU7QUFDVixpQkFBaUI7QUFDakI7O0FBRUE7QUFDQSwrQkFBK0I7QUFDL0Isa0JBQWtCO0FBQ2xCLGFBQWE7QUFDYixRQUFRO0FBQ1IsWUFBWTtBQUNaLGVBQWU7QUFDZixVQUFVO0FBQ1YsaUJBQWlCO0FBQ2pCLFlBQVk7QUFDWixtQkFBbUI7QUFDbkIsa0JBQWtCO0FBQ2xCLFlBQVk7QUFDWixhQUFhO0FBQ2IsdUJBQXVCO0FBQ3ZCLG1CQUFtQjtBQUNuQjs7QUFFQTtBQUNBLHFCQUFxQjtBQUNyQixjQUFjO0FBQ2Qsc0JBQXNCO0FBQ3RCLHVCQUF1QjtBQUN2QixlQUFlO0FBQ2Y7O0FBQ0E7QUFDQSxZQUFZO0FBQ1osYUFBYTtBQUNiLGlCQUFpQjtBQUNqQjs7QUFDQTtBQUNBLGtCQUFrQjtBQUNsQixTQUFTO0FBQ1QsWUFBWTtBQUNaLGVBQWU7QUFDZjs7QUFHQSxnSEFBZ0g7O0FBQ2hIOztFQUVFLGFBQWE7RUFDYixnQkFBZ0I7RUFDaEIsY0FBYztFQUNkLGtCQUFrQjtBQUNwQjs7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQix3Q0FBd0M7QUFDMUM7O0FBQ0M7QUFDRDtBQUNBOztBQUNDO0FBQ0Q7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E7RUFDRSxjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixZQUFZO0VBQ1osZUFBZTtFQUNmLGtCQUFrQjs7QUFFcEI7O0FBRUE7RUFDRSxVQUFVO0VBQ1YsTUFBTTtBQUNSOztBQUVBOzs7SUFHSTs7QUFHSjs7QUFFQTs7QUFFQTs7RUFFRSxrQkFBa0I7RUFDbEIsTUFBTTtFQUNOLE9BQU87RUFDUCxtQ0FBMkI7VUFBM0IsMkJBQTJCO0FBQzdCOztBQUVBO0VBQ0UsZUFBZTtFQUNmLFdBQVc7RUFDWCxpQkFBaUI7RUFDakIsbUNBQTJCO1VBQTNCLDJCQUEyQjtFQUMzQiw2Q0FBNkM7RUFDN0MsNkJBQTZCO0VBQzdCLG1CQUFtQjs7QUFFckI7O0FBR0E7RUFDRSxrQkFBa0I7RUFDbEIsU0FBUztFQUNULGFBQWE7RUFDYixZQUFZO0VBQ1osY0FBYztFQUNkLGtCQUFrQjs7RUFFbEIsd0JBQXdCO0VBQ3hCLGdCQUFnQjtFQUNoQjs7bUJBRWlCO0FBQ25COztBQUdBO0VBQ0Usa0JBQWtCO0VBQ2xCLFNBQVM7RUFDVCxhQUFhO0VBQ2IsWUFBWTtFQUNaLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsd0JBQXdCO0VBQ3hCLGdCQUFnQjtFQUNoQjs7bUJBRWlCO0FBQ25COztBQUdBO0VBQ0Usa0JBQWtCO0VBQ2xCLFNBQVM7RUFDVCxhQUFhO0VBQ2IsWUFBWTtFQUNaLGNBQWM7RUFDZCxrQkFBa0I7O0VBRWxCLHdCQUF3QjtFQUN4QixnQkFBZ0I7RUFDaEI7O21CQUVpQjtBQUNuQjs7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsY0FBYztFQUNkLHlCQUF5QjtFQUN6QixlQUFlO0VBQ2YsbUNBQTJCO1VBQTNCLDJCQUEyQjtBQUM3Qjs7QUFFQTtFQUNFLGdCQUFnQjtFQUNoQixxQkFBcUI7RUFDckIseUJBQXlCO0VBQ3pCLGVBQWU7QUFDakI7O0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsU0FBUztFQUNULFdBQVc7RUFDWCxlQUFlO0VBQ2YscUJBQXFCO0VBQ3JCLGdCQUFnQjtFQUNoQixxQ0FBcUM7QUFDdkM7O0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsTUFBTTtFQUNOLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsV0FBVztBQUNiOztBQUVBO0VBQ0Usa0JBQWtCO0VBQ2xCLFNBQVM7RUFDVCxlQUFlO0VBQ2YsVUFBVTtFQUNWLG1DQUEyQjtVQUEzQiwyQkFBMkI7O0VBRTNCLGdCQUFnQjtFQUNoQix5QkFBeUI7RUFDekIsb0JBQW9COztFQUVwQixxQkFBcUI7RUFDckIsaUJBQWlCO0VBQ2pCLG1CQUFtQjs7RUFFbkIsZUFBZTtFQUNmLFlBQVk7QUFDZDs7QUFHQTtFQUNFLDBCQUEwQjtFQUMxQixrQkFBa0I7RUFDbEIsTUFBTTtFQUNOLE9BQU87RUFDUCxXQUFXO0VBQ1gsV0FBVztFQUNYLHlCQUF5QjtFQUN6QixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLHVCQUF1QjtFQUN2QixtQkFBbUI7RUFDbkIsNkJBQTZCO0VBQzdCLG1CQUFtQjs7QUFFckI7O0FBQ0E7QUFDQSxZQUFZO0FBQ1osWUFBWTtBQUNaLGlCQUFpQjtBQUNqQixtQkFBbUI7QUFDbkIsbUJBQW1CO0FBQ25CLGlCQUFpQjtBQUNqQjs7QUFFQTtDQUNDLGtCQUFrQjtDQUNsQiw0QkFBdUI7Q0FBdkIsdUJBQXVCO0NBQ3ZCLGlDQUFpQztBQUNsQzs7QUFDQTtDQUNDLFlBQVk7Q0FDWixpQkFBaUI7Q0FDakIsYUFBYTtDQUNiLGtCQUFrQjtDQUNsQixhQUFhO0FBQ2Q7O0FBQ0E7Q0FDQyxlQUFlO0NBQ2YsZ0JBQWdCO0NBQ2hCLGVBQWU7Q0FDZixrQkFBa0I7QUFDbkI7O0FBQ0E7Q0FDQyxlQUFlO0NBQ2YsZ0JBQWdCO0NBQ2hCLGVBQWU7Q0FDZixrQkFBa0I7QUFDbkI7O0FBQ0E7Q0FDQyx5QkFBeUI7Q0FDekIsZ0JBQWdCO0NBQ2hCLGVBQWU7Q0FDZixjQUFjO0FBQ2Y7O0FBQ0E7Q0FDQyxlQUFlO0NBQ2YsY0FBYztBQUNmOztBQUNBO0NBQ0MsYUFBYTtDQUNiLDhCQUE4QjtDQUM5QixTQUFTO0NBQ1QsVUFBVTtBQUNYOztBQUNBO0NBQ0MsZ0JBQWdCO0NBQ2hCLGFBQWE7Q0FDYixzQkFBc0I7Q0FDdEIsdUJBQXVCO0NBQ3ZCLG1CQUFtQjtBQUNwQjs7QUFDQTtDQUNDLFNBQVM7Q0FDVCxnQkFBZ0I7Q0FDaEIsY0FBYztBQUNmOztBQUNBO0NBQ0MsU0FBUztDQUNULGNBQWM7QUFDZjs7QUFDQTtDQUNDLHlCQUF5QjtDQUN6QixtQkFBbUI7QUFDcEI7O0FBQ0E7Q0FDQyxtQkFBbUI7Q0FDbkIsaUJBQWlCO0FBQ2xCOztBQUNBO0NBQ0MsbUJBQW1CO0FBQ3BCOztBQUNBO0NBQ0MsWUFBWTtDQUNaLGVBQWU7Q0FDZixtQkFBbUI7Q0FDbkIsWUFBWTtDQUNaLGlCQUFpQjtBQUNsQjs7QUFDQTtDQUNDLG1CQUFtQjtBQUNwQjs7QUFDQTtDQUNDLGtCQUFrQjtBQUNuQjs7QUFDQTtDQUNDLFlBQVk7Q0FDWixrQkFBa0I7Q0FDbEIsYUFBYTtDQUNiLHNCQUFzQjtDQUN0QixZQUFZO0NBQ1oscUJBQXFCO0NBQ3JCLHlCQUF5QjtDQUN6Qix1QkFBdUI7Q0FDdkIsWUFBWTtBQUNiOztBQUNBO0NBQ0MsVUFBVTtDQUNWLGlCQUFpQjtBQUNsQjs7QUFDQTtDQUNDLFVBQVU7Q0FDVixpQkFBaUI7Q0FDakIsWUFBWTtDQUNaLGVBQWU7Q0FDZixnQkFBZ0I7QUFDakI7O0FBQ0E7Q0FDQyxTQUFTO0FBQ1Y7O0FBQ0E7Q0FDQyxXQUFXO0NBQ1gsV0FBVztDQUNYLG1CQUFtQjtDQUNuQixjQUFjO0NBQ2Qsa0JBQWtCO0FBQ25COztBQUNBO0NBQ0MsV0FBVztDQUNYLGtCQUFrQjtDQUNsQixVQUFVO0NBQ1YsTUFBTTtDQUNOLE9BQU87Q0FDUCxXQUFXO0NBQ1gsZUFBZTtBQUNoQjs7QUFDQTtDQUNDLFdBQVc7Q0FDWCxrQkFBa0I7Q0FDbEIsVUFBVTtDQUNWLE1BQU07Q0FDTixRQUFRO0NBQ1IsV0FBVztDQUNYLGVBQWU7QUFDaEI7O0FBQ0E7Q0FDQyxlQUFlO0NBQ2YsZ0JBQWdCO0NBQ2hCLGtCQUFrQjtDQUNsQixTQUFTO0NBQ1QsY0FBYztBQUNmOztBQUNBO0NBQ0MsV0FBVztDQUNYLFlBQVk7Q0FDWixXQUFXO0NBQ1gsU0FBUztDQUNULFVBQVU7Q0FDVixtQkFBbUI7Q0FDbkIsa0JBQWtCO0FBQ25COztBQUNBO0NBQ0MsV0FBVztDQUNYLHdCQUFtQjtDQUFuQixtQkFBbUI7Q0FDbkIsZ0JBQWdCO0NBQ2hCLGtCQUFrQjtBQUNuQjs7QUFDQTtDQUNDLGdCQUFnQjtDQUNoQiwwQkFBMEI7Q0FDMUIsYUFBYTtBQUNkOztBQUNBO0NBQ0MsWUFBWTtDQUNaLFdBQVc7Q0FDWCxpQkFBaUI7QUFDbEI7O0FBQ0E7Q0FDQyxZQUFZO0NBQ1osdUJBQXVCO0NBQ3ZCLGtCQUFrQjtDQUNsQixhQUFhO0NBQ2Isc0JBQXNCO0NBQ3RCLFlBQVk7QUFDYjs7QUFDQTtDQUNDLGVBQWU7Q0FDZixnQkFBZ0I7Q0FDaEIsV0FBVztDQUNYLG1CQUFtQjtDQUNuQixnQkFBZ0I7Q0FDaEIsdUJBQXVCO0NBQ3ZCLFNBQVM7Q0FDVCxjQUFjO0FBQ2Y7O0FBQ0E7Q0FDQyxlQUFlO0NBQ2YsZ0JBQWdCO0NBQ2hCLGdCQUFnQjtDQUNoQixVQUFVO0NBQ1YsbUJBQW1CO0NBQ25CLHVCQUF1QjtDQUN2QixTQUFTO0NBQ1QsY0FBYztBQUNmOztBQUNBO0NBQ0Msa0JBQWtCO0NBQ2xCLFVBQVU7Q0FDVixZQUFZO0NBQ1osZUFBZTtDQUNmLFdBQVc7QUFDWjs7QUFDQTtDQUNDLGVBQWU7Q0FDZixnQkFBZ0I7Q0FDaEIsa0JBQWtCO0NBQ2xCLFNBQVM7Q0FDVCxjQUFjO0FBQ2Y7O0FBQ0E7Q0FDQyxXQUFXO0NBQ1gsWUFBWTtDQUNaLFdBQVc7Q0FDWCxTQUFTO0NBQ1QsV0FBVztDQUNYLG1CQUFtQjtDQUNuQixrQkFBa0I7QUFDbkI7O0FBQ0E7Q0FDQyxTQUFTO0NBQ1QsY0FBYztBQUNmOztBQUNBO0NBQ0MseUJBQXlCO0NBQ3pCLG1CQUFtQjtDQUNuQixhQUFhO0FBQ2Q7O0FBQ0E7Q0FDQyxZQUFZO0FBQ2I7O0FBQ0E7Q0FDQyxTQUFTO0NBQ1QsZ0JBQWdCO0NBQ2hCLGNBQWM7QUFDZjs7QUFDQTtDQUNDLFNBQVM7Q0FDVCxnQkFBZ0I7Q0FDaEIsY0FBYztBQUNmOztBQUNBO0NBQ0MsU0FBUztDQUNULGNBQWM7QUFDZjs7QUFDQTtDQUNDLHNCQUFzQjtBQUN2Qjs7QUFDQTtBQUNBLG1CQUFtQjtBQUNuQjs7QUFDQTtBQUNBLGFBQWE7QUFDYixtQkFBbUI7QUFDbkIsMkJBQTJCO0FBQzNCLGtCQUFrQjtBQUNsQixZQUFZO0FBQ1osaUJBQWlCO0FBQ2pCOztBQUNBO0NBQ0MsU0FBUztDQUNULGtCQUFrQjtDQUNsQixRQUFRO0NBQ1IsNENBQTRDO0NBQzVDLFlBQVk7QUFDYjs7QUFDQTtDQUNDLFdBQVc7Q0FDWCxjQUFjO0NBQ2Qsa0JBQWtCO0FBQ25COztBQUNBO0NBQ0MsMEJBQTBCO0NBQzFCLG1CQUFtQjtDQUNuQixZQUFZO0NBQ1osNkJBQTZCO0NBQzdCLHNCQUFzQjtDQUN0QixXQUFXO0FBQ1o7O0FBQ0E7Q0FDQyxtQkFBbUI7Q0FDbkIsa0JBQWtCO0NBQ2xCLFdBQVc7Q0FDWCx5QkFBeUI7Q0FDekIseUJBQXlCO0NBQ3pCLDZCQUE2QjtDQUM3QixXQUFXO0FBQ1o7O0FBQ0E7Q0FDQyx1QkFBdUI7Q0FDdkIsWUFBWTtDQUNaLG1CQUFtQjtDQUNuQixjQUFjO0NBQ2QsZUFBZTtDQUNmLFlBQVk7Q0FDWixpQkFBaUI7Q0FDakIsVUFBVTtDQUNWLGFBQWE7Q0FDYixlQUFlO0NBQ2Ysa0JBQWtCO0NBQ2xCLDZCQUE2QjtDQUM3QixzQkFBc0I7Q0FDdEIsV0FBVztDQUNYLFVBQVU7Q0FDVix5QkFBeUI7QUFDMUI7O0FBQ0E7Q0FDQyxzQkFBc0I7QUFDdkI7O0FBQ0E7Q0FDQyxzQkFBc0I7QUFDdkI7O0FBQ0E7Q0FDQyxvQkFBb0I7QUFDckI7O0FBQ0E7Q0FDQywwREFBMEQ7Q0FDMUQsUUFBUTtBQUNUOztBQUNBO0NBQ0MseUJBQXlCO0NBQ3pCLG1CQUFtQjtDQUNuQixZQUFZO0NBQ1osWUFBWTtBQUNiOztBQUNBO0NBQ0MsVUFBVTtDQUNWLFlBQVk7QUFDYjs7QUFDQTtDQUNDLGVBQWU7Q0FDZixlQUFlO0NBQ2YsbUJBQW1CO0NBQ25CLE9BQU87Q0FDUCxRQUFRO0NBQ1IsYUFBYTtDQUNiLGFBQWE7Q0FDYix1QkFBdUI7Q0FDdkIsbUJBQW1CO0NBQ25CLFNBQVM7QUFDVjs7QUFDQTtDQUNDLFVBQVU7Q0FDVixtQkFBbUI7Q0FDbkIsYUFBYTtDQUNiLHNCQUFzQjtDQUN0Qiw4QkFBOEI7QUFDL0I7O0FBQ0E7Q0FDQyxZQUFZO0NBQ1osWUFBWTtDQUNaLGFBQWE7Q0FDYixhQUFhO0NBQ2IsdUJBQXVCO0NBQ3ZCLHFCQUFxQjtDQUNyQixrQkFBa0I7QUFDbkI7O0FBQ0E7QUFDQSxZQUFZO0FBQ1osa0JBQWtCO0FBQ2xCLFlBQVk7QUFDWixRQUFRO0FBQ1IsWUFBWTtBQUNaLGVBQWU7QUFDZixVQUFVO0FBQ1YsaUJBQWlCO0FBQ2pCOztBQUVBO0FBQ0EsK0JBQStCO0FBQy9CLGtCQUFrQjtBQUNsQixhQUFhO0FBQ2IsUUFBUTtBQUNSLFlBQVk7QUFDWixlQUFlO0FBQ2YsVUFBVTtBQUNWLGlCQUFpQjtBQUNqQixZQUFZO0FBQ1osbUJBQW1CO0FBQ25CLGtCQUFrQjtBQUNsQixZQUFZO0FBQ1osYUFBYTtBQUNiLHVCQUF1QjtBQUN2QixtQkFBbUI7QUFDbkI7O0FBRUE7Q0FDQyxxQkFBcUI7Q0FDckIsY0FBYztDQUNkLHNCQUFzQjtDQUN0Qix1QkFBdUI7Q0FDdkIsZUFBZTtBQUNoQjs7QUFDQTtDQUNDLFlBQVk7Q0FDWixhQUFhO0NBQ2IsaUJBQWlCO0FBQ2xCOztBQUNBO0NBQ0Msa0JBQWtCO0NBQ2xCLFNBQVM7Q0FDVCxZQUFZO0NBQ1osZUFBZTtBQUNoQjs7QUFNQTtBQUNBLDZCQUE2QjtBQUM3QixhQUFhO0FBQ2IsdUJBQXVCO0FBQ3ZCLHNCQUFzQjtBQUN0Qjs7QUFDQTtBQUNBLGFBQWE7QUFDYixhQUFhO0FBQ2IsZ0JBQWdCO0FBQ2hCLGlCQUFpQjtBQUNqQjs7QUFDQTtBQUNBLGVBQWU7QUFDZixjQUFjO0FBQ2Q7O0FBRUE7QUFDQSxrQkFBa0I7QUFDbEIscUJBQXFCO0FBQ3JCLGtCQUFrQjtBQUNsQixZQUFZO0FBQ1osWUFBWTtBQUNaLFVBQVU7QUFDVixrQkFBa0I7QUFDbEI7O0FBRUE7QUFDQSxZQUFZO0FBQ1osZUFBZTtBQUNmLHFCQUFxQjs7QUFFckIsZ0JBQWdCO0FBQ2hCLFlBQVk7QUFDWixhQUFhO0FBQ2IsWUFBWTtBQUNaLFlBQVk7QUFDWixtQkFBbUI7QUFDbkIsVUFBVTtBQUNWLGtCQUFrQjtBQUNsQixNQUFNO0FBQ04sUUFBUTtBQUNSLGdCQUFnQjtBQUNoQixVQUFVO0FBQ1YsbURBQW1EO0FBQ25ELGVBQWU7QUFDZjs7QUFFQTtBQUNBLDhCQUE4QjtBQUM5Qjs7QUFFQTtBQUNBLFlBQVk7QUFDWixVQUFVO0FBQ1YsOEJBQThCO0FBQzlCLFlBQVk7QUFDWjs7QUFDQTtBQUNBLFlBQVk7QUFDWixXQUFXO0FBQ1gscUJBQXFCO0FBQ3JCLFlBQVk7QUFDWixZQUFZO0FBQ1o7eUJBQ3lCO0FBQ3pCLHFCQUFxQjtBQUNyQixZQUFZO0FBQ1osa0JBQWtCO0FBQ2xCLE1BQU07QUFDTixRQUFRO0FBQ1IsVUFBVTtBQUNWLGVBQWU7QUFDZixZQUFZO0FBQ1osZUFBZTtBQUNmLDZCQUE2QjtBQUM3Qjs7QUFFQTtBQUNBLFlBQVk7QUFDWjs7QUFFQTtFQUNFLFNBQVM7RUFDVCxnQkFBZ0I7RUFDaEIsY0FBYztBQUNoQjs7QUFDQTtFQUNFLGdCQUFnQjtFQUNoQixhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLHVCQUF1QjtFQUN2QixtQkFBbUI7QUFDckI7O0FBQ0E7RUFDRSxTQUFTO0VBQ1QsY0FBYztBQUNoQjs7QUFDQTtFQUNFLGFBQWE7RUFDYiw4QkFBOEI7RUFDOUIsU0FBUztFQUNULFVBQVU7QUFDWjs7QUFDQTtFQUNFLHlCQUF5QjtFQUN6QixnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLGNBQWM7QUFDaEI7O0FBQ0E7RUFDRSxlQUFlO0VBQ2YsY0FBYztBQUNoQjs7QUFDQTtFQUNFLFdBQVc7RUFDWCxZQUFZO0VBQ1osYUFBYTtFQUNiLG1CQUFtQjs7QUFFckI7O0FBR0E7QUFDQSxpQkFBaUI7QUFDakIsY0FBYzs7QUFFZDs7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQixlQUFlO0FBQ2Y7O0FBRUE7QUFDQSxjQUFjO0FBQ2QsZUFBZTtBQUNmOztBQUNBO0FBQ0EsZUFBZTtBQUNmLGNBQWM7O0FBRWQ7O0FBQ0E7RUFDRSxlQUFlO0FBQ2pCLGNBQWM7QUFDZDs7QUFDQTtBQUNBLGFBQWE7QUFDYixtQkFBbUI7QUFDbkIsdUJBQXVCO0FBQ3ZCLG1CQUFtQjtBQUNuQixXQUFXO0FBQ1g7O0FBR0E7QUFDQSxZQUFZO0FBQ1osYUFBYTtBQUNiLFlBQVk7QUFDWixnQkFBZ0I7QUFDaEI7O0FBRUE7QUFDQSxZQUFZO0FBQ1osYUFBYTtBQUNiLFlBQVk7QUFDWixnQkFBZ0I7QUFDaEI7O0FBS0E7Ozs7OztBQU1BLGdCQUFnQjtBQUNoQixrQkFBa0I7QUFDbEIsWUFBWTtBQUNaLFdBQVc7QUFDWCxZQUFZO0FBQ1osZUFBZTtBQUNmLGdCQUFnQjtBQUNoQjs7QUFFQTtBQUNBLGlCQUFpQjtBQUNqQixxQkFBcUI7O0FBRXJCOztBQUNBO0FBQ0EsY0FBYztBQUNkLGdCQUFnQjtBQUNoQjs7QUFHQTtBQUNBLHlCQUF5QjtBQUN6Qiw2QkFBNkI7QUFDN0IsaUJBQWlCO0FBQ2pCLGtCQUFrQjtBQUNsQixzQkFBc0I7QUFDdEIsdUJBQXVCO0FBQ3ZCOztBQUVBOztBQUVBLGdCQUFnQjtBQUNoQixrQkFBa0I7QUFDbEIsdUJBQXVCO0FBQ3ZCLGNBQWM7QUFDZCxnQkFBZ0I7QUFDaEI7O0FBRUE7QUFDQSxXQUFXO0FBQ1gsZ0JBQWdCO0FBQ2hCLGdCQUFnQjtBQUNoQjs7QUFFQTtBQUNBLGNBQWM7QUFDZCxtQkFBbUI7QUFDbkIsU0FBUztBQUNULFdBQVc7QUFDWCxVQUFVO0FBQ1YsMEJBQTBCO0FBQzFCLDBCQUEwQjtBQUMxQixtQkFBbUI7QUFDbkIsYUFBYTtBQUNiLG1CQUFtQjtBQUNuQixZQUFZO0FBQ1osYUFBYTtBQUNiLG1EQUFtRDs7RUFFakQ7O0FBQ0E7SUFDRSxtQkFBbUI7SUFDbkIsMkJBQTJCO0lBQzNCLHdCQUF3QjtFQUMxQjs7QUFFQTtJQUNFLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsUUFBUTtJQUNSLFNBQVM7SUFDVCxVQUFVO0lBQ1YsV0FBVztJQUNYLFNBQVM7SUFDVCxZQUFZO0lBQ1osVUFBVTtJQUNWLHlCQUF5QjtJQUN6QixrQkFBa0I7SUFDbEIsaURBQWlEO0VBQ25EOztBQUVGO0FBQ0EseUJBQXlCOztBQUV6Qjs7QUFDRTtJQUNFLHlCQUF5QjtFQUMzQjs7QUFFRjtFQUNFLGFBQWE7RUFDYiw2Q0FBNkM7RUFDN0MsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQiw2QkFBNkI7RUFDN0IsbUJBQW1CO0FBQ3JCOztBQUNBO0VBQ0UsU0FBUztBQUNYOztBQUNBO0VBQ0UsbUNBQW1DO0VBQ25DLG1CQUFtQjtFQUNuQixhQUFhO0VBQ2IsU0FBUztFQUNULGFBQWE7RUFDYixZQUFZO0FBQ2Q7O0FBQ0E7RUFDRSxlQUFlO0FBQ2pCOztBQUNBO0VBQ0UsZUFBZTtBQUNqQixpQkFBaUI7QUFDakIsbUJBQW1CO0FBQ25COztBQUNBO0VBQ0UsZUFBZTtBQUNqQjs7QUFFQTtFQUNFLGtCQUFrQjtBQUNwQixhQUFhO0FBQ2IsZUFBZTtBQUNmLGlCQUFpQjtBQUNqQjs7QUFFQTtFQUNFLDZCQUFxQjtVQUFyQixxQkFBcUIsR0FBRyxZQUFZLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLFlBQVksQ0FBQyxpQkFBaUI7QUFDMUc7O0FBRUE7QUFDQSxZQUFZO0FBQ1osYUFBYTtBQUNiLGdCQUFnQjtBQUNoQixpQkFBaUI7QUFDakI7O0FBQ0E7QUFDQSxlQUFlO0FBQ2YsWUFBWTtBQUNaOztBQUNBO0FBQ0EsYUFBYTtBQUNiOztBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCOztBQUVBO0FBQ0EsYUFBYTtBQUNiOztBQUdBOztBQUVBOztBQUNBO0FBQ0Esa0JBQWtCO0FBQ2xCOztBQUVBLDJGQUEyRjs7QUFDM0Y7QUFDQSxZQUFZO0FBQ1osZUFBZTtBQUNmOztBQUNBO0FBQ0EsdUJBQXVCO0NBQ3RCLFlBQVk7Q0FDWixhQUFhO0FBQ2Q7O0FBQ0E7Q0FDQyx1QkFBdUI7QUFDeEI7O0FBQ0E7QUFDQSxhQUFhO1lBQ0Qsc0JBQXNCO1lBQ3RCLHVCQUF1QjtZQUN2QixVQUFVO1lBQ1Ysb0JBQW9CO0FBQ2hDOztBQUVBO0FBQ0EsYUFBYTtBQUNiLGtCQUFrQjtBQUNsQixtQkFBbUI7QUFDbkIsa0JBQWtCO0FBQ2xCLHNCQUFzQjtBQUN0Qjs7QUFDQTtFQUNFLGtCQUFrQixDQUFDLGFBQWEsQ0FBQyxZQUFZO0FBQy9DOztBQUNBO0VBQ0UsNkJBQTZCO0VBQzdCLFlBQVk7RUFDWixrQ0FBa0M7RUFDbEMseUJBQXlCO0VBQ3pCLGFBQWE7QUFDZjs7QUFFQTtFQUNFLHNCQUFzQjtFQUN0QixlQUFlO0VBQ2Y7QUFDRjs7QUFDQTtFQUNFLHVCQUF1QjtFQUN2QixlQUFlO0VBQ2Y7QUFDRjs7QUFDQTtFQUNFLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gsWUFBWTtFQUNaLGFBQWE7RUFDYixZQUFZO0VBQ1osY0FBYztFQUNkLGtCQUFrQjtFQUNsQix3QkFBd0I7RUFDeEIsZ0JBQWdCO0FBQ2xCOztBQUNBO0VBQ0UsV0FBVztFQUNYO0FBQ0Y7O0FBQ0E7RUFDRSxXQUFXOztBQUViOztBQUVBO0VBQ0UsZ0NBQTZEO0VBQzdELDRCQUE0QjtFQUM1QiwrQkFBK0I7RUFDL0IsZ0JBQWdCO0FBQ2xCOztBQUNBOztBQUVBOztBQUNBLHVCQUF1QixvQkFBb0IsQ0FBQzs7QUFDNUM7RUFDRSxrQkFBa0I7RUFDbEIsV0FBVztFQUNYO0FBQ0Y7O0FBRUE7RUFDRSxpQkFBaUI7RUFDakIsbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQjtBQUNGOztBQUNBO0VBQ0Usb0JBQW9CO0VBQ3BCLFlBQVk7QUFDZDs7QUFFRSwwR0FBMEc7O0FBQzFHOztJQUVFO01BQ0UsYUFBYTtFQUNqQiw2Q0FBNkM7RUFDN0MsYUFBYTtFQUNiLHNCQUFzQjtFQUN0Qiw2QkFBNkI7RUFDN0IsbUJBQW1CO0lBQ2pCO0lBQ0E7TUFDRSxpQkFBaUI7RUFDckIsYUFBYTtFQUNiLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsd0JBQXdCO0VBQ3hCLGtCQUFrQjtJQUNoQjs7SUFFQTtNQUNFLGFBQWE7RUFDakIsc0JBQXNCO0VBQ3RCLHVCQUF1QjtFQUN2QixXQUFXO0VBQ1gsbUJBQW1CO0lBQ2pCO0lBQ0E7TUFDRSxpQkFBaUI7TUFDakIsZ0JBQWdCO01BQ2hCLGFBQWE7TUFDYixzQkFBc0I7TUFDdEIsdUJBQXVCO01BQ3ZCLG1CQUFtQjtJQUNyQjtJQUNBO01BQ0UsZUFBZTtJQUNqQjs7O0lBR0E7TUFDRSxlQUFlO0VBQ25CLGVBQWU7RUFDZixtQkFBbUI7RUFDbkIsT0FBTztFQUNQLFFBQVE7RUFDUixhQUFhO0VBQ2IsYUFBYTtFQUNiLHVCQUF1QjtFQUN2QixtQkFBbUI7RUFDbkIsU0FBUztFQUNULFVBQVU7SUFDUjtJQUNBO01BQ0UsWUFBWTtNQUNaLFlBQVk7TUFDWixlQUFlO0VBQ25CLGVBQWU7RUFDZixtQkFBbUI7RUFDbkIsT0FBTztFQUNQLFFBQVE7RUFDUixZQUFZO0VBQ1osYUFBYTtFQUNiLHVCQUF1QjtFQUN2QixtQkFBbUI7RUFDbkIsU0FBUztFQUNULFVBQVU7RUFDVixpQkFBaUI7RUFDakIsZUFBZTs7SUFFYjtJQUNBO01BQ0UsWUFBWTtNQUNaLFlBQVk7TUFDWixlQUFlO0VBQ25CLGVBQWU7RUFDZixtQkFBbUI7RUFDbkIsT0FBTztFQUNQLFFBQVE7RUFDUixhQUFhO0VBQ2IsYUFBYTtFQUNiLHVCQUF1QjtFQUN2QixtQkFBbUI7RUFDbkIsU0FBUztFQUNULFVBQVU7SUFDUjtJQUNBO01BQ0UsVUFBVTtNQUNWLG1CQUFtQjtNQUNuQixhQUFhO01BQ2Isc0JBQXNCO01BQ3RCLDhCQUE4QjtHQUNqQztJQUNDO01BQ0UsZUFBZTtNQUNmLGVBQWU7TUFDZixtQkFBbUI7TUFDbkIsT0FBTztNQUNQLFFBQVE7TUFDUixhQUFhO01BQ2IsYUFBYTtNQUNiLHVCQUF1QjtNQUN2QixtQkFBbUI7TUFDbkIsVUFBVTtNQUNWLFdBQVc7TUFDWCxjQUFjO0lBQ2hCOztHQUVEO0lBQ0MsVUFBVTtJQUNWLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDhCQUE4QjtFQUNoQzs7O0VBR0E7SUFDRSx5QkFBeUI7SUFDekIsbUJBQW1CO0lBQ25CLFlBQVk7SUFDWixZQUFZO0VBQ2Q7RUFDQTtJQUNFLFVBQVU7SUFDVixZQUFZO0VBQ2Q7O0VBRUE7SUFDRSxlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLGNBQWM7SUFDZCxrQkFBa0I7SUFDbEIsa0JBQWtCO0NBQ3JCO0NBQ0E7SUFDRyxhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLDJCQUEyQjtJQUMzQixrQkFBa0I7SUFDbEIsWUFBWTtBQUNoQjtBQUNBO0VBQ0UsYUFBYTtFQUNiLHVCQUFrQjtFQUFsQixrQkFBa0I7RUFDbEIsc0JBQXNCO0VBQ3RCLHVCQUF1QjtFQUN2QixtQkFBbUI7RUFDbkIsU0FBUztBQUNYO0FBQ0E7RUFDRSxZQUFZO0VBQ1osWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLGlCQUFpQjtFQUNqQixtQkFBbUI7QUFDckI7QUFDQTtFQUNFLHVCQUFrQjtFQUFsQixrQkFBa0I7QUFDcEI7O0FBRUE7RUFDRSxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixrQkFBa0I7QUFDcEI7QUFDQTtDQUNDLFVBQVU7Q0FDVixZQUFZO0FBQ2I7QUFDQTtDQUNDLG1CQUFtQjtBQUNwQjs7QUFFQTtBQUNBLGlCQUFpQjtBQUNqQixhQUFhO0FBQ2IsWUFBWTtBQUNaO0FBQ0E7QUFDQSw2QkFBNkI7QUFDN0IsWUFBWTtBQUNaLGtDQUFrQztBQUNsQyx5QkFBeUI7QUFDekIsYUFBYTtBQUNiOzs7QUFHQTtBQUNBLGdDQUE2RDtBQUM3RCw0QkFBNEI7QUFDNUIsK0JBQStCO0FBQy9CLGdCQUFnQjtBQUNoQjtBQUNBO0FBQ0EsYUFBYTtBQUNiLHNCQUFzQjtBQUN0Qix1QkFBdUI7QUFDdkI7QUFDQTtBQUNBLGdCQUFnQjtBQUNoQjs7Q0FFQztBQUNELG1CQUFtQjtBQUNuQjtDQUNDO0FBQ0QsU0FBUztBQUNUO0NBQ0M7QUFDRCxTQUFTO0FBQ1Q7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0FBQ0EsZUFBZTtBQUNmLFdBQVc7QUFDWDtBQUNBO0FBQ0EsY0FBYztBQUNkLFdBQVc7QUFDWDtBQUNBOztBQUVBO0FBQ0EsaUJBQWlCO0FBQ2pCLGNBQWM7QUFDZCxhQUFhO0FBQ2IsZUFBZTtBQUNmLGFBQWE7QUFDYixzQkFBc0I7QUFDdEIsdUJBQXVCO0FBQ3ZCLG1CQUFtQjtBQUNuQjtBQUNBO0FBQ0EsZ0JBQWdCO0FBQ2hCLFlBQVk7QUFDWjs7O0VBR0U7TUFDSSxxQkFBcUI7TUFDckIscUJBQXFCO01BQ3JCLHdCQUF3QjtNQUN4QixrQkFBa0I7TUFDbEIsa0JBQWtCO01BQ2xCLG9CQUFvQjtNQUNwQixtQkFBbUI7TUFDbkIsY0FBYztNQUNkLFVBQVU7QUFDaEI7O0VBRUUiLCJmaWxlIjoic3JjL2FwcC9ob21lL2hvbWUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG5mb290ZXJ7XHJcbiAgYmFja2dyb3VuZDogIzExMUQ1RSAhaW1wb3J0YW50O1xyXG4gIGNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50O1xyXG5cclxufVxyXG5cclxuLm5hdmJhci1uYXZ7XHJcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmb250LXNpemU6IDE0cHg7XHJcbiAgfVxyXG4gIC5uYXYtbGlua3tcclxuICAgICAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG4gIC5uYXZsaW5rd2hpdHtcclxuICAgICAgY29sb3I6ICMxMTFENUUgIWltcG9ydGFudDtcclxuICB9XHJcbiAgLmNvbntcclxuICAgICAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XHJcbiAgICAgIGJvcmRlcjogbm9uZTtcclxuICAgICAgYmFja2dyb3VuZDogcmVkO1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICAgICB3aWR0aDogMTIwcHg7XHJcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIH1cclxuICAudG9wbmF2IGxpIGE6aG92ZXIge1xyXG4gICAgICBib3JkZXItYm90dG9tOiAwLjFweCBzb2xpZCByZWQ7XHJcblxyXG4gIH1cclxuICAubmF2X3R7XHJcbiAgICAgIGNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50O1xyXG4gIH1cclxuICAubmF2X2d7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICMxMTFENUU7XHJcbiAgfVxyXG4gIC5uYXZ3aGl0e1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRUJFQ0YwO1xyXG5cclxuICB9XHJcbiAgLm5hdl9pbWd7XHJcbiAgICAgIHdpZHRoOiA4MHB4O1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG4gIH1cclxuICAgICAubmF2YmFyLWJyYW5kIHtcclxuICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgICBwYWRkaW5nLXRvcDogLjMxMjVyZW07XHJcbiAgICAgIHBhZGRpbmctYm90dG9tOiAuMzEyNXJlbTtcclxuICAgICAgbWFyZ2luLXJpZ2h0OiAxcmVtO1xyXG4gICAgICBmb250LXNpemU6IDEuMjVyZW07XHJcbiAgICAgIGxpbmUtaGVpZ2h0OiBpbmhlcml0O1xyXG4gICAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gICAgICBtYXJnaW4tbGVmdDogNzVweDtcclxuICAgICAgei1pbmRleDogNTtcclxuICB9XHJcblxyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiBiYXIgbW92ZSAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uZmxvYXRfYWN0aW9ucyB7XHJcbnBvc2l0aW9uOiBmaXhlZDtcclxuYmFja2dyb3VuZDogcmVkO1xyXG5ib3JkZXItcmFkaXVzOiAxMHB4O1xyXG5sZWZ0OiAwO1xyXG50b3A6IDM1JTtcclxucGFkZGluZzogMTBweDtcclxuZGlzcGxheTogZmxleDtcclxuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbndpZHRoOiA3JTtcclxufVxyXG4uZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IHtcclxucGFkZGluZzogMDtcclxubWFyZ2luOiAwIDAgLTMwcHggMDtcclxuZGlzcGxheTogZmxleDtcclxuZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuanVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG59XHJcbi5mbG9hdF9hY3Rpb25zIC5hY3Rpb25zX2NvbnRlbnQgLmFjdGlvbl9pdGVtcyB7XHJcbnBhZGRpbmc6IDVweDtcclxud2lkdGg6IDEyMHB4O1xyXG5oZWlnaHQ6IDEyMHB4O1xyXG5kaXNwbGF5OiBmbGV4O1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuYWxpZ24tY29udGVudDogY2VudGVyO1xyXG5wb3NpdGlvbjogcmVsYXRpdmU7XHJcbn1cclxuLmZsb2F0X2FjdGlvbnMgLmFjdGlvbnNfY29udGVudCAuYWN0aW9uX2l0ZW1zOjpiZWZvcmV7XHJcbmNvbnRlbnQ6IFwiPlwiO1xyXG5wb3NpdGlvbjogYWJzb2x1dGU7XHJcbnJpZ2h0OiAtMzBweDtcclxudG9wOiAxNSU7XHJcbmNvbG9yOiB3aGl0ZTtcclxuZm9udC1zaXplOiAyMHB4O1xyXG53aWR0aDogNDAlO1xyXG5mb250LXdlaWdodDogYm9sZDtcclxufVxyXG5cclxuLmZsb2F0X2FjdGlvbnMgLmFjdGlvbnNfY29udGVudCAuYWN0aW9uX2l0ZW1zOmhvdmVyOmFmdGVye1xyXG5jb250ZW50OiBcIlRlc3RleiBtb24gw6lnaWJpbGl0w6lcIjtcclxucG9zaXRpb246IGFic29sdXRlO1xyXG5yaWdodDogLTIwMHB4O1xyXG50b3A6IDE1JTtcclxuY29sb3I6IGJsYWNrO1xyXG5mb250LXNpemU6IDE3cHg7XHJcbndpZHRoOiA0MCU7XHJcbmJhY2tncm91bmQ6IHdoaXRlO1xyXG53aWR0aDogMTgwcHg7XHJcbmJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbnRleHQtYWxpZ246IGNlbnRlcjtcclxuaGVpZ2h0OiA0MHB4O1xyXG5kaXNwbGF5OiBmbGV4O1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuYWxpZ24taXRlbXM6IGNlbnRlcjtcclxufVxyXG5cclxuLmZsb2F0X2FjdGlvbnMgLmFjdGlvbnNfY29udGVudCAuYWN0aW9uX2l0ZW1zIC5pdGVtX2hyZWYge1xyXG50ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbmRpc3BsYXk6IGJsb2NrO1xyXG5mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuZm9udC1zaXplOiAxNHB4O1xyXG59XHJcbi5mbG9hdF9hY3Rpb25zIC5hY3Rpb25zX2NvbnRlbnQgLmFjdGlvbl9pdGVtcyAuaXRlbV9ocmVmIC5paHJlZl9sb2dvIHtcclxud2lkdGg6IDEwMHB4O1xyXG5oZWlnaHQ6IDEwMHB4O1xyXG5tYXJnaW4tbGVmdDogMjVweDtcclxufVxyXG4uZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IC5hY3Rpb25faXRlbXMgLml0ZW1faHJlZiAuaWhyZWZfdGV4dCB7XHJcbnRleHQtYWxpZ246IGNlbnRlcjtcclxubWFyZ2luOiAwO1xyXG5jb2xvcjogd2hpdGU7XHJcbm1hcmdpbi10b3A6IDdweDtcclxufVxyXG5cclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqaG9tZSAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5vdXRlci1kaXYsXHJcbi5pbm5lci1kaXYge1xyXG4gIGhlaWdodDogMzc4cHg7XHJcbiAgbWF4LXdpZHRoOiAzMDBweDtcclxuICBtYXJnaW46IDAgYXV0bztcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbn1cclxuXHJcbi5vdXRlci1kaXYge1xyXG4gIHBlcnNwZWN0aXZlOiA5MDBweDtcclxuICBwZXJzcGVjdGl2ZS1vcmlnaW46IDUwJSBjYWxjKDUwJSAtIDE4ZW0pO1xyXG59XHJcbiAub25le1xyXG5tYXJnaW46IDExcHggMHB4IDBweCA1NTZweFxyXG59XHJcbiAudHdve1xyXG5tYXJnaW46IC01ODBweCAwIDAgMTA2NXB4XHJcbn1cclxuLnRocmVle1xyXG5tYXJnaW46IC0xMnB4IDAgMCAxMDU5cHhcclxufVxyXG4uaW5uZXItZGl2IHtcclxuICBtYXJnaW46IDAgYXV0bztcclxuICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICBjb2xvcjogYmxhY2s7XHJcbiAgZm9udC1zaXplOiAxcmVtO1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuIFxyXG59XHJcblxyXG46aG92ZXIgLnNvY2lhbC1pY29uIHtcclxuICBvcGFjaXR5OiAxO1xyXG4gIHRvcDogMDtcclxufVxyXG5cclxuLyomOmhvdmVyIC5mcm9udF9fZmFjZS1waG90byxcclxuICAmOmhvdmVyIC5mcm9udF9fZm9vdGVyIHtcclxuICAgIG9wYWNpdHk6IDE7XHJcbiAgfSovXHJcblxyXG5cclxuLm91dGVyLWRpdjpob3ZlciAuaW5uZXItZGl2IHtcclxuICBcclxufVxyXG5cclxuLmZyb250LFxyXG4uYmFjayB7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIHRvcDogMDtcclxuICBsZWZ0OiAwO1xyXG4gIGJhY2tmYWNlLXZpc2liaWxpdHk6IGhpZGRlbjtcclxufVxyXG5cclxuLmZyb250IHtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgaGVpZ2h0OiA4NSU7XHJcbiAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgYmFja2ZhY2UtdmlzaWJpbGl0eTogaGlkZGVuO1xyXG4gIGJveC1zaGFkb3c6IDAgMCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4xKSBpbnNldDtcclxuICBib3gtc2hhZG93OiAwcHggMXB4IDE1cHggZ3JleTtcclxuICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG5cclxufVxyXG5cclxuXHJcbi5mcm9udF9fZmFjZS1waG90bzEge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICB0b3A6IDEwcHg7XHJcbiAgaGVpZ2h0OiAxMjBweDtcclxuICB3aWR0aDogMTIwcHg7XHJcbiAgbWFyZ2luOiAwIGF1dG87XHJcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG5cclxuICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAvKiBiYWNrZmFjZS12aXNpYmlsaXR5OiBoaWRkZW47XHJcbiAgICAgICB0cmFuc2l0aW9uOiBhbGwgMC42cyBjdWJpYy1iZXppZXIoMC44LCAtMC40LCAwLjIsIDEuNyk7XHJcbiAgICAgICB6LWluZGV4OiAzOyovXHJcbn1cclxuXHJcblxyXG4uZnJvbnRfX2ZhY2UtcGhvdG8yIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgdG9wOiAxMHB4O1xyXG4gIGhlaWdodDogMTIwcHg7XHJcbiAgd2lkdGg6IDEyMHB4O1xyXG4gIG1hcmdpbjogMCBhdXRvO1xyXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAvKiBiYWNrZmFjZS12aXNpYmlsaXR5OiBoaWRkZW47XHJcbiAgICAgICB0cmFuc2l0aW9uOiBhbGwgMC42cyBjdWJpYy1iZXppZXIoMC44LCAtMC40LCAwLjIsIDEuNyk7XHJcbiAgICAgICB6LWluZGV4OiAzOyovXHJcbn1cclxuXHJcblxyXG4uZnJvbnRfX2ZhY2UtcGhvdG8zIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgdG9wOiAxMHB4O1xyXG4gIGhlaWdodDogMTIwcHg7XHJcbiAgd2lkdGg6IDEyMHB4O1xyXG4gIG1hcmdpbjogMCBhdXRvO1xyXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuXHJcbiAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgLyogYmFja2ZhY2UtdmlzaWJpbGl0eTogaGlkZGVuO1xyXG4gICAgICAgdHJhbnNpdGlvbjogYWxsIDAuNnMgY3ViaWMtYmV6aWVyKDAuOCwgLTAuNCwgMC4yLCAxLjcpO1xyXG4gICAgICAgei1pbmRleDogMzsqL1xyXG59XHJcblxyXG4uZnJvbnRfX3RleHQge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICB0b3A6IDM1cHg7XHJcbiAgbWFyZ2luOiAwIGF1dG87XHJcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdFwiO1xyXG4gIGZvbnQtc2l6ZTogMThweDtcclxuICBiYWNrZmFjZS12aXNpYmlsaXR5OiBoaWRkZW47XHJcbn1cclxuXHJcbi5mcm9udF9fdGV4dC1oZWFkZXIge1xyXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgZm9udC1mYW1pbHk6IFwiT3N3YWxkXCI7XHJcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICBmb250LXNpemU6IDIwcHg7XHJcbn1cclxuXHJcbi5mcm9udF9fdGV4dC1wYXJhIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgdG9wOiAtNXB4O1xyXG4gIGNvbG9yOiAjMDAwO1xyXG4gIGZvbnQtc2l6ZTogMTRweDtcclxuICBsZXR0ZXItc3BhY2luZzogMC40cHg7XHJcbiAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICBmb250LWZhbWlseTogXCJNb250c2VycmF0XCIsIHNhbnMtc2VyaWY7XHJcbn1cclxuXHJcbi5mcm9udC1pY29ucyB7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIHRvcDogMDtcclxuICBmb250LXNpemU6IDE0cHg7XHJcbiAgbWFyZ2luLXJpZ2h0OiA2cHg7XHJcbiAgY29sb3I6IGdyYXk7XHJcbn1cclxuXHJcbi5mcm9udF9fdGV4dC1ob3ZlciB7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIHRvcDogMTBweDtcclxuICBmb250LXNpemU6IDEwcHg7XHJcbiAgY29sb3I6IHJlZDtcclxuICBiYWNrZmFjZS12aXNpYmlsaXR5OiBoaWRkZW47XHJcblxyXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICBsZXR0ZXItc3BhY2luZzogLjRweDtcclxuXHJcbiAgYm9yZGVyOiAycHggc29saWQgcmVkO1xyXG4gIHBhZGRpbmc6IDhweCAxNXB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDMwcHg7XHJcblxyXG4gIGJhY2tncm91bmQ6IHJlZDtcclxuICBjb2xvcjogd2hpdGU7XHJcbn1cclxuXHJcblxyXG4uYmFjayB7XHJcbiAgdHJhbnNmb3JtOiByb3RhdGVZKDE4MGRlZyk7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRvcDogMDtcclxuICBsZWZ0OiAwO1xyXG4gIGhlaWdodDogODUlO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmZlZmU7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgYm94LXNoYWRvdzogMHB4IDFweCAxNXB4IGdyZXk7XHJcbiAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuXHJcbn1cclxuLmNhcnJlIHtcclxud2lkdGg6IDIwMHB4O1xyXG5oZWlnaHQ6IDkwcHg7XHJcbmJhY2tncm91bmQ6IHdoaXRlO1xyXG5ib3JkZXItcmFkaXVzOiAxOHB4O1xyXG5tYXJnaW4tbGVmdDogLTI5MHB4O1xyXG5tYXJnaW4tdG9wOiAzMzVweDtcclxufVxyXG5cclxuLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLnNlY3Rpb25faGVhZGluZyB7XHJcbiBiYWNrZ3JvdW5kOiMxMTFkNWU7XHJcbiBtaW4taGVpZ2h0OiBmaXQtY29udGVudDtcclxuIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiAxMDBweDtcclxufVxyXG4ucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLmltZ193cmFwcGVyIC5oZWFkaW5nX2ltZyB7XHJcbiBoZWlnaHQ6IDEwMCU7XHJcbiBtYXgtaGVpZ2h0OiA1ODVweDtcclxuIG1hcmdpbi10b3A6IDE7XHJcbiBtYXJnaW4tbGVmdDogLTEzcHg7XHJcbiB3aWR0aDogMTA1MHB4O1xyXG59XHJcbi5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5zZWN0aW9uX2hlYWRpbmcgLmhlYWRpbmdfd3JhcHBlciAudGl0bGVfaGVhZGluZyB7XHJcbiBmb250LXNpemU6IDYwcHg7XHJcbiBmb250LXdlaWdodDogODAwO1xyXG4gY29sb3I6ICAjZmZmZmZmO1xyXG4gd2lkdGg6IG1heC1jb250ZW50O1xyXG59XHJcbi5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5zZWN0aW9uX2hlYWRpbmcgLmhlYWRpbmdfd3JhcHBlciAuZGVzY19oZWFkaW5nIHtcclxuIGZvbnQtc2l6ZTogNDBweDtcclxuIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiBjb2xvcjogICNmZmZmZmY7XHJcbiB3aWR0aDogbWF4LWNvbnRlbnQ7XHJcbn1cclxuLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLm91cl9zdWNjZXNzIC5zdWNjZXNzX3dyYXBwZXIgLnN1Y2Nlc3NfdHh0IHtcclxuIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiBmb250LXdlaWdodDogNDAwO1xyXG4gZm9udC1zaXplOiAxNHB4O1xyXG4gY29sb3I6ICMxMTFkNWU7XHJcbn1cclxuLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLm91cl9zdWNjZXNzIC5zdWNjZXNzX3dyYXBwZXIgLnN1Y2Nlc3NfZGVzYyB7XHJcbiBmb250LXNpemU6IDI4cHg7XHJcbiBjb2xvcjogIzExMWQ1ZTtcclxufVxyXG4ucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAub3VyX3N1Y2Nlc3MgLnN1Y2Nlc3Nfd3JhcHBlciAuc3VjY2Vzc19saXN0IHtcclxuIGRpc3BsYXk6IGZsZXg7XHJcbiBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiBtYXJnaW46IDA7XHJcbiBwYWRkaW5nOiAwO1xyXG59XHJcbi5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5vdXJfc3VjY2VzcyAuc3VjY2Vzc193cmFwcGVyIC5zdWNjZXNzX2xpc3QgLnN1Y2Nlc3NfaXRlbSB7XHJcbiBsaXN0LXN0eWxlOiBub25lO1xyXG4gZGlzcGxheTogZmxleDtcclxuIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbn1cclxuLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLm91cl9zdWNjZXNzIC5zdWNjZXNzX3dyYXBwZXIgLnN1Y2Nlc3NfbGlzdCAuc3VjY2Vzc19pdGVtIC5pdGVtX251bSB7XHJcbiBtYXJnaW46IDA7XHJcbiBmb250LXdlaWdodDogNzAwO1xyXG4gY29sb3I6ICMxMTFkNWU7XHJcbn1cclxuLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLm91cl9zdWNjZXNzIC5zdWNjZXNzX3dyYXBwZXIgLnN1Y2Nlc3NfbGlzdCAuc3VjY2Vzc19pdGVtIC5pdGVtX2Rlc2Mge1xyXG4gbWFyZ2luOiAwO1xyXG4gY29sb3I6ICMxMTFkNWU7XHJcbn1cclxuLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLmNhcmQtYmxvZyAuY2FyZF93cmFwcGVyIC5yb3cge1xyXG4gYmFja2dyb3VuZC1jb2xvcjogIzExMWQ1ZTtcclxuIGJvcmRlci1yYWRpdXM6IDMwcHg7XHJcbn1cclxuLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLmNhcmQtYmxvZyAuY2FyZF93cmFwcGVyIC5yb3cgLmNhcmQtYm9keSB7XHJcbiBtYXJnaW4tYm90dG9tOiAxNHB4O1xyXG4gbWFyZ2luLXRvcDogLTIycHg7XHJcbn1cclxuLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLmNhcmQtYmxvZyAuY2FyZF93cmFwcGVyIC5yb3cgLmNhcmQtYm9keSAudGVzdC1idG4xIHtcclxuIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbn1cclxuLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLmNhcmQtYmxvZyAuY2FyZF93cmFwcGVyIC5yb3cgLmNhcmQtYm9keSAudGVzdC1idG4yIHtcclxuIGJvcmRlcjogbm9uZTtcclxuIGJhY2tncm91bmQ6IHJlZDtcclxuIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiBjb2xvcjogd2hpdGU7XHJcbiBwYWRkaW5nOiA4cHggMzBweDtcclxufVxyXG4ucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuY2FyZC1ibG9nIC5jYXJkX3dyYXBwZXIgLnJvdyAuY2FyZC1ib2R5IC50ZXN0LWJ0bjMge1xyXG4gYm9yZGVyLXJhZGl1czogMjBweDtcclxufVxyXG4ucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuY2FyZC1ibG9nIC5jYXJkX3dyYXBwZXIgLnJvdyAuY2FyZC1ncm91cCB7XHJcbiB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLmNhcmQtYmxvZyAuY2FyZF93cmFwcGVyIC5yb3cgLmNhcmQtZ3JvdXAgLmNhcmQge1xyXG4gYm9yZGVyOiBub25lO1xyXG4gcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gZGlzcGxheTogZmxleDtcclxuIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiBtaW4td2lkdGg6IDA7XHJcbiB3b3JkLXdyYXA6IGJyZWFrLXdvcmQ7XHJcbiBiYWNrZ3JvdW5kLWNvbG9yOiAjMTExZDVlO1xyXG4gYm9yZGVyLXJhZGl1czogMTIuMjVyZW07XHJcbiBjb2xvcjogd2hpdGU7XHJcbn1cclxuLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLmNhcmQtYmxvZyAuY2FyZF93cmFwcGVyIC5yb3cgLmNhcmQtZ3JvdXAgLmNhcmQgLmNhcmQtaW1nLXRvcCB7XHJcbiB3aWR0aDogNTAlO1xyXG4gbWFyZ2luLWxlZnQ6IDkxcHg7XHJcbn1cclxuLnByaW1hcnlfYm9keSAuZnJlbmNoX21hcCAubWFwX3dyYXBwZXIgLm1hcF9ob2xkZXIgLm1hcF9zdmcgZyBwYXRoIHtcclxuIGZpbGw6IGdyYXk7XHJcbiBzdHJva2Utd2lkdGg6IDFweDtcclxuIHN0cm9rZTogZ3JheTtcclxuIGN1cnNvcjogcG9pbnRlcjtcclxuIHRyYW5zaXRpb246IDAuM3M7XHJcbn1cclxuLnByaW1hcnlfYm9keSAuZnJlbmNoX21hcCAubWFwX3dyYXBwZXIgLm1hcF9ob2xkZXIgLm1hcF9zdmcgZyBwYXRoOmhvdmVyIHtcclxuIGZpbGw6IHJlZDtcclxufVxyXG4ucHJpbWFyeV9ib2R5IC5kaXZpZGVyIC5kaXZpZGVyX2xpZ25lIHtcclxuIHdpZHRoOiAxMDAlO1xyXG4gaGVpZ2h0OiA1cHg7XHJcbiBiYWNrZ3JvdW5kOiAjMTExZDVlO1xyXG4gZGlzcGxheTogYmxvY2s7XHJcbiBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbn1cclxuLnByaW1hcnlfYm9keSAuZGl2aWRlciAuZGl2aWRlcl9saWduZTo6YmVmb3JlIHtcclxuIGNvbnRlbnQ6IFwiXCI7XHJcbiBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiB3aWR0aDogMzAlO1xyXG4gdG9wOiAwO1xyXG4gbGVmdDogMDtcclxuIGhlaWdodDogNXB4O1xyXG4gYmFja2dyb3VuZDogcmVkO1xyXG59XHJcbi5wcmltYXJ5X2JvZHkgLmRpdmlkZXIgLmRpdmlkZXJfbGlnbmU6OmFmdGVyIHtcclxuIGNvbnRlbnQ6IFwiXCI7XHJcbiBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiB3aWR0aDogMzAlO1xyXG4gdG9wOiAwO1xyXG4gcmlnaHQ6IDA7XHJcbiBoZWlnaHQ6IDVweDtcclxuIGJhY2tncm91bmQ6IHJlZDtcclxufVxyXG4ucHJpbWFyeV9ib2R5IC5hY3R1YWxpdHkgLmFjdHVhbGl0eV93cmFwcGVyIC50aXRsZSAudGl0bGVfdHh0IHtcclxuIGZvbnQtc2l6ZTogNDVweDtcclxuIGZvbnQtd2VpZ2h0OiA4MDA7XHJcbiBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiBtYXJnaW46IDA7XHJcbiBjb2xvcjogIzExMWQ1ZTtcclxufVxyXG4ucHJpbWFyeV9ib2R5IC5hY3R1YWxpdHkgLmFjdHVhbGl0eV93cmFwcGVyIC50aXRsZSAudGl0bGVfdHh0OjphZnRlciB7XHJcbiBjb250ZW50OiBcIlwiO1xyXG4gd2lkdGg6IDE3MHB4O1xyXG4gaGVpZ2h0OiAzcHg7XHJcbiBib3R0b206IDA7XHJcbiBsZWZ0OiA5MHB4O1xyXG4gYmFja2dyb3VuZDogIzExMWQ1ZTtcclxuIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxufVxyXG4ucHJpbWFyeV9ib2R5IC5hY3R1YWxpdHkgLmFjdHVhbGl0eV93cmFwcGVyIC5jYXJkX3dyYXBwZXIge1xyXG4gd2lkdGg6IDEwMCU7XHJcbiBoZWlnaHQ6IGZpdC1jb250ZW50O1xyXG4gb3ZlcmZsb3c6IGhpZGRlbjtcclxuIGJvcmRlci1yYWRpdXM6IDVweDtcclxufVxyXG4ucHJpbWFyeV9ib2R5IC5hY3R1YWxpdHkgLmFjdHVhbGl0eV93cmFwcGVyIC5jYXJkX3dyYXBwZXIgLmNhcmRfaW1nIHtcclxuIG92ZXJmbG93OiBoaWRkZW47XHJcbiBib3JkZXItcmFkaXVzOiAwIDAgNXB4IDVweDtcclxuIGhlaWdodDogMTUwcHg7XHJcbn1cclxuLnByaW1hcnlfYm9keSAuYWN0dWFsaXR5IC5hY3R1YWxpdHlfd3JhcHBlciAuY2FyZF93cmFwcGVyIC5jYXJkX2ltZyAuaW1hZ2Uge1xyXG4gaGVpZ2h0OiAxMDAlO1xyXG4gd2lkdGg6IDEwMCU7XHJcbiBvYmplY3QtZml0OiBjb3ZlcjtcclxufVxyXG4ucHJpbWFyeV9ib2R5IC5hY3R1YWxpdHkgLmFjdHVhbGl0eV93cmFwcGVyIC5jYXJkX3dyYXBwZXIgLmNhcmRfdHh0IHtcclxuIGhlaWdodDogNjBweDtcclxuIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gZGlzcGxheTogZmxleDtcclxuIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiBwYWRkaW5nOiA1cHg7XHJcbn1cclxuLnByaW1hcnlfYm9keSAuYWN0dWFsaXR5IC5hY3R1YWxpdHlfd3JhcHBlciAuY2FyZF93cmFwcGVyIC5jYXJkX3R4dCAudGl0bGUge1xyXG4gZm9udC1zaXplOiAxNnB4O1xyXG4gZm9udC13ZWlnaHQ6IDYwMDtcclxuIHdpZHRoOiAxMDAlO1xyXG4gd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuIG92ZXJmbG93OiBoaWRkZW47XHJcbiB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcclxuIG1hcmdpbjogMDtcclxuIGNvbG9yOiAjMTExZDVlO1xyXG59XHJcbi5wcmltYXJ5X2JvZHkgLmFjdHVhbGl0eSAuYWN0dWFsaXR5X3dyYXBwZXIgLmNhcmRfd3JhcHBlciAuY2FyZF90eHQgLmRlc2Mge1xyXG4gZm9udC1zaXplOiAxNHB4O1xyXG4gZm9udC13ZWlnaHQ6IDUwMDtcclxuIG92ZXJmbG93OiBoaWRkZW47XHJcbiB3aWR0aDogODAlO1xyXG4gd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gbWFyZ2luOiAwO1xyXG4gY29sb3I6ICMxMTFkNWU7XHJcbn1cclxuLnByaW1hcnlfYm9keSAuYWN0dWFsaXR5IC5hY3R1YWxpdHlfd3JhcHBlciAuY2FyZF93cmFwcGVyIC5jYXJkX3R4dCAuc2VlX21vcmUge1xyXG4gcG9zaXRpb246IGFic29sdXRlO1xyXG4gcmlnaHQ6IDVweDtcclxuIGJvdHRvbTogMTBweDtcclxuIGZvbnQtc2l6ZTogMTJweDtcclxuIGNvbG9yOiBncmF5O1xyXG59XHJcbi5wcmltYXJ5X2JvZHkgLm91cl9wcm9wb3NhbCAucHJvcG9zYWxfd3JhcHBlciAudGl0bGUgLnRpdGxlX3R4dCB7XHJcbiBmb250LXNpemU6IDQ1cHg7XHJcbiBmb250LXdlaWdodDogODAwO1xyXG4gcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gbWFyZ2luOiAwO1xyXG4gY29sb3I6ICMxMTFkNWU7XHJcbn1cclxuLnByaW1hcnlfYm9keSAub3VyX3Byb3Bvc2FsIC5wcm9wb3NhbF93cmFwcGVyIC50aXRsZSAudGl0bGVfdHh0OjphZnRlciB7XHJcbiBjb250ZW50OiBcIlwiO1xyXG4gd2lkdGg6IDE3MHB4O1xyXG4gaGVpZ2h0OiAzcHg7XHJcbiBib3R0b206IDA7XHJcbiBsZWZ0OiAyNTBweDtcclxuIGJhY2tncm91bmQ6ICMxMTFkNWU7XHJcbiBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbn1cclxuLnByaW1hcnlfYm9keSAub3VyX3Byb3Bvc2FsIC5wcm9wb3NhbF93cmFwcGVyIC50aXRsZSAuZGVzY190eHQge1xyXG4gbWFyZ2luOiAwO1xyXG4gY29sb3I6ICMxMTFkNWU7XHJcbn1cclxuLnByaW1hcnlfYm9keSAub3VyX3Byb3Bvc2FsIC5wcm9wb3NhbF93cmFwcGVyIC5jYXJkX3dyYXBwZXIge1xyXG4gYm9yZGVyOiAycHggc29saWQgIzY1YjliNztcclxuIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiBwYWRkaW5nOiAxMHB4O1xyXG59XHJcbi5wcmltYXJ5X2JvZHkgLm91cl9wcm9wb3NhbCAucHJvcG9zYWxfd3JhcHBlciAuY2FyZF93cmFwcGVyIC5jYXJkX2hlYWRlciB7XHJcbiBoZWlnaHQ6IDgwcHg7XHJcbn1cclxuLnByaW1hcnlfYm9keSAub3VyX3Byb3Bvc2FsIC5wcm9wb3NhbF93cmFwcGVyIC5jYXJkX3dyYXBwZXIgLmNhcmRfaGVhZGVyIC50aXRsZSB7XHJcbiBtYXJnaW46IDA7XHJcbiBmb250LXdlaWdodDogOTAwO1xyXG4gY29sb3I6ICMxMTFkNWU7XHJcbn1cclxuLnByaW1hcnlfYm9keSAub3VyX3Byb3Bvc2FsIC5wcm9wb3NhbF93cmFwcGVyIC5jYXJkX3dyYXBwZXIgLmNhcmRfaGVhZGVyIC5kZXNjIHtcclxuIG1hcmdpbjogMDtcclxuIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiBjb2xvcjogIzExMWQ1ZTtcclxufVxyXG4ucHJpbWFyeV9ib2R5IC5vdXJfcHJvcG9zYWwgLnByb3Bvc2FsX3dyYXBwZXIgLmNhcmRfd3JhcHBlciAuY2FyZF9kZXNjIC5kZXNjIHtcclxuIG1hcmdpbjogMDtcclxuIGNvbG9yOiAjMTExZDVlO1xyXG59XHJcbi5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jICosIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jICo6YmVmb3JlLCAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAqOmFmdGVyIHtcclxuIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbn1cclxuLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgYm9keSB7XHJcbmJhY2tncm91bmQ6ICNmNWY1ZjU7XHJcbn1cclxuLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2Mge1xyXG5kaXNwbGF5OiBmbGV4O1xyXG5mbGV4LWRpcmVjdGlvbjogcm93O1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XHJcbm1hcmdpbi1sZWZ0OiB1bnNldDtcclxud2lkdGg6IDY1MHB4O1xyXG5tYXJnaW4tbGVmdDogNDVweDtcclxufVxyXG4ucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyBtYWluIHtcclxuIGxlZnQ6IDUwJTtcclxuIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuIHRvcDogNTAlO1xyXG4gdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC01MCUpIHRyYW5zbGF0ZVkoLTUwJSk7XHJcbiB3aWR0aDogMzAwcHg7XHJcbn1cclxuLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaDpiZWZvcmUsIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2g6YWZ0ZXIge1xyXG4gY29udGVudDogXCJcIjtcclxuIGRpc3BsYXk6IGJsb2NrO1xyXG4gcG9zaXRpb246IGFic29sdXRlO1xyXG59XHJcbi5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2g6YmVmb3JlIHtcclxuIGJvcmRlcjogNXB4IHNvbGlkICNmZmZmZmYgO1xyXG4gYm9yZGVyLXJhZGl1czogMjBweDtcclxuIGhlaWdodDogNDBweDtcclxuIHRyYW5zaXRpb246IGFsbCAwLjNzIGVhc2Utb3V0O1xyXG4gdHJhbnNpdGlvbi1kZWxheTogMC4zcztcclxuIHdpZHRoOiA0MHB4O1xyXG59XHJcbi5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2g6YWZ0ZXIge1xyXG4gYmFja2dyb3VuZDogI2ZmZmZmZjtcclxuIGJvcmRlci1yYWRpdXM6IDNweDtcclxuIGhlaWdodDogNXB4O1xyXG4gdHJhbnNmb3JtOiByb3RhdGUoLTQ1ZGVnKTtcclxuIHRyYW5zZm9ybS1vcmlnaW46IDAlIDEwMCU7XHJcbiB0cmFuc2l0aW9uOiBhbGwgMC4zcyBlYXNlLW91dDtcclxuIHdpZHRoOiAxNXB4O1xyXG59XHJcbi5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2hfX2lucHV0IHtcclxuIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4gYm9yZGVyOiBub25lO1xyXG4gYm9yZGVyLXJhZGl1czogMjBweDtcclxuIGRpc3BsYXk6IGJsb2NrO1xyXG4gZm9udC1zaXplOiAyMHB4O1xyXG4gaGVpZ2h0OiA0MHB4O1xyXG4gbGluZS1oZWlnaHQ6IDQwcHg7XHJcbiBvcGFjaXR5OiAwO1xyXG4gb3V0bGluZTogbm9uZTtcclxuIHBhZGRpbmc6IDAgMTVweDtcclxuIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuIHRyYW5zaXRpb246IGFsbCAwLjNzIGVhc2Utb3V0O1xyXG4gdHJhbnNpdGlvbi1kZWxheTogMC42cztcclxuIHdpZHRoOiA0MHB4O1xyXG4gei1pbmRleDogMTtcclxuIGNvbG9yOiByZ2IoMjIzLCAyMjMsIDIyMyk7XHJcbn1cclxuLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaC0taGlkZTpiZWZvcmUge1xyXG4gdHJhbnNpdGlvbi1kZWxheTogMC4zcztcclxufVxyXG4ucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoLS1oaWRlOmFmdGVyIHtcclxuIHRyYW5zaXRpb24tZGVsYXk6IDAuNnM7XHJcbn1cclxuLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaC0taGlkZSAuc2VhcmNoX19pbnB1dCB7XHJcbiB0cmFuc2l0aW9uLWRlbGF5OiAwcztcclxufVxyXG4ucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoLS1zaG93OmFmdGVyIHtcclxuIHRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKSB0cmFuc2xhdGVYKDE1cHgpIHRyYW5zbGF0ZVkoLTJweCk7XHJcbiB3aWR0aDogMDtcclxufVxyXG4ucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoLS1zaG93OmJlZm9yZSB7XHJcbiBib3JkZXI6IDVweCBzb2xpZCAjZmZmZmZmO1xyXG4gYm9yZGVyLXJhZGl1czogMjBweDtcclxuIGhlaWdodDogNDBweDtcclxuIHdpZHRoOiA1MDBweDtcclxufVxyXG4ucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoLS1zaG93IC5zZWFyY2hfX2lucHV0IHtcclxuIG9wYWNpdHk6IDE7XHJcbiB3aWR0aDogNTAwcHg7XHJcbn1cclxuLnByaW1hcnlfYm9keSAuZmxvYXRfYWN0aW9ucyB7XHJcbiBwb3NpdGlvbjogZml4ZWQ7XHJcbiBiYWNrZ3JvdW5kOiByZWQ7XHJcbiBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gbGVmdDogMDtcclxuIHRvcDogMzUlO1xyXG4gcGFkZGluZzogMTBweDtcclxuIGRpc3BsYXk6IGZsZXg7XHJcbiBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiB3aWR0aDogNyU7XHJcbn1cclxuLnByaW1hcnlfYm9keSAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IHtcclxuIHBhZGRpbmc6IDA7XHJcbiBtYXJnaW46IDAgMCAtMzBweCAwO1xyXG4gZGlzcGxheTogZmxleDtcclxuIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbn1cclxuLnByaW1hcnlfYm9keSAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IC5hY3Rpb25faXRlbXMge1xyXG4gcGFkZGluZzogNXB4O1xyXG4gd2lkdGg6IDEyMHB4O1xyXG4gaGVpZ2h0OiAxMjBweDtcclxuIGRpc3BsYXk6IGZsZXg7XHJcbiBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcclxuIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxufVxyXG4ucHJpbWFyeV9ib2R5IC5mbG9hdF9hY3Rpb25zIC5hY3Rpb25zX2NvbnRlbnQgLmFjdGlvbl9pdGVtczo6YmVmb3Jle1xyXG5jb250ZW50OiBcIj5cIjtcclxucG9zaXRpb246IGFic29sdXRlO1xyXG5yaWdodDogLTEwcHg7XHJcbnRvcDogMTUlO1xyXG5jb2xvcjogd2hpdGU7XHJcbmZvbnQtc2l6ZTogMjBweDtcclxud2lkdGg6IDQwJTtcclxuZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbn1cclxuXHJcbi5wcmltYXJ5X2JvZHkgLmZsb2F0X2FjdGlvbnMgLmFjdGlvbnNfY29udGVudCAuYWN0aW9uX2l0ZW1zOmhvdmVyOmFmdGVye1xyXG5jb250ZW50OiBcIlRlc3RleiBtb24gw6lnaWJpbGl0w6lcIjtcclxucG9zaXRpb246IGFic29sdXRlO1xyXG5yaWdodDogLTE4MHB4O1xyXG50b3A6IDE1JTtcclxuY29sb3I6IGJsYWNrO1xyXG5mb250LXNpemU6IDE3cHg7XHJcbndpZHRoOiA0MCU7XHJcbmJhY2tncm91bmQ6IHdoaXRlO1xyXG53aWR0aDogMTgwcHg7XHJcbmJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbnRleHQtYWxpZ246IGNlbnRlcjtcclxuaGVpZ2h0OiA0MHB4O1xyXG5kaXNwbGF5OiBmbGV4O1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuYWxpZ24taXRlbXM6IGNlbnRlcjtcclxufVxyXG5cclxuLnByaW1hcnlfYm9keSAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IC5hY3Rpb25faXRlbXMgLml0ZW1faHJlZiB7XHJcbiB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiBkaXNwbGF5OiBibG9jaztcclxuIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuIGZvbnQtc2l6ZTogMTRweDtcclxufVxyXG4ucHJpbWFyeV9ib2R5IC5mbG9hdF9hY3Rpb25zIC5hY3Rpb25zX2NvbnRlbnQgLmFjdGlvbl9pdGVtcyAuaXRlbV9ocmVmIC5paHJlZl9sb2dvIHtcclxuIHdpZHRoOiAxMDBweDtcclxuIGhlaWdodDogMTAwcHg7XHJcbiBtYXJnaW4tbGVmdDogMjVweDtcclxufVxyXG4ucHJpbWFyeV9ib2R5IC5mbG9hdF9hY3Rpb25zIC5hY3Rpb25zX2NvbnRlbnQgLmFjdGlvbl9pdGVtcyAuaXRlbV9ocmVmIC5paHJlZl90ZXh0IHtcclxuIHRleHQtYWxpZ246IGNlbnRlcjtcclxuIG1hcmdpbjogMDtcclxuIGNvbG9yOiB3aGl0ZTtcclxuIG1hcmdpbi10b3A6IDdweDtcclxufVxyXG5cclxuXHJcblxyXG5cclxuXHJcbi5qdW1ib3Ryb24ge1xyXG5tYXJnaW46IC0yMXB4IC0yMnB4IDBweCAtMjRweDtcclxuaGVpZ2h0OiA4MDBweDtcclxuY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XHJcbmJvcmRlci1yYWRpdXM6IDAgMCAxNSU7XHJcbn1cclxuLmltZ2RhbWUge1xyXG53aWR0aDogMTAwMHB4O1xyXG5oZWlnaHQ6IDUwMHB4O1xyXG5tYXJnaW4tdG9wOiAtMTAlO1xyXG5tYXJnaW4tbGVmdDogLTEwJTtcclxufVxyXG4udGV4dF90cmFuc2l0aW9uIHtcclxuZm9udC1zaXplOiA2MHB4O1xyXG5jb2xvcjogIzExMWQ1ZTtcclxufVxyXG5cclxuI3dyYXAge1xyXG5tYXJnaW46IDUwcHggMTAwcHg7XHJcbmRpc3BsYXk6IGlubGluZS1ibG9jaztcclxucG9zaXRpb246IHJlbGF0aXZlO1xyXG5oZWlnaHQ6IDYwcHg7XHJcbmZsb2F0OiByaWdodDtcclxucGFkZGluZzogMDtcclxucG9zaXRpb246IHJlbGF0aXZlO1xyXG59XHJcblxyXG5pbnB1dFt0eXBlPVwidGV4dFwiXSB7XHJcbmhlaWdodDogNTBweDtcclxuZm9udC1zaXplOiAzMHB4O1xyXG5kaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcblxyXG5mb250LXdlaWdodDogMTAwO1xyXG5ib3JkZXI6IG5vbmU7XHJcbm91dGxpbmU6IG5vbmU7XHJcbmNvbG9yOiB3aGl0ZTtcclxucGFkZGluZzogM3B4O1xyXG5wYWRkaW5nLXJpZ2h0OiA2MHB4O1xyXG53aWR0aDogMHB4O1xyXG5wb3NpdGlvbjogYWJzb2x1dGU7XHJcbnRvcDogMDtcclxucmlnaHQ6IDA7XHJcbmJhY2tncm91bmQ6IG5vbmU7XHJcbnotaW5kZXg6IDM7XHJcbnRyYW5zaXRpb246IHdpZHRoIDAuNHMgY3ViaWMtYmV6aWVyKDAsIDAuNzk1LCAwLCAxKTtcclxuY3Vyc29yOiBwb2ludGVyO1xyXG59XHJcblxyXG5pbnB1dFt0eXBlPVwidGV4dFwiXTpmb2N1czpob3ZlciB7XHJcbmJvcmRlci1ib3R0b206IDFweCBzb2xpZCB3aGl0ZTtcclxufVxyXG5cclxuaW5wdXRbdHlwZT1cInRleHRcIl06Zm9jdXMge1xyXG53aWR0aDogNzAwcHg7XHJcbnotaW5kZXg6IDE7XHJcbmJvcmRlci1ib3R0b206IDFweCBzb2xpZCB3aGl0ZTtcclxuY3Vyc29yOiB0ZXh0O1xyXG59XHJcbmlucHV0W3R5cGU9XCJzdWJtaXRcIl0ge1xyXG5oZWlnaHQ6IDUwcHg7XHJcbndpZHRoOiA1MHB4O1xyXG5kaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbmNvbG9yOiB3aGl0ZTtcclxuZmxvYXQ6IHJpZ2h0O1xyXG5iYWNrZ3JvdW5kOiB1cmwoZGF0YTppbWFnZS9wbmc7YmFzZTY0LGlWQk9SdzBLR2dvQUFBQU5TVWhFVWdBQUFEQUFBQUF3Q0FNQUFBQmczQW0xQUFBQUdYUkZXSFJUYjJaMGQyRnlaUUJCWkc5aVpTQkpiV0ZuWlZKbFlXUjVjY2xsUEFBQUFETlFURlJGVTFOVDlmWDFsSlNVWGw1ZTFkWFZmbjUrYzNOejZ1cnF2NysvdExTMGlZbUpxYW1wbjUrZnlzckszOS9mYVdscC8vLy9WaTRaeXdBQUFCRjBVazVULy8vLy8vLy8vLy8vLy8vLy8vLy8vd0FsclpsaUFBQUJMa2xFUVZSNDJyU1dXUmJESUFoRkhlT1V0TjMvYWdzMXphQTRjSHJLWjhKRlJId29Ya3dUdndHUDFRbzBiWU9iQVB3aUxtYk5BSEJXRkJabEQ5ajBKeGZsRFZpSU9iTkhHL0RvOFBSSFRKazBUZXpBaHY3cWxvSzBKSkVCaCtGOCtVL2hvcElFTE9XZmlaVUNET1pEMVJBRE9RS0E3NW9xNGN2VmtjVCtPZEhucXFwUUNJVFdBam5XVmdHUVVXejEybEp1R3dHb2FXZ0JLelJWQmNDeXBnVWtPQW9XZ0JYL0wwQ214TjQwdTZ4d2NJSjFjT3pXWURmZnAzYXhzUU95dmRrWGlIOUZLUkZ3UFJIWVpVYVhNZ1BMZWlXN1FoYkRSY2l5TFhKYUtoZUN1TGJpVm9xeDFEVlJ5SDI2eWIwaHN1b09GRVBzb3orQlZFME1SbFpOakdaY1JReUhZa21NcDJoQlRJemRrekNUYy9wTHFPbkJyazcveVpkQU9xL3E1TlBCSDFmN3g3ZkdQNEMzQUFNQVFyaHpYOXpoY0dzQUFBQUFTVVZPUks1Q1lJST0pXHJcbiAgY2VudGVyIGNlbnRlciBuby1yZXBlYXQ7XHJcbnRleHQtaW5kZW50OiAtMTAwMDBweDtcclxuYm9yZGVyOiBub25lO1xyXG5wb3NpdGlvbjogYWJzb2x1dGU7XHJcbnRvcDogMDtcclxucmlnaHQ6IDA7XHJcbnotaW5kZXg6IDI7XHJcbmN1cnNvcjogcG9pbnRlcjtcclxub3BhY2l0eTogMC40O1xyXG5jdXJzb3I6IHBvaW50ZXI7XHJcbnRyYW5zaXRpb246IG9wYWNpdHkgMC40cyBlYXNlO1xyXG59XHJcblxyXG5pbnB1dFt0eXBlPVwic3VibWl0XCJdOmhvdmVyIHtcclxub3BhY2l0eTogMC44O1xyXG59XHJcblxyXG4uaXRlbV9udW0ge1xyXG4gIG1hcmdpbjogMDtcclxuICBmb250LXdlaWdodDogNzAwO1xyXG4gIGNvbG9yOiAjMTExZDVlO1xyXG59XHJcbi5zdWNjZXNzX2l0ZW0ge1xyXG4gIGxpc3Qtc3R5bGU6IG5vbmU7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbn1cclxuLml0ZW1fZGVzYyB7XHJcbiAgbWFyZ2luOiAwO1xyXG4gIGNvbG9yOiAjMTExZDVlO1xyXG59XHJcbi5zdWNjZXNzX2xpc3Qge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gIG1hcmdpbjogMDtcclxuICBwYWRkaW5nOiAwO1xyXG59XHJcbi5zdWNjZXNzX3R4dCB7XHJcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICBmb250LXdlaWdodDogNDAwO1xyXG4gIGZvbnQtc2l6ZTogMTRweDtcclxuICBjb2xvcjogIzExMWQ1ZTtcclxufVxyXG4uc3VjY2Vzc19kZXNjIHtcclxuICBmb250LXNpemU6IDI4cHg7XHJcbiAgY29sb3I6ICMxMTFkNWU7XHJcbn1cclxuLmNvbnRhaW5lcl9ib3gge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogYXV0bztcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcblxyXG59XHJcblxyXG5cclxuaDF7XHJcbmZvbnQtd2VpZ2h0OiBib2xkO1xyXG5jb2xvcjogIzExMWQ1ZTtcclxuXHJcbn1cclxuLmt7XHJcbmZvbnQtd2VpZ2h0OiBib2xkO1xyXG5tYXJnaW46IGluaGVyaXQ7XHJcbn1cclxuXHJcbmg1e1xyXG5jb2xvcjogIzExMWQ1ZTtcclxuZm9udC1zaXplOiAxNXB4O1xyXG59XHJcbnB7XHJcbmZvbnQtc2l6ZTogMTJweDtcclxuY29sb3I6ICMxMTFkNWU7XHJcblxyXG59XHJcbi5mb290ZXIgcHtcclxuICBmb250LXNpemU6IDEycHg7XHJcbmNvbG9yOiAjZmZmZmZmO1xyXG59XHJcbi5hY3R1e1xyXG5kaXNwbGF5OiBmbGV4O1xyXG5mbGV4LWRpcmVjdGlvbjogcm93O1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuYWxpZ24taXRlbXM6IGNlbnRlcjtcclxud2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcblxyXG4ubWFpbiB7XHJcbndpZHRoOiA4MDBweDtcclxuaGVpZ2h0OiAzNTBweDtcclxucGFkZGluZzogM2VtO1xyXG5tYXJnaW4tbGVmdDogMjUlO1xyXG59XHJcblxyXG4udGV4dGUge1xyXG53aWR0aDogODAwcHg7XHJcbmhlaWdodDogMzUwcHg7XHJcbnBhZGRpbmc6IDNlbTtcclxubWFyZ2luLWxlZnQ6IDI1JTtcclxufVxyXG5cclxuXHJcblxyXG5cclxuLmZyZW5jaF9tYXAsXHJcbi5tYXBfd3JhcHBlcixcclxuLm1hcF9ob2xkZXIsXHJcbi5tYXBfc3ZnLFxyXG5nLFxyXG5wYXRoIHtcclxuZmlsbDogZ2FpbnNib3JvIDtcclxuc3Ryb2tlLXdpZHRoOiAxcHggO1xyXG5zdHJva2U6IGdyYXk7XHJcbndpZHRoOiAxMDAlO1xyXG5oZWlnaHQ6IDEwMCU7XHJcbmN1cnNvcjogcG9pbnRlcjtcclxudHJhbnNpdGlvbjogLjJzIDtcclxufVxyXG5cclxuaDF7XHJcbmZvbnQtd2VpZ2h0OiBib2xkO1xyXG5tYXJnaW46IDUwcHggMCA1MHB4IDA7XHJcblxyXG59XHJcbi5wcmltYXJ5X2JvZHkgLmZyZW5jaF9tYXAgLm1hcF93cmFwcGVyIC5tYXBfaG9sZGVyIC5tYXBfc3ZnIGcgcGF0aDpob3ZlciB7XHJcbmZpbGw6ICMxMTFENUUgO1xyXG5zdHJva2U6ICNmZmQyZDIgO1xyXG59XHJcblxyXG5cclxuI3VzLW1hcCB7XHJcbmRpc3BsYXk6IGJsb2NrICFpbXBvcnRhbnQ7XHJcbnBvc2l0aW9uOiBhYnNvbHV0ZSAhaW1wb3J0YW50O1xyXG50b3A6IDAgIWltcG9ydGFudDtcclxubGVmdDogMCAhaW1wb3J0YW50O1xyXG53aWR0aDogMTAwJSAhaW1wb3J0YW50O1xyXG5oZWlnaHQ6IDEwMCUgIWltcG9ydGFudDtcclxufVxyXG5cclxucGF0aDpob3ZlcixcclxuY2lyY2xlOmhvdmVyIHtcclxuc3Ryb2tlOiAjMDAyODY4IDtcclxuc3Ryb2tlLXdpZHRoOiAycHggO1xyXG5zdHJva2UtbGluZWpvaW46IHJvdW5kIDtcclxuZmlsbDogIzAwMjg2OCA7XHJcbmN1cnNvcjogcG9pbnRlciA7XHJcbn1cclxuXHJcbiNwYXRoIHtcclxuZmlsbDogbm9uZSA7XHJcbnN0cm9rZTogI0E5QTlBOSA7XHJcbmN1cnNvcjogZGVmYXVsdCA7XHJcbn1cclxuXHJcbiNpbmZvLWJveCB7XHJcbmRpc3BsYXk6IG5vbmUgO1xyXG5wb3NpdGlvbjogYWJzb2x1dGUgO1xyXG50b3A6IDBweCA7XHJcbmxlZnQ6IDBweCAgO1xyXG56LWluZGV4OiAxO1xyXG5iYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmIDtcclxuYm9yZGVyOiAycHggc29saWQgI2ZmZmZmZiA7XHJcbmJvcmRlci1yYWRpdXM6IDVweCA7XHJcbnBhZGRpbmc6IDVweCA7XHJcbmZvbnQtZmFtaWx5OiBhcmlhbCA7XHJcbndpZHRoOiAxMDBweDtcclxuaGVpZ2h0OiAxMDBweDtcclxuYm94LXNoYWRvdzogcmdiYSgwLCAwLCAwLCAwLjE1KSAxLjk1cHggMS45NXB4IDIuNnB4O1xyXG5cclxuICB9XHJcbiAgLnJvdW5ke1xyXG4gICAgYm9yZGVyLXJhZGl1czogMzBweDtcclxuICAgIC13ZWJraXQtYm9yZGVyLXJhZGl1czogMzBweDtcclxuICAgIC1tb3otYm9yZGVyLXJhZGl1czogMzBweDtcclxuICB9XHJcblxyXG4gIC5yaWdodC1pbjphZnRlcntcclxuICAgIGNvbnRlbnQ6ICcgJztcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHdpZHRoOiAwO1xyXG4gICAgaGVpZ2h0OiAwO1xyXG4gICAgbGVmdDogYXV0bztcclxuICAgIHJpZ2h0OiAzNXB4O1xyXG4gICAgdG9wOiA4MHB4O1xyXG4gICAgYm90dG9tOiBhdXRvO1xyXG4gICAgei1pbmRleDogMDtcclxuICAgIHRyYW5zZm9ybTogcm90YXRlKDIyNWRlZyk7IFxyXG4gICAgYm9yZGVyOiAxMnB4IHNvbGlkO1xyXG4gICAgYm9yZGVyLWNvbG9yOiB3aGl0ZSB0cmFuc3BhcmVudCB0cmFuc3BhcmVudCB3aGl0ZTtcclxuICB9XHJcbiBcclxuLmFjdC1jb250ZW50e1xyXG5iYWNrZ3JvdW5kLWNvbG9yOiAjRUJFQ0YwO1xyXG5cclxufVxyXG4gIC5hY3QtaW1ne1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0VCRUNGMDtcclxuICB9XHJcblxyXG4ucHJvcG9zb25ze1xyXG4gIGhlaWdodDogMzAwcHg7XHJcbiAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KCNmZmZmZmYsICNlNWU2ZTcpO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG59XHJcbi5wcm9wb3NvbnMgaDF7XHJcbiAgbWFyZ2luOiAwO1xyXG59XHJcbi5Tb3VzQntcclxuICBib3JkZXI6IDNweCBzb2xpZCByZ2IoNDEsIDE5MSwgMjExKTtcclxuICBib3JkZXItcmFkaXVzOiAxNXB4O1xyXG4gIGhlaWdodDogMTYwcHg7XHJcbiAgbWFyZ2luOiAwO1xyXG4gIHBhZGRpbmc6IDIwcHg7XHJcbiAgd2lkdGg6IDMwMHB4O1xyXG59XHJcbi5Tb3VzQiBoMXtcclxuICBmb250LXNpemU6IDMwcHg7XHJcbn1cclxuLlNvdXNCICBoM3tcclxuICBmb250LXNpemU6IDE1cHg7XHJcbmZvbnQtd2VpZ2h0OiBib2xkO1xyXG5saW5lLWhlaWdodDogbm9ybWFsO1xyXG59XHJcbi5Tb3VzQiAgIHB7XHJcbiAgZm9udC1zaXplOiAxNXB4O1xyXG59XHJcblxyXG4uU291c0IgaXtcclxuICBtYXJnaW4tbGVmdDogMjc3cHg7XHJcbnotaW5kZXg6IGF1dG87XHJcbmZvbnQtc2l6ZTogMjZweDtcclxubWFyZ2luLXRvcDogLTUwcHg7XHJcbn1cclxuXHJcbi5idG50ZXN0e1xyXG4gIHRleHQtZGVjb3JhdGlvbjphdXRvIDsgIGJvcmRlcjogbm9uZTsgYmFja2dyb3VuZDogcmVkO2JvcmRlci1yYWRpdXM6IDI1cHg7Y29sb3I6IHdoaXRlO3BhZGRpbmc6IDVweCAxNXB4O1xyXG59XHJcblxyXG4udGVzdHtcclxud2lkdGg6IDEwMHB4O1xyXG5oZWlnaHQ6IDIwMHB4O1xyXG5tYXJnaW4tdG9wOiAyMHB4O1xyXG5tYXJnaW4tcmlnaHQ6IDVweDtcclxufVxyXG4udGVzdCBpe1xyXG5mb250LXNpemU6IDMwcHg7XHJcbmNvbG9yOiB3aGl0ZTtcclxufVxyXG4uc2hvd3Rlc3R7XHJcbmRpc3BsYXk6IG5vbmU7XHJcbn1cclxuLnNob3d0ZXN0IGJ1dHRvbntcclxubWFyZ2luLXRvcDogLTMwcHg7XHJcbn1cclxuXHJcbi50ZXN0OmhvdmVyICsgLnNob3d0ZXN0IHtcclxuZGlzcGxheTpibG9jaztcclxufVxyXG5cclxuXHJcbi5hY3Rpb25faXRlbXN7XHJcblxyXG59XHJcbi5jb250ZW50e1xyXG53aWR0aDogbWF4LWNvbnRlbnQ7XHJcbn1cclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqaG9tZSBvdGhlcioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5ob21le1xyXG5jb2xvcjogd2hpdGU7XHJcbmZvbnQtc2l6ZTogMThweDtcclxufVxyXG4udGhyZWVCe1xyXG5iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuIGNvbG9yOiB3aGl0ZTtcclxuIHBhZGRpbmc6IDIwcHg7XHJcbn1cclxuLnRocmVlQjpob3ZlcntcclxuIGJvcmRlcjogNXB4IHNvbGlkIHdoaXRlO1xyXG59XHJcbi5jaGlmZnJle1xyXG5kaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgd2lkdGg6IDQwJTtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGluaGVyaXQ7XHJcbn1cclxuXHJcbi50cmFuc3Rpb257XHJcbmRpc3BsYXk6IGZsZXg7XHJcbnBvc2l0aW9uOiBhYnNvbHV0ZTtcclxubWFyZ2luLWxlZnQ6IC0zNTBweDtcclxud2lkdGg6IG1heC1jb250ZW50O1xyXG5mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG59XHJcbi5jYXJkMXtcclxuICBtYXJnaW4tcmlnaHQ6IDUwcHg7aGVpZ2h0OiAzNDVweDt3aWR0aDogMzE1cHg7XHJcbn1cclxuLmNhcmQxIC5ib3h7XHJcbiAgYm94LXNoYWRvdzogMHB4IDFweCAxNXB4IGdyZXk7XHJcbiAgYm9yZGVyOiBub25lO1xyXG4gIGJvcmRlci1yYWRpdXM6IDcxcHggMTRweCA3MXB4IDE0cHg7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcclxuICBwYWRkaW5nOiA0MHB4O1xyXG59XHJcblxyXG4uY2FyZDEgLmJveCBoNHtcclxuICBwYWRkaW5nOiA2OXB4IDAgMCAyNnB4O1xyXG4gIGZvbnQtc2l6ZTogMjBweDtcclxuICBjb2xvcjojMDBCRkZGXHJcbn1cclxuLmNhcmQxIC5ib3ggaDZ7XHJcbiAgcGFkZGluZzogMHB4IDAgMHB4IDU0cHg7XHJcbiAgZm9udC1zaXplOiAxMHB4O1xyXG4gIGNvbG9yOiNjN2M3YzdcclxufVxyXG4uY2FyZDEgLmltYWdle1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICB0b3A6IC0xMzJweDtcclxuICBsZWZ0OiAtMTE2cHg7XHJcbiAgaGVpZ2h0OiAxMDBweDtcclxuICB3aWR0aDogMTAwcHg7XHJcbiAgbWFyZ2luOiAwIGF1dG87XHJcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICBvdmVyZmxvdzogaGlkZGVuO1xyXG59XHJcbi5jYXJkMSAuaW1hZ2UgaW1ne1xyXG4gIHdpZHRoOiAxMjAlO1xyXG4gIGhlaWdodDogMTIwJVxyXG59XHJcbi5sYXN0QntcclxuICB3aWR0aDogMTAwJTtcclxuIFxyXG59XHJcblxyXG4uc2Vjb25kLWJsb2N7XHJcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKC4uLy4uL2Fzc2V0cy9jcG5pbWFnZXMvc2l0ZWNwbi9zaW4ucG5nKTtcclxuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gIGJhY2tncm91bmQtcG9zaXRpb246NjU4cHggMjM0cHg7XHJcbiAgbWFyZ2luLXRvcDoxMDBweDtcclxufVxyXG4uc2Vjb25kLWJsb2MgLmJsb2NrMXtcclxuIFxyXG59XHJcbi5zZWNvbmQtYmxvYyAuYmxvY2sxIHB7cGFkZGluZy1yaWdodDogNDhyZW07fVxyXG4uYmxvY2s1IGltZ3tcclxuICBtYXJnaW4tbGVmdDogMTkwcHg7XHJcbiAgaGVpZ2h0OjEwMCU7XHJcbiAgd2lkdGg6MTAwJVxyXG59XHJcblxyXG4uYmxvY2s2e1xyXG4gIHBhZGRpbmctdG9wOiAzMHB4OyBcclxuICBwYWRkaW5nLWxlZnQ6IDExMHB4O1xyXG4gIG1hcmdpbi10b3A6IC01MDFweDsgXHJcbiAgZmxvYXQ6cmlnaHRcclxufVxyXG4uYmxvY2szIHB7XHJcbiAgcGFkZGluZy1yaWdodDogMjVyZW07XHJcbiAgd2lkdGg6IDgwMHB4O1xyXG59XHJcblxyXG4gIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnJlc3BvbnNpdmUgY3NzICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiAgQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNzY4cHgpIHtcclxuXHJcbiAgICAucHJvcG9zb25ze1xyXG4gICAgICBoZWlnaHQ6IDgwMHB4O1xyXG4gIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgjZmZmZmZmLCAjZTVlNmU3KTtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIH1cclxuICAgIC5Tb3VzQiBpe1xyXG4gICAgICBtYXJnaW4tbGVmdDogNzdweDtcclxuICB6LWluZGV4OiBhdXRvO1xyXG4gIGZvbnQtc2l6ZTogMjZweDtcclxuICBtYXJnaW4tdG9wOiA3MHB4O1xyXG4gIHRyYW5zZm9ybTogcm90YXRlKDkwZGVnKTtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB9XHJcbiAgXHJcbiAgICAuY2hpZmZyZXtcclxuICAgICAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB9XHJcbiAgICAuc3VjY2Vzc19pdGVte1xyXG4gICAgICBtYXJnaW4tbGVmdDogMjBweDtcclxuICAgICAgbGlzdC1zdHlsZTogbm9uZTtcclxuICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB9XHJcbiAgICAua3tcclxuICAgICAgbWFyZ2luOiBpbml0aWFsO1xyXG4gICAgfVxyXG4gICBcclxuICBcclxuICAgIC5wcmltYXJ5X2JvZHkgIC5mbG9hdF9hY3Rpb25ze1xyXG4gICAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgYmFja2dyb3VuZDogcmVkO1xyXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgbGVmdDogMDtcclxuICB0b3A6IDM1JTtcclxuICBwYWRkaW5nOiAxMHB4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICB3aWR0aDogMyU7XHJcbiAgaGVpZ2h0OiA1JTtcclxuICAgIH1cclxuICAgIC5wcmltYXJ5X2JvZHkgIC5mbG9hdF9hY3Rpb25zOjphZnRlcntcclxuICAgICAgY29udGVudDogXCI+XCI7XHJcbiAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgcG9zaXRpb246IGZpeGVkO1xyXG4gIGJhY2tncm91bmQ6IHJlZDtcclxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gIGxlZnQ6IDA7XHJcbiAgdG9wOiAzNSU7XHJcbiAgcGFkZGluZzogMHB4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICB3aWR0aDogNSU7XHJcbiAgaGVpZ2h0OiA1JTtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxuICBmb250LXNpemU6IDIwcHg7XHJcbiAgXHJcbiAgICB9XHJcbiAgICAucHJpbWFyeV9ib2R5ICAuZmxvYXRfYWN0aW9uczpob3ZlcjphZnRlcntcclxuICAgICAgY29udGVudDogXCI+XCI7XHJcbiAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgcG9zaXRpb246IGZpeGVkO1xyXG4gIGJhY2tncm91bmQ6IHJlZDtcclxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gIGxlZnQ6IDA7XHJcbiAgdG9wOiAzNSU7XHJcbiAgcGFkZGluZzogMTBweDtcclxuICBkaXNwbGF5OiBub25lO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgd2lkdGg6IDMlO1xyXG4gIGhlaWdodDogNSU7XHJcbiAgICB9XHJcbiAgICAucHJpbWFyeV9ib2R5IC5mbG9hdF9hY3Rpb25zIC5hY3Rpb25zX2NvbnRlbnQge1xyXG4gICAgICBwYWRkaW5nOiAwO1xyXG4gICAgICBtYXJnaW46IDAgMCAtMzBweCAwO1xyXG4gICAgICBkaXNwbGF5OiBub25lO1xyXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgIH1cclxuICAgIC5wcmltYXJ5X2JvZHkgIC5mbG9hdF9hY3Rpb25zOmhvdmVye1xyXG4gICAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICAgIGJhY2tncm91bmQ6IHJlZDtcclxuICAgICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgICAgbGVmdDogMDtcclxuICAgICAgdG9wOiAzNSU7XHJcbiAgICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICB3aWR0aDogMjAlO1xyXG4gICAgICBoZWlnaHQ6IDIzJTtcclxuICAgICAgei1pbmRleDogOTk5OTk7XHJcbiAgICB9XHJcbiAgXHJcbiAgIC5wcmltYXJ5X2JvZHkgLmZsb2F0X2FjdGlvbnM6aG92ZXIgLmFjdGlvbnNfY29udGVudCB7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgbWFyZ2luOiAwIDAgLTMwcHggMDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gIH1cclxuICBcclxuICBcclxuICAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoLS1zaG93OmJlZm9yZSB7XHJcbiAgICBib3JkZXI6IDVweCBzb2xpZCAjMTExZDVlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuICAgIGhlaWdodDogNDBweDtcclxuICAgIHdpZHRoOiAzMDBweDtcclxuICB9XHJcbiAgLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaC0tc2hvdyAuc2VhcmNoX19pbnB1dCB7XHJcbiAgICBvcGFjaXR5OiAxO1xyXG4gICAgd2lkdGg6IDMwMHB4O1xyXG4gIH1cclxuXHJcbiAgcHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLmltZ193cmFwcGVyIC5oZWFkaW5nX2ltZyB7XHJcbiAgICBmb250LXNpemU6IDQwcHg7XHJcbiAgICBmb250LXdlaWdodDogODAwO1xyXG4gICAgY29sb3I6ICNmZmZmZmY7XHJcbiAgICB3aWR0aDogbWF4LWNvbnRlbnQ7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiB9XHJcbiAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcclxuICAgIG1hcmdpbi1sZWZ0OiB1bnNldDtcclxuICAgIHdpZHRoOiAzOTBweDtcclxufVxyXG4udHJhbnN0aW9ue1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBtYXJnaW46IDA7XHJcbn1cclxuLmNhcnJlIHtcclxuICB3aWR0aDogMjAwcHg7XHJcbiAgaGVpZ2h0OiA5MHB4O1xyXG4gIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gIGJvcmRlci1yYWRpdXM6IDE4cHg7XHJcbiAgbWFyZ2luLWxlZnQ6IDExMHB4O1xyXG4gIG1hcmdpbi10b3A6IDI1MHB4O1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbn1cclxuLmNvbnRlbnR7XHJcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xyXG59XHJcblxyXG4ucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLnRpdGxlX2hlYWRpbmcge1xyXG4gIGZvbnQtc2l6ZTogMzBweDtcclxuICBmb250LXdlaWdodDogODAwO1xyXG4gIGNvbG9yOiAgI2ZmZmZmZjtcclxuICB3aWR0aDogbWF4LWNvbnRlbnQ7XHJcbn1cclxuLmxhc3RCe1xyXG4gd2lkdGg6IDgwJTtcclxuIG1hcmdpbjogNTBweDtcclxufVxyXG4udGV4dC1jZW50ZXJ7XHJcbiBtYXJnaW4tYm90dG9tOiA1MHB4O1xyXG59XHJcblxyXG4uY2FyZDF7XHJcbm1hcmdpbi1yaWdodDogMHB4O1xyXG5oZWlnaHQ6IDM0NXB4O1xyXG53aWR0aDogMzE1cHg7XHJcbn1cclxuLmNhcmQxIC5ib3h7XHJcbmJveC1zaGFkb3c6IDBweCAxcHggMTVweCBncmV5O1xyXG5ib3JkZXI6IG5vbmU7XHJcbmJvcmRlci1yYWRpdXM6IDcxcHggMTRweCA3MXB4IDE0cHg7XHJcbmJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XHJcbnBhZGRpbmc6IDQwcHg7XHJcbn1cclxuXHJcblxyXG4uc2Vjb25kLWJsb2N7XHJcbmJhY2tncm91bmQtaW1hZ2U6IHVybCguLi8uLi9hc3NldHMvY3BuaW1hZ2VzL3NpdGVjcG4vc2luLnBuZyk7XHJcbmJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbmJhY2tncm91bmQtcG9zaXRpb246NjU4cHggMjM0cHg7XHJcbm1hcmdpbi10b3A6MTAwcHg7XHJcbn1cclxuLnNlY29uZC1ibG9jIC5ibG9jazF7XHJcbmRpc3BsYXk6IGZsZXg7XHJcbmZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbmFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xyXG59XHJcbi5zZWNvbmQtYmxvYyAuYmxvY2sxIHB7XHJcbnBhZGRpbmctcmlnaHQ6IDA7XHJcbn1cclxuXHJcbiAub25le1xyXG5tYXJnaW46IDQwcHggMCAwIDAgO1xyXG59XHJcbiAudHdve1xyXG5tYXJnaW46IDA7XHJcbn1cclxuIC50aHJlZXtcclxubWFyZ2luOiAwO1xyXG59XHJcbi5vdXRlci1kaXZ7XHJcbiAgbWFyZ2luLWxlZnQ6IDE0JTtcclxufVxyXG4uYmxvY2s1e1xyXG5tYXgtd2lkdGg6IDEwMCU7XHJcbndpZHRoOiAxMDAlO1xyXG59XHJcbi5ibG9jazUgaW1ne1xyXG5tYXJnaW4tbGVmdDogMDtcclxuaGVpZ2h0OjEwMCU7XHJcbndpZHRoOjEwMCVcclxufVxyXG5cclxuLmJsb2NrNntcclxucGFkZGluZy10b3A6IDMwcHg7IFxyXG5wYWRkaW5nLWxlZnQ6MDtcclxubWFyZ2luLXRvcDogMDsgXHJcbm1heC13aWR0aDogMTAwJTtcclxuZGlzcGxheTogZmxleDtcclxuZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbn1cclxuLmJsb2NrMyBwe1xyXG5wYWRkaW5nLXJpZ2h0OiAwO1xyXG53aWR0aDogNTAwcHg7XHJcbn1cclxuXHJcblxyXG4gIC5uYXZiYXItYnJhbmQge1xyXG4gICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgIHBhZGRpbmctdG9wOiAuMzEyNXJlbTtcclxuICAgICAgcGFkZGluZy1ib3R0b206IC4zMTI1cmVtO1xyXG4gICAgICBtYXJnaW4tcmlnaHQ6IDFyZW07XHJcbiAgICAgIGZvbnQtc2l6ZTogMS4yNXJlbTtcclxuICAgICAgbGluZS1oZWlnaHQ6IGluaGVyaXQ7XHJcbiAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICAgIG1hcmdpbi1sZWZ0OiAwO1xyXG4gICAgICB6LWluZGV4OiA1O1xyXG59XHJcblxyXG4gIH1cclxuIl19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](HomeComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-home',
                templateUrl: './home.component.html',
                styleUrls: ['./home.component.css']
            }]
    }], function () { return [{ type: src_app_services_token_storage_service__WEBPACK_IMPORTED_MODULE_1__["TokenStorageService"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }]; }, null); })();


/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _services_token_storage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/token-storage.service */ "./src/app/services/token-storage.service.ts");







class LoginComponent {
    /**********************life Cycle ***********************/
    constructor(fb, router, _Activatedroute, auth, tokenStorage) {
        this.fb = fb;
        this.router = router;
        this._Activatedroute = _Activatedroute;
        this.auth = auth;
        this.tokenStorage = tokenStorage;
        this.LoginForm = this.fb.group({
            email: [null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]],
            password: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]],
        });
    }
    ngOnInit() {
    }
    /************************login *************************/
    onSubmit() {
        this.auth.login(this.LoginForm.value.email, this.LoginForm.value.password).subscribe(data => {
            this.tokenStorage.saveToken(data.token);
            this.tokenStorage.saveUser(data.user);
            location.href = '/crm/profile';
        });
    }
}
LoginComponent.ɵfac = function LoginComponent_Factory(t) { return new (t || LoginComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_token_storage_service__WEBPACK_IMPORTED_MODULE_4__["TokenStorageService"])); };
LoginComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: LoginComponent, selectors: [["app-login"]], decls: 51, vars: 1, consts: [[1, "container-fluid", "px-1", "px-md-5", "px-lg-1", "px-xl-5", "py-5", "mx-auto"], [1, "card", "card0", "border-0"], [1, "row", "d-flex"], [1, "col-lg-6"], [1, "card1", "pb-5"], [1, "row"], ["src", "https://i.imgur.com/CXQmsmF.png", 1, "logo"], [1, "row", "px-3", "justify-content-center", "mt-4", "mb-5", "border-line"], ["src", "https://i.imgur.com/uNGdWHi.png", 1, "image"], [1, "card2", "card", "border-0", "px-4", "py-5"], [1, "row", "mb-4", "px-3"], [1, "mb-0", "mr-4", "mt-2"], [1, "facebook", "text-center", "mr-3"], [1, "fa", "fa-facebook"], [1, "twitter", "text-center", "mr-3"], [1, "fa", "fa-twitter"], [1, "linkedin", "text-center", "mr-3"], [1, "fa", "fa-linkedin"], [1, "row", "px-3", "mb-4"], [1, "line"], [1, "or", "text-center"], [3, "formGroup", "ngSubmit"], [1, "row", "px-3"], [1, "mb-1"], [1, "mb-0", "text-sm"], ["type", "text", "name", "email", "formControlName", "email", "placeholder", "Enter a valid email address", 1, "mb-4"], ["type", "password", "name", "password", "formControlName", "password", "placeholder", "Enter password"], [1, "custom-control", "custom-checkbox", "custom-control-inline"], ["id", "chk1", "type", "checkbox", "name", "chk", 1, "custom-control-input"], ["for", "chk1", 1, "custom-control-label", "text-sm"], ["href", "#", 1, "ml-auto", "mb-0", "text-sm"], [1, "row", "mb-3", "px-3"], ["type", "submit", 1, "btn", "btn-blue", "text-center"], [1, "font-weight-bold"], [1, "text-danger"]], template: function LoginComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "img", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "img", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "h6", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Sign in with");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "small", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "Or");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](24, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "form", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function LoginComponent_Template_form_ngSubmit_25_listener() { return ctx.onSubmit(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "label", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "h6", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "Email Address");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](30, "input", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "label", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "h6", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34, "Password");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "input", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](38, "input", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "label", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](40, "Remember me");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "a", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42, "Forgot Password?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "button", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45, "Login");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "small", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](48, "Don't have an account? ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "a", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](50, "Register");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.LoginForm);
    } }, directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"]], styles: ["body[_ngcontent-%COMP%] {\r\n    color: #000;\r\n    overflow-x: hidden;\r\n    height: 100%;\r\n    background-color: #B0BEC5;\r\n    background-repeat: no-repeat;\r\n    margin: 20px;\r\n}\r\n\r\n.card0[_ngcontent-%COMP%] {\r\n    box-shadow: 0px 4px 8px 0px #757575;\r\n    border-radius: 0px\r\n}\r\n\r\n.card2[_ngcontent-%COMP%] {\r\n    margin: 0px 40px\r\n}\r\n\r\n.logo[_ngcontent-%COMP%] {\r\n    width: 200px;\r\n    height: 100px;\r\n    margin-top: 20px;\r\n    margin-left: 35px\r\n}\r\n\r\n.image[_ngcontent-%COMP%] {\r\n    width: 360px;\r\n    height: 280px\r\n}\r\n\r\n.border-line[_ngcontent-%COMP%] {\r\n    border-right: 1px solid #EEEEEE\r\n}\r\n\r\n.facebook[_ngcontent-%COMP%] {\r\n    background-color: #3b5998;\r\n    color: #fff;\r\n    font-size: 18px;\r\n    padding-top: 5px;\r\n    border-radius: 50%;\r\n    width: 35px;\r\n    height: 35px;\r\n    cursor: pointer\r\n}\r\n\r\n.twitter[_ngcontent-%COMP%] {\r\n    background-color: #1DA1F2;\r\n    color: #fff;\r\n    font-size: 18px;\r\n    padding-top: 5px;\r\n    border-radius: 50%;\r\n    width: 35px;\r\n    height: 35px;\r\n    cursor: pointer\r\n}\r\n\r\n.linkedin[_ngcontent-%COMP%] {\r\n    background-color: #2867B2;\r\n    color: #fff;\r\n    font-size: 18px;\r\n    padding-top: 5px;\r\n    border-radius: 50%;\r\n    width: 35px;\r\n    height: 35px;\r\n    cursor: pointer\r\n}\r\n\r\n.line[_ngcontent-%COMP%] {\r\n    height: 1px;\r\n    width: 45%;\r\n    background-color: #E0E0E0;\r\n    margin-top: 10px\r\n}\r\n\r\n.or[_ngcontent-%COMP%] {\r\n    width: 10%;\r\n    font-weight: bold\r\n}\r\n\r\n.text-sm[_ngcontent-%COMP%] {\r\n    font-size: 14px !important\r\n}\r\n\r\n[_ngcontent-%COMP%]::placeholder {\r\n    color: #BDBDBD;\r\n    opacity: 1;\r\n    font-weight: 300\r\n}\r\n\r\n[_ngcontent-%COMP%]:-ms-input-placeholder {\r\n    color: #BDBDBD;\r\n    font-weight: 300\r\n}\r\n\r\n[_ngcontent-%COMP%]::-ms-input-placeholder {\r\n    color: #BDBDBD;\r\n    font-weight: 300\r\n}\r\n\r\ninput[_ngcontent-%COMP%], textarea[_ngcontent-%COMP%] {\r\n    padding: 10px 12px 10px 12px;\r\n    border: 1px solid lightgrey;\r\n    border-radius: 2px;\r\n    margin-bottom: 5px;\r\n    margin-top: 2px;\r\n    width: 100%;\r\n    box-sizing: border-box;\r\n    color: #2C3E50;\r\n    font-size: 14px;\r\n    letter-spacing: 1px\r\n}\r\n\r\ninput[_ngcontent-%COMP%]:focus, textarea[_ngcontent-%COMP%]:focus {\r\n    box-shadow: none !important;\r\n    border: 1px solid #304FFE;\r\n    outline-width: 0\r\n}\r\n\r\nbutton[_ngcontent-%COMP%]:focus {\r\n    box-shadow: none !important;\r\n    outline-width: 0\r\n}\r\n\r\na[_ngcontent-%COMP%] {\r\n    color: inherit;\r\n    cursor: pointer\r\n}\r\n\r\n.btn-blue[_ngcontent-%COMP%] {\r\n    background-color: #1A237E;\r\n    width: 150px;\r\n    color: #fff;\r\n    border-radius: 2px\r\n}\r\n\r\n.btn-blue[_ngcontent-%COMP%]:hover {\r\n    background-color: #000;\r\n    cursor: pointer\r\n}\r\n\r\n.bg-blue[_ngcontent-%COMP%] {\r\n    color: #fff;\r\n    background-color: #1A237E\r\n}\r\n\r\n@media screen and (max-width: 991px) {\r\n    .logo[_ngcontent-%COMP%] {\r\n        margin-left: 0px\r\n    }\r\n\r\n    .image[_ngcontent-%COMP%] {\r\n        width: 300px;\r\n        height: 220px\r\n    }\r\n\r\n    .border-line[_ngcontent-%COMP%] {\r\n        border-right: none\r\n    }\r\n\r\n    .card2[_ngcontent-%COMP%] {\r\n        border-top: 1px solid #EEEEEE !important;\r\n        margin: 0px 15px\r\n    }\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9naW4vbG9naW4uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLFdBQVc7SUFDWCxrQkFBa0I7SUFDbEIsWUFBWTtJQUNaLHlCQUF5QjtJQUN6Qiw0QkFBNEI7SUFDNUIsWUFBWTtBQUNoQjs7QUFFQTtJQUNJLG1DQUFtQztJQUNuQztBQUNKOztBQUVBO0lBQ0k7QUFDSjs7QUFFQTtJQUNJLFlBQVk7SUFDWixhQUFhO0lBQ2IsZ0JBQWdCO0lBQ2hCO0FBQ0o7O0FBRUE7SUFDSSxZQUFZO0lBQ1o7QUFDSjs7QUFFQTtJQUNJO0FBQ0o7O0FBRUE7SUFDSSx5QkFBeUI7SUFDekIsV0FBVztJQUNYLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLFdBQVc7SUFDWCxZQUFZO0lBQ1o7QUFDSjs7QUFFQTtJQUNJLHlCQUF5QjtJQUN6QixXQUFXO0lBQ1gsZUFBZTtJQUNmLGdCQUFnQjtJQUNoQixrQkFBa0I7SUFDbEIsV0FBVztJQUNYLFlBQVk7SUFDWjtBQUNKOztBQUVBO0lBQ0kseUJBQXlCO0lBQ3pCLFdBQVc7SUFDWCxlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtJQUNsQixXQUFXO0lBQ1gsWUFBWTtJQUNaO0FBQ0o7O0FBRUE7SUFDSSxXQUFXO0lBQ1gsVUFBVTtJQUNWLHlCQUF5QjtJQUN6QjtBQUNKOztBQUVBO0lBQ0ksVUFBVTtJQUNWO0FBQ0o7O0FBRUE7SUFDSTtBQUNKOztBQUVBO0lBQ0ksY0FBYztJQUNkLFVBQVU7SUFDVjtBQUNKOztBQUVBO0lBQ0ksY0FBYztJQUNkO0FBQ0o7O0FBRUE7SUFDSSxjQUFjO0lBQ2Q7QUFDSjs7QUFFQTs7SUFFSSw0QkFBNEI7SUFDNUIsMkJBQTJCO0lBQzNCLGtCQUFrQjtJQUNsQixrQkFBa0I7SUFDbEIsZUFBZTtJQUNmLFdBQVc7SUFDWCxzQkFBc0I7SUFDdEIsY0FBYztJQUNkLGVBQWU7SUFDZjtBQUNKOztBQUVBOztJQUlJLDJCQUEyQjtJQUMzQix5QkFBeUI7SUFDekI7QUFDSjs7QUFFQTtJQUdJLDJCQUEyQjtJQUMzQjtBQUNKOztBQUVBO0lBQ0ksY0FBYztJQUNkO0FBQ0o7O0FBRUE7SUFDSSx5QkFBeUI7SUFDekIsWUFBWTtJQUNaLFdBQVc7SUFDWDtBQUNKOztBQUVBO0lBQ0ksc0JBQXNCO0lBQ3RCO0FBQ0o7O0FBRUE7SUFDSSxXQUFXO0lBQ1g7QUFDSjs7QUFFQTtJQUNJO1FBQ0k7SUFDSjs7SUFFQTtRQUNJLFlBQVk7UUFDWjtJQUNKOztJQUVBO1FBQ0k7SUFDSjs7SUFFQTtRQUNJLHdDQUF3QztRQUN4QztJQUNKO0FBQ0oiLCJmaWxlIjoic3JjL2FwcC9sb2dpbi9sb2dpbi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiYm9keSB7XHJcbiAgICBjb2xvcjogIzAwMDtcclxuICAgIG92ZXJmbG93LXg6IGhpZGRlbjtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNCMEJFQzU7XHJcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gICAgbWFyZ2luOiAyMHB4O1xyXG59XHJcblxyXG4uY2FyZDAge1xyXG4gICAgYm94LXNoYWRvdzogMHB4IDRweCA4cHggMHB4ICM3NTc1NzU7XHJcbiAgICBib3JkZXItcmFkaXVzOiAwcHhcclxufVxyXG5cclxuLmNhcmQyIHtcclxuICAgIG1hcmdpbjogMHB4IDQwcHhcclxufVxyXG5cclxuLmxvZ28ge1xyXG4gICAgd2lkdGg6IDIwMHB4O1xyXG4gICAgaGVpZ2h0OiAxMDBweDtcclxuICAgIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgICBtYXJnaW4tbGVmdDogMzVweFxyXG59XHJcblxyXG4uaW1hZ2Uge1xyXG4gICAgd2lkdGg6IDM2MHB4O1xyXG4gICAgaGVpZ2h0OiAyODBweFxyXG59XHJcblxyXG4uYm9yZGVyLWxpbmUge1xyXG4gICAgYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgI0VFRUVFRVxyXG59XHJcblxyXG4uZmFjZWJvb2sge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzNiNTk5ODtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgcGFkZGluZy10b3A6IDVweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIHdpZHRoOiAzNXB4O1xyXG4gICAgaGVpZ2h0OiAzNXB4O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyXHJcbn1cclxuXHJcbi50d2l0dGVyIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMxREExRjI7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgIHBhZGRpbmctdG9wOiA1cHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICB3aWR0aDogMzVweDtcclxuICAgIGhlaWdodDogMzVweDtcclxuICAgIGN1cnNvcjogcG9pbnRlclxyXG59XHJcblxyXG4ubGlua2VkaW4ge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzI4NjdCMjtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgcGFkZGluZy10b3A6IDVweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIHdpZHRoOiAzNXB4O1xyXG4gICAgaGVpZ2h0OiAzNXB4O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyXHJcbn1cclxuXHJcbi5saW5lIHtcclxuICAgIGhlaWdodDogMXB4O1xyXG4gICAgd2lkdGg6IDQ1JTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNFMEUwRTA7XHJcbiAgICBtYXJnaW4tdG9wOiAxMHB4XHJcbn1cclxuXHJcbi5vciB7XHJcbiAgICB3aWR0aDogMTAlO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGRcclxufVxyXG5cclxuLnRleHQtc20ge1xyXG4gICAgZm9udC1zaXplOiAxNHB4ICFpbXBvcnRhbnRcclxufVxyXG5cclxuOjpwbGFjZWhvbGRlciB7XHJcbiAgICBjb2xvcjogI0JEQkRCRDtcclxuICAgIG9wYWNpdHk6IDE7XHJcbiAgICBmb250LXdlaWdodDogMzAwXHJcbn1cclxuXHJcbjotbXMtaW5wdXQtcGxhY2Vob2xkZXIge1xyXG4gICAgY29sb3I6ICNCREJEQkQ7XHJcbiAgICBmb250LXdlaWdodDogMzAwXHJcbn1cclxuXHJcbjo6LW1zLWlucHV0LXBsYWNlaG9sZGVyIHtcclxuICAgIGNvbG9yOiAjQkRCREJEO1xyXG4gICAgZm9udC13ZWlnaHQ6IDMwMFxyXG59XHJcblxyXG5pbnB1dCxcclxudGV4dGFyZWEge1xyXG4gICAgcGFkZGluZzogMTBweCAxMnB4IDEwcHggMTJweDtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkIGxpZ2h0Z3JleTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDJweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDVweDtcclxuICAgIG1hcmdpbi10b3A6IDJweDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgIGNvbG9yOiAjMkMzRTUwO1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgbGV0dGVyLXNwYWNpbmc6IDFweFxyXG59XHJcblxyXG5pbnB1dDpmb2N1cyxcclxudGV4dGFyZWE6Zm9jdXMge1xyXG4gICAgLW1vei1ib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XHJcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcclxuICAgIGJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICMzMDRGRkU7XHJcbiAgICBvdXRsaW5lLXdpZHRoOiAwXHJcbn1cclxuXHJcbmJ1dHRvbjpmb2N1cyB7XHJcbiAgICAtbW96LWJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcclxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xyXG4gICAgYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xyXG4gICAgb3V0bGluZS13aWR0aDogMFxyXG59XHJcblxyXG5hIHtcclxuICAgIGNvbG9yOiBpbmhlcml0O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyXHJcbn1cclxuXHJcbi5idG4tYmx1ZSB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMUEyMzdFO1xyXG4gICAgd2lkdGg6IDE1MHB4O1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbiAgICBib3JkZXItcmFkaXVzOiAycHhcclxufVxyXG5cclxuLmJ0bi1ibHVlOmhvdmVyIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMwMDA7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXJcclxufVxyXG5cclxuLmJnLWJsdWUge1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMUEyMzdFXHJcbn1cclxuXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MXB4KSB7XHJcbiAgICAubG9nbyB7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDBweFxyXG4gICAgfVxyXG5cclxuICAgIC5pbWFnZSB7XHJcbiAgICAgICAgd2lkdGg6IDMwMHB4O1xyXG4gICAgICAgIGhlaWdodDogMjIwcHhcclxuICAgIH1cclxuXHJcbiAgICAuYm9yZGVyLWxpbmUge1xyXG4gICAgICAgIGJvcmRlci1yaWdodDogbm9uZVxyXG4gICAgfVxyXG5cclxuICAgIC5jYXJkMiB7XHJcbiAgICAgICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNFRUVFRUUgIWltcG9ydGFudDtcclxuICAgICAgICBtYXJnaW46IDBweCAxNXB4XHJcbiAgICB9XHJcbn0iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](LoginComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-login',
                templateUrl: './login.component.html',
                styleUrls: ['./login.component.css']
            }]
    }], function () { return [{ type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }, { type: _services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] }, { type: _services_token_storage_service__WEBPACK_IMPORTED_MODULE_4__["TokenStorageService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/map-french/map-french.component.ts":
/*!****************************************************!*\
  !*** ./src/app/map-french/map-french.component.ts ***!
  \****************************************************/
/*! exports provided: MapFrenchComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapFrenchComponent", function() { return MapFrenchComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material/menu */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/menu.js");



class MapFrenchComponent {
    constructor() {
        this.cp = 0;
        this.message = " Info about the action";
    }
    ngOnInit() {
        this.map = document.getElementById('svgContent');
        this.paths = this.map.getElementsByTagName('path');
        for (var i = 0; i < this.paths.length; i++) {
            this.paths[i].addEventListener("click", function (e) {
                console.log("Dpt: " + e.target.getAttribute('data-name') + e.target.getAttribute('data-department') + e.target.getAttribute('d'));
                this.cp = e.target.getAttribute('data-department');
                console.log('cp', this.cp);
            });
        }
    }
    region() {
        let map = document.getElementById('map');
        let paths = map.getElementsByTagName('path');
    }
}
MapFrenchComponent.ɵfac = function MapFrenchComponent_Factory(t) { return new (t || MapFrenchComponent)(); };
MapFrenchComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: MapFrenchComponent, selectors: [["app-map-french"]], decls: 128, vars: 0, consts: [[1, "mapfrench_container"], [1, "mapfrench_wrapper"], ["id", "svgContent", "version", "1.1", "xmlns", "http://www.w3.org/2000/svg", 0, "xmlns", "xlink", "http://www.w3.org/1999/xlink", "x", "0px", "y", "0px", "viewBox", "0 0 700 590", 0, "xml", "space", "preserve", 1, "map_content"], ["data-name", "Guadeloupe", "data-department", "971", "data-code_insee", "01", 1, "region"], ["data-name", "Guadeloupe", "data-department", "971", "d", "M35.87,487.13l0.7,7.2l-4.5-1.1l-2,1.7l-5.8-0.6l-1.7-1.2l4.9,0.5l3.2-4.4L35.87,487.13z M104.87,553.63 l-4.4-1.8l-1.9,0.8l0.2,2.1l-1.9,0.3l-2.2,4.9l0.7,2.4l1.7,2.9l3.4,1.2l3.4-0.5l5.3-5l-0.4-2.5L104.87,553.63z M110.27,525.53 l-6.7-2.2l-2.4-4.2l-11.1-2.5l-2.7-5.7l-0.7-7.7l-6.2-4.7l-5.9,5.5l-0.8,2.9l1.2,4.5l3.1,1.2l-1,3.4l-2.6,1.2l-2.5,5.1l-1.9-0.2 l-1,1.9l-4.3-0.7l1.8-0.7l-3.5-3.7l-10.4-4.1l-3.4,1.6l-2.4,4.8l-0.5,3.5l3.1,9.7l0.6,12l6.3,9l0.6,2.7c3-1.2,6-2.5,9.1-3.7l5.9-6.9 l-0.4-8.7l-2.8-5.3l0.2-5.5l3.6,0.2l0.9-1.7l1.4,3.1l6.8,2l13.8-4.9L110.27,525.53z", 1, "departement"], ["data-name", "Martinique", "data-department", "972", "data-code_insee", "02", 1, "region"], ["data-name", "Martinique", "data-department", "972", "d", "m44.23,433.5l1.4-4.1l-6.2-7.5l0.3-5.8l4.8-4 l4.9-0.9l17,9.9l7,8.8l9.4-5.2l1.8,2.2l-2.8,0.8l0.7,2.6l-2.9,1l-2.2-2.4l-1.9,1.7l0.6,2.5l5.1,1.6l-5.3,4.9l1.6,2.3l4.5-1.5 l-0.8,5.6l3.7,0.2l7.6,19l-1.8,5.5l-4.1,5.1h-2.6l-2-3l3.7-5.7l-4.3,1.7l-2.5-2.5l-2.4,1.2l-6-2.8l-5.5,0.1l-5.4,3.5l-2.4-2.1 l0.2-2.7l-2-2l2.5-4.9l3.4-2.5l4.9,3.4l3.2-1.9l-4.4-4.7l0.2-2.4l-1.8,1.2l-7.2-1.1l-7.6-7L44.23,433.5z", 1, "departement"], ["data-name", "Guyane", "data-department", "973", "data-code_insee", "03", 1, "region"], ["data-name", "Guyane", "data-department", "973", "d", "m95.2,348.97l-11.7,16.4l0.3,2.4l-7.3,14.9 l-4.4,3.9l-2.6,1.3l-2.3-1.7l-4.4,0.8l0.7-1.8l-10.6-0.3l-4.3,0.8l-4.1,4.1l-9.1-4.4l6.6-11.8l0.3-6l4.2-10.8l-8.3-9.6l-2.7-8 l-0.6-11.4l3.8-7.5l5.9-5.4l1-4l4.2,0.5l-2.3-2l24.7,8.6l9.2,8.8l3.1,0.3l-0.7,1.2l6.1,4l1.4,4.1l-2.4,3.1l2.6-1.6l0.1-5.5l4,3.5 l2.4,7L95.2,348.97z", 1, "departement"], ["data-name", "La R\u00E9union", "data-department", "974", "data-code_insee", "04", 1, "region"], ["data-name", "La R\u00E9union", "data-department", "974", "d", "m41.33,265.3l-6.7-8.5l1.3-6l4.1-2.4l0.7-7.9 l3.3,0.4l7.6-6.1l5.7-0.8l21,4l5,5.3v4.1l7.3,10.1l6.7,4.5l1,3.6l-3.3,7.9l0.9,9.6l-3.4,3.5l-17.3,2.9l-19.6-6.5l-3.8-3.6l-4.7-1.2 l-0.9-2.5l-3.6-2.3L41.33,265.3z", 1, "departement"], ["data-name", "Mayotte", "data-department", "976", "data-code_insee", "06", 1, "region"], ["data-name", "Mayotte", "data-department", "976", "d", "m57.79,157.13l11.32,5.82l-3.24,7.46l-5.66,7.52l5.66,8.37l-4.04,5.7l-5.66,8.01l5.66,4.37l-7.28,4.37l-8.09-2.73l-4.04-5.04v-4.85l-3.24-6.55l7.28,3.88l4.04,1.13v-7.14l-4.85-8.43v-14.8l-8.09-2.61l-3.24-2.67v-5.76l8.9-6.79l7.28,10.19L57.79,157.13z M78.07,164.38l-5.56,3.42l4.81,5.59l3.93-4.79L78.07,164.38z", 1, "departement"], ["data-name", "Ile-de-France", "data-code_insee", "11", 1, "region"], ["data-name", "Paris", "data-department", "75", "d", "M641.8,78.3l-0.2,3.8l-1,2.6l-8.3-1.7l-6-0.6l-5.2,3h-4l-2.5-0.3l-0.4-0.1l-13.5-5l-3-3.8l-4.3-1.9l-0.5-0.2 l0.4-1.9l1.3-3.1l2.7-2.1l2.9-1.1l3.9,0.5h0.1l0.9-2.2l7.1-4.6l14-0.1l1.8,3.6l1.8,2.4l0.6,0.9l0.1,0.4L631,68l0.4,5.4l0.4,1.8v0.1 l-0.3,0.8l0.1,3.6l0.6-0.5l1.6-1.6l2-0.5l2-0.5L641.8,78.3z M396.8,154.7l-3.2-0.5l-2.5,1.7l3,3.5l5.3-0.1l-1.8-1.9L396.8,154.7z", 1, "departement"], ["data-name", "Seine-et-Marne", "data-department", "77", "d", "m441.1,176.1l-2.9,0.8l0.4,8.5l-15.4,3 l-0.2,5.8l-3.9,5.4l-11.2,2.7l-9.2-0.7l2.6-1.5l0.6-2.7l-4.2-4.3L397,190l3.4-4.8l4-17.2l-0.5-1l1.1-4.1l-0.3-2.9v-0.1l-1.3-4.7 l1.3-2.5l-1.7-5.1l0.1-0.1l1.7-2.3l-0.2-2l6.9,1l2-2.2l2.5,1.6l8.1-2.9l2.6,0.7l1.8,2.5l-0.7,2.8l3.9,4.2l9.3,6l-0.4,2l-2.6,2.2 l3.5,8.3l2.6,1.7L441.1,176.1z", 1, "departement"], ["data-name", "Yvelines", "data-department", "78", "d", "m364.1,158.1l-3.6-6.6l-1.8-5.8l2.3-2.6 l3.8,0.1l9.5,0.8l9,3.6l5.5,6.1l-2,3.1l3.2,5.2l-7.1,5.4l-1.6,2.6l0.7,2.9l-4.6,8.6l-3.1,0.7L372,180l-1.2-5.6l-6.2-5.4L364.1,158.1z", 1, "departement"], ["data-name", "Essonne", "data-department", "91", "d", "m401.6,164.8l2.3,2.2l0.5,1l-4,17.2L397,190 l-3.7-0.6l-2.8,1.8l-1.5-2.7l-1.9,2.9l-6.9,0.7l-2.8-10.6l4.6-8.6l-0.7-2.9l1.6-2.6l7.1-5.4v-0.1l3.7,1.6l5.1,2.1L401.6,164.8z", 1, "departement"], ["data-name", "Hauts-de-Seine", "data-department", "92", "d", "M391.1,155.9l3,3.5l-0.4,4.1l-3.7-1.6v0.1l-3.2-5.2l2-3.1l3.6-2.6l1.3,2l-0.1,1.1L391.1,155.9z M612.6,54.1 l1.6-0.7l0.7-1.9l0.5-1.8l-0.1-1.1l-0.2-1.4l-4.6-1.9l-4.6-0.9l-4,1.3l-7.6,5.6l-6.1,5.8l-5.3,3l-1,1l-3.75,7.4l1.79,7.17 l-0.06,0.07l0.01,0.06l-2.74,3.23l0.68,2.44l2.5,4.8l3.3-0.5l1,5.2l3.9-0.3l1.4,3.5l3.4,1.6l0.5,2.1l5.3,4.2l4.3,1.3l-0.1,4.9 l5.7,3.5l3.15-5.91l-0.7-5.46l0.72-1.2l0.4-1.3l0.7-2.1l-1.4-1.9l0.3-1.2l0.8-2.8l-1-2.6l0.5-0.3l0.5-0.3l0.9-0.5l0.7-1.1l-0.4-0.1 l-13.5-5l-3-3.8l-4.3-1.9l-0.5-0.2l0.3-1.9l1.4-3.1l2.7-2.1l2.8-1.1h0.1l3.9,0.5l0.9-2.2l7.2-4.6l-0.7-2l-0.6-2l1.4-0.7L612.6,54.1z", 1, "departement"], ["data-name", "Seine-Saint-Denis", "data-department", "93", "d", "M404.7,152.7l-1.3,2.5l1.3,4.7v0.1l-7.1-2.6l-0.8-2.7l-3.2-0.5l0.1-1.1l-1.3-2l3.3-1.3l2.6,1.1 c1.6-1.1,3.2-2.2,4.7-3.3L404.7,152.7z M663.2,73.89l0.06-0.08l-0.02-0.04l2.61-3.38l-3.95-0.3l-1.6-5.9l0.06-0.06l-0.02-0.06 l6.36-6.56l0.1-5.42l1.1-4l-1.2-3.4l-5.1-8l0.07-0.08l-0.03-0.04l2.65-3.33l-0.89-4.04l-4.5-2.9l-4.1,1.7l-6.4,8.8l-8.2,6.2 l-0.7-0.2l-7.8-1.1l-1.9,1l-5.1-4.6l-1.3-0.2l-1.9-0.7l-5.1,3l-1.6,2.7l-1-1.2l-5.9-2.1l-1.96,2.25v0.2l0.66,2.45l3.9,0.8l4.7,1.9 l0.1,1.4l0.1,1.1l-0.2,0.9l-0.3,0.9l-0.7,1.9l-1.6,0.7l-0.3,0.8l-1.4,0.7l0.6,2l0.7,2l13.9-0.2l0.1,0.1l1.8,3.6l1.8,2.4l0.6,0.8 l0.1,0.5L631,68l0.4,5.4l0.4,1.8l5.9-0.5l0.5-0.3c0.1,0,0.1,0,0.2,0l6.3-2.8l2.9,0.4l0.7,1.3l3,1.5l4,2.9c0,0.1,0.1,0.2,0.2,0.2 l0.7,0.5l6,6.2l0.8,0.6c0.1,0,0.2,0.1,0.3,0.1l3.6,2.6l0.04-0.13l0.43-1.3l0.23-0.68l-1.8-6L663.2,73.89z", 1, "departement"], ["data-name", "Val-de-Marne", "data-department", "94", "d", "M404.7,160l0.3,2.9l-1.1,4.1l-2.3-2.2l-2.8,0.8l-5.1-2.1l0.4-4.1l5.3-0.1l-1.8-1.9L404.7,160z M668.09,102.2 h0.06l-0.02-0.12l3.31-0.19l-1.55-3.58l-3.69-2.41l0.8-8h-0.1l-3.6-2.6c-0.1,0-0.2-0.1-0.3-0.1l-0.8-0.6l-6-6.2l-0.7-0.5 c-0.1,0-0.2-0.1-0.2-0.2l-4-2.9l-3-1.5l-0.7-1.3l-2.9-0.4l-6.3,2.8c-0.1,0-0.1,0-0.2,0l-0.5,0.3l-5.9,0.5v0.1l-0.3,0.8l0.1,3.6 l0.6-0.5l1.6-1.7l2-0.4l2-0.5l4,1.7l-0.2,3.8l-1,2.6l-8.3-1.7l-6-0.6l-5.2,3h-4l-2.5-0.3l-0.6,1.1h-0.1l-0.9,0.5l-0.5,0.3l-0.5,0.3 l1,2.5v0.1l-0.8,2.8l-0.3,1.2l1.4,1.9l-0.7,2.1l-0.4,1.3l-0.7,1.2l0.78,5.38h0.06l2.1,0.2l4.7,2.8l3.1-2.2l0.1,5.5l3.3,2.4l4.9-1.8 l0.7,2.5l5.2-2.3l0.5,1.3l1.7,1.7l4.6-3.6l2.1-0.5l5.2-1.8l1.9,6.8l1.7,2.5l3.3,1.8l5.44,1.88l-0.68-5.05l0.05-0.08l-0.01-0.04 l2.5-4.2l2.73-2.74l-1.38-3.64l0.07-0.06l-0.03-0.07l2.35-1.96L668.09,102.2z", 1, "departement"], ["data-name", "Val-d\u2019Oise", "data-department", "95", "d", "m374.3,144l-9.5-0.8l4-9.5l1.6,3.2l5.6,1.1 l6.3-1.8l9.2,2.2l2.2-1.6l10.9,6.4l0.2,2l-1.7,2.3l-0.1,0.1c-1.5,1.1-3.1,2.2-4.7,3.3l-2.6-1.1l-3.3,1.3l-3.6,2.6l-5.5-6.1 L374.3,144z", 1, "departement"], ["data-name", "Centre-Val de Loire", "data-code_insee", "24", 1, "region"], ["data-name", "Cher", "data-department", "18", "d", "m385.3,235.4l5-2.4l13.5,3.1l3.9,4.8l9-1.7l2,6.5l-1.7,5.8l2.7,2.1 l3.1,7.6l0.3,5.9l2.2,2l-0.2,5.8l-1.3,8.9h-0.1h-4l-4.8,3.7l-8.4,2.9l-2.3,1.9l1.7,5.3l-1.7,2.4l-8.7,1l-3.5,5.9v0.1l-4.9-0.2 l1.5-3.5l-0.9-8.9l-4.7-7.9l1.4-2.7l-2.3-2.2l2.5-5.1l-2.3-11.7l-11.6-1.6l2.8-5.5l2.8,0.1l0.6-2.8l9.7-2l-2.1-5.9l5.9-4.1 L385.3,235.4z", 1, "departement"], ["data-name", "Eure-et-Loir", "data-department", "28", "d", "m333.1,200.9l-2.1-3.8l-1.1-7.5l7.5-5.1 l-0.5-4.6l0.2-4.5l-4.8-4.4l-0.1-3.2l2.4-2.6l6-1.1l5.3-3.2l2.8,1.6l6-1.3l-0.2-2.8l6-6.9l3.6,6.6l0.5,10.9l6.2,5.4l1.2,5.6l2.3,2.2 l3.1-0.7l2.8,10.6l-0.5,1.5l-4.8,10.8l-8.5,0.6l-6,2.8l0.2,2.8l-3.3-1.9l-5.5,3.5L339,201.4l-6.3,1.3L333.1,200.9z", 1, "departement"], ["data-name", "Indre", "data-department", "36", "d", "m357.8,308.5l-2.8,2.9l-1.7-2.5l-5.8,1.1 l-2.6-1.1l1.5-2.8l-2.5-1.3l-2.6-5.4h-2.9l-4.6-4.4l0.8-5.8l-2.1-3l5.6-0.5l-1-2.7l3.3-11.9l5.1-2.7l2.3,1.7l2.6-3.5l2.5-2.1l-1-4.9 l6-3.2l2.5,1.3l1.5-2.6l6.4-0.9l5.2,3.5l-2.8,5.5l11.6,1.6l2.3,11.7l-2.5,5.1l2.3,2.2l-1.4,2.7l4.7,7.9l0.9,8.9l-1.5,3.5l-2.7,0.8 l-13.2-2.7l-1.9,2.5L357.8,308.5z", 1, "departement"], ["data-name", "Indre-et-Loire", "data-department", "37", "d", "m303.9,263l-5.5-3.2v-0.1l5.8-15.3l1.7-9.3 l0.7-2.4l6.1,2.6l-0.5-3.3l2.8,0.3l7.7-4.5l10.5,0.5l-0.2,5.5l2.2-1.8l6,3.4l-0.7,2.7l3.4,5.1l-1.2,9.1l2.4,1.9l2.6-1.3l4.2,6.7 l1,4.9l-2.5,2.1l-2.6,3.5l-2.3-1.7l-5.1,2.7l-3.3,11.9l1,2.7l-5.6,0.5l-7.1-10l-0.3-3.1l-5.3-3l1.4,2.9l-10,0.4l-2.8-1.4l-1.3-6.1 l-2.9,0.3L303.9,263z", 1, "departement"], ["data-name", "Loir-et-Cher", "data-department", "41", "d", "m357.9,256.4l-6,3.2l-4.2-6.7l-2.6,1.3 l-2.4-1.9l1.2-9.1l-3.4-5.1l0.7-2.7l-6-3.4l-2.2,1.8l0.2-5.5l-10.5-0.5l0.6-3.5l3.2-1.1l6.3-10.6l-0.4-5.5l-1.7-2.2l2-2.1v-0.1 l6.3-1.3l12.8,10.8l5.5-3.5l3.3,1.9l2.5,7.1l-1.8,3.2l1.7,5.6l3-1.3l2.4,1.5l1.1,3.8l2.9,0.6l1.9-2.3l15.2,1.6l0.8,2.6l-5,2.4 l5.1,7.6l-5.9,4.1l2.1,5.9l-9.7,2l-0.6,2.8l-2.8-0.1l-5.2-3.5l-6.4,0.9l-1.5,2.6L357.9,256.4z", 1, "departement"], ["data-name", "Loiret", "data-department", "45", "d", "m393.3,189.4l3.7,0.6l0.7,3.1l4.2,4.3l-0.6,2.7 l-2.6,1.5l9.2,0.7l11.2-2.7l6.7,7.5l0.4,5.8l-4.6,4.9l1.1,2.9l-1.6,2.4l-5.3,3.3l3,2.8l2.2,6.9l-2.8,0.7l-1.5,2.4l-9,1.7l-3.9-4.8 l-13.5-3.1l-0.8-2.6l-15.2-1.6l-1.9,2.3l-2.9-0.6l-1.1-3.8l-2.4-1.5l-3,1.3l-1.7-5.6l1.8-3.2l-2.5-7.1l-0.2-2.8l6-2.8l8.5-0.6 l4.8-10.8l0.5-1.5l6.9-0.7l1.9-2.9l1.5,2.7L393.3,189.4z", 1, "departement"], ["data-name", "Bourgogne-Franche-Comt\u00E9", "data-code_insee", "27", 1, "region"], ["data-name", "Cote-d\u2019Or", "data-department", "21", "d", "m523.6,241.7l3.9,8.2l-1.2,1.3l-1.8,8.2 l-6.2,6.8l-1.1,4.1v-0.1l-15,1.5l-8.8,4.2l-5.6-6.3l-5.5-1.9l-1.3-2.6l-5.7-1.7l-2.4-2.6V260l0.4-3.2l-3.7-1.2l-1.3-6h0.1l-1.3-2.7 l1.3-8.1l6.7-10.4l-1.7-2.3l2.8-2.1l0.3-3.7l-3.1-3.9l1.9-3.1l2.2-2l6.1-0.9l4.7-3.9l3.9,0.5l3.5,0.7l0.5,2.7l2.6,1l-0.3,2.9 l2.9,0.3l1.8,2.2l1,3.1l-2.8,2.4l2.3,4.8l9.2,2l3,1.6v2.8l4.8-1.9h0.1l2.7-1.6l2,3l0.1,3.2l-4.6,4.1L523.6,241.7z", 1, "departement"], ["data-name", "Doubs", "data-department", "25", "d", "m590.1,245.2l-2.4,2.2l0.4,3l-4.8,6.2l-4.8,4 l-0.4,2.9l-2.5,2.7l-5.7,1.7l-0.3,0.3l-1.7,2.3l0.9,2.7l-0.7,4.5l0.5,2.5l-9.5,8.8l-2.9,5.2l-0.22,0.69l-3.68-3.49l3.6-7.4l2.1-2.3 l-4.2-4.1l-2.9-0.5l-5.8-10.1l-3,0.8l-1.5-2.5l-2,2.1l-1.2-2.5l3-5.1l-5.2-7.8l22.3-10.2l3-4.7l5.6-1.9l2.8,0.9l1.8-2.2l3.2-0.4 l0.5-2.8l5.9,0.8l0.2-0.1h0.1l5.9,2.7l-1.4,2.5l1.4,2.4l0.41-0.46l-0.11,0.16l-2.2,4.9l7-0.7L590.1,245.2z", 1, "departement"], ["data-name", "Jura", "data-department", "39", "d", "m552.3,291.4l3.68,3.49L553.4,303l-5.3,7.2 l-5.5,3.2l-3.8,0.2l-0.4-2.8l-3.4-1.6l-4,4.4l-2.9,0.1l-0.1-3h-2.9l-4.3-7.7l2.8-1.1l-0.8-5.3l2.8-5l-2.2-8.7l-2.5-1.6l5-3.7 l-8.3-4.4l-0.4-2.9l1.1-4.1l6.2-6.8l1.8-8.2l1.2-1.3l2.3,2l5.4,0.1l5.2,7.8l-3,5.1l1.2,2.5l2-2.1l1.5,2.5l3-0.8l5.8,10.1l2.9,0.5 l4.2,4.1l-2.1,2.3L552.3,291.4z", 1, "departement"], ["data-name", "Ni\u00E8vre", "data-department", "58", "d", "m462.8,250l5.5-0.4l1.3,6l3.7,1.2l-0.4,3.2v0.8 l-1.1,0.3l-2.7,0.4v1.3l-2.8,1l0.3,5.9l-2.1,1.7l4,7l-1.9,2.1l0.7,2.9l-11.3,5.7l-7-2.8l-5.9,6l-4.4-3.7l-2.8,1.7l-6.4-0.2l-5.7-6.3 l1.3-8.9l0.2-5.8l-2.2-2l-0.3-5.9l-3.1-7.6l-2.7-2.1l1.7-5.8l-2-6.5l1.5-2.4l2.8-0.7v0.1h3.4l7.4,4.8h6l4.6-4.3l3.9,5.6l5.5,3 l5.8-0.9l0.9,3.7l2.8-0.9L462.8,250z", 1, "departement"], ["data-name", "Haute-Saone", "data-department", "70", "d", "m579.1,225.9l1.4,5.5l-0.2,0.1l-5.9-0.8 l-0.5,2.8l-3.2,0.4l-1.8,2.2l-2.8-0.9l-5.6,1.9l-3,4.7L535.2,252l-5.4-0.1l-2.3-2l-3.9-8.2l-2.6-1.4l4.6-4.1l-0.1-3.2l-2-3l-2.7,1.6 h-0.1l1.2-2.5l6.6-3.9l2.1,1.8l3.2-1l0.3-8.3l2-2.4l2.9,0.3l2.3-3.2l-0.2-1.4l8-5.8l7,4.3l5.8-1.6l4.9,3.6l5.1-2.2l8.4,6.6l-2.3,5.7 L579.1,225.9z", 1, "departement"], ["data-name", "Saone-et-Loire", "data-department", "71", "d", "m517.2,270.2v0.1l0.4,2.9l8.3,4.4l-5,3.7 l2.5,1.6l2.2,8.7l-2.8,5l0.8,5.3l-2.8,1.1l-4.8-3.3l-5.4,1.3l-5.9-1.5l-5.9,20.9l-5.7-7.7l-1.6,2.3l-2.5-1.5l-2.2,1.6l-2.2-1.7 l-2.3,1.9l-0.29,2.91L482,318.2v0.1l-5.7,3.8l-2.1-2.1l-8,1.5l-5.2-3.3v-3l3.7-4.6l0.5-5.5l-1.6-2.4l-7.9-2.9l-6.7-13.5l7,2.8 l11.3-5.7l-0.7-2.9l1.9-2.1l-4-7l2.1-1.7l-0.3-5.9l2.8-1l2.7-1.7l1.1-0.3l2.4,2.6l5.7,1.7l1.3,2.6l5.5,1.9l5.6,6.3l8.8-4.2 L517.2,270.2z", 1, "departement"], ["data-name", "Yonne", "data-department", "89", "d", "m425.8,207.1l-6.7-7.5l3.9-5.4l0.2-5.8l15.4-3 l3.6,1.5l4.5,5.5l2.5,8.3l2-2.2l3.6,4.1l5,10.9l12.6-1.6l2.9,1.4l-1.9,3.1l3.1,3.9l-0.3,3.7l-2.8,2.1l1.7,2.3l-6.7,10.4l-1.3,8.1 l1.3,2.7h-0.1l-5.5,0.4l-1.5-2.8l-2.8,0.9l-0.9-3.7l-5.8,0.9l-5.5-3l-3.9-5.6l-4.6,4.3h-6l-7.4-4.8H421v-0.1l-2.2-6.9l-3-2.8 l5.3-3.3l1.6-2.4l-1.1-2.9l4.6-4.9L425.8,207.1z", 1, "departement"], ["data-name", "Territoire de Belfort", "data-department", "90", "d", "m580.3,215.9l0.9-0.6l7.6,5l0.5,9l2.8-0.2l2,5 l-0.1,0.1l-2.79,0.39l-1.11-0.39l-3.19,4.34L586.5,239l-1.4-2.4l1.4-2.5l-5.9-2.7h-0.1l-1.4-5.5l-1.1-4.3L580.3,215.9z", 1, "departement"], ["data-name", "Normandie", "data-code_insee", "28", 1, "region"], ["data-name", "Calvados", "data-department", "14", "d", "m316.9,148l-0.7,2.2l-5.6-1l-7,1.7l-7.2,5.4 l-2.9,0.3l-5.7-1.1l-2.6,1.7l-4.9-3l-6.4,2.3l-2.7-1.3l-0.9,2.7l-5.4,2.9l-9.7-2.1l-1.8-2.4l4.5-5.3l-1.6-2.3l8.1-4.9l-2.2-8.2 l2-2.6l-8.4-3.1l-0.5-6.6v-0.1l0.1-0.7l1.8,0.8l1.9-2.1l3.4-0.3l9.4,3.3l13.9,1.5l6.9,3.4l5.7-0.7l4.7-2.5l4.1-3.7l5.1-1.1l0.3,8.3 h2.9l-2.3,2.1l2.8,9.4l-1.4,3L316.9,148z", 1, "departement"], ["data-name", "Eure", "data-department", "27", "d", "m316.4,153.4l-0.2-3.2l0.7-2.2l-2.3-4.1l1.4-3l-2.8-9.4l2.3-2.1h-2.9 l-0.3-8.3l1.7-0.4l0.28-0.1h1.52l-0.9-0.2l0.8-0.3l-1.29-0.3l5.89-2.4l7.6,5l3.4-0.7l4.9,3l-1.9,2.4l2.1,2.1l5.4,2.4l1.4-2.7 l8.2-2.5l4.8-7l13.1,3.3l3.5,8.4l-4,2.6l-4,9.5l-3.8-0.1l-2.3,2.6l1.8,5.8l-6,6.9l0.2,2.8l-6,1.3l-2.8-1.6l-5.3,3.2l-6,1.1l-2.4,2.6 l-3.4-2.1l1.7-2.3l-7.8-9.5L316.4,153.4z", 1, "departement"], ["data-name", "Manche", "data-department", "50", "d", "m255.2,158.7l9.7,2.1l4.1,4.2l-1.8,6.7 l-3.6,4.5h-0.1l-8.6-0.8l-5.4-2.3l-7.1,4.8l-2.7-1l-4.7-9.6l1.9-0.2l4.8,0.4l2.5-1.1l0.5-2.2l-2.4,1.3l-5.1-5.6l-0.3-5.3l2-6.1 l-0.3-4.9l-1.8-3.6l0.4-7.4l1.5-2l-2.5,0.3l-2-5l0.3-2.2l-2.4-1.2l-2.9-4.1l-0.7-5.9l-1.4-1.9l1.8-1.8l0.1-2.8l-0.5-2.3l-2.2-1.1 l-1-2.5l2.1-0.2l11.9,4.2h2.4l4-2.6l5.1,0.6l1.8,1.7l0.9,2.7l-3.2,5.2l4,6.5l1.1,4.3l-0.1,0.7v0.1l0.5,6.6l8.4,3.1l-2,2.6l2.2,8.2 l-8.1,4.9l1.6,2.3l-4.5,5.3L255.2,158.7z", 1, "departement"], ["data-name", "Orne", "data-department", "61", "d", "m266.9,179.9l-3.3-3.7l3.6-4.5l1.8-6.7 l-4.1-4.2l5.4-2.9l0.9-2.7l2.7,1.3l6.4-2.3l4.9,3l2.6-1.7l5.7,1.1l2.9-0.3l7.2-5.4l7-1.7l5.6,1l0.2,3.2l6.3,0.5l7.8,9.5l-1.7,2.3 l3.4,2.1l0.1,3.2l4.8,4.4l-0.2,4.5l0.5,4.6l-7.5,5.1l1.1,7.5l-3.2-0.7l-3.1-3.5l-2.9,1l-7.2-5l-1.6-8.4l-2.8-1.5l-11,5.9l-3-0.1 v-0.1v-2.9l-3.3-1.6l-1.9-6l-2.7-0.2l-0.7,2.7h-9.1l-6.7,3.3l-2.5-1.7L266.9,179.9z", 1, "departement"], ["data-name", "Seine-Maritime", "data-department", "76", "d", "m314.41,119.8l-7.61-1.8l-1.2-2l-0.1-2.3 l4.4-9.7l13.8-7.4L326,95l10.3-2.1l4.8-1.8l2.4,0.3L352,87l5.11-4.09l11.79,9.99l3.4,8.4l-3.1,4.7l1.4,8.7l-1.3,8l-13.1-3.3l-4.8,7 l-8.2,2.5l-1.4,2.7l-5.4-2.4l-2.1-2.1l1.9-2.4l-4.9-3l-3.4,0.7l-7.6-5L314.41,119.8z", 1, "departement"], ["data-name", "Hauts-de-France", "data-code_insee", "32", 1, "region"], ["data-name", "Aisne", "data-department", "02", "d", "m450.3,82.6l16.7,4.6l2.91,0.94L470.6,94l-1.3,3.5l1.3,3.1l-5,7.2 l-2.7,0.3l0.3,14.3l-1,2.8l-5.3-1.8l-8,4l-1.2,2.6l3.2,8l-5.5,2.3l1.6,2.4l-0.8,2.7l2.5,1.3l-7.7,10.2l-9.3-6l-3.9-4.2l0.7-2.8 l-1.8-2.5l-2.6-0.7l2.1-1.7l-0.5-2.8l-2.9-1.1l-2.4,1.5l-0.7-2.9l3,0.2l-2.9-4.5l2.6-1.7l2.4-5.7l2.6-1.1l-2.2-1.8l0.8-4.5 l-0.4-10.2l-2.3-7l3.9-8.1l0.4-3.8l12.6-0.6l2.6-2.2l2.3,1.7L450.3,82.6z", 1, "departement"], ["data-name", "Nord", "data-department", "59", "d", "m384.33,25.06l0.87-0.26l2,0.8l1.1-2.1l7.9-2.1 l2.9,0.3l4.4-1.9v-0.1l1.2,4.8l2.3,3.7l-1.6,1.9l0.6,0.8l1.2,5.8h3.4l2.7,5.1l3.1,1.5h2.1l0.6-2.4l8.1-3l3.8,7.5l0.1,1l1.3,5.2 l2,3.5h0.1l2.8,0.6l2.1-1.4l2.4-0.2l-0.5,2.2l2.2-0.7l2.8,1l1.8,4.4l-0.6,2.3l0.7,2.3l1.4,1.9l1.1-2.6l4.6-0.3l2.4,1.1L462,64l5.5,6 l2.3,0.2l-2.1,2.4l-1.4,4.7l2.6,0.2l1.4,3.3l-3.5,3.9l0.2,2.5l-16.7-4.6l-5.2,1.8l-2.3-1.7l-2.6,2.2l-12.6,0.6l-3.3-2.6l3.5-10.6 l-1.8-2.4l-3-0.4l0.7-2.7l-3.9-5.2l3.1-1.6l-3.8-5.3l-5.9-1l1-6.1l-1.3-2.5l-1.7,2.2l-11.6-0.5l-4.1-4.2l0.6-2.8l-5.5-2.6 L384.33,25.06z", 1, "departement"], ["data-name", "Oise", "data-department", "60", "d", "m372.8,131.1l-3.5-8.4l1.3-8l-1.4-8.7l3.1-4.7 l4.1,3.7l3.1-1.2l14.4,2.2l12.8,6.7l8.6-6.8l10.3-1.5l0.4,10.2l-0.8,4.5l2.2,1.8l-2.6,1.1l-2.4,5.7l-2.6,1.7l2.9,4.5l-3-0.2l0.7,2.9 l2.4-1.5l2.9,1.1l0.5,2.8l-2.1,1.7l-8.1,2.9l-2.5-1.6l-2,2.2l-6.9-1l-10.9-6.4l-2.2,1.6l-9.2-2.2L376,138l-5.6-1.1l-1.6-3.2 L372.8,131.1z", 1, "departement"], ["data-name", "Pas-de-Calais", "data-department", "62", "d", "m379.8,68.9l7.1,5.8l12-2.5l-2.6,5.7L398,81 l2.5-3.1l8.4,3.5l0.8-2.8l2.8,4.6l2.4-1.7l0.8,3.2l8.6-1.8l3.5-10.6l-1.8-2.4l-3-0.4l0.7-2.7l-3.9-5.2l3.1-1.6l-3.8-5.3l-5.9-1 l1-6.1l-1.3-2.5l-1.7,2.2l-11.6-0.5l-4.1-4.2l0.6-2.8l-5.5-2.6l-6.27-12.14L372.6,28.5l-6.4,5.4l0.9,5.6l-1.7,4.6l0.6,6.7l2,4.2 l-1.7-1.4l-0.3,9.7l2.27,1.58l10.53,1.02L379.8,68.9z", 1, "departement"], ["data-name", "Somme", "data-department", "80", "d", "m424.3,82.9l3.3,2.6l-0.4,3.8l-3.9,8.1l2.3,7 l-10.3,1.5l-8.6,6.8l-12.8-6.7l-14.4-2.2l-3.1,1.2l-4.1-3.7l-3.4-8.4l-11.79-9.99L359.5,81l3.4-6.6l1.9-1.1l0.1-0.1l1.4,1.8l3.5,0.3 l-5.6-6l1.2-5.1l2.9,0.7l-0.03-0.02l10.53,1.02l1,3l7.1,5.8l12-2.5l-2.6,5.7L398,81l2.5-3.1l8.4,3.5l0.8-2.8l2.8,4.6l2.4-1.7 l0.8,3.2L424.3,82.9z", 1, "departement"], ["data-name", "Grand Est", "data-code_insee", "44", 1, "region"], ["data-name", "Ardennes", "data-department", "08", "d", "m469.91,88.14l0.79,0.26l9.8,0.4l7.3-3.2l1.1-6 l4-3.8l2.8-0.2v3.8L494,81l-0.6,5.2l3.3,4.5l-1,2.4l0.6,3.1l1.4,1.9l3.3-0.9l4.3,2.4l2.8,3.8l4.9,0.6l2,1.7l-0.9,2.4l2.1-0.13 l-1.6,1.13l-2,2.7l-5.7-2.1l-1.9,2l0.8,8.8l-3.2,5.1l1.4,2.5l-4.2,3.6v0.1l-20.1-1.9l-9.8-6.6l-6.7-0.9l-0.3-14.3l2.7-0.3l5-7.2 l-1.3-3.1l1.3-3.5L469.91,88.14z", 1, "departement"], ["data-name", "Aube", "data-department", "10", "d", "m442.2,186.9l-3.6-1.5l-0.4-8.5l2.9-0.8l3-5 l3.2,4.5l9,1.2v-3.3l9.5-7.6l6.5-0.9l3.1,0.5l0.4,6.1l2.6,2c1.9,0.8,3.8,1.5,5.6,2.3l2.5-1.5l3.3,1.1l-0.6,3.4l2.4,5.2l5.6,3 l0.5,9.9l-0.1,2.7l-5.6,2.5l0.2,4.8l-3.9-0.5l-4.7,3.9l-6.1,0.9l-2.2,2l-2.9-1.4l-12.6,1.6l-5-10.9l-3.6-4.1l-2,2.2l-2.5-8.3 L442.2,186.9z", 1, "departement"], ["data-name", "Marne", "data-department", "51", "d", "m440.6,158.9l0.4-2l7.7-10.2l-2.5-1.3l0.8-2.7 l-1.6-2.4l5.5-2.3l-3.2-8l1.2-2.6l8-4l5.3,1.8l1-2.8l6.7,0.9l9.8,6.6l20.1,1.9l2.2,9l-1,4.1l2.6,1.3l-0.6,3.9l-3.1,1.1l-1.1,5.8 l3.2,4.6l0.5,4.1l-8.6,2.2l2.2,2.5l-2.3,2.2l0.7,2.9h-4.7l-3.3-1.1l-2.5,1.5c-1.8-0.8-3.7-1.5-5.6-2.3l-2.6-2l-0.4-6.1l-3.1-0.5 l-6.5,0.9l-9.5,7.6v3.3l-9-1.2l-3.2-4.5l-2.6-1.7l-3.5-8.3L440.6,158.9z", 1, "departement"], ["data-name", "Haute-Marne", "data-department", "52", "d", "m493.9,167.9l8.6-2.2l3.4,5.2l16.9,10.4 l-2.4,2.3l12.7,9.5l-1.7,8.6l5.5,4.7l0.2,3.1l2.7-1.1l1.3,2.5v0.1l0.2,1.4l-2.3,3.2l-2.9-0.3l-2,2.4l-0.3,8.3l-3.2,1l-2.1-1.8 l-6.6,3.9l-1.2,2.5l-4.8,1.9v-2.8l-3-1.6l-9.2-2l-2.3-4.8l2.8-2.4l-1-3.1l-1.8-2.2l-2.9-0.3l0.3-2.9l-2.6-1l-0.5-2.7l-3.5-0.7 l-0.2-4.8l5.6-2.5l0.1-2.7l-0.5-9.9l-5.6-3l-2.4-5.2l0.6-3.4h4.7l-0.7-2.9l2.3-2.2L493.9,167.9z", 1, "departement"], ["data-name", "Meurthe-et-Moselle", "data-department", "54", "d", "m588.2,170.9l1.9,1.3l-1.5,0.4l-10.6,7.6l-6.1-1.6l-1.6-2.7l-5.3,3.8 l-6,1l-2.4-1.8l-5.4,2l-1.1,2.8l-5.7,0.7l-4.1-4.8l0.1-2.9l-5.8-0.6l0.2-2.9l-2.5-2l1.7-2.8l-1.3-8.6l2.2-13.8l0.9-2.7l-4.9-11.5 l1.5-5.9l-1.2-2.7l-4.4-4.8l-5.3,2l-0.7-5.3l4.8-1.7l2-1.9h6.8l2.54,2.31L539.6,124l2.5,1.6l1.2,3.6l-1.7,3.1l1,5.6l-2.8,0.1 l4.3,7.5l11.5,4l-0.3,2.9l2.7,5.1l8.5,1.5l5.3,3.9l14.4,5.3L588.2,170.9z", 1, "departement"], ["data-name", "Meuse", "data-department", "55", "d", "m516.2,107.97l1.2-0.07l1.5,1.6l1.9,5.6 l0.7,5.3l5.3-2l4.4,4.8l1.2,2.7l-1.5,5.9l4.9,11.5l-0.9,2.7l-2.2,13.8l1.3,8.6l-1.7,2.8l2.5,2l-0.2,2.9l-1.9,2.3l-3-0.5l-6.9,3.4 l-16.9-10.4l-3.4-5.2l-0.5-4.1l-3.2-4.6l1.1-5.8l3.1-1.1l0.6-3.9l-2.6-1.3l1-4.1l-2.2-9v-0.1l4.2-3.6l-1.4-2.5l3.2-5.1l-0.8-8.8 l1.9-2l5.7,2.1l2-2.7L516.2,107.97z", 1, "departement"], ["data-name", "Moselle", "data-department", "57", "d", "m539.6,124l-2.65-10.19l0.65,0.59h2.4l1.5,2.1 l2.3,0.7l2.3-0.5l1-2.3l2-1.2l2.2-0.2l4.5,2.3l4.9-0.1l3.1,3.8l2.3,1.9l-0.5,2l3.7,3.2l2.8,4.5v2.3l4.2,0.7l1.2-1.9l-0.3-2.4 l2.6-0.2l3.8,1.8l1.4,3.5l2.1-1.5l2.5,1.9l5.8-0.4l5.3-4.2l2.2,1.4l0.5,2.1l2.4,2.4l3.2,1.5h0.03l-1.73,4.4l-1.4,2.6l-8.9,0.3 l-9.1-4.6l-0.8-2.8l-5,10.8l5.5,2.4l-1.6,2.5l2.3,1.7l1.3-2.5l3,0.3l4.3,3.4l-3,13.3l-2.3,1.8l-3.4-0.3l-2-2.7l-14.4-5.3l-5.3-3.9 l-8.5-1.5l-2.7-5.1l0.3-2.9l-11.5-4l-4.3-7.5l2.8-0.1l-1-5.6l1.7-3.1l-1.2-3.6L539.6,124z", 1, "departement"], ["data-name", "Bas-Rhin", "data-department", "67", "d", "m631.8,140.7l-2.8,9.4l-7.8,10.5l-2,1.5l-1.4,3.3l0.3,4.9l-2.4,7.2 l0.7,3.6l-1.5,2l-1.2,5.5l-3.16,6.23L605.9,193l-0.3-2.8l-8.5-5.6l-3.1-0.2l-5.2-2.2l1.3-10l-1.9-1.3l3.4,0.3l2.3-1.8l3-13.3 l-4.3-3.4l-3-0.3l-1.3,2.5l-2.3-1.7l1.6-2.5l-5.5-2.4l5-10.8l0.8,2.8l9.1,4.6l8.9-0.3l1.4-2.6l1.73-4.4l8.87,0.6l2.4-0.6 L631.8,140.7z", 1, "departement"], ["data-name", "Haut-Rhin", "data-department", "68", "d", "m605.9,193l4.64,1.83l-0.04,0.07v5.3l1.6,1.9 l0.2,3.4l-2.2,11.1l0.1,6.7l1.8,1.5l0.6,3.5l-2.2,2l-0.2,2.3l-3.1,0.9l0.5,2.2l-1.5,1.6h-2.7l-3.8,1.4l-3-1.1l0.3-2.5l-2.4-1.1 l-0.4,0.1l-2-5l-2.8,0.2l-0.5-9l-7.6-5l2.8-2.4v-6.2l4.8-7.8l4.1-13.5l1.1-1l3.1,0.2l8.5,5.6L605.9,193z", 1, "departement"], ["data-name", "Vosges", "data-department", "88", "d", "m520.4,183.6l2.4-2.3l6.9-3.4l3,0.5l1.9-2.3 l5.8,0.6l-0.1,2.9l4.1,4.8l5.7-0.7l1.1-2.8l5.4-2l2.4,1.8l6-1l5.3-3.8l1.6,2.7l6.1,1.6l10.6-7.6l1.5-0.4l-1.3,10l5.2,2.2l-1.1,1 l-4.1,13.5l-4.8,7.8v6.2l-2.8,2.4l-0.9,0.6l-8.4-6.6l-5.1,2.2l-4.9-3.6l-5.8,1.6l-7-4.3l-8,5.8v-0.1l-1.3-2.5l-2.7,1.1l-0.2-3.1 l-5.5-4.7l1.7-8.6L520.4,183.6z", 1, "departement"], ["data-name", "Pays de la Loire", "data-code_insee", "52", 1, "region"], ["data-name", "Loire-Atlantique", "data-department", "44", "d", "m213.1,265.2l1.8-1l-2.8-4.1l-7.8-3l3-1.3 l0.6-2.2l-0.5-2.5l1.4-2.1l5.8-1.1l-5.5-0.7l-6.6,3.7l-4.1-3.2l-2.2,1l-2.2-1.2l-0.5-4.9l0.9-2.5l3-0.5l-0.9-2.2l-0.18-0.31 l13.18-3.89l0.4-6l5.2-3.4l13.2-0.4l1.6-2.9l9-3.9l6.8,3.6l7.2,13.3l-2.7-0.4l-1.9,2.4l8.5,3.3l0.3,5.9l-14.3,2.1l-2.9,2.2l3,0.8 l3.6,4.7l0.8,2.8l-2.8,4.5l2.8,1.4l0.4,3l-4.8-3.5l-1.5,2.4l-3.2,0.7l0.5,3l-2.4,2.1l-2.3-1.7v-3.1l-3.4,0.2l-0.2,9.5l-11.7-5 L213.1,265.2z", 1, "departement"], ["data-name", "Maine-et-Loire", "data-department", "49", "d", "m270.6,269.2l-12.3,0.8l-10.6-3.8l-0.4-3 l-2.8-1.4l2.8-4.5l-0.8-2.8l-3.6-4.7l-3-0.8l2.9-2.2l14.3-2.1l-0.3-5.9l-8.5-3.3l1.9-2.4l2.7,0.4l-7.2-13.3l0.4-2.2l10.5,3.5 l2.1-1.9l8.7,3.6l3,0.4l5.9-2.7l5.1,1.7l0.6,2.7l6.7-0.2l0.2,3.5l2,2l3.1-1.3l5.2,3.3l7.4,0.1l-0.7,2.4l-1.7,9.3l-5.8,15.3v0.1 l-6.6,5.9l-2.3-2.3l-9.6,0.2l-5.6,0.8L270.6,269.2z", 1, "departement"], ["data-name", "Mayenne", "data-department", "53", "d", "m256.6,221.5l-10.5-3.5l3.6-8.6l5.5-2.2 l-1.9-17.3l1.5-2.4l0.1-12.1l8.6,0.8h0.1l3.3,3.7l2.4-1.6l2.5,1.7l6.7-3.3h9.1l0.7-2.7l2.7,0.2l1.9,6l3.3,1.6v2.9v0.1l-4.3,2.7 l0.3,6.9l-4.4,4l1.2,2.9l-5,4.6l1.4,3.4l-5.5,7.7l1.5,5.6l-5.1-1.7l-5.9,2.7l-3-0.4l-8.7-3.6L256.6,221.5z", 1, "departement"], ["data-name", "Sarthe", "data-department", "72", "d", "m312.7,235.3l-6.1-2.6l-7.4-0.1l-5.2-3.3 l-3.1,1.3l-2-2l-0.2-3.5l-6.7,0.2l-0.6-2.7l-1.5-5.6l5.5-7.7l-1.4-3.4l5-4.6l-1.2-2.9l4.4-4l-0.3-6.9l4.3-2.7l3,0.1l11-5.9l2.8,1.5 l1.6,8.4l7.2,5l2.9-1l3.1,3.5l3.2,0.7l2.1,3.8l-0.4,1.8v0.1l-2,2.1l1.7,2.2l0.4,5.5l-6.3,10.6l-3.2,1.1l-0.6,3.5l-7.7,4.5l-2.8-0.3 L312.7,235.3z", 1, "departement"], ["data-name", "Vend\u00E9e", "data-department", "85", "d", "m269.3,305.1l0.2-7.4l-4.7-17.9l-4.2-4.1l-2.3-5.7l-10.6-3.8l-4.8-3.5l-1.5,2.4l-3.2,0.7 l0.5,3l-2.4,2.1l-2.3-1.7v-3.1l-3.4,0.2l-0.2,9.5l-11.7-5l-5.6-5.6l-0.3,0.1l-0.8,2.6l-3.4,4.3l-1.2,2.3l0.2,2.4l8.7,9.5l2.7,5.6 l1.2,5.3l8,5.4l3.4,0.5l3.9,4.3l2.9-0.1l2,1.2l1.8,2.5l-0.9-2.1l3.9,3.3l0.5-2.7l2.4,0.3l7.1-2.7l-1.4,2.9l6.5-0.3l2.4,1.8l9.1-4.5 L269.3,305.1z", 1, "departement"], ["data-name", "Bretagne", "data-code_insee", "53", 1, "region", "region-53"], ["data-name", "Cotes-d\u2019Armor", "data-department", "22", "d", "m208.7,188.9l-4.9,7.1l-2.9,1.1l-1.5-2.7 l-3.5-0.9l-6.2,7.5l-1.8-6l-3,0.9l-12.9-6.5l-7.9,3l-12.46-3.29l2.06-4.11l-2.5-9.3l2.5-8.3l-3.6-4.7l1.1-4.3l1.2,1.4l3.2-0.4 l1.1-7.7l1.5-1.6l2.2-0.6l1.9,1.4h2.5l2.1-1l2.2,0.3l1.5-1.8l0.9,2L170,153l3-3.6l2.9-0.8l-0.1,2.3l-1.2,4.4l1.7-3.1l2.6-0.5l-1.1,2 l7.2,7.8l2.2,5.4l3,2l0.8,3.7l0.7-2.2l3-1l2.4-2.7l8.1-3.3l2.7-0.2l-2,2.5l2.9-1.1l1.8,4.4l1.3-1.9l2.5,0.2v-0.09l1.6,3.99h-0.3h0.3 l2.5,0.3l0.7,0.2l0.4,1.7l-1.9,13L208.7,188.9z", 1, "departement"], ["data-name", "Finist\u00E8re", "data-department", "29", "d", "m151.6,210.1l2,3.4l-0.8,1.4l-5.5-1.2l-1.2-1.9 l2.2-0.7l-3,0.8l-0.3-2.7v2.7l-2.5,0.7l-2.2-1l-4.2-6.1l-0.8,2.5l-2.3,0.2l-3.5-3.1l1.6-4.6l-2.4,4.3l1.3,1.9l-2.2,1l-1,2.8 l-5.9-0.2l-2.1-1.6l1.5-1.6l-1.5-5.5l-2.4-3.1l-2.8-1.8l1.6-1.7l-2.1,1.4l-7.5-2.2l2.2-1.3l12.5-1.8l1.8,1.8l2-1.3l0.7-2.5l-1.6-3.6 l-6.8-2.5l-1.5,2.6l-2.6-4.2l1.3-1.8l-0.3-2.2l1.7,2.3l4.9,1l4.6-0.8l2.1,3.1l5.4,1l-3.7-0.9l-2.8-2l2.2-0.5l-4.2-2l2-1.5l-2.6-0.2 l-2.7,0.8l-0.8-2.2l7.1-4.5l-4.4,2.2l-2.3,0.1l-7.5,2.9l-2.7-1.2l-2.7,1.2l-1.5-1.8l0.6-5.3l2.5-1.6l-2.2-0.9l0.8-2.6l1.8-1.6 l2.1-0.8l5.1,1.5l-1.9-1.1l2.5-1.2l1.6,1.4l-1.9-1.7l1.2-1.9l2.9-0.1l3.8-2l2.3,2.6l6.7-3.1l3,1.6l1-2.2l2.9-0.5l0.4,5l2.2-1.5 l1.3,2.5l1.2-4.5l4.7,0.3l1.2,1.7l-1.1,4.3l3.6,4.7l-2.5,8.3l2.5,9.3l-2.06,4.11l-0.04-0.01v0.1l-6.8,3.2l0.5,3.5l3.4,5.5l8.1,1.3 l0.1,5.4l-2.5,2.8L151.6,210.1z", 1, "departement"], ["data-name", "Ille-et-Vilaine", "data-department", "35", "d", "m255.2,207.2l-5.5,2.2l-3.6,8.6l-0.4,2.2 l-6.8-3.6l-9,3.9l-1.6,2.9l-13.2,0.4l-5.2,3.4l-1-5.8l3-0.7l-2.8-1.5l2.4-2.2l1-3.2l-2.4-1.7l1.6-2.6l-1.2-2.5l-5.1-2.8l-0.5-2.8 l3.5-0.9l-3.6-0.1l-1-4.4l4.9-7.1l9-2.5l1.9-13l-0.4-1.7l-0.7-0.2l-2.5-0.3l-1.6-3.99l0.05-0.86l0.05-0.85l0.7-0.1h2.1v0.1l1.7,4.4 l1.3,2l-0.5,2.1l1.4-2.1l-2.3-5.1l0.7-2.5l2.2-1.5l2.3-0.6l2.2,1l-1.5,2.3l2.9,2.4l7.3-0.6l4.7,9.6l2.7,1l7.1-4.8l5.4,2.3l-0.1,12.1 l-1.5,2.4L255.2,207.2z", 1, "departement"], ["data-name", "Morbihan", "data-department", "56", "d", "M167.7,242.6l2.9,1.2l-1.1,2.1l-5.1-1.2l-1.3-2.7l0.4-3l2.1,1.4L167.7,242.6z M209.1,219.2l2.4-2.2l1-3.2 l-2.4-1.7l1.6-2.6l-1.2-2.5l-5.1-2.8l-0.5-2.8l3.5-0.9l-3.6-0.1l-1-4.4l-2.9,1.1l-1.5-2.7l-3.5-0.9l-6.2,7.5l-1.8-6l-3,0.9 l-12.9-6.5l-7.9,3l-12.46-3.29l-0.04,0.09l-6.8,3.2l0.5,3.5l3.4,5.5l8.1,1.3l0.1,5.4l-2.5,2.8l-2.8-0.8l2,3.4l0.1,1.5l2.9,4.4 l2.3-0.2l1.5-1.7l-0.8-5.1l0.6,2.4l1.7,1.7l1.9-1.7l-2.5,4.2l2.2,1.4l-2.3-0.6l3.2,1.9l0.1,0.1l1.6,1l1.7-2.5l-1.6,3.1l2.1,2.6 l0.6,3.5l-0.9,2.8l2.1,1.1l-1.2-3l0.5-3.8l2.2,1.6l5.1,0.1l-0.7-5l1.4,2l2.1,1.5l4.8-0.5l2.1,2.4l-1,2.2l-2.1-0.6l-4.8,0.4l3.8,3.3 l12.9-0.9l3.1,1.5l-3.4,0.1l1.42,2.39l13.18-3.89l0.4-6l-1-5.8l3-0.7L209.1,219.2z", 1, "departement"], ["data-name", "Nouvelle-Aquitaine", "data-code_insee", "75", 1, "region"], ["data-name", "Charente", "data-department", "16", "d", "m294.8,379.2l-2,2v-0.1l-6.3-6.3l-6-1.2l1.7-3l-2.3-2l2.4-1.7l-1.5-2.6 l1.7-2.6l-2.4-1.7l-0.3-3l-5-3.1l2.2-2.1l-3.2-5.6l8.1-3.3l2.3,2l2.7-0.1l2.7-11.6l2.7-1.6l0.3-3l5.8-2.5l3.5,0.4l0.8-0.8h0.1l9.1,3 l2.9-0.8l-1.4-2.4l2.2-1.8l4.1,3.9l3.8-1.4l1.3-2.5l4.8,0.6l-0.2,5.1l4.7,3.6l-0.6,3.2l-2.6,1.1l-4,8l-2.8,0.6l-3.4,3.8h0.1 l-5.7,6.1l-2.1,5.3l-7.9,5.9l-0.7,5.7l-4.1,5.8L294.8,379.2z", 1, "departement"], ["data-name", "Charente-Maritime", "data-department", "17", "d", "M242.8,341.1l-1.4-5l-3.5-3l-1.3-2.3l1.5-3.6l1.7,1.8l2.9,0.5l1.4,8.4L242.8,341.1z M241.9,318.9l-5.8-4.5 l-4.4-1.5l-0.6,2.9l2.7,0.1l4.8,3.3L241.9,318.9z M286.5,374.8l-6-1.2l1.7-3l-2.3-2l2.4-1.7l-1.5-2.6l1.7-2.6l-2.4-1.7l-0.3-3 l-5-3.1l2.2-2.1l-3.2-5.6l8.1-3.3l2.3,2l2.7-0.1l2.7-11.6l-3.6-4.7l-17.4-6.7l-5.9-6.5v-3.7l-2.4-1.8l-6.5,0.3l1.4-2.9l-7.1,2.7 l0.5,0.1l-0.6,3.4l-4.5,5.9l2.4,0.3l2.2,1.7l3,7.2l-1.5,1.9l-0.2,5.1l-3.3,3.1l-0.1,2.6l-2.2,0.4l-1.5,1.7l1.1,4.3l9,6.5l1.5,2.6 l4.3,2.7l3.7,4.8l1.81,7.3l3.79-0.5l0.7,2.8l6.4,1.7l0.6,5.8l6.1,4.3l9.4,1l2-5l0.1-0.4v-0.1L286.5,374.8z", 1, "departement"], ["data-name", "Corr\u00E8ze", "data-department", "19", "d", "m363.6,392.3l-8.1,0.8l-3.5-7l-3.2-0.7l-0.2-3 l-2.3-1.5l2-1.8l-1.7-3l3.6-4.6l-2.9-4.7l1.6-2.7l2.5,1.2l4.7-4l5.7-1.3l4.9-4.6l8.7-4l7-3.4l11.2,5.2l2.3-2.6l2.7,0.8l2.4-2.4 l1.2,5.6l-1.7,2.4l1.2,7.9l0.7,6l-6.2-2l-0.6,3.5l-7.6,9.5l1.8,2.2l-2.3,1.9l-0.3,3.5l-3.1,1.1l1.5,3.4l-3.2,1.9h-0.1l-6.7-0.2 l-5.3,2.7L363.6,392.3z", 1, "departement"], ["data-name", "Creuse", "data-department", "23", "d", "m396.6,343.5l4.4,5.5l-2.4,2.4l-2.7-0.8 l-2.3,2.6l-11.2-5.2l-7,3.4l-0.6-5.9l-4.7-3l-6.4-0.5l-0.1-2.8l-2.9-1.5l0.9-3.4l-1.8-5.2l-6.6-9.8l3-5.3l-1.2-2.6l2.8-2.9l11.5-1.1 l1.9-2.5l13.2,2.7l2.7-0.8l4.9,0.2l1.1,3.9c2.5,1.6,4.9,3.2,7.4,4.8l3.6,8.4l-0.5,4.1l2.3,6.7L396.6,343.5z", 1, "departement"], ["data-name", "Dordogne", "data-department", "24", "d", "m307.7,414.3l-2.8-6.4l-1-1.3l0.9-2.9l-2.4-2.6l-2,3.2l-9.8-2.3l2-2 l0.2-5.7l2.8-5.5l-1.2-2.8l-3.7,0.6l2-5l0.1-0.4l2-2l5.5-0.7l4.1-5.8l0.7-5.7l7.9-5.9l2.1-5.3l5.7-6.1l6.2,3l-0.1,4.7l9.5-1.1 l7.2,5.6l-2,2.7l5.7,2.2l2.9,4.7l-3.6,4.6l1.7,3l-2,1.8l2.3,1.5l0.2,3l3.2,0.7l3.5,7l-0.7,5l-1.4,5.3l-4.5,3.2l0.6,3.6l-6,3.4 l-4.7,6.5l-4.2-4.2l-5.4,2.7l-1.5-6l-6.1,1l-2.2-1.8l-2.8,2L307.7,414.3z", 1, "departement"], ["data-name", "Gironde", "data-department", "33", "d", "m243.9,420.1l-5.8,2.6v-4.6l2.2-3.2l0.5-2.3 l1.9-1.7l1.8,1.4l3.1-0.2l-1.1-4.6l-3.5-3.4l-2.8,4l-1.2,3.8l6.2-50l0.9-2.8l3.3-3.4l1.4,4.7l9,9l2.8,7.6l1.7-3.1l-0.59-2.4 l3.79-0.5l0.7,2.8l6.4,1.7l0.6,5.8l6.1,4.3l9.4,1l3.7-0.6l1.2,2.8l-2.8,5.5l-0.2,5.7l-2,2l9.8,2.3l2-3.2l2.4,2.6l-0.9,2.9l1,1.3 l-3.1-0.1l-1.2,2.5l-2.7-0.9l-1.1,3.3l2.9,1.4l-8.5,8.6l-0.6,8.9l-3,2.3l1.5,2.5l-4.5,4l-2.1-2.7l-1.6,3.6h-6.4l-0.6-4.7l-11-7.7 l0.4-2.8l-17.2,0.7l1.5-5.4L243.9,420.1z", 1, "departement"], ["data-name", "Landes", "data-department", "40", "d", "m222.32,481.21l1.08-1.51l3.9-7.1l8.8-37.8 l2-11.7v-0.4l5.8-2.6l3.7,1.3l-1.5,5.4l17.2-0.7l-0.4,2.8l11,7.7l0.6,4.7h6.4l1.6-3.6l2.1,2.7l0.4,4.6l11.7,2.9l-3.6,5.2l0.7,2.6 l-0.4,2.9l-2.5,1.3l-0.6-3l-9.4,2.7l0.5,6.4l-4.2,11.1l1.6,2.7l-8.6,1.5l-3.3-1.1l-4.8,1.9l-2.2-2l-2.3,1.5l-2.5-2.3l-9.8,2 l-1.6,2.2l-2.5-1.4l-2.7,1.3l-1.2-2.8l-11,2.5L222.32,481.21z", 1, "departement"], ["data-name", "Lot-et-Garonne", "data-department", "47", "d", "m293.8,455.6v0.1l-0.7-2.6l3.6-5.2L285,445 l-0.4-4.6l4.5-4l-1.5-2.5l3-2.3l0.6-8.9l8.5-8.6l-2.9-1.4l1.1-3.3l2.7,0.9l1.2-2.5l3.1,0.1l2.8,6.4l8.9-0.5l2.8-2l2.2,1.8l6.1-1 l1.5,6l5.4-2.7l4.2,4.2l-3.4,3.1l2.7,9.1l-7.5,2v2.9l2.4,1.4l-4.4,5.5l1.3,2.7l-2.8-0.2l-3.6,4.7l-2.7,1.3l-8.6-1l-5,2.9l-8.3-0.7 l-1.4,2.5L293.8,455.6z", 1, "departement"], ["data-name", "Pyr\u00E9n\u00E9es-Atlantiques", "data-department", "64", "d", "m276.9,513.4l3.4-0.8l-0.4-2.9l8-9.3l-0.8-3.1 l2.7-1.4l-0.5-7.2h-2.9l1.5-2.8l-2.5-5.8l-6.6-0.3l-8.6,1.5l-3.3-1.1l-4.8,1.9l-2.2-2l-2.3,1.5l-2.5-2.3l-9.8,2l-1.6,2.2l-2.5-1.4 l-2.7,1.3l-1.2-2.8l-11,2.5l-3.98-1.89l-3.52,4.89l-2.7,1.9l-4.5,0.9l1.9,4.5l4.5-0.2l0.2,2.2l2.4,1l2.2-2.1l2.4,1.3l2.5,0.1 l1.4,2.8l-2.5,6.7l-2.1,2.2l1.3,2.2l4.3-0.1l0.7-3.4l2.3-0.1l-1.3,2.4l5.9,2.3l1.5,1.8h2.5l6.1,3.8l5.8,0.4l2.3-1l1.4,2.1l0.3,2.8 l2.7,1.3l3.9,4l2.1,0.9l1.1-2.1l2.7,2.1l3.6-1.1l0.19-0.16l1.41-9.34L276.9,513.4z", 1, "departement"], ["data-name", "Deux-S\u00E8vres", "data-department", "79", "d", "m292.3,331.6l-2.7,1.6l-3.6-4.7l-17.4-6.7 l-5.9-6.5v-3.7l9.1-4.5l-2.5-2l0.2-7.4l-4.7-17.9l-4.2-4.1l-2.3-5.7l12.3-0.8l3.7-4.8l5.6-0.8l9.6-0.2l2.3,2.3l3.4,9l-0.8,3l2.7,1.2 l-4.5,14.1l2.7-0.9l1.5,3l-3.4,5.5l0.5,5.8l2.1,2l-0.1,2.8l6.4,0.2l-3.2,8.5l4.5,3l-0.8,2.8h-0.1l-0.8,0.8l-3.5-0.4l-5.8,2.5 L292.3,331.6z", 1, "departement"], ["data-name", "Vienne", "data-department", "86", "d", "m329.6,320.8v3.5l-4.8-0.6l-1.3,2.5l-3.8,1.4 l-4.1-3.9l-2.2,1.8l1.4,2.4l-2.9,0.8l-9.1-3l0.8-2.8l-4.5-3l3.2-8.5l-6.4-0.2l0.1-2.8l-2.1-2l-0.5-5.8l3.4-5.5l-1.5-3l-2.7,0.9 l4.5-14.1l-2.7-1.2l0.8-3l-3.4-9l6.6-5.9l5.5,3.2l0.3,3.2l2.9-0.3l1.3,6.1l2.8,1.4l10-0.4l-1.4-2.9l5.3,3l0.3,3.1l7.1,10l2.1,3 l-0.8,5.8l4.6,4.4h2.9l2.6,5.4l2.5,1.3l-1.5,2.8l-0.8-0.3l-1.3,2.4l-3.3-0.9l-1.3,3l-5.6,2.7L329.6,320.8z", 1, "departement"], ["data-name", "Haute-Vienne", "data-department", "87", "d", "m348.9,364.1l-1.6,2.7l-5.7-2.2l2-2.7l-7.2-5.6 l-9.5,1.1l0.1-4.7l-6.2-3h-0.1l3.4-3.8l2.8-0.6l4-8l2.6-1.1l0.6-3.2l-4.7-3.6l0.2-5.1v-3.5l3-5l5.6-2.7l1.3-3l3.3,0.9l1.3-2.4 l0.8,0.3l2.6,1.1l5.8-1.1l1.7,2.5l1.2,2.6l-3,5.3l6.6,9.8l1.8,5.2l-0.9,3.4l2.9,1.5l0.1,2.8l6.4,0.5l4.7,3l0.6,5.9l-8.7,4l-4.9,4.6 l-5.7,1.3l-4.7,4L348.9,364.1z", 1, "departement"], ["data-name", "Occitanie", "data-code_insee", "76", 1, "region"], ["data-name", "Ari\u00E8ge", "data-department", "09", "d", "m369.82,543.59l0.78-0.89l-2.6-1.1l-2-2.1 l-3.7-0.1l-1.7-1.7l-2.8,0.4l-1.3,2.1l-2.4-0.8l-2.8-5.9l-10-0.6l-1.3-2.8l-13.2-3.9l-0.5-1.4l3.8-5.2l2.8-1v-5.9l3.9-4l2.8-1.1 l6.2,4.1l-0.4-5.6l5.4-1.6l-3-4.8l2.8-1.1l3.4,5.5l2.8-0.5l0.6-2.8l5.7,2.2l2-2.3l2.2,5.5l8.7,3.9l2.2,5.2l0.2,3.1l-2.2,2.3l2.4,2.5 l-1.2,3l-3.2,0.6l0.8,5.7l3.4,1.5l3.3-1.2l4.8,5.6l-7.4,0.2l-1.3,2.6L369.82,543.59z", 1, "departement"], ["data-name", "Aude", "data-department", "11", "d", "m435.07,504.37l-1.47,1.53l-5.2,9.3l-0.9,3.5 l0.15,9.57l-9.45-5.57l-8.2,5.4l-13.6-1l-2.7,1.4l1.4,6l-8.6,3.9l-4.8-5.6l-3.3,1.2l-3.4-1.5l-0.8-5.7l3.2-0.6l1.2-3l-2.4-2.5 l2.2-2.3l-0.2-3.1l-2.2-5.2l-8.7-3.9l-2.2-5.5l8.4-10l1.4,2.7l5.2-1.8l0.5-0.8l1.8,2.3l6.3,0.9l1.1-3.3l2.8-0.5l12,1.4l-0.5,2.8 l3.5,5l2.5-1.6l1.4,2.9l3.1-0.8l3.8-5.3l1,2.9l13.8,4.7l1.7,2L435.07,504.37z", 1, "departement"], ["data-name", "Aveyron", "data-department", "12", "d", "m430.8,440.7l9.4,4.5l-2,3.9l-2.8,1.1l8.4,4.1 l-4.3,5.3l0.3,1.5l-3.7,1l-3,5.3l-6.3-1.3l-0.1,8.7l-5.7-0.1l-1.3-2.8l-11.1-1.3l-4.2-5l-4.3-11.5l-4.8-4.3L385,444l-6.1,2.8 l-4.3-3.6l2.3-2.4l-3.1-2.7l0.4-3l-0.8-9.1l7.6-5l5.9-1.4l1.7-1.5h0.1l5.1-3.2l6.4,1.5l3.8-4.8l3-9.1l4.7-4.2l5.2,4l1.3,4.2l2.4,1.6 l-0.5,3l2.6,5.1v0.1l4.2,4.5l2.9,8.8l-0.5,8.7L430.8,440.7z", 1, "departement"], ["data-name", "Gard", "data-department", "30", "d", "m480,487.2l-2.8-0.6l-1.9-1.6l-1.1-3.4h-0.1 l3.3-4.4l-1.5-3l-6.1-6.7l-3-0.2l-0.2-3l-6.8-1.4l0.9-2.7l-1.9-2.6l-3.9,0.6l-4.2,3.9l-0.1,2.8l-5.3-2.5l-2.2,1.7l-0.4-2.9l-2.9-0.1 l-0.3-1.5l4.3-5.3l-8.4-4.1l2.8-1.1l2-3.9l7.8,3.4l3.9-0.5l0.1-3.3l8.7,2.2l6.3-1.8l-1.4-3l1.2-2.9l-3.9-7.7l3.6-2.5l1.1-2.1 l2.7,5.9l7.8,5l7.1-4.3l0.1,3.1l2.5-2.3h2.8l6,3.5l2.6,4.4l0.2,5.5l6.3,6.4l-4.5,5l-3.9,4.1l-1.9,10.6l-3.3-0.9l-4.2,4.8l1,2.7 l-5.8,1.8L480,487.2z", 1, "departement"], ["data-name", "Haute-Garonne", "data-department", "31", "d", "m326.8,526.2l-5.5-1.5l-1.2,2.4l0.2,7.6 l-8.8-0.7l-1.7,0.3l-0.6-7l5.5-3.2l2.6-5.3l-0.8-2.7l-3.1,0.3l0.6-3.5l-4.6-4l7.1-11.2l3.1-1.1l3.5-5.3l11.4,2.5l0.7-5.8l6.5-6.1 l-9.1-13.3l9.9-0.9l1.7,2.3l5.8-2.5l-2.2-2.3l11.7-4.3l1.4,6.3l2.6,1.2l0.2,2.8l2.3,2.1l-0.7,5.4l14.3,9.3l1,2.8l-0.5,0.8l-5.2,1.8 l-1.4-2.7l-8.4,10l-2,2.3l-5.7-2.2l-0.6,2.8l-2.8,0.5l-3.4-5.5l-2.8,1.1l3,4.8l-5.4,1.6l0.4,5.6l-6.2-4.1l-2.8,1.1l-3.9,4v5.9 l-2.8,1l-3.8,5.2L326.8,526.2z", 1, "departement"], ["data-name", "Gers", "data-department", "32", "d", "m330.6,461.7l2,6.9l9.1,13.3l-6.5,6.1l-0.7,5.8 l-11.4-2.5l-3.5,5.3l-3.1,1.1l-12.4-2.2l-1.4-3l-5.5,0.6l-2.6-8.7l-3.3-1.3l-2-3.5l-3.9,0.5l-6.6-0.3l-1.6-2.7l4.2-11.1l-0.5-6.4 l9.4-2.7l0.6,3l2.5-1.3l0.4-2.9v-0.1l3.7,0.7l1.4-2.5l8.3,0.7l5-2.9l8.6,1l2.7-1.3l5.3,1.7l-3.3,4.6L330.6,461.7z", 1, "departement"], ["data-name", "H\u00E9rault", "data-department", "34", "d", "m474.1,481.6l-2.4-0.1l-5.9,2.6l-3.6,3.2 l-7.2,4.6l-4.3,4.2l2.1-3.5l-4.3,6.6h-6.8l-5.5,4l-1.13,1.17l-0.17-0.17l-1.7-2l-13.8-4.7l-1-2.9l-3.8,5.3l-3.1,0.8l-1.4-2.9 l-2.5,1.6l-3.5-5l0.5-2.8l3.4-2l0.8-3l-0.7-9.7l6.1,2.2c2.3-1.5,4.6-2.9,6.8-4.4l5.7,0.1l0.1-8.7l6.3,1.3l3-5.3l3.7-1l2.9,0.1 l0.4,2.9l2.2-1.7l5.3,2.5l0.1-2.8l4.2-3.9l3.9-0.6l1.9,2.6l-0.9,2.7l6.8,1.4l0.2,3l3,0.2l6.1,6.7l1.5,3L474.1,481.6z", 1, "departement"], ["data-name", "Lot", "data-department", "46", "d", "m385.4,413.1l3.3,5h-0.1l-1.7,1.5L381,421 l-7.6,5l0.8,9.1l-6.2,0.8l-7.5,5.5l-2.6-2.3l-8.7,2.5l-0.5-4l-2.4,1.5l-2.7-1l-4.5-4l2.1-2.3l-3.1,0.5l-2.7-9.1l3.4-3.1l4.7-6.5 l6-3.4l-0.6-3.6l4.5-3.2l1.4-5.3l0.7-5l8.1-0.8l6.7,6.1l5.3-2.7l6.7,0.2l1,5.4l3.8,6L385.4,413.1z", 1, "departement"], ["data-name", "Loz\u00E8re", "data-department", "48", "d", "m463.4,418.7l4.2,8.3l-1.1,2.1l-3.6,2.5 l3.9,7.7l-1.2,2.9l1.4,3l-6.3,1.8l-8.7-2.2l-0.1,3.3l-3.9,0.5l-7.8-3.4l-9.4-4.5l-1.5-2.4l0.5-8.7l-2.9-8.8l-4.2-4.5v-0.1l6.9-15.9 l1.7,2.3l6.8-5.7l1-1l2.3,1.7l1.5,5.7l6.4,1.2l0.1-2.8l2.9,0.2l9,7.7L463.4,418.7z", 1, "departement"], ["data-name", "Hautes-Pyr\u00E9n\u00E9es", "data-department", "65", "d", "m314.7,524.1l-5.5,3.2l0.6,7l-0.7,0.2l-2.3-1.6 l-2.4,1.8l-2.5-0.5l-1.9-1.7l-3.9-0.3l-6.9,2.1l-2.2-0.9l-2.1-1.7l-1.1-2.5l-7.8-5.5l-2.11,1.84l1.41-9.34l1.6-2.8l3.4-0.8l-0.4-2.9 l8-9.3l-0.8-3.1l2.7-1.4l-0.5-7.2h-2.9l1.5-2.8l-2.5-5.8l3.9-0.5l2,3.5l3.3,1.3l2.6,8.7l5.5-0.6l1.4,3l12.4,2.2l-7.1,11.2l4.6,4 l-0.6,3.5l3.1-0.3l0.8,2.7L314.7,524.1z", 1, "departement"], ["data-name", "Pyr\u00E9n\u00E9es-Orientales", "data-department", "66", "d", "m427.65,528.27l0.25,15.63l3.9,3.3l1.9,3.8 h-2.3l-8.1-2.7l-6.9,3.9l-3-0.2l-2.4,1.1l-0.6,2.4l-2.1,1.2l-2.4-0.7l-2.9,1l-4-3.1l-7-2.9l-2.5,1.4h-3l-1,2.1l-4.6,2l-1.9-1.7 l-1.7-4.8l-7.5-2l-2-2.1l2.02-2.31l7.98-2.39l1.3-2.6l7.4-0.2l8.6-3.9l-1.4-6l2.7-1.4l13.6,1l8.2-5.4L427.65,528.27z", 1, "departement"], ["data-name", "Tarn", "data-department", "81", "d", "m419.7,471.9l1.3,2.8c-2.2,1.5-4.5,2.9-6.8,4.4 l-6.1-2.2l0.7,9.7l-0.8,3l-3.4,2l-12-1.4l-2.8,0.5l-1.1,3.3l-6.3-0.9l-1.8-2.3l-1-2.8l-14.3-9.3l0.7-5.4l-2.3-2.1l-0.2-2.8l-2.6-1.2 l-1.4-6.3l0.5-2.8l4.8-3.2l1-2.7L364,450l3-1.1l2.7,1.1l9.2-3.2l6.1-2.8l10.3,5.8l4.8,4.3l4.3,11.5l4.2,5L419.7,471.9z", 1, "departement"], ["data-name", "Tarn-et-Garonne", "data-department", "82", "d", "m360,458.1l-0.5,2.8l-11.7,4.3l2.2,2.3 l-5.8,2.5l-1.7-2.3l-9.9,0.9l-2-6.9l-5.1-4.1l3.3-4.6l-5.3-1.7l3.6-4.7l2.8,0.2l-1.3-2.7l4.4-5.5l-2.4-1.4v-2.9l7.5-2l3.1-0.5 l-2.1,2.3l4.5,4l2.7,1l2.4-1.5l0.5,4l8.7-2.5l2.6,2.3l7.5-5.5l6.2-0.8l-0.4,3l3.1,2.7l-2.3,2.4l4.3,3.6l-9.2,3.2l-2.7-1.1l-3,1.1 l1.8,2.2l-1,2.7L360,458.1z", 1, "departement"], ["data-name", "Auvergne-Rhone-Alpes", "data-code_insee", "84", 1, "region"], ["data-name", "Ain", "data-department", "01", "d", "m542,347l-5.7,6.7l-11.2-15.2l-2.8,0.7l-3,5.1 l-6-2l-6.4,0.5l-3.7-5.7l-2.8,0.5l-3.1-9.2l1.5-8l5.9-20.9l5.9,1.5l5.4-1.3l4.8,3.3l4.3,7.7h2.9l0.1,3l2.9-0.1l4-4.4l3.4,1.6 l0.4,2.8l3.8-0.2l5.5-3.2l5.3-7.2l4.5,2.7l-1.8,4.7l0.3,2.5l-4.4,1.5l-1.9,2l0.2,2.8l0.46,0.19l-4.36,4.71h-2.9l0.8,9.3L542,347z", 1, "departement"], ["data-name", "Allier", "data-department", "03", "d", "m443.1,292.3l5.9-6l6.7,13.5l7.9,2.9l1.6,2.4l-0.5,5.5l-3.7,4.6 l-3.9,1.3l-0.5,3l1.5,12.4l-5.5,4.8l-3.5-4.3l-6.4-0.4l-1.4-3.2l-13.1-0.5l-1.6-2.5l-3.3,0.5l-4.4-4.5l1.2-2.8l-2.3-1.7l-11.2,8 l-2.5-1.2l-3.6-8.4c-2.5-1.6-4.9-3.2-7.4-4.8L392,307v-0.1l3.5-5.9l8.7-1l1.7-2.4l-1.7-5.3l2.3-1.9l8.4-2.9l4.8-3.7h4h0.1l5.7,6.3 l6.4,0.2l2.8-1.7L443.1,292.3z", 1, "departement"], ["data-name", "Ard\u00E8che", "data-department", "07", "d", "m496.5,434.2l0.1,3.7l-6-3.5h-2.8l-2.5,2.3 l-0.1-3.1l-7.1,4.3l-7.8-5l-2.7-5.9l-4.2-8.3l-2.1-9.1l6.7-6.4l5.9-1.9l3.4-5.9l3.4-0.4l-0.7-2.8l2.6-2.3l1.5-5.2l2.6,1.2v-3.1 l0.9-4.1l3.5-0.8l3.2-4.9l5-2.7l2,4.2l0.5,10.3l3.8,11.3l-1.5,6.2l-3.5,4.5l1,7.1l-3,5.9L496.5,434.2z", 1, "departement"], ["data-name", "Cantal", "data-department", "15", "d", "m435.6,387.9l3.5,8l-1,1l-6.8,5.7l-1.7-2.3 l-6.9,15.9l-2.6-5.1l0.5-3l-2.4-1.6l-1.3-4.2l-5.2-4l-4.7,4.2l-3,9.1l-3.8,4.8l-6.4-1.5l-5.1,3.2l-3.3-5l1.7-5.8l-3.8-6l-1-5.4h0.1 l3.2-1.9l-1.5-3.4l3.1-1.1l0.3-3.5l2.3-1.9l-1.8-2.2l7.6-9.5l0.6-3.5l6.2,2l-0.7-6l7.5,3.5l1.5,2.5l6.7,0.3l6.5,5.4l3.7-4.1v3.9 l5.5,1.5l3.3,8.7l2.6,1.1L435.6,387.9z", 1, "departement"], ["data-name", "Drome", "data-department", "26", "d", "m535.1,404.4l-3,0.5l-0.8-17.5l-3,1.7l-8.2-1.9 l-2.7,1l1.1-6.3l-3.3-7.8l-4.9-2.7l-9,3.1l0.5,10.3l3.8,11.3l-1.5,6.2l-3.5,4.5l1,7.1l-3,5.9l-2.1,14.4l5.9,0.7l3.5,4.2l8.7-3.9 l2.4,1.4l2.5-2.2l0.5,5.8l9.3,0.9l0.1,2.8l5.2,2.3l4.3-4.8l2.3-0.1l1-0.2l0.2-4.7l-10-5.7l-1.5-2.6l3.2-5.1l4.2,1.4l2.5-2.5l-3-2.3 l2.5-6.7l5.8-0.3l0.3-3.4l-5.9-0.8L535.1,404.4z", 1, "departement"], ["data-name", "Is\u00E8re", "data-department", "38", "d", "m513.6,349.4l-0.3-7.1l6,2l3-5.1l2.8-0.7 l11.2,15.2l6.5,10.5l6.2,0.2l0.3-2.8l9.4,2.1l2.7,6.3l-2.3,5.5l1,5.4l5.2,1.5l-1.6,3.8l1.8,4.2l4.4,3.1l-0.4,5.8l-3.1-1.1l-12.6,3.9 l-0.9,2.8l-5.5,1.2l-1,3.1l-5.9-0.8l-5.4-4l-3,0.5l-0.8-17.5l-3,1.7l-8.2-1.9l-2.7,1l1.1-6.3l-3.3-7.8l-4.9-2.7l-9,3.1l-2-4.2v-4.4 l-0.2-1.1h0.1l4.4-3.9l-1.9-2.5l2.5-2.5l6.9-1.5L513.6,349.4z", 1, "departement"], ["data-name", "Loire", "data-department", "42", "d", "m499.3,365.9v4.4l-5,2.7l-3.2,4.9l-3.5,0.8 l-2.2-2.4l-2.6,1l-0.7-5.5l-6-2.2l-6.2,3l-2.8,0.4l-2.3-2l-2.8,0.8l3-7.1l-2.7-7.5l-4.6-3.8l-4.7-7.7l2.1-6.3l-2.5-2.7l5.5-4.8 l-1.5-12.4l0.5-3l3.9-1.3v3l5.2,3.3l8-1.5l2.1,2.1l5.7-3.8l0.01-0.09l2.09,2.99l-4.9,3.5l-1.6,8.6l5.2,6.7l-1.7,5.9l2.3,1.6 l-1.3,2.5l1.1,3l4.6,4.1l5.9,2.1l0.9,3l4.6,2.6h-0.1L499.3,365.9z", 1, "departement"], ["data-name", "Haute-Loire", "data-department", "43", "d", "m485.4,376.3l2.2,2.4l-0.9,4.1v3.1l-2.6-1.2 l-1.5,5.2l-2.6,2.3l0.7,2.8l-3.4,0.4l-3.4,5.9l-5.9,1.9l-6.7,6.4l-9-7.7l-2.9-0.2l-0.1,2.8l-6.4-1.2l-1.5-5.7l-2.3-1.7l-3.5-8 l3.4-0.2l-2.6-1.1l-3.3-8.7l-5.5-1.5v-3.9v-0.1l9.6-3.2l8.5,0.1l5.2,3.2l11.1-0.7l2.8-0.8l2.3,2l2.8-0.4l6.2-3l6,2.2l0.7,5.5 L485.4,376.3z", 1, "departement"], ["data-name", "Puy-de-Dome", "data-department", "63", "d", "m449.1,332.4l3.5,4.3l2.5,2.7l-2.1,6.3l4.7,7.7 l4.6,3.8l2.7,7.5l-3,7.1l-11.1,0.7l-5.2-3.2l-8.5-0.1l-9.6,3.2v0.1l-3.7,4.1l-6.5-5.4l-6.7-0.3l-1.5-2.5l-7.5-3.5l-1.2-7.9l1.7-2.4 L401,349l-4.4-5.5l9.3-8.6l-2.3-6.7l0.5-4.1l2.5,1.2l11.2-8l2.3,1.7l-1.2,2.8l4.4,4.5l3.3-0.5l1.6,2.5l13.1,0.5l1.4,3.2L449.1,332.4z", 1, "departement"], ["data-name", "Rhone", "data-department", "69", "d", "m493.1,312.7l5.7,7.7l-1.5,8l3.1,9.2l2.8-0.5 l3.7,5.7l6.4-0.5l0.3,7.1l-2.5,5l-6.9,1.5l-2.5,2.5l1.9,2.5l-4.4,3.9l-4.6-2.6l-0.9-3l-5.9-2.1l-4.6-4.1l-1.1-3l1.3-2.5l-2.3-1.6 l1.7-5.9l-5.2-6.7l1.6-8.6l4.9-3.5l-2.09-2.99l0.29-2.91l2.3-1.9l2.2,1.7l2.2-1.6l2.5,1.5L493.1,312.7z", 1, "departement"], ["data-name", "Savoie", "data-department", "73", "d", "m603.7,362l-1,10.3l-3.1,1.4l-2.2,0.7l-4.5,3.4 l-1.5,2.4l-2.5-1.4l-5.1,1.3l-2,1.8v0.1l-6.8,1.9l-2,2l-7.7-3.5l-5.2-1.5l-1-5.4l2.3-5.5l-2.7-6.3l-9.4-2.1l-0.3,2.8l-6.2-0.2 l-6.5-10.5l5.7-6.7l2.3-13.6l2.7,6.7l2.7,0.9l1.3,2.5l3,1.7l2.6-1.6l3.2,0.8l4.6,3.6l9.4-13.9l2.4,1.6l-0.6,3l2.3,1.8l6.2,2.3 l2.2-1.5l0.62-0.76l1.88,4.66l2.7,1.1l1.5,1.9l2.8,0.4l-0.7,3l1.3,5.2l5.1,4L603.7,362z", 1, "departement"], ["data-name", "Haute-Savoie", "data-department", "74", "d", "m547,340.1l-2.7-6.7l-0.8-9.3h2.9l4.36-4.71 l2.24,0.91l2.3-1l2.3,0.1l3.4-3.5l2.1-1l1-2.3l-2.8-1.3l1.8-5.1l2.4-0.8l2.3,1l3.6-2.9l9.5-1.3l3.2,0.6l-0.5,2.7l4.2,4.1l-2.1,6.4 l-0.6,1.5l4.6,1.7l-0.1,4.8l2-1.4l4.6,6.6l-1.3,5l-2.5,1.7l-4.9,0.9l-0.6,3.7l0.02,0.04l-0.62,0.76l-2.2,1.5l-6.2-2.3l-2.3-1.8 l0.6-3l-2.4-1.6l-9.4,13.9l-4.6-3.6l-3.2-0.8l-2.6,1.6l-3-1.7l-1.3-2.5L547,340.1z", 1, "departement"], ["data-name", "Provence-Alpes-Cote d'Azur", "data-code_insee", "93", 1, "region"], ["data-name", "Alpes-de-Haute-Provence", "data-department", "04", "d", "m596.5,409.9l0.57-0.5l-0.37,4.5l-2.2,1.5 l-0.6,2.9l3.5,4l-1.8,4.8l0.19,0.21L589,435.1l-2,5.3l4.3,8.5l7,7.7l-5.2-0.6l-5.2,3.8l1.2,2.6l-3,1.4l-9.8,0.4l-1.2,3.5l-5.9-3.6 l-10.1,8.5l-4-4.8l-2.7,1.8l-5.3-0.2l-6.1-6l-3.4-1.1l1.7-2.5l-3.7-5.2l1.2-3l-2.2-5.4l4.3-4.8l2.3-0.1l1-0.2l5.9-1.4l3.8,1 l-3.4-4.9l3.9,1.1l1.4-8.6l5.3-4l3.3-0.7l3.5,4.5l0.7-3.8l3.8-4.2l11.1,3.3l9-10.2L596.5,409.9z", 1, "departement"], ["data-name", "Hautes-Alpes", "data-department", "05", "d", "m597.1,409l-0.03,0.4l-0.57,0.5l-6,3.3l-9,10.2 l-11.1-3.3l-3.8,4.2l-0.7,3.8l-3.5-4.5l-3.3,0.7l-5.3,4l-1.4,8.6l-3.9-1.1l3.4,4.9l-3.8-1l-5.9,1.4l0.2-4.7l-10-5.7l-1.5-2.6 l3.2-5.1l4.2,1.4l2.5-2.5l-3-2.3l2.5-6.7l5.8-0.3l0.3-3.4l1-3.1l5.5-1.2l0.9-2.8l12.6-3.9l3.1,1.1l0.4-5.8l-4.4-3.1l-1.8-4.2 l1.6-3.8l7.7,3.5l2-2l6.8-1.9l1.8,4.5l2.4,0.6l1.1,2l0.4,3l1.2,2.2l3,2.3l5.7,0.5l2.2,1.3l-0.7,2.1l3.2,4.7l-3,1.5L597.1,409z", 1, "departement"], ["data-name", "Alpes-Maritimes", "data-department", "06", "d", "m605.3,477.1l-3.2-0.1l-1.3,1.8l-0.1,2.2 l-0.42,0.77l-2.18-3.97l0.8-2.9l-5.6-2.6l-1.7-5.6l-5.5-2.9l3-1.4l-1.2-2.6l5.2-3.8l5.2,0.6l-7-7.7l-4.3-8.5l2-5.3l6.79-7.79 l6.91,7.79l6.9,1.6l4.2,2.8l2.5-0.4l1.8,1.4l10.3-2.4l2.7-1.8l-0.3,2.6l1.5,2.2l0.3,3.2l-1.6,1.9l-0.2,2.3l-2.7,1.6l-3.3,5l-0.5,1.6 l1.1,2.7l-1.1,2.7l-3.5,2.9l-2.3,0.5l-0.9,2.4l-3-0.9l-1.5,2.1l-2.3,0.5L609,472l0.1,2.8l-2.4,0.6L605.3,477.1z", 1, "departement"], ["data-name", "Bouches-du-Rhone", "data-department", "13", "d", "m545,500.2l2.5-2l-2.2-6.3l1.1-2.6l2.7-0.5 l-5.5-9.1l2-5.3l3.3-0.8l-1.9-3.8l-0.1-0.1l-6.6,4.3l-3.2,0.2l-12-4.8l-3.5,0.7l-4.5-2.3l-5.5-5.7l-10.4-2.9l-3.9,4.1l-1.9,10.6 l-3.3-0.9l-4.2,4.8l1,2.7l-5.8,1.8l-3.1,4.9l0.2,0.1h13.2l2.2,0.9l1,2.2l-1.6,1.5l2.2,1.4l7.4,0.1l3.2,1.3l1.8-1.7l-1.5-2.8l0.4-2.4 l4.9,1l3,5.3l10-0.8l2.6-1.1l1.8,2l-0.2,2.5l1,2l-1.2,2.2h9.2l1.3,2l2.2-0.8l1.7,0.2L545,500.2z", 1, "departement"], ["data-name", "Var", "data-department", "83", "d", "m600.28,481.77l-1.38,2.53l-6.8,1.7l-0.7,2.5 l-5.5,5.7l5,0.7l-2,4.8l-4,0.2l-4.8,2.5l-3.5,1.1l0.1,2.7l-4.9-1.5l-2.7,0.5l-1.6,1.6l-0.4,2.3l-2.2,1.6l1.4-1.8l-2.4-1.7l-2.2,0.7 l-1.6-1.6l-3.1,0.1l0.9,2.2l-2.3-0.4l-1.5,1.7l-3-1.1l0.6-2.3l-6.4-4.1l-0.5-0.1l0.2-2.1l2.5-2l-2.2-6.3l1.1-2.6l2.7-0.5l-5.5-9.1 l2-5.3l3.3-0.8l-1.9-3.8l0.1-0.4l5.3,0.2l2.7-1.8l4,4.8l10.1-8.5l5.9,3.6l1.2-3.5l9.8-0.4l5.5,2.9l1.7,5.6l5.6,2.6l-0.8,2.9 L600.28,481.77z", 1, "departement"], ["data-name", "Vaucluse", "data-department", "84", "d", "m541,463.4l6.1,6l-0.1,0.4l-0.1-0.1l-6.6,4.3 l-3.2,0.2l-12-4.8l-3.5,0.7l-4.5-2.3l-5.5-5.7l-10.4-2.9l4.5-5l-6.3-6.4l-0.2-5.5l-2.6-4.4l-0.1-3.7l5.9,0.7l3.5,4.2l8.7-3.9 l2.4,1.4l2.5-2.2l0.5,5.8l9.3,0.9l0.1,2.8l5.2,2.3l2.2,5.4l-1.2,3l3.7,5.2l-1.7,2.5L541,463.4z", 1, "departement"], ["data-name", "Corse", "data-department", "20", "data-code_insee", "94", 1, "region"], ["data-name", "Corse-du-Sud", "data-department", "2A", "d", "m640.5,554.2l3.2-1.7l0.7,8.4l-0.15,0.54 l-1.85,4.86l-2.7,1.9l3.3,0.4l-5.8,14.7l-3.1-1.2l-1.2-2.8l-11.2-3.4l-4.8-4.4l0.2-3l4.9-3.3l-9.5-1.9l2.7-7l-0.9-5.8l-7.3,2.6 l3-8.4l2.6-1.6l-7.9-4.4l-1.1-5.5l5.3-3.8l-3.8-4.2l-2.6,1l0.5-2.7l13.6,2.1l1.2,3.5l6,3.4l6,5.9l0.5,3.2l2.7,1.1l3.7,11 L640.5,554.2z", 1, "departement"], ["data-name", "Haute-Corse", "data-department", "2B", "d", "m643.7,551.5v1l-3.2,1.7l-3.8-0.5l-3.7-11 l-2.7-1.1l-0.5-3.2l-6-5.9l-6-3.4l-1.2-3.5l-13.6-2.1v-0.2l3.9-5l-0.3-3.4l2.2-2.8l2.8-0.3l0.9-2.9l10.7-4.2l3.5-4.9l8.6,1.3 l-0.5-17.4l2.4-2l2.9,1.1l0.18,0.89l1.52,8.21l-0.5,10.6l4,5.6l3.8,26l-5.4,11.9V551.5L643.7,551.5z", 1, "departement"], ["yPosition", "above"], ["aboveMenu", "matMenu"], ["mat-menu-item", ""]], template: function MapFrenchComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "svg", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "g", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "path", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "g", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "path", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "g", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "path", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "g", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "path", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "g", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "path", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "g", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "path", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "path", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "path", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "path", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "path", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "path", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](20, "path", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "path", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "g", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "path", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](24, "path", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "path", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "path", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](27, "path", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](28, "path", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "g", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](30, "path", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](31, "path", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](32, "path", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](33, "path", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](34, "path", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "path", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](36, "path", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](37, "path", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "g", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](39, "path", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](40, "path", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](41, "path", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](42, "path", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](43, "path", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "g", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](45, "path", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](46, "path", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](47, "path", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](48, "path", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](49, "path", 49);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "g", 50);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](51, "path", 51);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](52, "path", 52);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](53, "path", 53);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](54, "path", 54);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](55, "path", 55);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](56, "path", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](57, "path", 57);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](58, "path", 58);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](59, "path", 59);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](60, "path", 60);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "g", 61);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](62, "path", 62);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](63, "path", 63);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](64, "path", 64);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](65, "path", 65);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](66, "path", 66);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "g", 67);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](68, "path", 68);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](69, "path", 69);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](70, "path", 70);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](71, "path", 71);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "g", 72);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](73, "path", 73);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](74, "path", 74);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](75, "path", 75);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](76, "path", 76);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](77, "path", 77);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](78, "path", 78);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](79, "path", 79);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](80, "path", 80);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](81, "path", 81);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](82, "path", 82);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](83, "path", 83);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](84, "path", 84);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "g", 85);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](86, "path", 86);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](87, "path", 87);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](88, "path", 88);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](89, "path", 89);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](90, "path", 90);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](91, "path", 91);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](92, "path", 92);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](93, "path", 93);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](94, "path", 94);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](95, "path", 95);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](96, "path", 96);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](97, "path", 97);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](98, "path", 98);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "g", 99);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](100, "path", 100);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](101, "path", 101);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](102, "path", 102);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](103, "path", 103);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](104, "path", 104);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](105, "path", 105);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](106, "path", 106);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](107, "path", 107);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](108, "path", 108);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](109, "path", 109);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](110, "path", 110);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](111, "path", 111);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](112, "g", 112);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](113, "path", 113);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](114, "path", 114);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](115, "path", 115);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](116, "path", 116);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](117, "path", 117);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](118, "path", 118);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](119, "g", 119);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](120, "path", 120);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](121, "path", 121);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceHTML"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](122, "mat-menu", 122, 123);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](124, "button", 124);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](125, "Item 1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](126, "button", 124);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](127, "Item 2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, directives: [_angular_material_menu__WEBPACK_IMPORTED_MODULE_1__["_MatMenu"], _angular_material_menu__WEBPACK_IMPORTED_MODULE_1__["MatMenuItem"]], styles: [".mapfrench_container[_ngcontent-%COMP%] {\r\n  height: 100%;\r\n}\r\n.mapfrench_container[_ngcontent-%COMP%]   .mapfrench_wrapper[_ngcontent-%COMP%] {\r\n  display: flex;\r\n  flex-direction: column;\r\n}\r\n.mapfrench_container[_ngcontent-%COMP%]   .mapfrench_wrapper[_ngcontent-%COMP%]   .map_title[_ngcontent-%COMP%] {\r\n  text-align: center;\r\n}\r\n.mapfrench_container[_ngcontent-%COMP%]   .mapfrench_wrapper[_ngcontent-%COMP%]   .map_content[_ngcontent-%COMP%] {\r\n  height: 100%;\r\n  max-height: 550px;\r\n}\r\n.mapfrench_container[_ngcontent-%COMP%]   .mapfrench_wrapper[_ngcontent-%COMP%]   .map_content[_ngcontent-%COMP%]   g[_ngcontent-%COMP%]   path[_ngcontent-%COMP%] {\r\n  fill: #7f8076;\r\n  stroke: white;\r\n  stroke-width: 1px;\r\n  cursor: pointer;\r\n  transition: 0.3s;\r\n  position: relative;\r\n}\r\n.mapfrench_container[_ngcontent-%COMP%]   .mapfrench_wrapper[_ngcontent-%COMP%]   .map_content[_ngcontent-%COMP%]   g[_ngcontent-%COMP%]   path.selected[_ngcontent-%COMP%] {\r\n  fill: red;\r\n}\r\n.mapfrench_container[_ngcontent-%COMP%]   .mapfrench_wrapper[_ngcontent-%COMP%]   .map_content[_ngcontent-%COMP%]   g[_ngcontent-%COMP%]:hover   path[_ngcontent-%COMP%] {\r\n  fill: blue;\r\n\r\n}\r\npolygon[_ngcontent-%COMP%]{\r\n  -webkit-clip-path: polygon(0% 0%, 100% 0%, 100% 75%, 75% 75%, 64% 100%, 50% 75%, 0% 75%);\r\n          clip-path: polygon(0% 0%, 100% 0%, 100% 75%, 75% 75%, 64% 100%, 50% 75%, 0% 75%);\r\n\r\n}\r\n.mapfrench_container[_ngcontent-%COMP%]   .mapfrench_wrapper[_ngcontent-%COMP%]   .map_content[_ngcontent-%COMP%]   .popup[_ngcontent-%COMP%]{\r\n  outline: solid 1px rgb(182, 182, 182);\r\n\r\n  fill: #0000ff;\r\n}\r\n.mapfrench_container[_ngcontent-%COMP%]   .mapfrench_wrapper[_ngcontent-%COMP%]   .map_content[_ngcontent-%COMP%]   .popup[_ngcontent-%COMP%]   text[_ngcontent-%COMP%]{\r\n\r\n  fill: #0000ff;\r\n}\r\n.mapfrench_container[_ngcontent-%COMP%]   .mapfrench_wrapper[_ngcontent-%COMP%]   .map_content[_ngcontent-%COMP%]   g[_ngcontent-%COMP%]   path[_ngcontent-%COMP%]:hover {\r\n  fill: red;\r\n  stroke: white;\r\n  stroke-width: 1px;\r\n  cursor: pointer;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWFwLWZyZW5jaC9tYXAtZnJlbmNoLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxZQUFZO0FBQ2Q7QUFDQTtFQUNFLGFBQWE7RUFDYixzQkFBc0I7QUFDeEI7QUFDQTtFQUNFLGtCQUFrQjtBQUNwQjtBQUNBO0VBQ0UsWUFBWTtFQUNaLGlCQUFpQjtBQUNuQjtBQUVBO0VBQ0UsYUFBYTtFQUNiLGFBQWE7RUFDYixpQkFBaUI7RUFDakIsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixrQkFBa0I7QUFDcEI7QUFDQTtFQUNFLFNBQVM7QUFDWDtBQUNBO0VBQ0UsVUFBVTs7QUFFWjtBQUNBO0VBQ0Usd0ZBQWdGO1VBQWhGLGdGQUFnRjs7QUFFbEY7QUFDQTtFQUNFLHFDQUFxQzs7RUFFckMsYUFBYTtBQUNmO0FBQ0E7O0VBRUUsYUFBYTtBQUNmO0FBRUE7RUFDRSxTQUFTO0VBQ1QsYUFBYTtFQUNiLGlCQUFpQjtFQUNqQixlQUFlO0FBQ2pCIiwiZmlsZSI6InNyYy9hcHAvbWFwLWZyZW5jaC9tYXAtZnJlbmNoLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubWFwZnJlbmNoX2NvbnRhaW5lciB7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5tYXBmcmVuY2hfY29udGFpbmVyIC5tYXBmcmVuY2hfd3JhcHBlciB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG59XHJcbi5tYXBmcmVuY2hfY29udGFpbmVyIC5tYXBmcmVuY2hfd3JhcHBlciAubWFwX3RpdGxlIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuLm1hcGZyZW5jaF9jb250YWluZXIgLm1hcGZyZW5jaF93cmFwcGVyIC5tYXBfY29udGVudCB7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIG1heC1oZWlnaHQ6IDU1MHB4O1xyXG59XHJcblxyXG4ubWFwZnJlbmNoX2NvbnRhaW5lciAubWFwZnJlbmNoX3dyYXBwZXIgLm1hcF9jb250ZW50IGcgcGF0aCB7XHJcbiAgZmlsbDogIzdmODA3NjtcclxuICBzdHJva2U6IHdoaXRlO1xyXG4gIHN0cm9rZS13aWR0aDogMXB4O1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxuICB0cmFuc2l0aW9uOiAwLjNzO1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxufVxyXG4ubWFwZnJlbmNoX2NvbnRhaW5lciAubWFwZnJlbmNoX3dyYXBwZXIgLm1hcF9jb250ZW50IGcgcGF0aC5zZWxlY3RlZCB7XHJcbiAgZmlsbDogcmVkO1xyXG59XHJcbi5tYXBmcmVuY2hfY29udGFpbmVyIC5tYXBmcmVuY2hfd3JhcHBlciAubWFwX2NvbnRlbnQgZzpob3ZlciBwYXRoIHtcclxuICBmaWxsOiBibHVlO1xyXG5cclxufVxyXG5wb2x5Z29ue1xyXG4gIGNsaXAtcGF0aDogcG9seWdvbigwJSAwJSwgMTAwJSAwJSwgMTAwJSA3NSUsIDc1JSA3NSUsIDY0JSAxMDAlLCA1MCUgNzUlLCAwJSA3NSUpO1xyXG5cclxufVxyXG4ubWFwZnJlbmNoX2NvbnRhaW5lciAubWFwZnJlbmNoX3dyYXBwZXIgLm1hcF9jb250ZW50IC5wb3B1cHtcclxuICBvdXRsaW5lOiBzb2xpZCAxcHggcmdiKDE4MiwgMTgyLCAxODIpO1xyXG5cclxuICBmaWxsOiAjMDAwMGZmO1xyXG59XHJcbi5tYXBmcmVuY2hfY29udGFpbmVyIC5tYXBmcmVuY2hfd3JhcHBlciAubWFwX2NvbnRlbnQgLnBvcHVwIHRleHR7XHJcblxyXG4gIGZpbGw6ICMwMDAwZmY7XHJcbn1cclxuXHJcbi5tYXBmcmVuY2hfY29udGFpbmVyIC5tYXBmcmVuY2hfd3JhcHBlciAubWFwX2NvbnRlbnQgZyBwYXRoOmhvdmVyIHtcclxuICBmaWxsOiByZWQ7XHJcbiAgc3Ryb2tlOiB3aGl0ZTtcclxuICBzdHJva2Utd2lkdGg6IDFweDtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuIl19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](MapFrenchComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-map-french',
                templateUrl: './map-french.component.html',
                styleUrls: ['./map-french.component.css']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/material-module.ts":
/*!************************************!*\
  !*** ./src/app/material-module.ts ***!
  \************************************/
/*! exports provided: MaterialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaterialModule", function() { return MaterialModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/cdk/a11y */ "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/a11y.js");
/* harmony import */ var _angular_cdk_clipboard__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/cdk/clipboard */ "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/clipboard.js");
/* harmony import */ var _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/cdk/drag-drop */ "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/drag-drop.js");
/* harmony import */ var _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/cdk/portal */ "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/portal.js");
/* harmony import */ var _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/cdk/scrolling */ "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/scrolling.js");
/* harmony import */ var _angular_cdk_stepper__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/cdk/stepper */ "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/stepper.js");
/* harmony import */ var _angular_cdk_table__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/cdk/table */ "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/table.js");
/* harmony import */ var _angular_cdk_tree__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/cdk/tree */ "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/tree.js");
/* harmony import */ var _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/autocomplete */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/autocomplete.js");
/* harmony import */ var _angular_material_badge__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/badge */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/badge.js");
/* harmony import */ var _angular_material_bottom_sheet__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/bottom-sheet */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/bottom-sheet.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");
/* harmony import */ var _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/button-toggle */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button-toggle.js");
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/card */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/card.js");
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material/checkbox */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/checkbox.js");
/* harmony import */ var _angular_material_chips__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/material/chips */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/chips.js");
/* harmony import */ var _angular_material_stepper__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/material/stepper */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/stepper.js");
/* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/material/datepicker */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/datepicker.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/dialog.js");
/* harmony import */ var _angular_material_divider__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/material/divider */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/divider.js");
/* harmony import */ var _angular_material_expansion__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/material/expansion */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/expansion.js");
/* harmony import */ var _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/material/grid-list */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/grid-list.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/icon.js");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/input.js");
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @angular/material/list */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/list.js");
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! @angular/material/menu */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/menu.js");
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! @angular/material/core */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! @angular/material/paginator */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/paginator.js");
/* harmony import */ var _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! @angular/material/progress-bar */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/progress-bar.js");
/* harmony import */ var _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! @angular/material/progress-spinner */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/progress-spinner.js");
/* harmony import */ var _angular_material_radio__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! @angular/material/radio */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/radio.js");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/select.js");
/* harmony import */ var _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! @angular/material/sidenav */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/sidenav.js");
/* harmony import */ var _angular_material_slider__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! @angular/material/slider */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/slider.js");
/* harmony import */ var _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! @angular/material/slide-toggle */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/slide-toggle.js");
/* harmony import */ var _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! @angular/material/snack-bar */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/snack-bar.js");
/* harmony import */ var _angular_material_sort__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! @angular/material/sort */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/sort.js");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/table.js");
/* harmony import */ var _angular_material_tabs__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! @angular/material/tabs */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/tabs.js");
/* harmony import */ var _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! @angular/material/toolbar */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/toolbar.js");
/* harmony import */ var _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! @angular/material/tooltip */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/tooltip.js");
/* harmony import */ var _angular_material_tree__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! @angular/material/tree */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/tree.js");
/* harmony import */ var _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! @angular/cdk/overlay */ "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/overlay.js");













































class MaterialModule {
}
MaterialModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: MaterialModule });
MaterialModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function MaterialModule_Factory(t) { return new (t || MaterialModule)(); }, imports: [_angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_1__["A11yModule"],
        _angular_cdk_clipboard__WEBPACK_IMPORTED_MODULE_2__["ClipboardModule"],
        _angular_cdk_stepper__WEBPACK_IMPORTED_MODULE_6__["CdkStepperModule"],
        _angular_cdk_table__WEBPACK_IMPORTED_MODULE_7__["CdkTableModule"],
        _angular_cdk_tree__WEBPACK_IMPORTED_MODULE_8__["CdkTreeModule"],
        _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__["DragDropModule"],
        _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_9__["MatAutocompleteModule"],
        _angular_material_badge__WEBPACK_IMPORTED_MODULE_10__["MatBadgeModule"],
        _angular_material_bottom_sheet__WEBPACK_IMPORTED_MODULE_11__["MatBottomSheetModule"],
        _angular_material_button__WEBPACK_IMPORTED_MODULE_12__["MatButtonModule"],
        _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_13__["MatButtonToggleModule"],
        _angular_material_card__WEBPACK_IMPORTED_MODULE_14__["MatCardModule"],
        _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_15__["MatCheckboxModule"],
        _angular_material_chips__WEBPACK_IMPORTED_MODULE_16__["MatChipsModule"],
        _angular_material_stepper__WEBPACK_IMPORTED_MODULE_17__["MatStepperModule"],
        _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_18__["MatDatepickerModule"],
        _angular_material_dialog__WEBPACK_IMPORTED_MODULE_19__["MatDialogModule"],
        _angular_material_divider__WEBPACK_IMPORTED_MODULE_20__["MatDividerModule"],
        _angular_material_expansion__WEBPACK_IMPORTED_MODULE_21__["MatExpansionModule"],
        _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_22__["MatGridListModule"],
        _angular_material_icon__WEBPACK_IMPORTED_MODULE_23__["MatIconModule"],
        _angular_material_input__WEBPACK_IMPORTED_MODULE_24__["MatInputModule"],
        _angular_material_list__WEBPACK_IMPORTED_MODULE_25__["MatListModule"],
        _angular_material_menu__WEBPACK_IMPORTED_MODULE_26__["MatMenuModule"],
        _angular_material_core__WEBPACK_IMPORTED_MODULE_27__["MatNativeDateModule"],
        _angular_material_paginator__WEBPACK_IMPORTED_MODULE_28__["MatPaginatorModule"],
        _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_29__["MatProgressBarModule"],
        _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_30__["MatProgressSpinnerModule"],
        _angular_material_radio__WEBPACK_IMPORTED_MODULE_31__["MatRadioModule"],
        _angular_material_core__WEBPACK_IMPORTED_MODULE_27__["MatRippleModule"],
        _angular_material_select__WEBPACK_IMPORTED_MODULE_32__["MatSelectModule"],
        _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_33__["MatSidenavModule"],
        _angular_material_slider__WEBPACK_IMPORTED_MODULE_34__["MatSliderModule"],
        _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_35__["MatSlideToggleModule"],
        _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_36__["MatSnackBarModule"],
        _angular_material_sort__WEBPACK_IMPORTED_MODULE_37__["MatSortModule"],
        _angular_material_table__WEBPACK_IMPORTED_MODULE_38__["MatTableModule"],
        _angular_material_tabs__WEBPACK_IMPORTED_MODULE_39__["MatTabsModule"],
        _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_40__["MatToolbarModule"],
        _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_41__["MatTooltipModule"],
        _angular_material_tree__WEBPACK_IMPORTED_MODULE_42__["MatTreeModule"],
        _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_43__["OverlayModule"],
        _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_4__["PortalModule"],
        _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_5__["ScrollingModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](MaterialModule, { exports: [_angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_1__["A11yModule"],
        _angular_cdk_clipboard__WEBPACK_IMPORTED_MODULE_2__["ClipboardModule"],
        _angular_cdk_stepper__WEBPACK_IMPORTED_MODULE_6__["CdkStepperModule"],
        _angular_cdk_table__WEBPACK_IMPORTED_MODULE_7__["CdkTableModule"],
        _angular_cdk_tree__WEBPACK_IMPORTED_MODULE_8__["CdkTreeModule"],
        _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__["DragDropModule"],
        _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_9__["MatAutocompleteModule"],
        _angular_material_badge__WEBPACK_IMPORTED_MODULE_10__["MatBadgeModule"],
        _angular_material_bottom_sheet__WEBPACK_IMPORTED_MODULE_11__["MatBottomSheetModule"],
        _angular_material_button__WEBPACK_IMPORTED_MODULE_12__["MatButtonModule"],
        _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_13__["MatButtonToggleModule"],
        _angular_material_card__WEBPACK_IMPORTED_MODULE_14__["MatCardModule"],
        _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_15__["MatCheckboxModule"],
        _angular_material_chips__WEBPACK_IMPORTED_MODULE_16__["MatChipsModule"],
        _angular_material_stepper__WEBPACK_IMPORTED_MODULE_17__["MatStepperModule"],
        _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_18__["MatDatepickerModule"],
        _angular_material_dialog__WEBPACK_IMPORTED_MODULE_19__["MatDialogModule"],
        _angular_material_divider__WEBPACK_IMPORTED_MODULE_20__["MatDividerModule"],
        _angular_material_expansion__WEBPACK_IMPORTED_MODULE_21__["MatExpansionModule"],
        _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_22__["MatGridListModule"],
        _angular_material_icon__WEBPACK_IMPORTED_MODULE_23__["MatIconModule"],
        _angular_material_input__WEBPACK_IMPORTED_MODULE_24__["MatInputModule"],
        _angular_material_list__WEBPACK_IMPORTED_MODULE_25__["MatListModule"],
        _angular_material_menu__WEBPACK_IMPORTED_MODULE_26__["MatMenuModule"],
        _angular_material_core__WEBPACK_IMPORTED_MODULE_27__["MatNativeDateModule"],
        _angular_material_paginator__WEBPACK_IMPORTED_MODULE_28__["MatPaginatorModule"],
        _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_29__["MatProgressBarModule"],
        _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_30__["MatProgressSpinnerModule"],
        _angular_material_radio__WEBPACK_IMPORTED_MODULE_31__["MatRadioModule"],
        _angular_material_core__WEBPACK_IMPORTED_MODULE_27__["MatRippleModule"],
        _angular_material_select__WEBPACK_IMPORTED_MODULE_32__["MatSelectModule"],
        _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_33__["MatSidenavModule"],
        _angular_material_slider__WEBPACK_IMPORTED_MODULE_34__["MatSliderModule"],
        _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_35__["MatSlideToggleModule"],
        _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_36__["MatSnackBarModule"],
        _angular_material_sort__WEBPACK_IMPORTED_MODULE_37__["MatSortModule"],
        _angular_material_table__WEBPACK_IMPORTED_MODULE_38__["MatTableModule"],
        _angular_material_tabs__WEBPACK_IMPORTED_MODULE_39__["MatTabsModule"],
        _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_40__["MatToolbarModule"],
        _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_41__["MatTooltipModule"],
        _angular_material_tree__WEBPACK_IMPORTED_MODULE_42__["MatTreeModule"],
        _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_43__["OverlayModule"],
        _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_4__["PortalModule"],
        _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_5__["ScrollingModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](MaterialModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                exports: [
                    _angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_1__["A11yModule"],
                    _angular_cdk_clipboard__WEBPACK_IMPORTED_MODULE_2__["ClipboardModule"],
                    _angular_cdk_stepper__WEBPACK_IMPORTED_MODULE_6__["CdkStepperModule"],
                    _angular_cdk_table__WEBPACK_IMPORTED_MODULE_7__["CdkTableModule"],
                    _angular_cdk_tree__WEBPACK_IMPORTED_MODULE_8__["CdkTreeModule"],
                    _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__["DragDropModule"],
                    _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_9__["MatAutocompleteModule"],
                    _angular_material_badge__WEBPACK_IMPORTED_MODULE_10__["MatBadgeModule"],
                    _angular_material_bottom_sheet__WEBPACK_IMPORTED_MODULE_11__["MatBottomSheetModule"],
                    _angular_material_button__WEBPACK_IMPORTED_MODULE_12__["MatButtonModule"],
                    _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_13__["MatButtonToggleModule"],
                    _angular_material_card__WEBPACK_IMPORTED_MODULE_14__["MatCardModule"],
                    _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_15__["MatCheckboxModule"],
                    _angular_material_chips__WEBPACK_IMPORTED_MODULE_16__["MatChipsModule"],
                    _angular_material_stepper__WEBPACK_IMPORTED_MODULE_17__["MatStepperModule"],
                    _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_18__["MatDatepickerModule"],
                    _angular_material_dialog__WEBPACK_IMPORTED_MODULE_19__["MatDialogModule"],
                    _angular_material_divider__WEBPACK_IMPORTED_MODULE_20__["MatDividerModule"],
                    _angular_material_expansion__WEBPACK_IMPORTED_MODULE_21__["MatExpansionModule"],
                    _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_22__["MatGridListModule"],
                    _angular_material_icon__WEBPACK_IMPORTED_MODULE_23__["MatIconModule"],
                    _angular_material_input__WEBPACK_IMPORTED_MODULE_24__["MatInputModule"],
                    _angular_material_list__WEBPACK_IMPORTED_MODULE_25__["MatListModule"],
                    _angular_material_menu__WEBPACK_IMPORTED_MODULE_26__["MatMenuModule"],
                    _angular_material_core__WEBPACK_IMPORTED_MODULE_27__["MatNativeDateModule"],
                    _angular_material_paginator__WEBPACK_IMPORTED_MODULE_28__["MatPaginatorModule"],
                    _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_29__["MatProgressBarModule"],
                    _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_30__["MatProgressSpinnerModule"],
                    _angular_material_radio__WEBPACK_IMPORTED_MODULE_31__["MatRadioModule"],
                    _angular_material_core__WEBPACK_IMPORTED_MODULE_27__["MatRippleModule"],
                    _angular_material_select__WEBPACK_IMPORTED_MODULE_32__["MatSelectModule"],
                    _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_33__["MatSidenavModule"],
                    _angular_material_slider__WEBPACK_IMPORTED_MODULE_34__["MatSliderModule"],
                    _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_35__["MatSlideToggleModule"],
                    _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_36__["MatSnackBarModule"],
                    _angular_material_sort__WEBPACK_IMPORTED_MODULE_37__["MatSortModule"],
                    _angular_material_table__WEBPACK_IMPORTED_MODULE_38__["MatTableModule"],
                    _angular_material_tabs__WEBPACK_IMPORTED_MODULE_39__["MatTabsModule"],
                    _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_40__["MatToolbarModule"],
                    _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_41__["MatTooltipModule"],
                    _angular_material_tree__WEBPACK_IMPORTED_MODULE_42__["MatTreeModule"],
                    _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_43__["OverlayModule"],
                    _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_4__["PortalModule"],
                    _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_5__["ScrollingModule"],
                ]
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/security/token-interceptor.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/security/token-interceptor.service.ts ***!
  \*******************************************************/
/*! exports provided: TokenInterceptorService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TokenInterceptorService", function() { return TokenInterceptorService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_token_storage_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/token-storage.service */ "./src/app/services/token-storage.service.ts");



;
class TokenInterceptorService {
    constructor(token) {
        this.token = token;
    }
    intercept(request, next) {
        request = request.clone({
            setHeaders: {
                Authorization: `Bearer ${this.token.getToken()}`
            }
        });
        return next.handle(request);
    }
}
TokenInterceptorService.ɵfac = function TokenInterceptorService_Factory(t) { return new (t || TokenInterceptorService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_services_token_storage_service__WEBPACK_IMPORTED_MODULE_1__["TokenStorageService"])); };
TokenInterceptorService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: TokenInterceptorService, factory: TokenInterceptorService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TokenInterceptorService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _services_token_storage_service__WEBPACK_IMPORTED_MODULE_1__["TokenStorageService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/services/auth.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/auth.service.ts ***!
  \******************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _baseUrl__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../baseUrl */ "./src/app/baseUrl.ts");





const headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpHeaders"]();
headers.append('Content-Type', 'multipart/form-data');
headers.append('Accept', 'application/json');
class AuthService {
    constructor(http) {
        this.http = http;
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpHeaders"]().set('Content-Type', 'application/json');
    }
    login(email, password) {
        return this.http.post(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + '/api/sign/in', {
            email,
            password
        }, { withCredentials: false });
    }
}
AuthService.ɵfac = function AuthService_Factory(t) { return new (t || AuthService)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"])); };
AuthService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: AuthService, factory: AuthService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](AuthService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "./src/app/services/crm/test-eligibilite.service.ts":
/*!**********************************************************!*\
  !*** ./src/app/services/crm/test-eligibilite.service.ts ***!
  \**********************************************************/
/*! exports provided: TestEligibiliteService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestEligibiliteService", function() { return TestEligibiliteService; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_app_baseUrl__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/baseUrl */ "./src/app/baseUrl.ts");





const headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpHeaders"]();
headers.append('Content-Type', 'multipart/form-data');
headers.append('Accept', 'application/json');
class TestEligibiliteService {
    constructor(http) {
        this.http = http;
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpHeaders"]().set('Content-Type', 'application/json');
    }
    /*********************** test/activities/get ****************************/
    getActivites() {
        return this.http.get(src_app_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/activities/get");
    }
    /*********************** test/transitions/get ****************************/
    getTransitions() {
        return this.http.get(src_app_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/transitions/get");
    }
    /*********************** test/grants/region/get ****************************/
    regionalGrant(region, budget, naf) {
        return this.http.get(src_app_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/grants/region/" + region + "/" + budget + "/" + naf);
    }
    /*********************** test/grants/cpn/get ****************************/
    cpnGrant(service, budget) {
        return this.http.get(src_app_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/grants/cpn/" + service + "/" + budget);
    }
    /*********************** test/events/get ****************************/
    getEvents() {
        return this.http.get(src_app_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/events/get");
    }
    /*********************** test/events/add ****************************/
    addEvents(form) {
        return this.http.post(src_app_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/events/add", form);
    }
    /*********************** test/service/turnover ****************************/
    getServiceTurnover(range) {
        return this.http.get(src_app_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/service/turnover/" + range[0] + "/" + range[1], { withCredentials: false });
    }
    /*********************** test/company/siren ****************************/
    getCompanySiren(siren) {
        return this.http.get(src_app_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/company/siren/" + { siren }, { withCredentials: false });
    }
    /*********************** test/contact/save ****************************/
    addContact(form) {
        return this.http.post(src_app_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/contact/save", form, { withCredentials: false });
    }
    /*********************** test/contact/confirm ****************************/
    contactConfirm(form) {
        return this.http.post(src_app_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/contact/confirm", form, { withCredentials: false });
    }
    /*********************** test/zoom/generate ****************************/
    addZoom(form) {
        return this.http.post(src_app_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/zoom/generate", form, { withCredentials: false });
    }
    /*********************** test/timer/save ****************************/
    addTimer(form) {
        return this.http.post(src_app_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/timer/save", form, { withCredentials: false });
    }
}
TestEligibiliteService.ɵfac = function TestEligibiliteService_Factory(t) { return new (t || TestEligibiliteService)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"])); };
TestEligibiliteService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: TestEligibiliteService, factory: TestEligibiliteService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](TestEligibiliteService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "./src/app/services/token-storage.service.ts":
/*!***************************************************!*\
  !*** ./src/app/services/token-storage.service.ts ***!
  \***************************************************/
/*! exports provided: TokenStorageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TokenStorageService", function() { return TokenStorageService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';
const projet = 'projet';
class TokenStorageService {
    constructor() { }
    signOut() {
        window.sessionStorage.clear();
    }
    saveToken(token) {
        window.sessionStorage.removeItem(TOKEN_KEY);
        window.sessionStorage.setItem(TOKEN_KEY, token);
    }
    getToken() {
        return window.sessionStorage.getItem(TOKEN_KEY);
    }
    getUSERKEY() {
        return window.sessionStorage.getItem(USER_KEY);
    }
    saveUser(user) {
        window.sessionStorage.removeItem(USER_KEY);
        window.sessionStorage.setItem(USER_KEY, JSON.stringify(user));
    }
    saveProjectId(id) {
        window.sessionStorage.setItem(projet, id);
    }
    getProjectId() {
        window.sessionStorage.getItem(projet);
    }
    getUser() {
        if (window.sessionStorage.getItem(USER_KEY)) {
            const user = window.sessionStorage.getItem(USER_KEY);
            if (user) {
                return user;
            }
        }
        return {};
    }
}
TokenStorageService.ɵfac = function TokenStorageService_Factory(t) { return new (t || TokenStorageService)(); };
TokenStorageService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: TokenStorageService, factory: TokenStorageService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TokenStorageService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/test/test.component.ts":
/*!****************************************!*\
  !*** ./src/app/test/test.component.ts ***!
  \****************************************/
/*! exports provided: TestComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestComponent", function() { return TestComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _services_crm_test_eligibilite_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/crm/test-eligibilite.service */ "./src/app/services/crm/test-eligibilite.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");
/* harmony import */ var _map_french_map_french_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../map-french/map-french.component */ "./src/app/map-french/map-french.component.ts");
/* harmony import */ var _angular_material_radio__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/radio */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/radio.js");
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/list */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/list.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/icon.js");
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/core */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _fullcalendar_angular__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @fullcalendar/angular */ "./node_modules/@fullcalendar/angular/__ivy_ngcc__/fesm2015/fullcalendar-angular.js");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/form-field.js");















function TestComponent_div_3_Template(rf, ctx) { if (rf & 1) {
    const _r22 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Testez votre \u00E9ligibilit\u00E9");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "button", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_3_Template_button_click_6_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r22); const ctx_r21 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r21.checkForm(ctx_r21.test.active.step); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "Commencer");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 31);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "app-map-french");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Choisissez votre r\u00E9gion");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Quel est votre secteur d'activit\u00E9 ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "input", 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c0 = function (a0) { return { butttonREd: a0 }; };
function TestComponent_div_6_Template(rf, ctx) { if (rf & 1) {
    const _r24 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 35);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 36);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Status juridique");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 37);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "button", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_6_Template_button_click_8_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r24); const ctx_r23 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r23.getstatus("SARL"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "SARL");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "button", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_6_Template_button_click_10_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r24); const ctx_r25 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r25.getstatus("SAS"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "SAS");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "button", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_6_Template_button_click_13_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r24); const ctx_r26 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r26.getstatus("SASU"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "SASU");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "button", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_6_Template_button_click_15_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r24); const ctx_r27 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r27.getstatus("EURL"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "EURL");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "button", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_6_Template_button_click_17_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r24); const ctx_r28 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r28.getstatus("MICRO-ENT"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "MICRO-ENT");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](5, _c0, ctx_r3.testEgibFormGroup.get("status").value === "SARL"));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](7, _c0, ctx_r3.testEgibFormGroup.get("status").value === "SAS"));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](9, _c0, ctx_r3.testEgibFormGroup.get("status").value === "SASU"));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](11, _c0, ctx_r3.testEgibFormGroup.get("status").value === "EURL"));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](13, _c0, ctx_r3.testEgibFormGroup.get("status").value === "MICRO-ENT"));
} }
function TestComponent_div_7_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 40);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Nom de votre entreprise");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "input", 42);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_8_Template(rf, ctx) { if (rf & 1) {
    const _r31 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Avez vous perdu du chiffre d'affaires pendant la crise sanitaire ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 37);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Baisse");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "input", 46, 47);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 48);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "button", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_8_Template_button_click_14_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r31); const ctx_r30 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r30.incTurn(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "+");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "button", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_8_Template_button_click_16_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r31); const ctx_r32 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r32.decTurn(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "-");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "Augmentation");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](22, "input", 49);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div", 48);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "button", 50);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "+");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "button", 50);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "-");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "mat-radio-button", 51);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, "Etat Stable");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("readonly", true);
} }
function TestComponent_div_9_option_7_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "option", 56);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const item_r34 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r34);
} }
function TestComponent_div_9_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 52);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Avez vous d\u00E9ja obtenu des aides de l'\u00E9tat");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "img", 53);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "select", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, TestComponent_div_9_option_7_Template, 2, 1, "option", 55);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r6.test.data[ctx_r6.test.active.step].options);
} }
function TestComponent_div_10_option_11_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "option", 56);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const item_r36 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r36);
} }
function TestComponent_div_10_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 57);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 58);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "img", 59);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Dernier chiffre d'affaires r\u00E9alis\u00E9 ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 60);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 61);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "input", 62);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "datalist", 63);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](11, TestComponent_div_10_option_11_Template, 2, 1, "option", 55);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r7.test.data[7].labels);
} }
function TestComponent_div_11_option_7_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "option");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const item_r38 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r38);
} }
function TestComponent_div_11_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 64);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 65);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Nombre de salari\u00E9s ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "select", 66);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, TestComponent_div_11_option_7_Template, 2, 1, "option", 67);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r8.test.data[ctx_r8.test.active.step].options);
} }
function TestComponent_div_12_div_1_div_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 73);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 74);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Avez vous un site internet pour votre entreprise ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "mat-radio-group", 75);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-radio-button", 76);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Oui");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "mat-radio-button", 77);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Non");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_12_div_1_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 78);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 79);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Quel est votre type de site internet ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "mat-radio-group", 80);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-radio-button", 81);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "E-commerce");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "mat-radio-button", 82);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Vitrine");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "mat-radio-button", 83);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Market-place");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 84);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "Nombre de Vente");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "input", 49);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 48);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "button", 50);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "+");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "button", 50);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "-");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "Nombre de Visite");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](28, "input", 85);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "div", 48);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "button", 50);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "+");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "button", 50);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "-");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "div", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](36, "Nombre d'utilisateurs");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](38, "input", 86);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "div", 48);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "button", 50);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](41, "+");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "button", 50);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43, "-");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_12_div_1_div_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 87);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 88);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Lien du site ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "input", 89);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_12_div_1_div_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 90);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "D\u00E2te de d\u00E9veloppement ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "img", 91);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "select", 92);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "option", 93);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Ch\u00E9que num\u00E9rique et aide num\u00E9rique de votre r\u00E9gion");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_12_div_1_div_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 94);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 88);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "L'agence qui a d\u00E9velopp\u00E9 votre site ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "input", 89);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-radio-button", 81);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Internet/Freelance");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_12_div_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TestComponent_div_12_div_1_div_1_Template, 11, 0, "div", 68);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, TestComponent_div_12_div_1_div_2_Template, 44, 0, "div", 69);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, TestComponent_div_12_div_1_div_3_Template, 7, 0, "div", 70);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, TestComponent_div_12_div_1_div_4_Template, 9, 0, "div", 71);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, TestComponent_div_12_div_1_div_5_Template, 9, 0, "div", 72);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r39 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r39.test.active.subStep == 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r39.test.active.subStep == 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r39.test.active.subStep == 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r39.test.active.subStep == 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r39.test.active.subStep == 5);
} }
function TestComponent_div_12_div_2_div_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 99);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 100);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Utilisez-vous des Plate-formes en ligne ou des logiciels pour faciliter vos ventes ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "mat-radio-group", 101);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-radio-button", 76);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Oui");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "mat-radio-button", 77);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Non");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_12_div_2_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 102);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Quels types d'outils utilisez-vous pour vos ventes ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "(Exemple:un CRM)");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "select", 92);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "option", 93);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "img", 103);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, " Sage");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_12_div_2_div_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 104);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 105);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Utilisez-vous des Plate-formes en ligne ou des logiciels pour faciliter votre logistique interne ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "mat-radio-group", 80);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-radio-button", 81);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Oui");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "mat-radio-button", 82);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Non");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_12_div_2_div_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 106);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Quels types d'outils utilisez-vous pour votre logistique ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "(Exemple:un ERP)");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "select", 92);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "option", 93);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "img", 103);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, " Sage");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_12_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TestComponent_div_12_div_2_div_1_Template, 11, 0, "div", 95);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, TestComponent_div_12_div_2_div_2_Template, 11, 0, "div", 96);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, TestComponent_div_12_div_2_div_3_Template, 11, 0, "div", 97);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, TestComponent_div_12_div_2_div_4_Template, 11, 0, "div", 98);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r40 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r40.test.active.subStep == 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r40.test.active.subStep == 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r40.test.active.subStep == 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r40.test.active.subStep == 4);
} }
function TestComponent_div_12_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TestComponent_div_12_div_1_Template, 6, 5, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, TestComponent_div_12_div_2_Template, 5, 4, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r9.test.active.subStepCat == 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r9.test.active.subStepCat == 2);
} }
function TestComponent_div_13_mat_list_item_8_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-list-item");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-icon", 110);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "folder");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "h4", 111);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "mat-radio-group", 112);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "mat-radio-button", 113);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const item_r51 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r51.name);
} }
function TestComponent_div_13_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 107);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "label", 108);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Service");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "input", 89);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 109);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-list");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](8, TestComponent_div_13_mat_list_item_8_Template, 7, 1, "mat-list-item", 67);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r10.transition);
} }
function TestComponent_div_14_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 114);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Budget d'investissement");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "img", 53);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "select", 115);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "option", 116);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "300$");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "option", 117);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "400$");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "option", 118);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "500$");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "option", 119);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "600$");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_15_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 120);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 121);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Num\u00E9ro de Siret");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "input", 122);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_16_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 123);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 124);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Adresse");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 125);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "label", 108);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Adresse");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "input", 126);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 127);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "label", 108);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "ville");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "input", 128);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "label", 108);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "code postal");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "input", 129);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_17_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 130);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Fiche de rensignement");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "img", 131);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 125);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "label", 108);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Nom");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "input", 132);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 125);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "label", 108);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Pr\u00E9nom");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "input", 133);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 125);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "label", 108);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "mail");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "input", 134);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 125);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "label", 108);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "Num\u00E9ro de portable");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "input", 135);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 125);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "label", 108);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, "Num\u00E9ro d'entreprise");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "input", 136);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 127);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "mat-radio-group", 137);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "mat-radio-button", 138);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "G\u00E9rant");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "mat-radio-button", 139);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "Associ\u00E9");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "mat-radio-button", 140);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "Dir\u00E9cteur");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "mat-radio-button", 141);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35, "autre");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_18_Template(rf, ctx) { if (rf & 1) {
    const _r53 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 142);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 143);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "F\u00E9licitaion");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "vous \u00EAtes \u00E9ligible");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "img", 144);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Vous b\u00E9neficiez d'un ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "strong");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Ch\u00E8que Num\u00E9rique");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "de ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "strong", 145);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "offert par le ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "strong");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "CPN");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "img", 146);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 147);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_18_Template_div_click_22_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r53); const ctx_r52 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r52.nextStep(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "img", 148);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", ctx_r15.test.result.cpn.amount, " \u20AC");
} }
function TestComponent_div_19_Template(rf, ctx) { if (rf & 1) {
    const _r55 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 142);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 143);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "F\u00E9licitaion");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "vous \u00EAtes \u00E9ligible");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "img", 144);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Vous b\u00E9neficiez d'un ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "strong");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Ch\u00E8que commerce connect\u00E9");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "de ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "strong", 145);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "5000 $");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "offert par le ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "strong");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "CPN");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "img", 146);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 147);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_19_Template_div_click_22_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r55); const ctx_r54 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r54.nextStep(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "img", 148);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_20_Template(rf, ctx) { if (rf & 1) {
    const _r57 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 142);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "h1", 149);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Malheureusement");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h1", 149);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "vous n'\u00EAtes pas \u00E9ligible");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "img", 150);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "\u00E0 l'aide de votre r\u00E9gion");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 147);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_20_Template_div_click_10_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r57); const ctx_r56 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r56.nextStep(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "img", 148);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_21_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 151);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 152);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Vos disponibilit\u00E9s");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 153);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "full-calendar", 154, 155);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "mat-radio-group", 80);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "mat-radio-button", 81);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "Entretien vid\u00E9o direct");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "mat-radio-button", 82);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Visite de courtoisie");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r18 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("options", ctx_r18.calendarOption);
} }
function TestComponent_div_22_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 156);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "img", 157);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "F\u00E9licitaion");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Un conseiller entrera en contact avec");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, " vous dans 30min");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 158);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "Suivez-nous");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 159);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "img", 160);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "img", 161);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "img", 162);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "img", 163);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c1 = function (a0) { return { checkIcon: a0 }; };
function TestComponent_div_27_Template(rf, ctx) { if (rf & 1) {
    const _r60 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 164);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "a", 165);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_27_Template_a_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r60); const ctx_r59 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r59.prevStep(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "mat-icon", 166);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "arrow_back_ios");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 167);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "mat-icon", 168);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "circle");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "mat-icon", 168);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "circle");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "mat-icon", 168);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "circle");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "mat-icon", 168);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "circle");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "mat-icon", 168);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "circle");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "mat-icon", 168);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "circle");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "mat-icon", 168);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "circle");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "mat-icon", 168);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, "circle");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "mat-icon", 168);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "circle");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "mat-icon", 168);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34, "circle");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "mat-icon", 168);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37, "circle");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "mat-icon", 168);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](40, "circle");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "mat-icon", 168);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43, "circle");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "mat-icon", 168);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, "circle");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "mat-icon", 168);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](49, "circle");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "mat-icon", 168);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](52, "circle");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "a", 165);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_27_Template_a_click_53_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r60); const ctx_r61 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r61.checkForm(ctx_r61.test.active.step); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "mat-icon", 166);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](55, "arrow_forward_ios");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r20 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](16, _c1, ctx_r20.test.active.step == 1));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](18, _c1, ctx_r20.test.active.step == 2));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](20, _c1, ctx_r20.test.active.step == 3));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](22, _c1, ctx_r20.test.active.step == 4));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](24, _c1, ctx_r20.test.active.step == 5));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](26, _c1, ctx_r20.test.active.step == 6));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](28, _c1, ctx_r20.test.active.step == 7));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](30, _c1, ctx_r20.test.active.step == 8));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](32, _c1, ctx_r20.test.active.step == 9));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](34, _c1, ctx_r20.test.active.step == 10));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](36, _c1, ctx_r20.test.active.step == 11));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](38, _c1, ctx_r20.test.active.step == 12));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](40, _c1, ctx_r20.test.active.step == 13));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](42, _c1, ctx_r20.test.active.step == 14));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](44, _c1, ctx_r20.test.active.step == 15));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](46, _c1, ctx_r20.test.active.step == 16));
} }
class TestComponent {
    /***********************************life cycle *******************/
    constructor(_formBuilder, testService) {
        this._formBuilder = _formBuilder;
        this.testService = testService;
        this.i = 9;
        /**********************************************turnover ******************************************/
        this.turn = 1;
        /*****************************************test step **************************************/
        this.test = {
            active: {
                step: 0,
                subStep: 1,
                subStepCat: 0,
                stepType: "form",
                popup: false,
                confirmed: false,
            },
            result: {
                isOpen: false,
                isLoading: false,
                isCpn: true,
                regional: {
                    id: null,
                    region: null,
                    eligible: false,
                    voucher: null,
                    amount: null,
                },
                cpn: {
                    id: null,
                    amount: null,
                    originalPrice: null,
                    sellPrice: null,
                },
            },
            zoom: {
                generating: false,
                generated: false,
            },
            orientations: [],
            data: [
                {
                    step: 0,
                    title: "Bienvenue"
                },
                {
                    step: 1,
                    title: "Renseigner le code postal",
                },
                {
                    step: 2,
                    title: "Nom de l'entreprise"
                },
                {
                    step: 3,
                    title: "Statut juridique"
                },
                {
                    step: 4,
                    title: "Secteur d'activité",
                    options: [],
                },
                {
                    step: 5,
                    title: "Avez vous perdu du chiffre d'affaires pendant la crise sanitaire",
                    labels: [
                        "Baisse",
                        "",
                        "",
                        "",
                        "",
                        "-50%",
                        "",
                        "",
                        "",
                        "",
                        "Stable",
                        "",
                        "",
                        "",
                        "",
                        "50%",
                        "",
                        "",
                        "",
                        "",
                        "Hausse"
                    ],
                },
                {
                    step: 6,
                    title: "Avez vous déja obtenu des aides de l'état",
                    options: [
                        "Chéque numérique et aide numérique de votre région",
                        "Crédit d'impôt",
                        "Fond de solidarité",
                        "Chaumage partiel",
                        "Aucune aide",
                    ],
                },
                {
                    step: 7,
                    title: "Dernier chiffre d'affaires réalisé",
                    labels: [
                        "5k €",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "700k €",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "3.5m €",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "7.5m €",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "30m €",
                    ],
                    selectedRange: [0, 1],
                    range: [
                        5000,
                        50000,
                        100000,
                        200000,
                        300000,
                        400000,
                        500000,
                        600000,
                        700000,
                        800000,
                        900000,
                        1000000,
                        1500000,
                        2000000,
                        2500000,
                        3000000,
                        3500000,
                        4000000,
                        4500000,
                        5000000,
                        5500000,
                        6000000,
                        6500000,
                        7000000,
                        7500000,
                        8000000,
                        8500000,
                        9000000,
                        9500000,
                        10000000,
                        15000000,
                        20000000,
                        25000000,
                        30000000,
                    ],
                },
                {
                    step: 8,
                    title: "Nombre de salariés",
                    options: [
                        "de 0 à 5 Personnes",
                        "de 5 à 10 Personnes",
                        "de 10 à 20 Personnes",
                        "de 20 à 30 Personnes",
                        "de 30 à 40 Personnes",
                        "de 40 à 50 Personnes",
                        "plus de 50 Personnes",
                    ],
                },
                {
                    step: 9,
                    title: "Type de site",
                    website: [
                        {
                            subStep: 0,
                            title: ""
                        },
                        {
                            subStep: 1,
                            title: "Avez vous un site internet pour votre entreprise"
                        },
                        {
                            subStep: 2,
                            title: "Type de site"
                        },
                        {
                            subStep: 3,
                            title: "Lien de site"
                        },
                        {
                            subStep: 4,
                            title: "Date de développement"
                        },
                        {
                            subStep: 5,
                            title: "L'agence qui a développé votre site"
                        }
                    ],
                    crm: [
                        {
                            subStep: 0,
                            title: ""
                        },
                        {
                            subStep: 1,
                            title: "Avez vous un crm pour votre entreprise"
                        },
                        {
                            subStep: 2,
                            title: "Type de crm"
                        },
                        {
                            subStep: 3,
                            title: "Le crm a été développé"
                        },
                        {
                            subStep: 4,
                            title: "Quel type de ERP vous utilisez",
                            options: [
                                "Zoho",
                                "SAP",
                                "Sage",
                                "Oracle",
                                "NetSuite",
                                "Cegid",
                                "Microsoft Dynamics",
                                "Divalto",
                                "WaveSoft",
                                "Odoo",
                                "Archipelia",
                                "Axonaut",
                            ]
                        },
                        {
                            subStep: 5,
                            title: "Quel type de CRM vous utilisez",
                            options: [
                                "Zoho",
                                "Habspot",
                                "Microsoft Dynamics",
                                "SalesForce",
                                "Pipedrive",
                                "Agile",
                                "Axonaut",
                                "FreshSales",
                                "Teamleader",
                                "Facture",
                                "Crème",
                                "Suite",
                            ]
                        },
                        {
                            subStep: 6,
                            title: "Date de développement"
                        }
                    ]
                },
                {
                    step: 10,
                    title: "Quel projet est à subventionner pour votre transition numérique",
                    tabServices: null,
                    loading: false,
                    services: ["Services éligible", "Services suplémentaire"],
                    tabCategories: null,
                    categories: ["Tous", "Graphique", "Développement", "Montage", "Marketing"],
                    options: [],
                },
                {
                    step: 11,
                    title: "Budget d'investissement",
                    budget: 5,
                    min: 400,
                    target: 500,
                    max: 100000,
                },
                {
                    step: 12,
                    title: "Numéros d'identification",
                    loading: false,
                },
                {
                    step: 13,
                    title: "Adresse",
                },
                {
                    step: 14,
                    title: "Fiche de renseignement",
                    options: [
                        "Gérant",
                        "Directeur",
                        "Associé",
                        "Autre"
                    ],
                },
                {
                    step: 15,
                    title: "Vos disponibilités",
                },
                {
                    step: 16,
                    title: "Type de client",
                    items: ['☹️', '🙁', '😐', '🙂', '😊', '😍'],
                    labels: [
                        "agressif",
                        "indécis",
                        "anxieux",
                        "économe",
                        "compréhensif",
                        "roi",
                    ]
                },
                {
                    step: 17,
                    title: "Merci pour votre temps",
                },
            ],
        };
        /*************************form  ************************/
        this.testEgibFormGroup = this._formBuilder.group({
            codeP: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern("[0-9 ]{5}")]],
            nomSoc: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            activite: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            status: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            help: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            personneSal: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            turnover: [50, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            lastTurnover: [0, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            haveSite: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            haveCrm: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            liensite: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            datesite: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            siteVal: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            dateCrm: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            typeCRM: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            typeERP: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            typeSite: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            crmDev: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            agence: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            budget: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            service: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            siret: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern("[0-9 ]{14}")]],
            siren: [''],
            naf: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            adresse: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            zipcode: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern("[0-9 ]{5}")]],
            region: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            city: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            country: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            prenom: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            nom: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            phone: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            phoneEntrep: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            post: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
        });
        /*************************************calandreir ***************************/
        this.calendarOption = {
            customButtons: {
                myCustomButton: {
                    text: 'custom!',
                    click: function () {
                        alert('clicked the custom button!');
                    }
                }
            },
            locale: "fr",
            initialView: 'dayGridMonth',
            //initialEvents: INITIAL_EVENTS, // alternatively, use the events setting to fetch from a feed
            weekends: true,
            editable: true,
            selectable: true,
            selectMirror: true,
            droppable: false,
            displayEventTime: true,
            disableDragging: false,
            timeZone: 'UTC',
            refetchResourcesOnNavigate: true,
            headerToolbar: {
                left: 'prev,next today',
                center: 'title',
                right: 'dayGridMonth,dayGridWeek,dayGridDay'
            },
            dayMaxEvents: true,
        };
    }
    ngOnInit() {
        this.getTransition();
    }
    /*********************** selection turnover ******************/
    selectedRange(val) {
        this.testEgibFormGroup.value.turnover = val;
    }
    /***********************select transition ******************/
    getTransition() {
        this.testService.getTransitions().subscribe(res => {
            this.transition = res.data;
            console.log("transition", this.transition);
            this.elegible = res.data.filter(data => data.category === 'Services');
            this.graphic = res.data.filter(data => data.category === 'Graphique');
            this.montage = res.data.filter(data => data.category === 'Montage');
            this.marketing = res.data.filter(data => data.category === 'Marketing');
            this.development = res.data.filter(data => data.category === 'Développement');
            console.log("elegi", this.elegible);
            console.log("suplimet", this.development);
        });
    }
    incTurn() {
        this.turn++;
        this.testEgibFormGroup.get('turnover').setValue(this.turn);
    }
    decTurn() {
        this.turn--;
        this.testEgibFormGroup.get('turnover').setValue(this.turn);
    }
    /**********************************************Last turnover ******************************************/
    setLastTurnover(val) {
        const step = this.test.active.step;
        let range = [
            val[0],
            val[1]
        ];
        console.log('val', val);
        console.log('range', range);
        this.testService.getServiceTurnover(range).subscribe(response => {
            this.testEgibFormGroup.get('service').setValue(response.data.data.transition_id);
            this.testEgibFormGroup.get('lastTurnover').setValue(response.data.data.id);
            this.testEgibFormGroup.get('budget').setValue(Math.ceil(response.data.data.budget / 100) * 100);
            this.test.data[11].budget = Math.ceil(response.data.data.budget / 100);
            this.test.data[11].min = Math.ceil(response.data.data.budget_min / 100) * 100;
            this.test.data[11].target = Math.ceil(response.data.data.budget / 100) * 100;
            this.test.data[11].max = Math.ceil(response.data.data.budget_max / 100) * 100;
        });
    }
    /******************************************************** get naf with siret**********************************/
    getNafCompany(siret) {
        let siren = siret.substring(0, 9);
        this.testEgibFormGroup.get('siren').setValue(siren);
        this.test.data[this.test.active.step].loading = true;
        if (siren.length == 9 && siret.length == 14) {
            this.testService.getCompanySiren(siren).subscribe(response => {
                this.test.data[this.test.active.step].loading = false;
                this.testEgibFormGroup.get('naf').setValue(response.data.ape);
            });
        }
    }
    /******************************************************** save client to database**********************************/
    setContactForm(formData) {
        this.testService.addContact(JSON.stringify({
            advisorName: this.testEgibFormGroup.value.nomSoc,
            contactID: null,
            meetingType: null,
            address: {
                line: this.testEgibFormGroup.value.adresse,
                zipcode: this.testEgibFormGroup.value.zipcode,
                region: this.testEgibFormGroup.value.region,
                departement: this.testEgibFormGroup.value.departement,
                city: this.testEgibFormGroup.value.city,
                country: this.testEgibFormGroup.value.country,
            },
            contacts: {
                firstName: this.testEgibFormGroup.value.nom,
                lastName: this.testEgibFormGroup.value.prenom,
                email: this.testEgibFormGroup.value.email,
                phone: this.testEgibFormGroup.value.phone,
                position: this.testEgibFormGroup.value.post,
                type: 3,
                comment: null,
            },
            companies: {
                name: this.testEgibFormGroup.value.nomSoc,
                status: this.testEgibFormGroup.value.status,
                activity: this.testEgibFormGroup.value.activite,
                help: this.testEgibFormGroup.value.help,
                salaries: this.testEgibFormGroup.value.personneSal,
                siret: this.testEgibFormGroup.value.siret,
                siren: this.testEgibFormGroup.value.siren,
                naf: this.testEgibFormGroup.value.naf,
                phone: this.testEgibFormGroup.value.phoneEntrep,
                turnover: this.testEgibFormGroup.value.turnover,
                lastTurnover: this.testEgibFormGroup.value.lastTurnover,
            },
            development: {
                haveWebsite: null,
                websiteType: null,
                websiteValue: null,
                websiteLink: null,
                websiteDate: null,
                haveCrm: null,
                crmType: null,
                crmDev: null,
                crmName: null,
                erpName: null,
                crmDate: null,
                agencyName: null,
            },
            investment: {
                service: this.testEgibFormGroup.value.service,
                budget: this.testEgibFormGroup.value.budget,
                digitalTransitions: [],
            },
        }))
            .subscribe(response => {
            if (!response.data.error) {
                this.test.formData.contactID = response.data.cid;
            }
        });
    }
    /********************************************************status**********************************/
    getstatus(data) {
        console.log('activi', data);
        this.testEgibFormGroup.get('status').setValue(data);
    }
    /******************************************nextmodule  *************************************************/
    nextStep() {
        this.test.active.step += 1;
    }
    nextSubStep() {
        this.test.active.subStep += 1;
    }
    /******************************************prevmodule  *************************************************/
    prevStep() {
        this.test.active.step -= 1;
    }
    /****************************************************** resultat de test **************************************/
    showResult() {
        console.log("data result", this.testEgibFormGroup.value);
        let budget = this.testEgibFormGroup.value.budget;
        let service = this.testEgibFormGroup.value.service;
        let region = this.testEgibFormGroup.value.region;
        let naf = this.testEgibFormGroup.value.naf;
        console.log("isopen", this.test.result.isOpen);
        console.log("isCpn", this.test.result.isCpn);
        console.log("isLoading", this.test.result.isLoading);
        switch (this.test.result.isOpen) {
            case true:
                if (this.test.result.isCpn) {
                    this.test.result.isCpn = false;
                    this.test.result.isLoading = true;
                    this.testService.regionalGrant(region, budget, naf)
                        .subscribe(response => {
                        console.log("regionalGrant", response);
                        if (response.data.eligible) {
                            this.test.result.regional.id = response.data.id;
                            this.test.result.regional.eligible = response.data.eligible;
                            this.test.result.regional.voucher = response.data.voucher;
                            this.test.result.regional.amount = response.data.amount;
                            this.test.result.regional.region = response.data.region;
                        }
                        else {
                            this.test.result.regional.eligible = response.data.eligible;
                            this.test.result.regional.voucher = null;
                            this.test.result.regional.amount = null;
                        }
                        this.test.result.isLoading = false;
                    });
                    /*  .catch(error=>{
                        this.test.result.isLoading = false;
                        console.log(error)
                      });*/
                }
                else {
                    this.setContactForm(this.test.formData);
                    this.test.result.isCpn = true;
                    this.test.result.isOpen = false;
                    this.nextStep();
                }
                break;
            case false:
                this.test.result.isOpen = true;
                this.test.result.isLoading = true;
                this.testService.cpnGrant(service, budget)
                    .subscribe(response => {
                    console.log("cpnGrant", response);
                    this.test.result.cpn.id = response.data.id;
                    this.test.result.cpn.amount = response.data.grants;
                    this.test.result.cpn.originalPrice = response.data.original_price;
                    this.test.result.cpn.sellPrice = response.data.sell_price;
                    this.test.result.isLoading = false;
                });
                /*.catch(error=>{
                  this.test.result.isLoading = false;
                  console.log(error)
                })*/
                break;
        }
        console.log("test", this.test.result);
    }
    /******************************************************chekform **************************************/
    checkForm(step) {
        console.log('step', step);
        let subStep = this.test.active.subStep;
        let subStepCat = this.test.active.subStepCat;
        switch (step) {
            case 0:
                this.nextStep();
                break;
            case 1:
                this.nextStep();
                break;
            case 2:
                if (this.testEgibFormGroup.value.activite == '') {
                    alert("champ est obligatoir");
                }
                else {
                    this.nextStep();
                }
                break;
            case 3:
                if (this.testEgibFormGroup.value.status == '') {
                    alert("champ est obligatoir");
                }
                else {
                    this.nextStep();
                }
                break;
            case 4:
                if (this.testEgibFormGroup.value.nomSoc == '') {
                    alert("champ est obligatoir");
                }
                else {
                    this.nextStep();
                }
                break;
            case 5:
                if (this.testEgibFormGroup.value.turnover == '') {
                    alert("champ est obligatoir");
                }
                else {
                    this.nextStep();
                }
                break;
            case 6:
                if (this.testEgibFormGroup.value.help == '') {
                    alert("champ est obligatoir");
                }
                else {
                    this.nextStep();
                }
                break;
            case 7:
                this.setLastTurnover([500, 600]);
                this.nextStep();
                break;
            case 8:
                if (this.testEgibFormGroup.value.personneSal == "de 0 à 5 Personnes" || this.testEgibFormGroup.value.personneSal == "de 5 à 10 Personnes") {
                    this.test.active.subStepCat = 1;
                    this.test.active.subStep = 1;
                    this.nextStep();
                }
                else {
                    this.test.active.subStepCat = 2;
                    this.test.active.subStep = 1;
                    this.nextStep();
                }
                break;
            case 9:
                switch (subStepCat) {
                    case 1:
                        switch (subStep) {
                            case 1:
                                if (this.testEgibFormGroup.value.haveSite == "oui") {
                                    this.nextSubStep();
                                }
                                else {
                                    this.nextStep();
                                }
                                ;
                                break;
                            case 2:
                                this.nextSubStep();
                                break;
                            case 3:
                                this.nextSubStep();
                                break;
                            case 4:
                                this.nextSubStep();
                                break;
                            case 5:
                                this.nextStep();
                                break;
                        }
                        break;
                    case 2:
                        switch (subStep) {
                            case 1:
                                if (this.testEgibFormGroup.value.haveCrm == "oui") {
                                    this.nextSubStep();
                                }
                                else {
                                    this.test.active.subStepCat = 1;
                                }
                                ;
                                break;
                            case 2:
                                this.nextSubStep();
                                break;
                            case 3:
                                this.nextSubStep();
                                break;
                            case 4:
                                this.nextSubStep();
                                break;
                            case 5:
                                this.nextSubStep();
                                break;
                            case 6:
                                this.test.active.subStepCat = 1;
                                this.test.active.subStep = 1;
                                break;
                        }
                        break;
                }
                break;
            case 10:
                if (this.testEgibFormGroup.value.service == '' || this.testEgibFormGroup.value.service == null) {
                    alert("champ est obligatoir");
                }
                else {
                    this.nextStep();
                }
                break;
            case 11:
                if (this.testEgibFormGroup.value.budget == '') {
                    alert("champ est obligatoir");
                }
                else {
                    this.nextStep();
                }
                break;
            case 12:
                if (this.testEgibFormGroup.value.siret == '') {
                    alert("champ est obligatoir");
                }
                else {
                    this.getNafCompany(this.testEgibFormGroup.value.siret);
                    this.nextStep();
                }
                break;
            case 13:
                if (this.testEgibFormGroup.value.adresse == '' && this.testEgibFormGroup.value.zipcode == '' && this.testEgibFormGroup.value.region == ''
                    && this.testEgibFormGroup.value.city == '' && this.testEgibFormGroup.value.country == '') {
                    alert("champ est obligatoir");
                }
                else {
                    this.nextStep();
                }
                break;
            case 14:
                if (this.testEgibFormGroup.value.nom == '' && this.testEgibFormGroup.value.prenom == '' && this.testEgibFormGroup.value.email == ''
                    && this.testEgibFormGroup.value.phone == '' && this.testEgibFormGroup.value.phoneEntrep == '') {
                    alert("champ est obligatoir");
                }
                else {
                    this.showResult();
                }
                break;
            case 15:
                this.nextStep();
                break;
            case 16:
                this.nextStep();
                break;
        }
    }
}
TestComponent.ɵfac = function TestComponent_Factory(t) { return new (t || TestComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_crm_test_eligibilite_service__WEBPACK_IMPORTED_MODULE_2__["TestEligibiliteService"])); };
TestComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: TestComponent, selectors: [["app-test"]], decls: 28, vars: 23, consts: [[1, "container"], [1, "body"], [3, "formGroup"], ["class", "slide0", 4, "ngIf"], ["class", "slide1", 4, "ngIf"], ["class", "slide2", 4, "ngIf"], ["class", "slide3", 4, "ngIf"], ["class", "slide4", 4, "ngIf"], ["class", "slide5", 4, "ngIf"], ["class", "slide6", 4, "ngIf"], ["class", "slide7", 4, "ngIf"], ["class", "slide8", 4, "ngIf"], [4, "ngIf"], ["class", "slide27", 4, "ngIf"], ["class", "slide18", 4, "ngIf"], ["class", "slide19", 4, "ngIf"], ["class", "slide20", 4, "ngIf"], ["class", "slide26", 4, "ngIf"], ["class", "slide21", 4, "ngIf"], ["class", "slide24", 4, "ngIf"], ["class", "slide25", 4, "ngIf"], [1, "footer"], [1, "left"], [3, "routerLink"], ["src", "assets/images/logo/logo.png", "alt", ""], ["class", "center", 4, "ngIf"], [1, "slide0"], [1, "image"], ["src", "assets/photos/1.png", "height", "30%", "alt", ""], [1, "test"], ["mat-stroked-button", "", "color", "primary", 3, "click"], [1, "slide1"], [1, "slide2"], ["src", "assets/photos/4.png", "height", "30%", "alt", ""], ["type", "text", "formControlName", "activite", "placeholder", "Choisir un activit\u00E9"], [1, "slide3"], ["src", "assets/photos/5.png", "height", "30%", "alt", ""], [1, "blockBtn"], [1, "sousBlock"], ["mat-stroked-button", "", 3, "ngClass", "click"], [1, "slide4"], ["src", "assets/photos/3.png", "height", "30%", "alt", ""], ["type", "text", "placeholder", "nom societe", "formControlName", "nomSoc"], [1, "slide5"], ["src", "assets/photos/6.png", "height", "30%", "alt", ""], [1, "qty"], ["type", "text", "formControlName", "turnover", 3, "readonly"], ["turnover", ""], [1, "qtyBtn"], ["type", "text", "value", "+50%"], ["mat-stroked-button", "", "color", "primary"], ["value", "0"], [1, "slide6"], ["src", "assets/photos/7.png", "alt", ""], ["name", "", "formControlName", "help"], ["value", "item", 4, "ngFor", "ngForOf"], ["value", "item"], [1, "slide7"], [1, "colum1"], ["src", "assets/photos/8.png", "height", "30%", "alt", ""], [1, "colum2"], [1, "__range", "__range-step"], ["step", "1", "min", "0", "max", "test.data[test.active.step].range.length-1", "formControlName", "lastTurnover", "type", "range", "list", "tickmarks", 1, "slider"], ["id", "tickmarks"], [1, "slide8"], ["src", "assets/photos/9.png", "height", "30%", "alt", ""], ["placeholder", "choisir un Nombre de salari\u00E9s", "formControlName", "personneSal"], [4, "ngFor", "ngForOf"], ["class", "slide9", 4, "ngIf"], ["class", "slide10", 4, "ngIf"], ["class", "slide11", 4, "ngIf"], ["class", "slide12", 4, "ngIf"], ["class", "slide13", 4, "ngIf"], [1, "slide9"], ["src", "assets/photos/10.png", "height", "30%", "alt", ""], ["aria-label", "Select an option", "formControlName", "haveSite"], ["value", "oui"], ["value", "non"], [1, "slide10"], ["src", "assets/photos/11.png", "height", "30%", "alt", ""], ["aria-label", "Select an option"], ["value", "1"], ["value", "2"], ["value", "3"], [1, "block"], ["type", "text", "value", "+10%"], ["type", "text", "value", "+1000%"], [1, "slide11"], ["src", "assets/photos/12.png", "height", "30%", "alt", ""], ["type", "text"], [1, "slide12"], ["src", "assets/photos/13.png", "alt", ""], ["name", "", "id", ""], ["value", ""], [1, "slide13"], ["class", "slide14", 4, "ngIf"], ["class", "slide15", 4, "ngIf"], ["class", "slide16", 4, "ngIf"], ["class", "slide17", 4, "ngIf"], [1, "slide14"], ["src", "assets/photos/17.png", "height", "30%", "alt", ""], ["aria-label", "Select an option", "formControlName", "haveCrm"], [1, "slide15"], ["src", "assets/photos/logo-sage.png", "height", "20px", "alt", ""], [1, "slide16"], ["src", "assets/photos/14.png", "height", "30%", "alt", ""], [1, "slide17"], [1, "slide27"], ["for", ""], [1, "contentTab"], ["mat-list-icon", ""], ["mat-line", ""], ["aria-label", "Select an option", "formControlName", "service"], ["value", "{{", "item.id", "", "}}", ""], [1, "slide18"], ["name", "", "formControlName", "budget"], ["value", "300"], ["value", "400"], ["value", "500"], ["value", "600"], [1, "slide19"], ["src", "assets/photos/18.png", "height", "30%", "alt", ""], ["type", "text", "formControlName", "siret", "placeholder", "EX: 13168813881"], [1, "slide20"], ["src", "assets/photos/19.png", "alt", ""], [1, "block1"], ["type", "text", "formControlName", "adresse"], [1, "block2"], ["type", "text", "formControlName", "city"], ["type", "text", "formControlName", "zipcode"], [1, "slide26"], ["src", "assets/photos/20.png", "alt", ""], ["type", "text", "formControlName", "nom"], ["type", "text", "formControlName", "prenom"], ["type", "text", "formControlName", "email"], ["type", "text", "formControlName", "phone"], ["type", "text", "formControlName", "phoneEntrep"], ["aria-label", "Select an option", "formControlName", "post"], ["value", "G\u00E9rant"], ["value", "Associ\u00E9"], ["value", "Dir\u00E9cteur"], ["value", "autre"], [1, "slide21"], ["src", "assets/photos/21.png", "alt", ""], ["src", "assets/photos/24.png", "alt", ""], [1, "prix"], ["src", "assets/photos/22.png", "alt", ""], [1, "nextIcon", 3, "click"], ["src", "assets/photos/23.png", "alt", ""], [1, "faild"], ["src", "assets/photos/25.png", "alt", ""], [1, "slide24"], ["src", "assets/photos/27.png", "height", "30%", "alt", ""], [1, "calend"], [2, "width", "100%", 3, "options"], ["fullcalendar", ""], [1, "slide25"], ["src", "assets/photos/28.png", "alt", ""], [1, "nextIcon"], [1, "socialMedia"], ["src", "assets/photos/icone-Facebook.png", "alt", ""], ["src", "assets/photos/icone-Instagram.png", "alt", ""], ["src", "assets/photos/icone-Linkedin.png", "alt", ""], ["src", "assets/photos/icone-youtube.png", "alt", ""], [1, "center"], [3, "click"], ["matSuffix", "", 1, "iconNex"], [1, "listP"], ["matSuffix", "", 1, "point", 3, "ngClass"]], template: function TestComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "form", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, TestComponent_div_3_Template, 8, 0, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, TestComponent_div_4_Template, 6, 0, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, TestComponent_div_5_Template, 7, 0, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](6, TestComponent_div_6_Template, 19, 15, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, TestComponent_div_7_Template, 7, 0, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](8, TestComponent_div_8_Template, 31, 1, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](9, TestComponent_div_9_Template, 8, 1, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](10, TestComponent_div_10_Template, 12, 1, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](11, TestComponent_div_11_Template, 8, 1, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](12, TestComponent_div_12_Template, 3, 2, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](13, TestComponent_div_13_Template, 9, 1, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](14, TestComponent_div_14_Template, 15, 0, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](15, TestComponent_div_15_Template, 7, 0, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](16, TestComponent_div_16_Template, 17, 0, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](17, TestComponent_div_17_Template, 36, 0, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](18, TestComponent_div_18_Template, 24, 1, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](19, TestComponent_div_19_Template, 24, 0, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](20, TestComponent_div_20_Template, 12, 0, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](21, TestComponent_div_21_Template, 14, 1, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](22, TestComponent_div_22_Template, 20, 0, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "a", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "img", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](27, TestComponent_div_27_Template, 56, 48, "div", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.testEgibFormGroup);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/cpn/Home");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step != 0);
    } }, directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["NgIf"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterLinkWithHref"], _angular_material_button__WEBPACK_IMPORTED_MODULE_5__["MatButton"], _map_french_map_french_component__WEBPACK_IMPORTED_MODULE_6__["MapFrenchComponent"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["NgClass"], _angular_material_radio__WEBPACK_IMPORTED_MODULE_7__["MatRadioButton"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["SelectControlValueAccessor"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["NgForOf"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgSelectOption"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_x"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["RangeValueAccessor"], _angular_material_radio__WEBPACK_IMPORTED_MODULE_7__["MatRadioGroup"], _angular_material_list__WEBPACK_IMPORTED_MODULE_8__["MatList"], _angular_material_list__WEBPACK_IMPORTED_MODULE_8__["MatListItem"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_9__["MatIcon"], _angular_material_list__WEBPACK_IMPORTED_MODULE_8__["MatListIconCssMatStyler"], _angular_material_core__WEBPACK_IMPORTED_MODULE_10__["MatLine"], _fullcalendar_angular__WEBPACK_IMPORTED_MODULE_11__["FullCalendarComponent"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_12__["MatSuffix"]], styles: [".container[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  flex-direction: column;\r\n  width: 100%;\r\n  height: 100%;\r\n  max-width: 100%;\r\n  margin: 0;\r\n  padding: 0;\r\n}\r\n.body[_ngcontent-%COMP%]{\r\n  width: 100%;\r\n  height: 100%;\r\n  display: flex;\r\n  flex-direction:column;\r\n  justify-content: center;\r\n}\r\n.footer[_ngcontent-%COMP%]{\r\n  width: 100%;\r\n  max-height: 150px;\r\n  min-height: 150px;\r\n  height: 100%;\r\n  background-color:  #111D5Eff;\r\n  display: flex;\r\n  flex-direction: row;\r\n}\r\n.left[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: flex-start;\r\n  height: 100%;\r\n  align-items: center;\r\n  width: 10%;\r\n}\r\n.point[_ngcontent-%COMP%]{\r\n  font-size: 10px;\r\n}\r\nmat-icon[_ngcontent-%COMP%]{\r\n  color:rgb(153, 153, 153);\r\n}\r\nmat-icon[_ngcontent-%COMP%]:hover{\r\n  color: white;\r\n  cursor: pointer;\r\n}\r\n.checkIcon[_ngcontent-%COMP%]{\r\n  color: white;\r\n}\r\n.center[_ngcontent-%COMP%]{\r\n  width: 100%;\r\n  height: 100%;\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: center;\r\n  align-items: center;\r\n}\r\n\r\n.butttonREd[_ngcontent-%COMP%]{\r\n  background-color: rgb(206, 0, 0);\r\n  color: white;\r\n}\r\nbutton[_ngcontent-%COMP%]:hover{\r\n  background-color: rgb(145, 136, 136);\r\n  color: white;\r\n}\r\n\r\n.slide1[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: space-between;\r\n  width: 100%;\r\n  height: 100%;\r\n}\r\n.slide1[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  width: 70%;\r\n  height: 100%;\r\n}\r\n.slide1[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n  order: 1;\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: center;\r\n  align-items: center;\r\n  width: 40%;\r\n  height: 60%;\r\n}\r\n\r\n.slide0[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: space-between;\r\n  width: 100%;\r\n  height: 100%;\r\n}\r\n.slide0[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  width: 52%;\r\n  height: 100%;\r\n}\r\n.slide0[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n  order: 1;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items: center;\r\n  width:40%;\r\n  height: 100%;\r\n}\r\n.slide0[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n  font-size: 50px;\r\n}\r\n.slide0[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]{\r\n  width: 150px;\r\n  height: 50px;\r\n  border-radius: 30px;\r\n  border-color: rgb(192, 3, 3);\r\n}\r\n\r\n.slide2[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: space-between;\r\n  width: 100%;\r\n  height: 100%;\r\n}\r\n.slide2[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  order: 1;\r\n  width: 55%;\r\n  height: 100%;\r\n}\r\n.slide2[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items: flex-flex-start;\r\n  width:40%;\r\n  height: 100%;\r\n}\r\n.slide2[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n  font-size: 50px;\r\n}\r\n.slide2[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n  width: 80%;\r\n  height: 40px;\r\n  border-color: rgb(212, 5, 5);\r\n}\r\n\r\n.slide3[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: space-between;\r\n  width: 100%;\r\n  height: 100%;\r\n}\r\n.slide3[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  width: 54%;\r\n  height: 100%;\r\n}\r\n.slide3[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n  order: 1;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: space-around;\r\n  align-items: center;\r\n  margin-top: 50px;\r\n  width:40%;\r\n  height: 70%;\r\n}\r\n.slide3[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n  font-size: 50px;\r\n}\r\n.slide3[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n\r\n}\r\n.slide3[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: center;\r\n\r\n}\r\n.slide3[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]{\r\n  width: 310px;\r\n  height: 50px;\r\n  margin: 5px;\r\n  border-color: rgb(192, 3, 3);\r\n}\r\n.slide3[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]{\r\n  width: 150px;\r\n  margin: 5px;\r\n}\r\n\r\n.slide4[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: space-between;\r\n  width: 100%;\r\n  height: 100%;\r\n}\r\n.slide4[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  margin-left: 50px;\r\n  order: 1;\r\n  width: 33%;\r\n  height: 100%;\r\n  display: flex;\r\n  z-index: 1;\r\n  margin-top: 55px;\r\n}\r\n.slide4[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items: flex-flex-start;\r\n  width:40%;\r\n  height: 100%;\r\n}\r\n.slide4[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n  font-size: 50px;\r\n}\r\n.slide4[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n  width: 80%;\r\n  height: 40px;\r\n  border-color: rgb(212, 5, 5);\r\n}\r\n\r\n.slide5[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: space-between;\r\n  width: 100%;\r\n  height: 100%;\r\n}\r\n.slide5[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  order: 1;\r\n  width: 50%;\r\n  height: 100%;\r\n  z-index: 1;\r\n  display: flex;\r\n  margin-top: 64px;\r\n}\r\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items: center;\r\n  width:40%;\r\n  height: 100%;\r\n}\r\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n  font-size: 50px;\r\n}\r\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: space-around;\r\n  width: 100%;\r\n  height: 20%;\r\n}\r\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: center;\r\n  align-items: center;\r\n}\r\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: center;\r\n}\r\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  width: 80px;\r\n  height: 50px;\r\n  border: 4px solid rgb(212, 5, 5);\r\n  text-align: center;\r\n}\r\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n  order: 1;\r\n  margin: 0 5px 0 0;\r\n  padding: 0;\r\n  display: flex;\r\n  align-items: center;\r\n  flex-direction: row;\r\n}\r\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   .qtyBtn[_ngcontent-%COMP%]{\r\n  order: 3;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: flex-flex-start;\r\n}\r\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   .qtyBtn[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]{\r\n  border: none;\r\n  border-bottom: 1px solid white;\r\n  margin-bottom: 1px;\r\n  width: 5px;\r\n  border-radius: 0px;\r\n  height: 25px;\r\n  color: white;\r\n  background-color: rgb(212, 5, 5);\r\n  display: flex;\r\n  flex-direction: column;\r\n  font-size: 20px;\r\n  justify-content: center;\r\n  align-items: center;\r\n}\r\n\r\n.slide6[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: space-between;\r\n  width: 100%;\r\n  height: 100%;\r\n}\r\n.slide6[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items:center;\r\n  width:53%;\r\n  height: 100%;\r\n}\r\n.slide6[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  height: 25%;\r\n  width: 15%;\r\n}\r\n.slide6[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n  order: 1;\r\n  font-size: 50px;\r\n  text-align: center;\r\n  margin-bottom: 20px;\r\n}\r\n.slide6[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items: flex-flex-start;\r\n  width:40%;\r\n  height: 100%;\r\n}\r\n.slide6[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   select[_ngcontent-%COMP%]{\r\n  width: 80%;\r\n  height: 40px;\r\n  border-color: rgb(212, 5, 5);\r\n}\r\n\r\n.slide7[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  width: 100%;\r\n  height: 100%;\r\n}\r\n.slide7[_ngcontent-%COMP%]   .colum1[_ngcontent-%COMP%]{\r\n  order: 1;\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: space-around;\r\n  width: 100%;\r\n  height: 100%;\r\n  margin-bottom: 20px;\r\n}\r\n.slide7[_ngcontent-%COMP%]   .colum2[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: center;\r\n  width: 100%;\r\n  margin-top: 20px;\r\n  height: 100%;\r\n}\r\n.slide7[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  margin-left: 50px;\r\n  order: 1;\r\n  width: 37%;\r\n  height: 100%;\r\n}\r\n.slide7[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items: flex-flex-start;\r\n  width:40%;\r\n  height: 100%;\r\n}\r\n.slide7[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n  font-size: 50px;\r\n}\r\n.slide7[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n  width: 80%;\r\n  height: 40px;\r\n  border-color: rgb(212, 5, 5);\r\n}\r\n.slide7[_ngcontent-%COMP%]   .progress[_ngcontent-%COMP%]{\r\n  order: 3;\r\n}\r\n.slider[_ngcontent-%COMP%] {\r\n  -webkit-appearance: none;\r\n  width: 100%;\r\n  height: 15px;\r\n  background: rgb(255, 255, 255);\r\n  outline: none;\r\n  border: 5px solid rgb(189, 8, 8);\r\n  border-radius: 8px;\r\n}\r\n\r\n.slider[_ngcontent-%COMP%]::-webkit-slider-thumb {\r\n  -webkit-appearance: none;\r\n  appearance: none;\r\n  width: 20px;\r\n  height: 60px;\r\n  background: rgb(248, 224, 5);\r\n  cursor: pointer;\r\n  border: 5px solid rgb(248, 224, 5);\r\n  border-radius: 50px;\r\n}\r\n\r\n.slider[_ngcontent-%COMP%]::-moz-range-thumb {\r\n  width: 20px;\r\n  height: 60px;\r\n  background: rgb(255, 255, 255);\r\n  cursor: pointer;\r\n  border: 5px solid rgb(189, 8, 8);\r\n  border-radius: 4px;\r\n}\r\n.__range[_ngcontent-%COMP%]{\r\n  width: 80%;\r\n  height: 100%;\r\n}\r\n.__range-step[_ngcontent-%COMP%]{\r\n  position: relative;\r\n}\r\n.__range-step[_ngcontent-%COMP%]{\r\n  position: relative;\r\n}\r\n.__range-max[_ngcontent-%COMP%]{\r\n  float: right;\r\n}\r\n.__range[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]::range-progress {\tbackground: rgb(189, 8, 8);\r\n}\r\n.slider[_ngcontent-%COMP%]   input[type=range][_ngcontent-%COMP%]::-moz-range-progress {\r\n  background-color: #c657a0;\r\n}\r\n.__range-step[_ngcontent-%COMP%]   datalist[_ngcontent-%COMP%] {\r\n  position:relative;\r\n  display: flex;\r\n  justify-content: space-between;\r\n  height: auto;\r\n  bottom: 10px;\r\n  \r\n  -webkit-user-select: none;   \r\n  user-select: none; \r\n  \r\n  pointer-events:none;\r\n}\r\n.__range-step[_ngcontent-%COMP%]   datalist[_ngcontent-%COMP%]   option[_ngcontent-%COMP%] {\r\n  width: 10px;\r\n  height: 10px;\r\n  min-height: 10px;\r\n  border-radius: 100px;\r\n  \r\n  white-space: nowrap;\r\n  padding:0;\r\n  line-height: 40px;\r\n}\r\n\r\n.slide8[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: space-around;\r\n  width: 100%;\r\n  height: 100%;\r\n}\r\n.slide8[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  display: flex;\r\n  align-items: center;\r\n  width: 37%;\r\n  height: 100%;\r\n}\r\n.slide8[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n  order: 1;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items: flex-flex-start;\r\n  width:40%;\r\n  height: 100%;\r\n}\r\n.slide8[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n  font-size: 50px;\r\n}\r\n.slide8[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   select[_ngcontent-%COMP%]{\r\n  width: 80%;\r\n  height: 40px;\r\n  border-color: rgb(212, 5, 5);\r\n}\r\n\r\n.slide9[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: space-around;\r\n  width: 100%;\r\n  height: 100%;\r\n}\r\n.slide9[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  order: 1;\r\n  display: flex;\r\n  align-items: center;\r\n  width: 37%;\r\n  height: 100%;\r\n}\r\n.slide9[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items: flex-flex-start;\r\n  width:40%;\r\n  height: 100%;\r\n}\r\n.slide9[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n  font-size: 50px;\r\n}\r\n.slide9[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   mat-radio-group[_ngcontent-%COMP%]{\r\n  margin-top: 50px;\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: space-around;\r\n  width: 100%;\r\n}\r\n\r\n.slide10[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: space-around;\r\n  width: 100%;\r\n  height: 100%;\r\n}\r\n.slide10[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  display: flex;\r\n  align-items: center;\r\n  width: 37%;\r\n  height: 100%;\r\n}\r\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n  order: 1;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items: flex-flex-start;\r\n  width:40%;\r\n  height: 100%;\r\n}\r\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n  font-size: 50px;\r\n}\r\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   mat-radio-group[_ngcontent-%COMP%]{\r\n  margin-top: 50px;\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: space-around;\r\n  width: 100%;\r\n}\r\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: space-around;\r\n  width: 100%;\r\n  margin-top: 20px;\r\n}\r\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: space-around;\r\n  align-items: center;\r\n}\r\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: center;\r\n}\r\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  width: 80px;\r\n  height: 50px;\r\n  border: 4px solid rgb(212, 5, 5);\r\n  text-align: center;\r\n}\r\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n  order: 1;\r\n  margin: 0 5px 0 0;\r\n  padding: 0;\r\n  display: flex;\r\n  align-items: center;\r\n  flex-direction: row;\r\n}\r\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   .qtyBtn[_ngcontent-%COMP%]{\r\n  order: 3;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: flex-flex-start;\r\n}\r\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   .qtyBtn[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]{\r\n  border: none;\r\n  border-bottom: 1px solid white;\r\n  margin-bottom: 1px;\r\n  width: 5px;\r\n  border-radius: 0px;\r\n  height: 25px;\r\n  color: white;\r\n  background-color: rgb(212, 5, 5);\r\n  display: flex;\r\n  flex-direction: column;\r\n  font-size: 20px;\r\n  justify-content: center;\r\n  align-items: center;\r\n}\r\n\r\n.slide11[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: space-around;\r\n  width: 100%;\r\n  height: 100%;\r\n}\r\n.slide11[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  align-items: center;\r\n  display: flex;\r\n  order: 1;\r\n  width: 37%;\r\n  height: 100%;\r\n}\r\n.slide11[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items: flex-flex-start;\r\n  width:40%;\r\n  height: 100%;\r\n}\r\n.slide11[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n  font-size: 50px;\r\n}\r\n.slide11[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n  width: 80%;\r\n  height: 40px;\r\n  border-color: rgb(212, 5, 5);\r\n}\r\n\r\n.slide12[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: space-between;\r\n  width: 100%;\r\n  height: 100%;\r\n}\r\n.slide12[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items:center;\r\n  width:53%;\r\n  height: 100%;\r\n}\r\n.slide12[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  height: 15%;\r\n  width: 15%;\r\n}\r\n.slide12[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n  order: 1;\r\n  font-size: 50px;\r\n  text-align: center;\r\n  margin-bottom: 20px;\r\n}\r\n.slide12[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items: flex-flex-start;\r\n  width:40%;\r\n  height: 100%;\r\n}\r\n.slide12[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   select[_ngcontent-%COMP%]{\r\n  width: 80%;\r\n  height: 40px;\r\n  border-color: rgb(212, 5, 5);\r\n}\r\n\r\n.slide13[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: space-between;\r\n  width: 100%;\r\n  height: 100%;\r\n}\r\n.slide13[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  margin-left: 50px;\r\n  order: 1;\r\n  display: flex;\r\n  align-items:center;\r\n  width: 37%;\r\n  height: 100%;\r\n}\r\n.slide13[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items: center;\r\n  width:40%;\r\n  height: 100%;\r\n}\r\n.slide13[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n  font-size: 50px;\r\n}\r\n.slide13[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n  width: 80%;\r\n  height: 40px;\r\n  margin: 50px 0 50px 0;\r\n  border-color: rgb(212, 5, 5);\r\n}\r\n\r\n.slide14[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: space-around;\r\n  width: 100%;\r\n  height: 100%;\r\n}\r\n.slide14[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  display: flex;\r\n  align-items: center;\r\n  width: 37%;\r\n  height: 100%;\r\n}\r\n.slide14[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n  order: 1;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items: flex-flex-start;\r\n  width:40%;\r\n  height: 100%;\r\n}\r\n.slide14[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n  font-size: 50px;\r\n}\r\n.slide14[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   mat-radio-group[_ngcontent-%COMP%]{\r\n  margin-top: 50px;\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: space-around;\r\n  width: 100%;\r\n}\r\n\r\n.slide15[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: space-between;\r\n  width: 100%;\r\n  height: 100%;\r\n}\r\n.slide15[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items:center;\r\n  width:53%;\r\n  height: 100%;\r\n}\r\n.slide15[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n  order: 2;\r\n\r\n}\r\n.slide15[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n  order: 1;\r\n  font-size: 50px;\r\n  text-align: center;\r\n  margin-bottom: 20px;\r\n}\r\n.slide15[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items: flex-flex-start;\r\n  width:40%;\r\n  height: 100%;\r\n}\r\n.slide15[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   select[_ngcontent-%COMP%]{\r\n  width: 80%;\r\n  height: 40px;\r\n  border-color: rgb(212, 5, 5);\r\n}\r\n\r\n.slide16[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: space-around;\r\n  width: 100%;\r\n  height: 100%;\r\n}\r\n.slide16[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  display: flex;\r\n  align-items: center;\r\n  width: 37%;\r\n  height: 100%;\r\n}\r\n.slide16[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n  order: 1;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items: flex-flex-start;\r\n  width:40%;\r\n  height: 100%;\r\n}\r\n.slide16[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n  font-size: 50px;\r\n}\r\n.slide16[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   mat-radio-group[_ngcontent-%COMP%]{\r\n  margin-top: 50px;\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: space-around;\r\n  width: 100%;\r\n}\r\n\r\n.slide17[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: space-between;\r\n  width: 100%;\r\n  height: 100%;\r\n}\r\n.slide17[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items:center;\r\n  width:53%;\r\n  height: 100%;\r\n}\r\n.slide17[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n  order: 2;\r\n\r\n}\r\n.slide17[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n  order: 1;\r\n  font-size: 50px;\r\n  text-align: center;\r\n  margin-bottom: 20px;\r\n}\r\n.slide17[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items: flex-flex-start;\r\n  width:40%;\r\n  height: 100%;\r\n}\r\n.slide17[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   select[_ngcontent-%COMP%]{\r\n  width: 80%;\r\n  height: 40px;\r\n  border-color: rgb(212, 5, 5);\r\n}\r\n\r\n.slide18[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: space-between;\r\n  width: 100%;\r\n  height: 100%;\r\n}\r\n.slide18[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items:center;\r\n  width:53%;\r\n  height: 100%;\r\n}\r\n.slide18[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  height: 30%;\r\n  width: 15%;\r\n}\r\n.slide18[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  font-size: 50px;\r\n  text-align: center;\r\n  margin-bottom: 20px;\r\n}\r\n.slide18[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n  order: 1;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items: center;\r\n  width:40%;\r\n  height: 100%;\r\n}\r\n.slide18[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   select[_ngcontent-%COMP%]{\r\n  width: 80%;\r\n  height: 40px;\r\n  border-color: rgb(212, 5, 5);\r\n}\r\n\r\n.slide19[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: space-between;\r\n  width: 100%;\r\n  height: 100%;\r\n}\r\n.slide19[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  margin-left: 50px;\r\n  display: flex;\r\n  align-items: center;\r\n  order: 1;\r\n  width: 37%;\r\n  height: 100%;\r\n}\r\n.slide19[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items: flex-flex-start;\r\n  width:40%;\r\n  height: 100%;\r\n}\r\n.slide19[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n  font-size: 50px;\r\n}\r\n.slide19[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n  width: 80%;\r\n  height: 40px;\r\n  border-color: rgb(212, 5, 5);\r\n}\r\n\r\n.slide20[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: space-between;\r\n  width: 100%;\r\n  height: 100%;\r\n}\r\n.slide20[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  align-items: center;\r\n  justify-content: center;\r\n  order: 1;\r\n  width: 60%;\r\n  height: 80%;\r\n}\r\n.slide20[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n  height: 80%;\r\n  width: 40%;\r\n  filter: drop-shadow(0.4rem 0.4rem 0.45rem rgba(0, 0, 30, 0.5));\r\n}\r\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items: flex-flex-start;\r\n  width:60%;\r\n  height: 100%;\r\n}\r\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n  font-size: 50px;\r\n}\r\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n  width: 90%;\r\n  height: 40px;\r\n  border-color: rgb(212, 5, 5);\r\n}\r\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]{\r\n  margin-top: 20px;\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: space-between;\r\n  width: 90%;\r\n}\r\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%] {\r\n  margin-top: 20px;\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: space-between;\r\n  width: 90%;\r\n}\r\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n  width: 40%;\r\n}\r\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%]   label[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  align-items: center;\r\n  margin: 0;\r\n}\r\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n  width: 90%;\r\n}\r\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   label[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  align-items: center;\r\n  margin: 0;\r\n}\r\n\r\n.slide21[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: center;\r\n  align-items: center;\r\n  width: 100%;\r\n  height: 70%;\r\n}\r\n.slide21[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  align-items: flex-flex-end;\r\n  justify-content: flex-flex-end;\r\n  order: 1;\r\n  width: 22%;\r\n  height: 100%;\r\n}\r\n.slide21[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n  height: 70%;\r\n  width: 30%;\r\n}\r\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content:flex-start;\r\n  align-items: center;\r\n  width:70%;\r\n  height: 80%;\r\n}\r\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n  line-height: normal;\r\n  color: green;\r\n  font-size: 50px;\r\n}\r\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n\r\n  font-size: 100px;\r\n  margin: 10px;\r\n}\r\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n  line-height: normal;\r\n  color: rgb(0, 0, 133);\r\n  font-size: 30px;\r\n}\r\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .prix[_ngcontent-%COMP%] {\r\n  color: red;\r\n  line-height: normal;\r\n}\r\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .faild[_ngcontent-%COMP%]{\r\n  color: red;\r\n  line-height: normal;\r\n  font-size: 60px;\r\n}\r\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n  width: 100px;\r\n}\r\n.slide21[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]{\r\n  order: 3;\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: flex-flex-start;\r\n  align-items: flex-flex-end;\r\n  width: 22%;\r\n  height: 90%;\r\n}\r\n.slide21[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n  color: green;\r\n  width: 80px;\r\n}\r\n\r\n.slide24[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: space-between;\r\n  width: 100%;\r\n  height: 100%;\r\n}\r\n.slide24[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  margin-left: 50px;\r\n  order: 2;\r\n  display: flex;\r\n  align-items:flex-flex-end;\r\n  width: 47%;\r\n  height: 100%;\r\n  margin-top: 10px;\r\n}\r\n.slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n  order: 1;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items: center;\r\n  width:40%;\r\n  height: 100%;\r\n}\r\n.slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n  font-size: 50px;\r\n  text-align: center;\r\n}\r\n.slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n  width: 80%;\r\n  height: 40px;\r\n  margin: 50px 0 50px 0;\r\n  border-color: rgb(212, 5, 5);\r\n}\r\n.slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   mat-radio-group[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: space-around;\r\n  align-items: flex-flex-start;\r\n  width: 100%;\r\n\r\n}\r\n.slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .calflex-end[_ngcontent-%COMP%]{\r\n  width: 450px;\r\n  height: 450px;\r\n}\r\n\r\n.slide25[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items: center;\r\n  width: 100%;\r\n  height: 100%;\r\n}\r\n.slide25[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  align-items: flex-flex-end;\r\n  justify-content: flex-flex-end;\r\n  order: 1;\r\n  width: 22%;\r\n  height: 20%;\r\n}\r\n.slide25[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content:flex-flex-start;\r\n  align-items: center;\r\n  width:70%;\r\n  height: 80%;\r\n}\r\n.slide25[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n  width: 50px;\r\n}\r\n.slide25[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n  color: rgb(0, 0, 133);\r\n  font-size: 40px;\r\n}\r\n.slide25[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]{\r\n  order: 3;\r\n}\r\n.slide25[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n  text-align: center;\r\n}\r\n.slide25[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]   .socialMedia[_ngcontent-%COMP%] {\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: center;\r\n}\r\n.slide25[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]   .socialMedia[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n  width: 60px;\r\n}\r\n\r\n.slide26[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: space-between;\r\n  width: 100%;\r\n  height: 100%;\r\n}\r\n.slide26[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items:center;\r\n  width:53%;\r\n  height: 100%;\r\n}\r\n.slide26[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  height: 30%;\r\n  width: 15%;\r\n}\r\n.slide26[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  font-size: 50px;\r\n  text-align: center;\r\n  margin-bottom: 20px;\r\n}\r\n.slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n  order: 1;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items: flex-flex-start;\r\n  width:60%;\r\n  height: 100%;\r\n}\r\n.slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n  font-size: 50px;\r\n}\r\n.slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n  width: 90%;\r\n  height: 40px;\r\n  border-color: rgb(212, 5, 5);\r\n}\r\n.slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]{\r\n  margin: 20px;\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: space-between;\r\n  align-items: flex-flex-start;\r\n  width: 90%;\r\n}\r\n.slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%] {\r\n  margin-top: 20px;\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: space-around;\r\n  width: 100%;\r\n}\r\n.slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%]   mat-radio-group[_ngcontent-%COMP%]  {\r\n  margin-top: 20px;\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: space-around;\r\n  width: 90%;\r\n}\r\n.slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n  width: 90%;\r\n}\r\n.slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   label[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  align-items: center;\r\n  margin: 0;\r\n}\r\n\r\n.slide27[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: space-between;\r\n  width: 100%;\r\n  height: 100%;\r\n}\r\n.slide27[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  order: 1;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items:center;\r\n  width:53%;\r\n  height: 100%;\r\n}\r\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: flex-flex-start;\r\n  width:40%;\r\n  height: 100%;\r\n\r\n}\r\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%] {\r\n  margin-bottom: 15px;\r\n  width: 50%;\r\n  border-radius: 50px;\r\n  background-color: rgb(134, 134, 134);\r\n  color:white;\r\n}\r\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   mat-list[_ngcontent-%COMP%] {\r\n  background-color: #fff;\r\n  width: 70%;\r\n}\r\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   mat-list[_ngcontent-%COMP%]   mat-list-item[_ngcontent-%COMP%] {\r\n  background-color: rgb(255, 255, 255);\r\n  margin-top: 0;\r\n  border:2px solid rgb(202, 3, 3);\r\n  border-bottom: 1px solid rgb(202, 3, 3);;\r\n}\r\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   mat-list[_ngcontent-%COMP%]   mat-list-item[_ngcontent-%COMP%]:hover{\r\n  background-color: rgb(218, 98, 98);\r\n}\r\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .contentTab[_ngcontent-%COMP%]{\r\n  width: 100%;\r\n  height: 500px;\r\n  overflow-y: scroll;\r\n}\r\n\r\n@media screen and (max-width: 768px) {\r\n\r\n  .body[_ngcontent-%COMP%]{\r\n    width: 100%;\r\n    height: 100%;\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: space-between;\r\n  }\r\n  .footer[_ngcontent-%COMP%]{\r\n    flex-direction: column;\r\n  }\r\n  .left[_ngcontent-%COMP%]{\r\n    justify-content: center;\r\n    align-items: center;\r\n    width: 100%;\r\n  }\r\n  .left[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n    width: 60px;\r\n  }\r\n  .point[_ngcontent-%COMP%]{\r\n    text-align: center;\r\n    font-size: 7px;\r\n  }\r\n  .center[_ngcontent-%COMP%]{\r\n    width: 90%;\r\n    margin-left: 20px;\r\n  }\r\n  .center[_ngcontent-%COMP%]   .listP[_ngcontent-%COMP%]{\r\n    text-align: center;\r\n  }\r\n\r\n  \r\n  .slide0[_ngcontent-%COMP%]{\r\n    flex-direction: column;\r\n    justify-content: flex-flex-start;\r\n    align-items: center ;\r\n    height: 100%;\r\n  }\r\n  .slide0[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    width: 80%;\r\n    height: 100%;\r\n  }\r\n\r\n  .slide0[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    width:100%;\r\n  }\r\n\r\n  .slide0[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n  }\r\n\r\n\r\n  \r\n  .slide1[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n    align-items: center;\r\n    width: 100%;\r\n    height: 100%;\r\n  }\r\n  .slide1[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    width: 90%;\r\n    height: 100%;\r\n  }\r\n\r\n  .slide1[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    width: 100%;\r\n  }\r\n  .slide1[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n  }\r\n\r\n  \r\n  .slide2[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content:space-between;\r\n    width: 100%;\r\n    height: 100%;\r\n  }\r\n  .slide2[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    width: 100%;\r\n    height: 100%;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content:flex-end;\r\n  }\r\n  .slide2[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-around;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n  }\r\n  .slide2[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n    text-align: center;\r\n  }\r\n\r\n  \r\n  .slide3[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height: 100%;\r\n  }\r\n  .slide3[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    width: 100%;\r\n    height: 100%;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: flex-end;\r\n  }\r\n  .slide3[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-around;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n  }\r\n  .slide3[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n    text-align: center;\r\n  }\r\n\r\n\r\n  \r\n  .slide4[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content:space-between;\r\n    align-items: center;\r\n    width: 100%;\r\n    height: 100%;\r\n  }\r\n  .slide4[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    width: 80%;\r\n    height: 100%;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content:flex-end;\r\n  }\r\n  .slide4[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-around;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n  }\r\n  .slide4[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n    text-align: center;\r\n  }\r\n\r\n\r\n  \r\n  .slide5[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n    align-items: center;\r\n    width: 100%;\r\n    height: 100%;\r\n\r\n  }\r\n  .slide5[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    width: 100%;\r\n    height: 100%;\r\n    z-index: 1;\r\n\r\n  }\r\n  .slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n  }\r\n  .slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n    text-align: center;\r\n    margin-block: auto;\r\n  }\r\n  .slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]{\r\n\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    width: 100%;\r\n    height: 20%;\r\n  }\r\n  .slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items: flex-end;\r\n    width: 75%;\r\n  }\r\n\r\n  .slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: center;\r\n    margin: 10px;\r\n  }\r\n\r\n  .slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    width: 80px;\r\n    height: 50.5px;\r\n    border: 4px solid rgb(212, 5, 5);\r\n    text-align: center;\r\n  }\r\n\r\n  \r\n  .slide6[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    width: 100%;\r\n    height: 100%;\r\n  }\r\n  .slide6[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items:center;\r\n    width:100%;\r\n    height: 100%;\r\n  }\r\n  .slide6[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    height: 15%;\r\n    width: 15%;\r\n  }\r\n  .slide6[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    font-size: 30px;\r\n    text-align: center;\r\n    margin-bottom: 20px;\r\n  }\r\n\r\n\r\n  .slide6[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: flex-flex-start;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n  }\r\n\r\n\r\n  \r\n  .slide7[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n    align-items: center;\r\n    width: 100%;\r\n    height: 100%;\r\n  }\r\n  .slide7[_ngcontent-%COMP%]   .colum1[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height: 100%;\r\n  }\r\n  .slide7[_ngcontent-%COMP%]   .colum2[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: center;\r\n    align-items: flex-end;\r\n    width: 100%;\r\n    height: 100%;\r\n  }\r\n  .slide7[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    width: 90%;\r\n    height: 100%;\r\n  }\r\n  .slide7[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: flex-end;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n    margin-top: 20px;\r\n  }\r\n  .slide7[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n    text-align: center;\r\n  }\r\n  .slide7[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n    width: 80%;\r\n    height: 20px;\r\n    border-color: rgb(212, 5, 5);\r\n  }\r\n  .slide7[_ngcontent-%COMP%]   .progress[_ngcontent-%COMP%]{\r\n    order: 3;\r\n  }\r\n\r\n  .slider[_ngcontent-%COMP%] {\r\n    -webkit-appearance: none;\r\n    width: 100%;\r\n    height: 15px;\r\n    background: rgb(255, 255, 255);\r\n    outline: none;\r\n    border: 5px solid rgb(189, 8, 8);\r\n    border-radius: 8px;\r\n  }\r\n\r\n\r\n  \r\n  .slider[_ngcontent-%COMP%]::-webkit-slider-thumb {\r\n    -webkit-appearance: none;\r\n    appearance: none;\r\n    width: 10px;\r\n    height: 40px;\r\n    background: rgb(248, 224, 5);\r\n    cursor: pointer;\r\n    border: 5px solid rgb(248, 224, 5);\r\n    border-radius: 30px;\r\n  }\r\n\r\n  \r\n  .slider[_ngcontent-%COMP%]::-moz-range-thumb {\r\n    width: 10px;\r\n    height: 30px;\r\n    background: rgb(255, 255, 255);\r\n    cursor: pointer;\r\n    border: 5px solid rgb(189, 8, 8);\r\n    border-radius: 4px;\r\n  }\r\n  .__range[_ngcontent-%COMP%]{\r\n    width: 80%;\r\n    height: 100%;\r\n  }\r\n  .__range-step[_ngcontent-%COMP%]{\r\n    position: relative;\r\n  }\r\n  .__range-step[_ngcontent-%COMP%]{\r\n    position: relative;\r\n  }\r\n\r\n  .__range-max[_ngcontent-%COMP%]{\r\n    float: right;\r\n  }\r\n\r\n\r\n\r\n  .__range[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]::range-progress {\tbackground: rgb(189, 8, 8);\r\n  }\r\n  .slider[_ngcontent-%COMP%]   input[type=range][_ngcontent-%COMP%]::-moz-range-progress {\r\n    background-color: #c657a0;\r\n  }\r\n  .__range-step[_ngcontent-%COMP%]   datalist[_ngcontent-%COMP%] {\r\n    position:relative;\r\n    display: flex;\r\n    justify-content: space-between;\r\n    height: auto;\r\n    bottom: 6px;\r\n    \r\n    -webkit-user-select: none;   \r\n    user-select: none; \r\n    \r\n    pointer-events:none;\r\n  }\r\n  .__range-step[_ngcontent-%COMP%]   datalist[_ngcontent-%COMP%]   option[_ngcontent-%COMP%] {\r\n    width: 10px;\r\n    height: 10px;\r\n    min-height: 10px;\r\n    border-radius: 100px;\r\n    \r\n    white-space: nowrap;\r\n    padding:0;\r\n    line-height: 40px;\r\n  }\r\n\r\n\r\n  \r\n  .slide8[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height: 100%;\r\n  }\r\n  .slide8[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    align-items: flex-flex-end;\r\n    width: 100%;\r\n    height: 100%;\r\n  }\r\n  .slide8[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-around;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n  }\r\n  .slide8[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n    text-align: center;\r\n  }\r\n\r\n  \r\n  .slide9[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height: 750px;\r\n  }\r\n  .slide9[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    align-items: center;\r\n    flex-direction: column;\r\n    justify-content: flex-end;\r\n    width: 100%;\r\n    height: 100%;\r\n  }\r\n  .slide9[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-around;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n  }\r\n  .slide9[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n    text-align: center;\r\n  }\r\n  .slide9[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   mat-radio-group[_ngcontent-%COMP%]{\r\n    margin-top: 50px;\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: space-around;\r\n    width: 100%;\r\n  }\r\n\r\n\r\n  \r\n  .slide10[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height: 750px;\r\n  }\r\n  .slide10[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    align-items: center;\r\n    width: 100%;\r\n    height: 100%;\r\n  }\r\n  .slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n  }\r\n  .slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n    text-align: center;\r\n  }\r\n  .slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    width: 100%;\r\n    margin-top: 20px;\r\n  }\r\n\r\n  .slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-around;\r\n    align-items: center;\r\n    margin-top: 10px;\r\n  }\r\n\r\n\r\n\r\n  \r\n  .slide11[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height: 750px;\r\n  }\r\n  .slide11[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    align-items: center;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: flex-end;\r\n    order: 2;\r\n    width: 100%;\r\n    height: 100%;\r\n  }\r\n  .slide11[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-around;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n  }\r\n  .slide11[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n    text-align: center;\r\n  }\r\n\r\n  \r\n  .slide12[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content:center;\r\n    width: 100%;\r\n    height: 700px;\r\n  }\r\n  .slide12[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-around;\r\n    align-items:center;\r\n    width:100%;\r\n    height: 100%;\r\n  }\r\n  .slide12[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    font-size: 30px;\r\n    text-align: center;\r\n    margin-bottom: 20px;\r\n  }\r\n\r\n\r\n  .slide12[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n  }\r\n\r\n\r\n  \r\n  .slide13[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    width: 100%;\r\n    height: 750px;\r\n  }\r\n  .slide13[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    width: 80%;\r\n    height: 100%;\r\n  }\r\n  .slide13[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n  }\r\n  .slide13[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n    text-align: center;\r\n  }\r\n\r\n  \r\n  .slide14[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height: 750px;\r\n  }\r\n  .slide14[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    align-items: center;\r\n    justify-content: flex-end;\r\n    width: 100%;\r\n    height: 100%;\r\n  }\r\n  .slide14[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-around;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n  }\r\n  .slide14[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n    text-align: center;\r\n  }\r\n  \r\n  .slide15[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height:750px;\r\n  }\r\n  .slide15[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items:center;\r\n    width:100%;\r\n    height: 100%;\r\n  }\r\n  .slide15[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n    order: 2;\r\n\r\n  }\r\n  .slide15[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    font-size: 30px;\r\n    text-align: center;\r\n    margin-bottom: 20px;\r\n  }\r\n\r\n\r\n\r\n  .slide15[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: flex-flex-start;\r\n    align-items:center;\r\n    width:100%;\r\n    height: 100%;\r\n  }\r\n\r\n\r\n\r\n  \r\n  .slide16[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height: 750px;\r\n  }\r\n  .slide16[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    align-items: center;\r\n    justify-content: flex-end;\r\n    width: 100%;\r\n    height: 100%;\r\n  }\r\n  .slide16[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: flex-flex-start;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n  }\r\n  .slide16[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n    text-align: center;\r\n  }\r\n\r\n  \r\n  .slide17[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height:750px;\r\n  }\r\n  .slide17[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items:center;\r\n    width:100%;\r\n    height: 100%;\r\n  }\r\n  .slide17[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    font-size: 30px;\r\n    text-align: center;\r\n    margin-bottom: 20px;\r\n  }\r\n\r\n  .slide17[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: flex-flex-start;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n  }\r\n\r\n  \r\n  .slide18[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    width: 100%;\r\n    height: 100%;\r\n  }\r\n  .slide18[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items:center;\r\n    width:100%;\r\n    height: 100%;\r\n  }\r\n  .slide18[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    height: 20%;\r\n    width: 15%;\r\n  }\r\n  .slide18[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    font-size: 30px;\r\n    text-align: center;\r\n    margin-bottom: 20px;\r\n  }\r\n\r\n  .slide18[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: flex-start ;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n  }\r\n\r\n\r\n  \r\n  .slide19[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    width: 100%;\r\n    height: 100%;\r\n  }\r\n  .slide19[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    margin-left: 50px;\r\n    display: flex;\r\n    align-items: center;\r\n    justify-content: center;\r\n    order: 2;\r\n    width: 90%;\r\n    height: 100%;\r\n  }\r\n  .slide19[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-around;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n  }\r\n  .slide19[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n    text-align: center;\r\n  }\r\n\r\n\r\n  \r\n  .slide20[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    width: 100%;\r\n    height: 100%;\r\n  }\r\n  .slide20[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    align-items: center;\r\n    justify-content: center;\r\n    order: 1;\r\n    width: 100%;\r\n    height: 80%;\r\n  }\r\n  .slide20[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n    height: 60%;\r\n    width: 40%;\r\n    filter: drop-shadow(0.4rem 0.4rem 0.45rem rgba(0, 0, 30, 0.5));\r\n  }\r\n  .slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: flex-start;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n  }\r\n  .slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n    text-align: center;\r\n  }\r\n  .slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]{\r\n    margin-top: 0px;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    width: 90%;\r\n  }\r\n  .slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%] {\r\n    margin-top: 0px;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    width: 90%;\r\n  }\r\n\r\n  .slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%]   label[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    align-items: flex-flex-start;\r\n    flex-direction: column;\r\n    margin-top: 10px;\r\n  }\r\n\r\n  \r\n  .slide21[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items: center;\r\n    width: 100%;\r\n    height: 100%;\r\n  }\r\n  .slide21[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    display:none;\r\n    flex-direction: column;\r\n    align-items: center;\r\n    justify-content: center;\r\n    order: 1;\r\n    width: 100%;\r\n    height: 10%;\r\n  }\r\n  .slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content:center;\r\n    align-items: center;\r\n    width:80%;\r\n    height: 80%;\r\n  }\r\n\r\n  .slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    line-height: normal;\r\n    color: green;\r\n    font-size: 30px;\r\n    text-align: center;\r\n  }\r\n  .slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n    line-height: normal;\r\n    font-size: 80px;\r\n    margin: 10px;\r\n  }\r\n  .slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n    line-height: normal;\r\n    color: rgb(0, 0, 133);\r\n    font-size: 30px;\r\n    text-align: center;\r\n  }\r\n  .slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .prix[_ngcontent-%COMP%] {\r\n    color: red;\r\n  }\r\n  .slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .faild[_ngcontent-%COMP%]{\r\n    line-height: normal;\r\n    width: 360px;\r\n    color: red;\r\n    font-size: 30px;\r\n    text-align: center;\r\n  }\r\n  .slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n    width: 80px;\r\n  }\r\n\r\n  .slide21[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]{\r\n    order: 3;\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: flex-end;\r\n    align-items: flex-flex-end;\r\n    width:97%;\r\n    height: 15%;\r\n  }\r\n  .slide21[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n    color: green;\r\n    width: 80px;\r\n  }\r\n\r\n\r\n\r\n  \r\n  .slide24[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    width: 100%;\r\n    height: 100%;\r\n  }\r\n  .slide24[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    margin-left: 50px;\r\n    order: 2;\r\n    display: flex;\r\n    align-items:flex-flex-end;\r\n    width: 88%;\r\n    height: 100%;\r\n  }\r\n  .slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: flex-end;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n    margin-top: 20px;\r\n  }\r\n  .slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n    text-align: center;\r\n  }\r\n  .slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n    width: 80%;\r\n    height: 40px;\r\n    margin: 50px 0 50px 0;\r\n    border-color: rgb(212, 5, 5);\r\n  }\r\n  .slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   mat-radio-group[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: space-around;\r\n    align-items: flex-flex-start;\r\n    width: 100%;\r\n    margin-top: 20px;\r\n\r\n  }\r\n  .slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .calflex-end[_ngcontent-%COMP%]{\r\n    width: 350px;\r\n    height: 350px;\r\n  }\r\n\r\n\r\n  \r\n  .slide25[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items: center;\r\n    width: 100%;\r\n    height: 100%;\r\n  }\r\n  .slide25[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    align-items: flex-flex-end;\r\n    justify-content: flex-flex-end;\r\n    order: 1;\r\n    width: 22%;\r\n    height: 20%;\r\n  }\r\n\r\n  .slide25[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content:flex-start;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 80%;\r\n  }\r\n\r\n  .slide25[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n    width: 50px;\r\n    margin-bottom: 30px;\r\n  }\r\n  .slide25[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n    text-align: center;\r\n    color: rgb(0, 0, 133);\r\n    font-size: 30px;\r\n    line-height: normal;\r\n  }\r\n\r\n  .slide25[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]{\r\n    order: 3;\r\n  }\r\n  .slide25[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n    text-align: center;\r\n    line-height: normal;\r\n  }\r\n  .slide25[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]   .socialMedia[_ngcontent-%COMP%] {\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: center;\r\n  }\r\n  .slide25[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]   .socialMedia[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n    width: 60px;\r\n  }\r\n\r\n\r\n  \r\n  .slide26[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n    align-items: center;\r\n    width: 100%;\r\n    height: 100%;\r\n  }\r\n  .slide26[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items:center;\r\n    width:53%;\r\n    height: 100%;\r\n  }\r\n  .slide26[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    height: 20%;\r\n    width: 15%;\r\n  }\r\n  .slide26[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    font-size: 30px;\r\n    text-align: center;\r\n    margin-bottom: 20px;\r\n  }\r\n\r\n  .slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items: flex-flex-start;\r\n    width:100%;\r\n    height: 100%;\r\n  }\r\n  .slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 50px;\r\n  }\r\n  .slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n    width: 90%;\r\n    height: 40px;\r\n    border-color: rgb(212, 5, 5);\r\n    margin-left: 5px;\r\n  }\r\n\r\n  .slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]{\r\n    margin: 20px;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-around;\r\n    align-items: flex-flex-start;\r\n    width: 90%;\r\n  }\r\n  .slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%] {\r\n    margin-top: 20px;\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: space-around;\r\n    width: 100%;\r\n  }\r\n  .slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%]   mat-radio-group[_ngcontent-%COMP%]  {\r\n    margin-top: 20px;\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: space-around;\r\n    width: 90%;\r\n  }\r\n\r\n\r\n  .slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n    width: 90%;\r\n  }\r\n  .slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   label[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    align-items: center;\r\n    margin: 0;\r\n  }\r\n\r\n\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGVzdC90ZXN0LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLFdBQVc7RUFDWCxZQUFZO0VBQ1osZUFBZTtFQUNmLFNBQVM7RUFDVCxVQUFVO0FBQ1o7QUFDQTtFQUNFLFdBQVc7RUFDWCxZQUFZO0VBQ1osYUFBYTtFQUNiLHFCQUFxQjtFQUNyQix1QkFBdUI7QUFDekI7QUFDQTtFQUNFLFdBQVc7RUFDWCxpQkFBaUI7RUFDakIsaUJBQWlCO0VBQ2pCLFlBQVk7RUFDWiw0QkFBNEI7RUFDNUIsYUFBYTtFQUNiLG1CQUFtQjtBQUNyQjtBQUNBO0VBQ0UsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQiwyQkFBMkI7RUFDM0IsWUFBWTtFQUNaLG1CQUFtQjtFQUNuQixVQUFVO0FBQ1o7QUFDQTtFQUNFLGVBQWU7QUFDakI7QUFDQTtFQUNFLHdCQUF3QjtBQUMxQjtBQUNBO0VBQ0UsWUFBWTtFQUNaLGVBQWU7QUFDakI7QUFDQTtFQUNFLFlBQVk7QUFDZDtBQUVBO0VBQ0UsV0FBVztFQUNYLFlBQVk7RUFDWixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLHVCQUF1QjtFQUN2QixtQkFBbUI7QUFDckI7QUFFQSwrRUFBK0U7QUFDL0U7RUFDRSxnQ0FBZ0M7RUFDaEMsWUFBWTtBQUNkO0FBQ0E7RUFDRSxvQ0FBb0M7RUFDcEMsWUFBWTtBQUNkO0FBQ0EsNkVBQTZFO0FBQzdFO0VBQ0UsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQiw4QkFBOEI7RUFDOUIsV0FBVztFQUNYLFlBQVk7QUFDZDtBQUNBO0VBQ0UsUUFBUTtFQUNSLFVBQVU7RUFDVixZQUFZO0FBQ2Q7QUFFQTtFQUNFLFFBQVE7RUFDUixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLHVCQUF1QjtFQUN2QixtQkFBbUI7RUFDbkIsVUFBVTtFQUNWLFdBQVc7QUFDYjtBQUVBLDZFQUE2RTtBQUM3RTtFQUNFLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsOEJBQThCO0VBQzlCLFdBQVc7RUFDWCxZQUFZO0FBQ2Q7QUFDQTtFQUNFLFFBQVE7RUFDUixVQUFVO0VBQ1YsWUFBWTtBQUNkO0FBQ0E7RUFDRSxRQUFRO0VBQ1IsYUFBYTtFQUNiLHNCQUFzQjtFQUN0Qix1QkFBdUI7RUFDdkIsbUJBQW1CO0VBQ25CLFNBQVM7RUFDVCxZQUFZO0FBQ2Q7QUFDQTtFQUNFLGVBQWU7QUFDakI7QUFDQTtFQUNFLFlBQVk7RUFDWixZQUFZO0VBQ1osbUJBQW1CO0VBQ25CLDRCQUE0QjtBQUM5QjtBQUVBLDZFQUE2RTtBQUM3RTtFQUNFLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsOEJBQThCO0VBQzlCLFdBQVc7RUFDWCxZQUFZO0FBQ2Q7QUFDQTtFQUNFLFFBQVE7RUFDUixVQUFVO0VBQ1YsWUFBWTtBQUNkO0FBQ0E7RUFDRSxRQUFRO0VBQ1IsYUFBYTtFQUNiLHNCQUFzQjtFQUN0Qix1QkFBdUI7RUFDdkIsNEJBQTRCO0VBQzVCLFNBQVM7RUFDVCxZQUFZO0FBQ2Q7QUFDQTtFQUNFLGVBQWU7QUFDakI7QUFDQTtFQUNFLFVBQVU7RUFDVixZQUFZO0VBQ1osNEJBQTRCO0FBQzlCO0FBRUEsNkVBQTZFO0FBQzdFO0VBQ0UsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQiw4QkFBOEI7RUFDOUIsV0FBVztFQUNYLFlBQVk7QUFDZDtBQUNBO0VBQ0UsUUFBUTtFQUNSLFVBQVU7RUFDVixZQUFZO0FBQ2Q7QUFDQTtFQUNFLFFBQVE7RUFDUixhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLDZCQUE2QjtFQUM3QixtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLFNBQVM7RUFDVCxXQUFXO0FBQ2I7QUFDQTtFQUNFLGVBQWU7QUFDakI7QUFDQTtFQUNFLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsdUJBQXVCOztBQUV6QjtBQUNBO0VBQ0UsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQix1QkFBdUI7O0FBRXpCO0FBQ0E7RUFDRSxZQUFZO0VBQ1osWUFBWTtFQUNaLFdBQVc7RUFDWCw0QkFBNEI7QUFDOUI7QUFDQTtFQUNFLFlBQVk7RUFDWixXQUFXO0FBQ2I7QUFFQSw2RUFBNkU7QUFDN0U7RUFDRSxhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLDhCQUE4QjtFQUM5QixXQUFXO0VBQ1gsWUFBWTtBQUNkO0FBQ0E7RUFDRSxpQkFBaUI7RUFDakIsUUFBUTtFQUNSLFVBQVU7RUFDVixZQUFZO0VBQ1osYUFBYTtFQUNiLFVBQVU7RUFDVixnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLFFBQVE7RUFDUixhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLHVCQUF1QjtFQUN2Qiw0QkFBNEI7RUFDNUIsU0FBUztFQUNULFlBQVk7QUFDZDtBQUNBO0VBQ0UsZUFBZTtBQUNqQjtBQUNBO0VBQ0UsVUFBVTtFQUNWLFlBQVk7RUFDWiw0QkFBNEI7QUFDOUI7QUFHQSw2RUFBNkU7QUFDN0U7RUFDRSxhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLDhCQUE4QjtFQUM5QixXQUFXO0VBQ1gsWUFBWTtBQUNkO0FBQ0E7RUFDRSxRQUFRO0VBQ1IsVUFBVTtFQUNWLFlBQVk7RUFDWixVQUFVO0VBQ1YsYUFBYTtFQUNiLGdCQUFnQjtBQUNsQjtBQUdBO0VBQ0UsUUFBUTtFQUNSLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsdUJBQXVCO0VBQ3ZCLG1CQUFtQjtFQUNuQixTQUFTO0VBQ1QsWUFBWTtBQUNkO0FBQ0E7RUFDRSxlQUFlO0FBQ2pCO0FBQ0E7RUFDRSxhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLDZCQUE2QjtFQUM3QixXQUFXO0VBQ1gsV0FBVztBQUNiO0FBQ0E7RUFDRSxhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLHVCQUF1QjtFQUN2QixtQkFBbUI7QUFDckI7QUFFQTtFQUNFLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsdUJBQXVCO0FBQ3pCO0FBRUE7RUFDRSxRQUFRO0VBQ1IsV0FBVztFQUNYLFlBQVk7RUFDWixnQ0FBZ0M7RUFDaEMsa0JBQWtCO0FBQ3BCO0FBQ0E7RUFDRSxRQUFRO0VBQ1IsaUJBQWlCO0VBQ2pCLFVBQVU7RUFDVixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLG1CQUFtQjtBQUNyQjtBQUVBO0VBQ0UsUUFBUTtFQUNSLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsZ0NBQWdDO0FBQ2xDO0FBQ0E7RUFDRSxZQUFZO0VBQ1osOEJBQThCO0VBQzlCLGtCQUFrQjtFQUNsQixVQUFVO0VBQ1Ysa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixZQUFZO0VBQ1osZ0NBQWdDO0VBQ2hDLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsZUFBZTtFQUNmLHVCQUF1QjtFQUN2QixtQkFBbUI7QUFDckI7QUFFQSw2RUFBNkU7QUFDN0U7RUFDRSxhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLDhCQUE4QjtFQUM5QixXQUFXO0VBQ1gsWUFBWTtBQUNkO0FBQ0E7RUFDRSxRQUFRO0VBQ1IsYUFBYTtFQUNiLHNCQUFzQjtFQUN0Qix1QkFBdUI7RUFDdkIsa0JBQWtCO0VBQ2xCLFNBQVM7RUFDVCxZQUFZO0FBQ2Q7QUFDQTtFQUNFLFFBQVE7RUFDUixXQUFXO0VBQ1gsVUFBVTtBQUNaO0FBQ0E7RUFDRSxRQUFRO0VBQ1IsZUFBZTtFQUNmLGtCQUFrQjtFQUNsQixtQkFBbUI7QUFDckI7QUFHQTtFQUNFLFFBQVE7RUFDUixhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLHVCQUF1QjtFQUN2Qiw0QkFBNEI7RUFDNUIsU0FBUztFQUNULFlBQVk7QUFDZDtBQUVBO0VBQ0UsVUFBVTtFQUNWLFlBQVk7RUFDWiw0QkFBNEI7QUFDOUI7QUFHQSw2RUFBNkU7QUFDN0U7RUFDRSxhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLHVCQUF1QjtFQUN2QixXQUFXO0VBQ1gsWUFBWTtBQUNkO0FBQ0E7RUFDRSxRQUFRO0VBQ1IsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQiw2QkFBNkI7RUFDN0IsV0FBVztFQUNYLFlBQVk7RUFDWixtQkFBbUI7QUFDckI7QUFDQTtFQUNFLFFBQVE7RUFDUixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLHVCQUF1QjtFQUN2QixXQUFXO0VBQ1gsZ0JBQWdCO0VBQ2hCLFlBQVk7QUFDZDtBQUNBO0VBQ0UsaUJBQWlCO0VBQ2pCLFFBQVE7RUFDUixVQUFVO0VBQ1YsWUFBWTtBQUNkO0FBQ0E7RUFDRSxRQUFRO0VBQ1IsYUFBYTtFQUNiLHNCQUFzQjtFQUN0Qix1QkFBdUI7RUFDdkIsNEJBQTRCO0VBQzVCLFNBQVM7RUFDVCxZQUFZO0FBQ2Q7QUFDQTtFQUNFLGVBQWU7QUFDakI7QUFDQTtFQUNFLFVBQVU7RUFDVixZQUFZO0VBQ1osNEJBQTRCO0FBQzlCO0FBQ0E7RUFDRSxRQUFRO0FBQ1Y7QUFFQTtFQUNFLHdCQUF3QjtFQUN4QixXQUFXO0VBQ1gsWUFBWTtFQUNaLDhCQUE4QjtFQUM5QixhQUFhO0VBQ2IsZ0NBQWdDO0VBQ2hDLGtCQUFrQjtBQUNwQjtBQUdBLHNCQUFzQjtBQUN0QjtFQUNFLHdCQUF3QjtFQUN4QixnQkFBZ0I7RUFDaEIsV0FBVztFQUNYLFlBQVk7RUFDWiw0QkFBNEI7RUFDNUIsZUFBZTtFQUNmLGtDQUFrQztFQUNsQyxtQkFBbUI7QUFDckI7QUFFQSxnQkFBZ0I7QUFDaEI7RUFDRSxXQUFXO0VBQ1gsWUFBWTtFQUNaLDhCQUE4QjtFQUM5QixlQUFlO0VBQ2YsZ0NBQWdDO0VBQ2hDLGtCQUFrQjtBQUNwQjtBQUNBO0VBQ0UsVUFBVTtFQUNWLFlBQVk7QUFDZDtBQUNBO0VBQ0Usa0JBQWtCO0FBQ3BCO0FBQ0E7RUFDRSxrQkFBa0I7QUFDcEI7QUFFQTtFQUNFLFlBQVk7QUFDZDtBQUlBLGlDQUFpQywwQkFBMEI7QUFDM0Q7QUFDQTtFQUNFLHlCQUF5QjtBQUMzQjtBQUNBO0VBQ0UsaUJBQWlCO0VBQ2pCLGFBQWE7RUFDYiw4QkFBOEI7RUFDOUIsWUFBWTtFQUNaLFlBQVk7RUFDWiwyQkFBMkI7RUFDM0IseUJBQXlCLEVBQUUsV0FBVyxFQUNkLFlBQVksRUFDYixlQUFlO0VBQ3RDLGlCQUFpQixFQUFFLGFBQWE7RUFDaEMseUJBQXlCO0VBQ3pCLG1CQUFtQjtBQUNyQjtBQUNBO0VBQ0UsV0FBVztFQUNYLFlBQVk7RUFDWixnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGNBQWM7RUFDZCxtQkFBbUI7RUFDbkIsU0FBUztFQUNULGlCQUFpQjtBQUNuQjtBQUdBLDZFQUE2RTtBQUM3RTtFQUNFLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsNkJBQTZCO0VBQzdCLFdBQVc7RUFDWCxZQUFZO0FBQ2Q7QUFDQTtFQUNFLFFBQVE7RUFDUixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLFVBQVU7RUFDVixZQUFZO0FBQ2Q7QUFDQTtFQUNFLFFBQVE7RUFDUixhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLHVCQUF1QjtFQUN2Qiw0QkFBNEI7RUFDNUIsU0FBUztFQUNULFlBQVk7QUFDZDtBQUNBO0VBQ0UsZUFBZTtBQUNqQjtBQUNBO0VBQ0UsVUFBVTtFQUNWLFlBQVk7RUFDWiw0QkFBNEI7QUFDOUI7QUFFQSw0RUFBNEU7QUFDNUU7RUFDRSxhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLDZCQUE2QjtFQUM3QixXQUFXO0VBQ1gsWUFBWTtBQUNkO0FBQ0E7RUFDRSxRQUFRO0VBQ1IsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQixVQUFVO0VBQ1YsWUFBWTtBQUNkO0FBQ0E7RUFDRSxRQUFRO0VBQ1IsYUFBYTtFQUNiLHNCQUFzQjtFQUN0Qix1QkFBdUI7RUFDdkIsNEJBQTRCO0VBQzVCLFNBQVM7RUFDVCxZQUFZO0FBQ2Q7QUFDQTtFQUNFLGVBQWU7QUFDakI7QUFDQTtFQUNFLGdCQUFnQjtFQUNoQixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLDZCQUE2QjtFQUM3QixXQUFXO0FBQ2I7QUFHQSw2RUFBNkU7QUFDN0U7RUFDRSxhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLDZCQUE2QjtFQUM3QixXQUFXO0VBQ1gsWUFBWTtBQUNkO0FBQ0E7RUFDRSxRQUFRO0VBQ1IsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQixVQUFVO0VBQ1YsWUFBWTtBQUNkO0FBQ0E7RUFDRSxRQUFRO0VBQ1IsYUFBYTtFQUNiLHNCQUFzQjtFQUN0Qix1QkFBdUI7RUFDdkIsNEJBQTRCO0VBQzVCLFNBQVM7RUFDVCxZQUFZO0FBQ2Q7QUFDQTtFQUNFLGVBQWU7QUFDakI7QUFDQTtFQUNFLGdCQUFnQjtFQUNoQixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLDZCQUE2QjtFQUM3QixXQUFXO0FBQ2I7QUFFQTtFQUNFLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsNkJBQTZCO0VBQzdCLFdBQVc7RUFDWCxnQkFBZ0I7QUFDbEI7QUFFQTtFQUNFLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsNkJBQTZCO0VBQzdCLG1CQUFtQjtBQUNyQjtBQUNBO0VBQ0UsUUFBUTtFQUNSLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsdUJBQXVCO0FBQ3pCO0FBRUE7RUFDRSxRQUFRO0VBQ1IsV0FBVztFQUNYLFlBQVk7RUFDWixnQ0FBZ0M7RUFDaEMsa0JBQWtCO0FBQ3BCO0FBQ0E7RUFDRSxRQUFRO0VBQ1IsaUJBQWlCO0VBQ2pCLFVBQVU7RUFDVixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLG1CQUFtQjtBQUNyQjtBQUVBO0VBQ0UsUUFBUTtFQUNSLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsZ0NBQWdDO0FBQ2xDO0FBQ0E7RUFDRSxZQUFZO0VBQ1osOEJBQThCO0VBQzlCLGtCQUFrQjtFQUNsQixVQUFVO0VBQ1Ysa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixZQUFZO0VBQ1osZ0NBQWdDO0VBQ2hDLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsZUFBZTtFQUNmLHVCQUF1QjtFQUN2QixtQkFBbUI7QUFDckI7QUFHQSw4RUFBOEU7QUFDOUU7RUFDRSxhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLDZCQUE2QjtFQUM3QixXQUFXO0VBQ1gsWUFBWTtBQUNkO0FBQ0E7RUFDRSxtQkFBbUI7RUFDbkIsYUFBYTtFQUNiLFFBQVE7RUFDUixVQUFVO0VBQ1YsWUFBWTtBQUNkO0FBQ0E7RUFDRSxRQUFRO0VBQ1IsYUFBYTtFQUNiLHNCQUFzQjtFQUN0Qix1QkFBdUI7RUFDdkIsNEJBQTRCO0VBQzVCLFNBQVM7RUFDVCxZQUFZO0FBQ2Q7QUFDQTtFQUNFLGVBQWU7QUFDakI7QUFDQTtFQUNFLFVBQVU7RUFDVixZQUFZO0VBQ1osNEJBQTRCO0FBQzlCO0FBRUEsOEVBQThFO0FBQzlFO0VBQ0UsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQiw4QkFBOEI7RUFDOUIsV0FBVztFQUNYLFlBQVk7QUFDZDtBQUNBO0VBQ0UsUUFBUTtFQUNSLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsdUJBQXVCO0VBQ3ZCLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsWUFBWTtBQUNkO0FBQ0E7RUFDRSxRQUFRO0VBQ1IsV0FBVztFQUNYLFVBQVU7QUFDWjtBQUNBO0VBQ0UsUUFBUTtFQUNSLGVBQWU7RUFDZixrQkFBa0I7RUFDbEIsbUJBQW1CO0FBQ3JCO0FBR0E7RUFDRSxRQUFRO0VBQ1IsYUFBYTtFQUNiLHNCQUFzQjtFQUN0Qix1QkFBdUI7RUFDdkIsNEJBQTRCO0VBQzVCLFNBQVM7RUFDVCxZQUFZO0FBQ2Q7QUFFQTtFQUNFLFVBQVU7RUFDVixZQUFZO0VBQ1osNEJBQTRCO0FBQzlCO0FBR0EsOEVBQThFO0FBQzlFO0VBQ0UsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQiw4QkFBOEI7RUFDOUIsV0FBVztFQUNYLFlBQVk7QUFDZDtBQUNBO0VBQ0UsaUJBQWlCO0VBQ2pCLFFBQVE7RUFDUixhQUFhO0VBQ2Isa0JBQWtCO0VBQ2xCLFVBQVU7RUFDVixZQUFZO0FBQ2Q7QUFDQTtFQUNFLFFBQVE7RUFDUixhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLHVCQUF1QjtFQUN2QixtQkFBbUI7RUFDbkIsU0FBUztFQUNULFlBQVk7QUFDZDtBQUNBO0VBQ0UsZUFBZTtBQUNqQjtBQUNBO0VBQ0UsVUFBVTtFQUNWLFlBQVk7RUFDWixxQkFBcUI7RUFDckIsNEJBQTRCO0FBQzlCO0FBR0EsNkVBQTZFO0FBQzdFO0VBQ0UsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQiw2QkFBNkI7RUFDN0IsV0FBVztFQUNYLFlBQVk7QUFDZDtBQUNBO0VBQ0UsUUFBUTtFQUNSLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsVUFBVTtFQUNWLFlBQVk7QUFDZDtBQUNBO0VBQ0UsUUFBUTtFQUNSLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsdUJBQXVCO0VBQ3ZCLDRCQUE0QjtFQUM1QixTQUFTO0VBQ1QsWUFBWTtBQUNkO0FBQ0E7RUFDRSxlQUFlO0FBQ2pCO0FBQ0E7RUFDRSxnQkFBZ0I7RUFDaEIsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQiw2QkFBNkI7RUFDN0IsV0FBVztBQUNiO0FBRUEsOEVBQThFO0FBQzlFO0VBQ0UsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQiw4QkFBOEI7RUFDOUIsV0FBVztFQUNYLFlBQVk7QUFDZDtBQUNBO0VBQ0UsUUFBUTtFQUNSLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsdUJBQXVCO0VBQ3ZCLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsWUFBWTtBQUNkO0FBQ0E7RUFDRSxRQUFROztBQUVWO0FBQ0E7RUFDRSxRQUFRO0VBQ1IsZUFBZTtFQUNmLGtCQUFrQjtFQUNsQixtQkFBbUI7QUFDckI7QUFJQTtFQUNFLFFBQVE7RUFDUixhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLHVCQUF1QjtFQUN2Qiw0QkFBNEI7RUFDNUIsU0FBUztFQUNULFlBQVk7QUFDZDtBQUVBO0VBQ0UsVUFBVTtFQUNWLFlBQVk7RUFDWiw0QkFBNEI7QUFDOUI7QUFHQSw2RUFBNkU7QUFDN0U7RUFDRSxhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLDZCQUE2QjtFQUM3QixXQUFXO0VBQ1gsWUFBWTtBQUNkO0FBQ0E7RUFDRSxRQUFRO0VBQ1IsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQixVQUFVO0VBQ1YsWUFBWTtBQUNkO0FBQ0E7RUFDRSxRQUFRO0VBQ1IsYUFBYTtFQUNiLHNCQUFzQjtFQUN0Qix1QkFBdUI7RUFDdkIsNEJBQTRCO0VBQzVCLFNBQVM7RUFDVCxZQUFZO0FBQ2Q7QUFDQTtFQUNFLGVBQWU7QUFDakI7QUFDQTtFQUNFLGdCQUFnQjtFQUNoQixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLDZCQUE2QjtFQUM3QixXQUFXO0FBQ2I7QUFJQSw4RUFBOEU7QUFDOUU7RUFDRSxhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLDhCQUE4QjtFQUM5QixXQUFXO0VBQ1gsWUFBWTtBQUNkO0FBQ0E7RUFDRSxRQUFRO0VBQ1IsYUFBYTtFQUNiLHNCQUFzQjtFQUN0Qix1QkFBdUI7RUFDdkIsa0JBQWtCO0VBQ2xCLFNBQVM7RUFDVCxZQUFZO0FBQ2Q7QUFDQTtFQUNFLFFBQVE7O0FBRVY7QUFDQTtFQUNFLFFBQVE7RUFDUixlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLG1CQUFtQjtBQUNyQjtBQUlBO0VBQ0UsUUFBUTtFQUNSLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsdUJBQXVCO0VBQ3ZCLDRCQUE0QjtFQUM1QixTQUFTO0VBQ1QsWUFBWTtBQUNkO0FBRUE7RUFDRSxVQUFVO0VBQ1YsWUFBWTtFQUNaLDRCQUE0QjtBQUM5QjtBQUdBLDhFQUE4RTtBQUM5RTtFQUNFLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsOEJBQThCO0VBQzlCLFdBQVc7RUFDWCxZQUFZO0FBQ2Q7QUFDQTtFQUNFLFFBQVE7RUFDUixhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLHVCQUF1QjtFQUN2QixrQkFBa0I7RUFDbEIsU0FBUztFQUNULFlBQVk7QUFDZDtBQUNBO0VBQ0UsUUFBUTtFQUNSLFdBQVc7RUFDWCxVQUFVO0FBQ1o7QUFDQTtFQUNFLFFBQVE7RUFDUixlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLG1CQUFtQjtBQUNyQjtBQUdBO0VBQ0UsUUFBUTtFQUNSLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsdUJBQXVCO0VBQ3ZCLG1CQUFtQjtFQUNuQixTQUFTO0VBQ1QsWUFBWTtBQUNkO0FBRUE7RUFDRSxVQUFVO0VBQ1YsWUFBWTtFQUNaLDRCQUE0QjtBQUM5QjtBQUlBLDhFQUE4RTtBQUM5RTtFQUNFLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsOEJBQThCO0VBQzlCLFdBQVc7RUFDWCxZQUFZO0FBQ2Q7QUFDQTtFQUNFLGlCQUFpQjtFQUNqQixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLFFBQVE7RUFDUixVQUFVO0VBQ1YsWUFBWTtBQUNkO0FBQ0E7RUFDRSxRQUFRO0VBQ1IsYUFBYTtFQUNiLHNCQUFzQjtFQUN0Qix1QkFBdUI7RUFDdkIsNEJBQTRCO0VBQzVCLFNBQVM7RUFDVCxZQUFZO0FBQ2Q7QUFDQTtFQUNFLGVBQWU7QUFDakI7QUFDQTtFQUNFLFVBQVU7RUFDVixZQUFZO0VBQ1osNEJBQTRCO0FBQzlCO0FBSUEsOEVBQThFO0FBQzlFO0VBQ0UsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQiw4QkFBOEI7RUFDOUIsV0FBVztFQUNYLFlBQVk7QUFDZDtBQUNBO0VBQ0UsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQix1QkFBdUI7RUFDdkIsUUFBUTtFQUNSLFVBQVU7RUFDVixXQUFXO0FBQ2I7QUFDQTtFQUNFLFdBQVc7RUFDWCxVQUFVO0VBQ1YsOERBQThEO0FBQ2hFO0FBQ0E7RUFDRSxRQUFRO0VBQ1IsYUFBYTtFQUNiLHNCQUFzQjtFQUN0Qix1QkFBdUI7RUFDdkIsNEJBQTRCO0VBQzVCLFNBQVM7RUFDVCxZQUFZO0FBQ2Q7QUFDQTtFQUNFLGVBQWU7QUFDakI7QUFDQTtFQUNFLFVBQVU7RUFDVixZQUFZO0VBQ1osNEJBQTRCO0FBQzlCO0FBRUE7RUFDRSxnQkFBZ0I7RUFDaEIsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQiw4QkFBOEI7RUFDOUIsVUFBVTtBQUNaO0FBQ0E7RUFDRSxnQkFBZ0I7RUFDaEIsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQiw4QkFBOEI7RUFDOUIsVUFBVTtBQUNaO0FBRUE7RUFDRSxVQUFVO0FBQ1o7QUFDQTtFQUNFLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsU0FBUztBQUNYO0FBQ0E7RUFDRSxVQUFVO0FBQ1o7QUFDQTtFQUNFLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsU0FBUztBQUNYO0FBRUEsOEVBQThFO0FBQzlFO0VBQ0UsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQix1QkFBdUI7RUFDdkIsbUJBQW1CO0VBQ25CLFdBQVc7RUFDWCxXQUFXO0FBQ2I7QUFDQTtFQUNFLGFBQWE7RUFDYiwwQkFBMEI7RUFDMUIsOEJBQThCO0VBQzlCLFFBQVE7RUFDUixVQUFVO0VBQ1YsWUFBWTtBQUNkO0FBQ0E7RUFDRSxXQUFXO0VBQ1gsVUFBVTtBQUNaO0FBQ0E7RUFDRSxRQUFRO0VBQ1IsYUFBYTtFQUNiLHNCQUFzQjtFQUN0QiwwQkFBMEI7RUFDMUIsbUJBQW1CO0VBQ25CLFNBQVM7RUFDVCxXQUFXO0FBQ2I7QUFFQTtFQUNFLG1CQUFtQjtFQUNuQixZQUFZO0VBQ1osZUFBZTtBQUNqQjtBQUNBOztFQUVFLGdCQUFnQjtFQUNoQixZQUFZO0FBQ2Q7QUFDQTtFQUNFLG1CQUFtQjtFQUNuQixxQkFBcUI7RUFDckIsZUFBZTtBQUNqQjtBQUNBO0VBQ0UsVUFBVTtFQUNWLG1CQUFtQjtBQUNyQjtBQUNBO0VBQ0UsVUFBVTtFQUNWLG1CQUFtQjtFQUNuQixlQUFlO0FBQ2pCO0FBQ0E7RUFDRSxZQUFZO0FBQ2Q7QUFJQTtFQUNFLFFBQVE7RUFDUixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLGdDQUFnQztFQUNoQywwQkFBMEI7RUFDMUIsVUFBVTtFQUNWLFdBQVc7QUFDYjtBQUNBO0VBQ0UsWUFBWTtFQUNaLFdBQVc7QUFDYjtBQUdBLDhFQUE4RTtBQUM5RTtFQUNFLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsOEJBQThCO0VBQzlCLFdBQVc7RUFDWCxZQUFZO0FBQ2Q7QUFDQTtFQUNFLGlCQUFpQjtFQUNqQixRQUFRO0VBQ1IsYUFBYTtFQUNiLHlCQUF5QjtFQUN6QixVQUFVO0VBQ1YsWUFBWTtFQUNaLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsUUFBUTtFQUNSLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsdUJBQXVCO0VBQ3ZCLG1CQUFtQjtFQUNuQixTQUFTO0VBQ1QsWUFBWTtBQUNkO0FBQ0E7RUFDRSxlQUFlO0VBQ2Ysa0JBQWtCO0FBQ3BCO0FBQ0E7RUFDRSxVQUFVO0VBQ1YsWUFBWTtFQUNaLHFCQUFxQjtFQUNyQiw0QkFBNEI7QUFDOUI7QUFDQTtFQUNFLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsNkJBQTZCO0VBQzdCLDRCQUE0QjtFQUM1QixXQUFXOztBQUViO0FBQ0E7RUFDRSxZQUFZO0VBQ1osYUFBYTtBQUNmO0FBR0EsOEVBQThFO0FBQzlFO0VBQ0UsYUFBYTtFQUNiLHNCQUFzQjtFQUN0Qix1QkFBdUI7RUFDdkIsbUJBQW1CO0VBQ25CLFdBQVc7RUFDWCxZQUFZO0FBQ2Q7QUFDQTtFQUNFLGFBQWE7RUFDYiwwQkFBMEI7RUFDMUIsOEJBQThCO0VBQzlCLFFBQVE7RUFDUixVQUFVO0VBQ1YsV0FBVztBQUNiO0FBRUE7RUFDRSxRQUFRO0VBQ1IsYUFBYTtFQUNiLHNCQUFzQjtFQUN0QiwrQkFBK0I7RUFDL0IsbUJBQW1CO0VBQ25CLFNBQVM7RUFDVCxXQUFXO0FBQ2I7QUFFQTtFQUNFLFdBQVc7QUFDYjtBQUNBO0VBQ0UscUJBQXFCO0VBQ3JCLGVBQWU7QUFDakI7QUFFQTtFQUNFLFFBQVE7QUFDVjtBQUNBO0VBQ0Usa0JBQWtCO0FBQ3BCO0FBQ0E7RUFDRSxhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLHVCQUF1QjtBQUN6QjtBQUNBO0VBQ0UsV0FBVztBQUNiO0FBR0EsOEVBQThFO0FBQzlFO0VBQ0UsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQiw4QkFBOEI7RUFDOUIsV0FBVztFQUNYLFlBQVk7QUFDZDtBQUNBO0VBQ0UsUUFBUTtFQUNSLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsdUJBQXVCO0VBQ3ZCLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsWUFBWTtBQUNkO0FBQ0E7RUFDRSxRQUFRO0VBQ1IsV0FBVztFQUNYLFVBQVU7QUFDWjtBQUNBO0VBQ0UsUUFBUTtFQUNSLGVBQWU7RUFDZixrQkFBa0I7RUFDbEIsbUJBQW1CO0FBQ3JCO0FBRUE7RUFDRSxRQUFRO0VBQ1IsYUFBYTtFQUNiLHNCQUFzQjtFQUN0Qix1QkFBdUI7RUFDdkIsNEJBQTRCO0VBQzVCLFNBQVM7RUFDVCxZQUFZO0FBQ2Q7QUFDQTtFQUNFLGVBQWU7QUFDakI7QUFDQTtFQUNFLFVBQVU7RUFDVixZQUFZO0VBQ1osNEJBQTRCO0FBQzlCO0FBRUE7RUFDRSxZQUFZO0VBQ1osYUFBYTtFQUNiLG1CQUFtQjtFQUNuQiw4QkFBOEI7RUFDOUIsNEJBQTRCO0VBQzVCLFVBQVU7QUFDWjtBQUNBO0VBQ0UsZ0JBQWdCO0VBQ2hCLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsNkJBQTZCO0VBQzdCLFdBQVc7QUFDYjtBQUNBO0VBQ0UsZ0JBQWdCO0VBQ2hCLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsNkJBQTZCO0VBQzdCLFVBQVU7QUFDWjtBQUdBO0VBQ0UsVUFBVTtBQUNaO0FBQ0E7RUFDRSxhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLFNBQVM7QUFDWDtBQUVBLDhFQUE4RTtBQUM5RTtFQUNFLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsOEJBQThCO0VBQzlCLFdBQVc7RUFDWCxZQUFZO0FBQ2Q7QUFDQTtFQUNFLFFBQVE7RUFDUixhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLHVCQUF1QjtFQUN2QixrQkFBa0I7RUFDbEIsU0FBUztFQUNULFlBQVk7QUFDZDtBQUtBO0VBQ0UsUUFBUTtFQUNSLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsZ0NBQWdDO0VBQ2hDLFNBQVM7RUFDVCxZQUFZOztBQUVkO0FBQ0E7RUFDRSxtQkFBbUI7RUFDbkIsVUFBVTtFQUNWLG1CQUFtQjtFQUNuQixvQ0FBb0M7RUFDcEMsV0FBVztBQUNiO0FBRUE7RUFDRSxzQkFBc0I7RUFDdEIsVUFBVTtBQUNaO0FBQ0E7RUFDRSxvQ0FBb0M7RUFDcEMsYUFBYTtFQUNiLCtCQUErQjtFQUMvQix1Q0FBdUM7QUFDekM7QUFDQTtFQUNFLGtDQUFrQztBQUNwQztBQUNBO0VBQ0UsV0FBVztFQUNYLGFBQWE7RUFDYixrQkFBa0I7QUFDcEI7QUFNQSwwR0FBMEc7QUFDMUc7O0VBRUU7SUFDRSxXQUFXO0lBQ1gsWUFBWTtJQUNaLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsOEJBQThCO0VBQ2hDO0VBQ0E7SUFDRSxzQkFBc0I7RUFDeEI7RUFDQTtJQUNFLHVCQUF1QjtJQUN2QixtQkFBbUI7SUFDbkIsV0FBVztFQUNiO0VBQ0E7SUFDRSxXQUFXO0VBQ2I7RUFDQTtJQUNFLGtCQUFrQjtJQUNsQixjQUFjO0VBQ2hCO0VBQ0E7SUFDRSxVQUFVO0lBQ1YsaUJBQWlCO0VBQ25CO0VBQ0E7SUFDRSxrQkFBa0I7RUFDcEI7O0VBRUEsNkVBQTZFO0VBQzdFO0lBQ0Usc0JBQXNCO0lBQ3RCLGdDQUFnQztJQUNoQyxvQkFBb0I7SUFDcEIsWUFBWTtFQUNkO0VBQ0E7SUFDRSxRQUFRO0lBQ1IsVUFBVTtJQUNWLFlBQVk7RUFDZDs7RUFFQTtJQUNFLFVBQVU7RUFDWjs7RUFFQTtJQUNFLGVBQWU7RUFDakI7OztFQUdBLDZFQUE2RTtFQUM3RTtJQUNFLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsOEJBQThCO0lBQzlCLG1CQUFtQjtJQUNuQixXQUFXO0lBQ1gsWUFBWTtFQUNkO0VBQ0E7SUFDRSxRQUFRO0lBQ1IsVUFBVTtJQUNWLFlBQVk7RUFDZDs7RUFFQTtJQUNFLFdBQVc7RUFDYjtFQUNBO0lBQ0UsZUFBZTtFQUNqQjs7RUFFQSw2RUFBNkU7RUFDN0U7SUFDRSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDZCQUE2QjtJQUM3QixXQUFXO0lBQ1gsWUFBWTtFQUNkO0VBQ0E7SUFDRSxRQUFRO0lBQ1IsV0FBVztJQUNYLFlBQVk7SUFDWixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHdCQUF3QjtFQUMxQjtFQUNBO0lBQ0UsUUFBUTtJQUNSLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsNkJBQTZCO0lBQzdCLG1CQUFtQjtJQUNuQixVQUFVO0lBQ1YsWUFBWTtFQUNkO0VBQ0E7SUFDRSxlQUFlO0lBQ2Ysa0JBQWtCO0VBQ3BCOztFQUVBLDZFQUE2RTtFQUM3RTtJQUNFLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsOEJBQThCO0lBQzlCLFdBQVc7SUFDWCxZQUFZO0VBQ2Q7RUFDQTtJQUNFLFFBQVE7SUFDUixXQUFXO0lBQ1gsWUFBWTtJQUNaLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIseUJBQXlCO0VBQzNCO0VBQ0E7SUFDRSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qiw2QkFBNkI7SUFDN0IsbUJBQW1CO0lBQ25CLFVBQVU7SUFDVixZQUFZO0VBQ2Q7RUFDQTtJQUNFLGVBQWU7SUFDZixrQkFBa0I7RUFDcEI7OztFQUdBLDZFQUE2RTtFQUM3RTtJQUNFLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsNkJBQTZCO0lBQzdCLG1CQUFtQjtJQUNuQixXQUFXO0lBQ1gsWUFBWTtFQUNkO0VBQ0E7SUFDRSxRQUFRO0lBQ1IsVUFBVTtJQUNWLFlBQVk7SUFDWixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHdCQUF3QjtFQUMxQjtFQUNBO0lBQ0UsUUFBUTtJQUNSLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsNkJBQTZCO0lBQzdCLG1CQUFtQjtJQUNuQixVQUFVO0lBQ1YsWUFBWTtFQUNkO0VBQ0E7SUFDRSxlQUFlO0lBQ2Ysa0JBQWtCO0VBQ3BCOzs7RUFHQSw2RUFBNkU7RUFDN0U7SUFDRSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDhCQUE4QjtJQUM5QixtQkFBbUI7SUFDbkIsV0FBVztJQUNYLFlBQVk7O0VBRWQ7RUFDQTtJQUNFLFFBQVE7SUFDUixXQUFXO0lBQ1gsWUFBWTtJQUNaLFVBQVU7O0VBRVo7RUFDQTtJQUNFLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDhCQUE4QjtJQUM5QixtQkFBbUI7SUFDbkIsVUFBVTtJQUNWLFlBQVk7RUFDZDtFQUNBO0lBQ0UsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQixrQkFBa0I7RUFDcEI7RUFDQTs7SUFFRSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2QixXQUFXO0lBQ1gsV0FBVztFQUNiO0VBQ0E7SUFDRSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2QixxQkFBcUI7SUFDckIsVUFBVTtFQUNaOztFQUVBO0lBQ0UsYUFBYTtJQUNiLG1CQUFtQjtJQUNuQix1QkFBdUI7SUFDdkIsWUFBWTtFQUNkOztFQUVBO0lBQ0UsUUFBUTtJQUNSLFdBQVc7SUFDWCxjQUFjO0lBQ2QsZ0NBQWdDO0lBQ2hDLGtCQUFrQjtFQUNwQjs7RUFFQSw2RUFBNkU7RUFDN0U7SUFDRSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2QixXQUFXO0lBQ1gsWUFBWTtFQUNkO0VBQ0E7SUFDRSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qix1QkFBdUI7SUFDdkIsa0JBQWtCO0lBQ2xCLFVBQVU7SUFDVixZQUFZO0VBQ2Q7RUFDQTtJQUNFLFFBQVE7SUFDUixXQUFXO0lBQ1gsVUFBVTtFQUNaO0VBQ0E7SUFDRSxRQUFRO0lBQ1IsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQixtQkFBbUI7RUFDckI7OztFQUdBO0lBQ0UsUUFBUTtJQUNSLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsZ0NBQWdDO0lBQ2hDLG1CQUFtQjtJQUNuQixVQUFVO0lBQ1YsWUFBWTtFQUNkOzs7RUFHQSw2RUFBNkU7RUFDN0U7SUFDRSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDhCQUE4QjtJQUM5QixtQkFBbUI7SUFDbkIsV0FBVztJQUNYLFlBQVk7RUFDZDtFQUNBO0lBQ0UsUUFBUTtJQUNSLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsOEJBQThCO0lBQzlCLFdBQVc7SUFDWCxZQUFZO0VBQ2Q7RUFDQTtJQUNFLFFBQVE7SUFDUixhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLHVCQUF1QjtJQUN2QixxQkFBcUI7SUFDckIsV0FBVztJQUNYLFlBQVk7RUFDZDtFQUNBO0lBQ0UsUUFBUTtJQUNSLFVBQVU7SUFDVixZQUFZO0VBQ2Q7RUFDQTtJQUNFLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHlCQUF5QjtJQUN6QixtQkFBbUI7SUFDbkIsVUFBVTtJQUNWLFlBQVk7SUFDWixnQkFBZ0I7RUFDbEI7RUFDQTtJQUNFLGVBQWU7SUFDZixrQkFBa0I7RUFDcEI7RUFDQTtJQUNFLFVBQVU7SUFDVixZQUFZO0lBQ1osNEJBQTRCO0VBQzlCO0VBQ0E7SUFDRSxRQUFRO0VBQ1Y7O0VBRUE7SUFDRSx3QkFBd0I7SUFDeEIsV0FBVztJQUNYLFlBQVk7SUFDWiw4QkFBOEI7SUFDOUIsYUFBYTtJQUNiLGdDQUFnQztJQUNoQyxrQkFBa0I7RUFDcEI7OztFQUdBLHNCQUFzQjtFQUN0QjtJQUNFLHdCQUF3QjtJQUN4QixnQkFBZ0I7SUFDaEIsV0FBVztJQUNYLFlBQVk7SUFDWiw0QkFBNEI7SUFDNUIsZUFBZTtJQUNmLGtDQUFrQztJQUNsQyxtQkFBbUI7RUFDckI7O0VBRUEsZ0JBQWdCO0VBQ2hCO0lBQ0UsV0FBVztJQUNYLFlBQVk7SUFDWiw4QkFBOEI7SUFDOUIsZUFBZTtJQUNmLGdDQUFnQztJQUNoQyxrQkFBa0I7RUFDcEI7RUFDQTtJQUNFLFVBQVU7SUFDVixZQUFZO0VBQ2Q7RUFDQTtJQUNFLGtCQUFrQjtFQUNwQjtFQUNBO0lBQ0Usa0JBQWtCO0VBQ3BCOztFQUVBO0lBQ0UsWUFBWTtFQUNkOzs7O0VBSUEsaUNBQWlDLDBCQUEwQjtFQUMzRDtFQUNBO0lBQ0UseUJBQXlCO0VBQzNCO0VBQ0E7SUFDRSxpQkFBaUI7SUFDakIsYUFBYTtJQUNiLDhCQUE4QjtJQUM5QixZQUFZO0lBQ1osV0FBVztJQUNYLDJCQUEyQjtJQUMzQix5QkFBeUIsRUFBRSxXQUFXLEVBQ2QsWUFBWSxFQUNiLGVBQWU7SUFDdEMsaUJBQWlCLEVBQUUsYUFBYTtJQUNoQyx5QkFBeUI7SUFDekIsbUJBQW1CO0VBQ3JCO0VBQ0E7SUFDRSxXQUFXO0lBQ1gsWUFBWTtJQUNaLGdCQUFnQjtJQUNoQixvQkFBb0I7SUFDcEIsY0FBYztJQUNkLG1CQUFtQjtJQUNuQixTQUFTO0lBQ1QsaUJBQWlCO0VBQ25COzs7RUFHQSw2RUFBNkU7RUFDN0U7SUFDRSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDhCQUE4QjtJQUM5QixXQUFXO0lBQ1gsWUFBWTtFQUNkO0VBQ0E7SUFDRSxRQUFRO0lBQ1IsYUFBYTtJQUNiLDBCQUEwQjtJQUMxQixXQUFXO0lBQ1gsWUFBWTtFQUNkO0VBQ0E7SUFDRSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qiw2QkFBNkI7SUFDN0IsbUJBQW1CO0lBQ25CLFVBQVU7SUFDVixZQUFZO0VBQ2Q7RUFDQTtJQUNFLGVBQWU7SUFDZixrQkFBa0I7RUFDcEI7O0VBRUEsNEVBQTRFO0VBQzVFO0lBQ0UsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qiw4QkFBOEI7SUFDOUIsV0FBVztJQUNYLGFBQWE7RUFDZjtFQUNBO0lBQ0UsUUFBUTtJQUNSLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsc0JBQXNCO0lBQ3RCLHlCQUF5QjtJQUN6QixXQUFXO0lBQ1gsWUFBWTtFQUNkO0VBQ0E7SUFDRSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qiw2QkFBNkI7SUFDN0IsbUJBQW1CO0lBQ25CLFVBQVU7SUFDVixZQUFZO0VBQ2Q7RUFDQTtJQUNFLGVBQWU7SUFDZixrQkFBa0I7RUFDcEI7RUFDQTtJQUNFLGdCQUFnQjtJQUNoQixhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLDZCQUE2QjtJQUM3QixXQUFXO0VBQ2I7OztFQUdBLDZFQUE2RTtFQUM3RTtJQUNFLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsOEJBQThCO0lBQzlCLFdBQVc7SUFDWCxhQUFhO0VBQ2Y7RUFDQTtJQUNFLFFBQVE7SUFDUixhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLFdBQVc7SUFDWCxZQUFZO0VBQ2Q7RUFDQTtJQUNFLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2QixtQkFBbUI7SUFDbkIsVUFBVTtJQUNWLFlBQVk7RUFDZDtFQUNBO0lBQ0UsZUFBZTtJQUNmLGtCQUFrQjtFQUNwQjtFQUNBO0lBQ0UsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qix1QkFBdUI7SUFDdkIsV0FBVztJQUNYLGdCQUFnQjtFQUNsQjs7RUFFQTtJQUNFLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsNkJBQTZCO0lBQzdCLG1CQUFtQjtJQUNuQixnQkFBZ0I7RUFDbEI7Ozs7RUFJQSw4RUFBOEU7RUFDOUU7SUFDRSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDhCQUE4QjtJQUM5QixXQUFXO0lBQ1gsYUFBYTtFQUNmO0VBQ0E7SUFDRSxtQkFBbUI7SUFDbkIsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qix5QkFBeUI7SUFDekIsUUFBUTtJQUNSLFdBQVc7SUFDWCxZQUFZO0VBQ2Q7RUFDQTtJQUNFLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDZCQUE2QjtJQUM3QixtQkFBbUI7SUFDbkIsVUFBVTtJQUNWLFlBQVk7RUFDZDtFQUNBO0lBQ0UsZUFBZTtJQUNmLGtCQUFrQjtFQUNwQjs7RUFFQSw4RUFBOEU7RUFDOUU7SUFDRSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHNCQUFzQjtJQUN0QixXQUFXO0lBQ1gsYUFBYTtFQUNmO0VBQ0E7SUFDRSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qiw2QkFBNkI7SUFDN0Isa0JBQWtCO0lBQ2xCLFVBQVU7SUFDVixZQUFZO0VBQ2Q7RUFDQTtJQUNFLFFBQVE7SUFDUixlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCLG1CQUFtQjtFQUNyQjs7O0VBR0E7SUFDRSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qix1QkFBdUI7SUFDdkIsbUJBQW1CO0lBQ25CLFVBQVU7SUFDVixZQUFZO0VBQ2Q7OztFQUdBLDhFQUE4RTtFQUM5RTtJQUNFLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsdUJBQXVCO0lBQ3ZCLFdBQVc7SUFDWCxhQUFhO0VBQ2Y7RUFDQTtJQUNFLFFBQVE7SUFDUixVQUFVO0lBQ1YsWUFBWTtFQUNkO0VBQ0E7SUFDRSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qix1QkFBdUI7SUFDdkIsbUJBQW1CO0lBQ25CLFVBQVU7SUFDVixZQUFZO0VBQ2Q7RUFDQTtJQUNFLGVBQWU7SUFDZixrQkFBa0I7RUFDcEI7O0VBRUEsNkVBQTZFO0VBQzdFO0lBQ0UsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qiw4QkFBOEI7SUFDOUIsV0FBVztJQUNYLGFBQWE7RUFDZjtFQUNBO0lBQ0UsUUFBUTtJQUNSLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIseUJBQXlCO0lBQ3pCLFdBQVc7SUFDWCxZQUFZO0VBQ2Q7RUFDQTtJQUNFLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDZCQUE2QjtJQUM3QixtQkFBbUI7SUFDbkIsVUFBVTtJQUNWLFlBQVk7RUFDZDtFQUNBO0lBQ0UsZUFBZTtJQUNmLGtCQUFrQjtFQUNwQjtFQUNBLDhFQUE4RTtFQUM5RTtJQUNFLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsOEJBQThCO0lBQzlCLFdBQVc7SUFDWCxZQUFZO0VBQ2Q7RUFDQTtJQUNFLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2QixrQkFBa0I7SUFDbEIsVUFBVTtJQUNWLFlBQVk7RUFDZDtFQUNBO0lBQ0UsUUFBUTs7RUFFVjtFQUNBO0lBQ0UsUUFBUTtJQUNSLGVBQWU7SUFDZixrQkFBa0I7SUFDbEIsbUJBQW1CO0VBQ3JCOzs7O0VBSUE7SUFDRSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0QixnQ0FBZ0M7SUFDaEMsa0JBQWtCO0lBQ2xCLFVBQVU7SUFDVixZQUFZO0VBQ2Q7Ozs7RUFJQSw2RUFBNkU7RUFDN0U7SUFDRSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDhCQUE4QjtJQUM5QixXQUFXO0lBQ1gsYUFBYTtFQUNmO0VBQ0E7SUFDRSxRQUFRO0lBQ1IsYUFBYTtJQUNiLG1CQUFtQjtJQUNuQix5QkFBeUI7SUFDekIsV0FBVztJQUNYLFlBQVk7RUFDZDtFQUNBO0lBQ0UsUUFBUTtJQUNSLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsZ0NBQWdDO0lBQ2hDLG1CQUFtQjtJQUNuQixVQUFVO0lBQ1YsWUFBWTtFQUNkO0VBQ0E7SUFDRSxlQUFlO0lBQ2Ysa0JBQWtCO0VBQ3BCOztFQUVBLDhFQUE4RTtFQUM5RTtJQUNFLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsOEJBQThCO0lBQzlCLFdBQVc7SUFDWCxZQUFZO0VBQ2Q7RUFDQTtJQUNFLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2QixrQkFBa0I7SUFDbEIsVUFBVTtJQUNWLFlBQVk7RUFDZDtFQUNBO0lBQ0UsUUFBUTtJQUNSLGVBQWU7SUFDZixrQkFBa0I7SUFDbEIsbUJBQW1CO0VBQ3JCOztFQUVBO0lBQ0UsUUFBUTtJQUNSLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsZ0NBQWdDO0lBQ2hDLG1CQUFtQjtJQUNuQixVQUFVO0lBQ1YsWUFBWTtFQUNkOztFQUVBLDhFQUE4RTtFQUM5RTtJQUNFLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsdUJBQXVCO0lBQ3ZCLFdBQVc7SUFDWCxZQUFZO0VBQ2Q7RUFDQTtJQUNFLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2QixrQkFBa0I7SUFDbEIsVUFBVTtJQUNWLFlBQVk7RUFDZDtFQUNBO0lBQ0UsUUFBUTtJQUNSLFdBQVc7SUFDWCxVQUFVO0VBQ1o7RUFDQTtJQUNFLFFBQVE7SUFDUixlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCLG1CQUFtQjtFQUNyQjs7RUFFQTtJQUNFLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDRCQUE0QjtJQUM1QixtQkFBbUI7SUFDbkIsVUFBVTtJQUNWLFlBQVk7RUFDZDs7O0VBR0EsOEVBQThFO0VBQzlFO0lBQ0UsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qix1QkFBdUI7SUFDdkIsV0FBVztJQUNYLFlBQVk7RUFDZDtFQUNBO0lBQ0UsaUJBQWlCO0lBQ2pCLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsdUJBQXVCO0lBQ3ZCLFFBQVE7SUFDUixVQUFVO0lBQ1YsWUFBWTtFQUNkO0VBQ0E7SUFDRSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qiw2QkFBNkI7SUFDN0IsbUJBQW1CO0lBQ25CLFVBQVU7SUFDVixZQUFZO0VBQ2Q7RUFDQTtJQUNFLGVBQWU7SUFDZixrQkFBa0I7RUFDcEI7OztFQUdBLDhFQUE4RTtFQUM5RTtJQUNFLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsdUJBQXVCO0lBQ3ZCLFdBQVc7SUFDWCxZQUFZO0VBQ2Q7RUFDQTtJQUNFLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsdUJBQXVCO0lBQ3ZCLFFBQVE7SUFDUixXQUFXO0lBQ1gsV0FBVztFQUNiO0VBQ0E7SUFDRSxXQUFXO0lBQ1gsVUFBVTtJQUNWLDhEQUE4RDtFQUNoRTtFQUNBO0lBQ0UsUUFBUTtJQUNSLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsMkJBQTJCO0lBQzNCLG1CQUFtQjtJQUNuQixVQUFVO0lBQ1YsWUFBWTtFQUNkO0VBQ0E7SUFDRSxlQUFlO0lBQ2Ysa0JBQWtCO0VBQ3BCO0VBQ0E7SUFDRSxlQUFlO0lBQ2YsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qix1QkFBdUI7SUFDdkIsVUFBVTtFQUNaO0VBQ0E7SUFDRSxlQUFlO0lBQ2YsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qix1QkFBdUI7SUFDdkIsVUFBVTtFQUNaOztFQUVBO0lBQ0UsYUFBYTtJQUNiLDRCQUE0QjtJQUM1QixzQkFBc0I7SUFDdEIsZ0JBQWdCO0VBQ2xCOztFQUVBLDhFQUE4RTtFQUM5RTtJQUNFLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsdUJBQXVCO0lBQ3ZCLG1CQUFtQjtJQUNuQixXQUFXO0lBQ1gsWUFBWTtFQUNkO0VBQ0E7SUFDRSxZQUFZO0lBQ1osc0JBQXNCO0lBQ3RCLG1CQUFtQjtJQUNuQix1QkFBdUI7SUFDdkIsUUFBUTtJQUNSLFdBQVc7SUFDWCxXQUFXO0VBQ2I7RUFDQTtJQUNFLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHNCQUFzQjtJQUN0QixtQkFBbUI7SUFDbkIsU0FBUztJQUNULFdBQVc7RUFDYjs7RUFFQTtJQUNFLG1CQUFtQjtJQUNuQixZQUFZO0lBQ1osZUFBZTtJQUNmLGtCQUFrQjtFQUNwQjtFQUNBO0lBQ0UsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixZQUFZO0VBQ2Q7RUFDQTtJQUNFLG1CQUFtQjtJQUNuQixxQkFBcUI7SUFDckIsZUFBZTtJQUNmLGtCQUFrQjtFQUNwQjtFQUNBO0lBQ0UsVUFBVTtFQUNaO0VBQ0E7SUFDRSxtQkFBbUI7SUFDbkIsWUFBWTtJQUNaLFVBQVU7SUFDVixlQUFlO0lBQ2Ysa0JBQWtCO0VBQ3BCO0VBQ0E7SUFDRSxXQUFXO0VBQ2I7O0VBRUE7SUFDRSxRQUFRO0lBQ1IsYUFBYTtJQUNiLG1CQUFtQjtJQUNuQix5QkFBeUI7SUFDekIsMEJBQTBCO0lBQzFCLFNBQVM7SUFDVCxXQUFXO0VBQ2I7RUFDQTtJQUNFLFlBQVk7SUFDWixXQUFXO0VBQ2I7Ozs7RUFJQSw4RUFBOEU7RUFDOUU7SUFDRSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2QixXQUFXO0lBQ1gsWUFBWTtFQUNkO0VBQ0E7SUFDRSxpQkFBaUI7SUFDakIsUUFBUTtJQUNSLGFBQWE7SUFDYix5QkFBeUI7SUFDekIsVUFBVTtJQUNWLFlBQVk7RUFDZDtFQUNBO0lBQ0UsUUFBUTtJQUNSLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIseUJBQXlCO0lBQ3pCLG1CQUFtQjtJQUNuQixVQUFVO0lBQ1YsWUFBWTtJQUNaLGdCQUFnQjtFQUNsQjtFQUNBO0lBQ0UsZUFBZTtJQUNmLGtCQUFrQjtFQUNwQjtFQUNBO0lBQ0UsVUFBVTtJQUNWLFlBQVk7SUFDWixxQkFBcUI7SUFDckIsNEJBQTRCO0VBQzlCO0VBQ0E7SUFDRSxhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLDZCQUE2QjtJQUM3Qiw0QkFBNEI7SUFDNUIsV0FBVztJQUNYLGdCQUFnQjs7RUFFbEI7RUFDQTtJQUNFLFlBQVk7SUFDWixhQUFhO0VBQ2Y7OztFQUdBLDhFQUE4RTtFQUM5RTtJQUNFLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsdUJBQXVCO0lBQ3ZCLG1CQUFtQjtJQUNuQixXQUFXO0lBQ1gsWUFBWTtFQUNkO0VBQ0E7SUFDRSxhQUFhO0lBQ2IsMEJBQTBCO0lBQzFCLDhCQUE4QjtJQUM5QixRQUFRO0lBQ1IsVUFBVTtJQUNWLFdBQVc7RUFDYjs7RUFFQTtJQUNFLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDBCQUEwQjtJQUMxQixtQkFBbUI7SUFDbkIsVUFBVTtJQUNWLFdBQVc7RUFDYjs7RUFFQTtJQUNFLFdBQVc7SUFDWCxtQkFBbUI7RUFDckI7RUFDQTtJQUNFLGtCQUFrQjtJQUNsQixxQkFBcUI7SUFDckIsZUFBZTtJQUNmLG1CQUFtQjtFQUNyQjs7RUFFQTtJQUNFLFFBQVE7RUFDVjtFQUNBO0lBQ0Usa0JBQWtCO0lBQ2xCLG1CQUFtQjtFQUNyQjtFQUNBO0lBQ0UsYUFBYTtJQUNiLG1CQUFtQjtJQUNuQix1QkFBdUI7RUFDekI7RUFDQTtJQUNFLFdBQVc7RUFDYjs7O0VBR0EsOEVBQThFO0VBQzlFO0lBQ0UsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qiw4QkFBOEI7SUFDOUIsbUJBQW1CO0lBQ25CLFdBQVc7SUFDWCxZQUFZO0VBQ2Q7RUFDQTtJQUNFLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2QixrQkFBa0I7SUFDbEIsU0FBUztJQUNULFlBQVk7RUFDZDtFQUNBO0lBQ0UsUUFBUTtJQUNSLFdBQVc7SUFDWCxVQUFVO0VBQ1o7RUFDQTtJQUNFLFFBQVE7SUFDUixlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCLG1CQUFtQjtFQUNyQjs7RUFFQTtJQUNFLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2Qiw0QkFBNEI7SUFDNUIsVUFBVTtJQUNWLFlBQVk7RUFDZDtFQUNBO0lBQ0UsZUFBZTtFQUNqQjtFQUNBO0lBQ0UsVUFBVTtJQUNWLFlBQVk7SUFDWiw0QkFBNEI7SUFDNUIsZ0JBQWdCO0VBQ2xCOztFQUVBO0lBQ0UsWUFBWTtJQUNaLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsNkJBQTZCO0lBQzdCLDRCQUE0QjtJQUM1QixVQUFVO0VBQ1o7RUFDQTtJQUNFLGdCQUFnQjtJQUNoQixhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLDZCQUE2QjtJQUM3QixXQUFXO0VBQ2I7RUFDQTtJQUNFLGdCQUFnQjtJQUNoQixhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLDZCQUE2QjtJQUM3QixVQUFVO0VBQ1o7OztFQUdBO0lBQ0UsVUFBVTtFQUNaO0VBQ0E7SUFDRSxhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLFNBQVM7RUFDWDs7O0FBR0YiLCJmaWxlIjoic3JjL2FwcC90ZXN0L3Rlc3QuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb250YWluZXJ7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogMTAwJTtcclxuICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgbWFyZ2luOiAwO1xyXG4gIHBhZGRpbmc6IDA7XHJcbn1cclxuLmJvZHl7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246Y29sdW1uO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG59XHJcbi5mb290ZXJ7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgbWF4LWhlaWdodDogMTUwcHg7XHJcbiAgbWluLWhlaWdodDogMTUwcHg7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICAjMTExRDVFZmY7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG59XHJcbi5sZWZ0e1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgd2lkdGg6IDEwJTtcclxufVxyXG4ucG9pbnR7XHJcbiAgZm9udC1zaXplOiAxMHB4O1xyXG59XHJcbm1hdC1pY29ue1xyXG4gIGNvbG9yOnJnYigxNTMsIDE1MywgMTUzKTtcclxufVxyXG5tYXQtaWNvbjpob3ZlcntcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgY3Vyc29yOiBwb2ludGVyO1xyXG59XHJcbi5jaGVja0ljb257XHJcbiAgY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG4uY2VudGVye1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogMTAwJTtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxufVxyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYnRuKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLmJ1dHR0b25SRWR7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDIwNiwgMCwgMCk7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG59XHJcbmJ1dHRvbjpob3ZlcntcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMTQ1LCAxMzYsIDEzNik7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG59XHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAxICoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uc2xpZGUxe1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTEgLmltYWdle1xyXG4gIG9yZGVyOiAyO1xyXG4gIHdpZHRoOiA3MCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcblxyXG4uc2xpZGUxIC50ZXN0e1xyXG4gIG9yZGVyOiAxO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIHdpZHRoOiA0MCU7XHJcbiAgaGVpZ2h0OiA2MCU7XHJcbn1cclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAwICoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uc2xpZGUwe1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTAgLmltYWdle1xyXG4gIG9yZGVyOiAyO1xyXG4gIHdpZHRoOiA1MiU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTAgLnRlc3R7XHJcbiAgb3JkZXI6IDE7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgd2lkdGg6NDAlO1xyXG4gIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUwIC50ZXN0IGgxe1xyXG4gIGZvbnQtc2l6ZTogNTBweDtcclxufVxyXG4uc2xpZGUwIC50ZXN0IGJ1dHRvbntcclxuICB3aWR0aDogMTUwcHg7XHJcbiAgaGVpZ2h0OiA1MHB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDMwcHg7XHJcbiAgYm9yZGVyLWNvbG9yOiByZ2IoMTkyLCAzLCAzKTtcclxufVxyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDIgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5zbGlkZTJ7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMiAuaW1hZ2V7XHJcbiAgb3JkZXI6IDE7XHJcbiAgd2lkdGg6IDU1JTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMiAudGVzdHtcclxuICBvcmRlcjogMjtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGZsZXgtZmxleC1zdGFydDtcclxuICB3aWR0aDo0MCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTIgLnRlc3QgaDF7XHJcbiAgZm9udC1zaXplOiA1MHB4O1xyXG59XHJcbi5zbGlkZTIgLnRlc3QgaW5wdXR7XHJcbiAgd2lkdGg6IDgwJTtcclxuICBoZWlnaHQ6IDQwcHg7XHJcbiAgYm9yZGVyLWNvbG9yOiByZ2IoMjEyLCA1LCA1KTtcclxufVxyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDMgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5zbGlkZTN7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMyAuaW1hZ2V7XHJcbiAgb3JkZXI6IDI7XHJcbiAgd2lkdGg6IDU0JTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMyAudGVzdHtcclxuICBvcmRlcjogMTtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBtYXJnaW4tdG9wOiA1MHB4O1xyXG4gIHdpZHRoOjQwJTtcclxuICBoZWlnaHQ6IDcwJTtcclxufVxyXG4uc2xpZGUzIC50ZXN0IGgxe1xyXG4gIGZvbnQtc2l6ZTogNTBweDtcclxufVxyXG4uc2xpZGUzIC50ZXN0IC5ibG9ja0J0bntcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcblxyXG59XHJcbi5zbGlkZTMgLnRlc3QgLmJsb2NrQnRuIC5zb3VzQmxvY2t7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5cclxufVxyXG4uc2xpZGUzIC50ZXN0IC5ibG9ja0J0biBidXR0b257XHJcbiAgd2lkdGg6IDMxMHB4O1xyXG4gIGhlaWdodDogNTBweDtcclxuICBtYXJnaW46IDVweDtcclxuICBib3JkZXItY29sb3I6IHJnYigxOTIsIDMsIDMpO1xyXG59XHJcbi5zbGlkZTMgLnRlc3QgLmJsb2NrQnRuIC5zb3VzQmxvY2sgYnV0dG9ue1xyXG4gIHdpZHRoOiAxNTBweDtcclxuICBtYXJnaW46IDVweDtcclxufVxyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDQgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5zbGlkZTR7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlNCAuaW1hZ2V7XHJcbiAgbWFyZ2luLWxlZnQ6IDUwcHg7XHJcbiAgb3JkZXI6IDE7XHJcbiAgd2lkdGg6IDMzJTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICB6LWluZGV4OiAxO1xyXG4gIG1hcmdpbi10b3A6IDU1cHg7XHJcbn1cclxuLnNsaWRlNCAudGVzdHtcclxuICBvcmRlcjogMjtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGZsZXgtZmxleC1zdGFydDtcclxuICB3aWR0aDo0MCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTQgLnRlc3QgaDF7XHJcbiAgZm9udC1zaXplOiA1MHB4O1xyXG59XHJcbi5zbGlkZTQgLnRlc3QgaW5wdXR7XHJcbiAgd2lkdGg6IDgwJTtcclxuICBoZWlnaHQ6IDQwcHg7XHJcbiAgYm9yZGVyLWNvbG9yOiByZ2IoMjEyLCA1LCA1KTtcclxufVxyXG5cclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSA1ICoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uc2xpZGU1e1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTUgLmltYWdle1xyXG4gIG9yZGVyOiAxO1xyXG4gIHdpZHRoOiA1MCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIHotaW5kZXg6IDE7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBtYXJnaW4tdG9wOiA2NHB4O1xyXG59XHJcblxyXG5cclxuLnNsaWRlNSAudGVzdHtcclxuICBvcmRlcjogMjtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICB3aWR0aDo0MCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTUgLnRlc3QgaDF7XHJcbiAgZm9udC1zaXplOiA1MHB4O1xyXG59XHJcbi5zbGlkZTUgLnRlc3QgLmJsb2NrQnRue1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDIwJTtcclxufVxyXG4uc2xpZGU1IC50ZXN0IC5ibG9ja0J0biAuc291c0Jsb2Nre1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG59XHJcblxyXG4uc2xpZGU1IC50ZXN0IC5ibG9ja0J0biAuc291c0Jsb2NrIC5xdHl7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG59XHJcblxyXG4uc2xpZGU1IC50ZXN0IC5ibG9ja0J0biAuc291c0Jsb2NrIC5xdHkgaW5wdXR7XHJcbiAgb3JkZXI6IDI7XHJcbiAgd2lkdGg6IDgwcHg7XHJcbiAgaGVpZ2h0OiA1MHB4O1xyXG4gIGJvcmRlcjogNHB4IHNvbGlkIHJnYigyMTIsIDUsIDUpO1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4uc2xpZGU1IC50ZXN0IC5ibG9ja0J0biAuc291c0Jsb2NrIC5xdHkgcHtcclxuICBvcmRlcjogMTtcclxuICBtYXJnaW46IDAgNXB4IDAgMDtcclxuICBwYWRkaW5nOiAwO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG59XHJcblxyXG4uc2xpZGU1IC50ZXN0IC5ibG9ja0J0biAuc291c0Jsb2NrIC5xdHkgLnF0eUJ0bntcclxuICBvcmRlcjogMztcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWZsZXgtc3RhcnQ7XHJcbn1cclxuLnNsaWRlNSAudGVzdCAuYmxvY2tCdG4gLnNvdXNCbG9jayAucXR5IC5xdHlCdG4gYnV0dG9ue1xyXG4gIGJvcmRlcjogbm9uZTtcclxuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgd2hpdGU7XHJcbiAgbWFyZ2luLWJvdHRvbTogMXB4O1xyXG4gIHdpZHRoOiA1cHg7XHJcbiAgYm9yZGVyLXJhZGl1czogMHB4O1xyXG4gIGhlaWdodDogMjVweDtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDIxMiwgNSwgNSk7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGZvbnQtc2l6ZTogMjBweDtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG59XHJcblxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgNiAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLnNsaWRlNntcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGU2IC5pbWFnZXtcclxuICBvcmRlcjogMjtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6Y2VudGVyO1xyXG4gIHdpZHRoOjUzJTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlNiAuaW1hZ2UgaW1ne1xyXG4gIG9yZGVyOiAyO1xyXG4gIGhlaWdodDogMjUlO1xyXG4gIHdpZHRoOiAxNSU7XHJcbn1cclxuLnNsaWRlNiAuaW1hZ2UgaDF7XHJcbiAgb3JkZXI6IDE7XHJcbiAgZm9udC1zaXplOiA1MHB4O1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG59XHJcblxyXG5cclxuLnNsaWRlNiAudGVzdHtcclxuICBvcmRlcjogMjtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGZsZXgtZmxleC1zdGFydDtcclxuICB3aWR0aDo0MCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcblxyXG4uc2xpZGU2IC50ZXN0IHNlbGVjdHtcclxuICB3aWR0aDogODAlO1xyXG4gIGhlaWdodDogNDBweDtcclxuICBib3JkZXItY29sb3I6IHJnYigyMTIsIDUsIDUpO1xyXG59XHJcblxyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDcgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5zbGlkZTd7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGU3IC5jb2x1bTF7XHJcbiAgb3JkZXI6IDE7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogMTAwJTtcclxuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG59XHJcbi5zbGlkZTcgLmNvbHVtMntcclxuICBvcmRlcjogMjtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgbWFyZ2luLXRvcDogMjBweDtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlNyAuaW1hZ2V7XHJcbiAgbWFyZ2luLWxlZnQ6IDUwcHg7XHJcbiAgb3JkZXI6IDE7XHJcbiAgd2lkdGg6IDM3JTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlNyAudGVzdHtcclxuICBvcmRlcjogMjtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGZsZXgtZmxleC1zdGFydDtcclxuICB3aWR0aDo0MCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTcgLnRlc3QgaDF7XHJcbiAgZm9udC1zaXplOiA1MHB4O1xyXG59XHJcbi5zbGlkZTcgLnRlc3QgaW5wdXR7XHJcbiAgd2lkdGg6IDgwJTtcclxuICBoZWlnaHQ6IDQwcHg7XHJcbiAgYm9yZGVyLWNvbG9yOiByZ2IoMjEyLCA1LCA1KTtcclxufVxyXG4uc2xpZGU3IC5wcm9ncmVzc3tcclxuICBvcmRlcjogMztcclxufVxyXG5cclxuLnNsaWRlciB7XHJcbiAgLXdlYmtpdC1hcHBlYXJhbmNlOiBub25lO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogMTVweDtcclxuICBiYWNrZ3JvdW5kOiByZ2IoMjU1LCAyNTUsIDI1NSk7XHJcbiAgb3V0bGluZTogbm9uZTtcclxuICBib3JkZXI6IDVweCBzb2xpZCByZ2IoMTg5LCA4LCA4KTtcclxuICBib3JkZXItcmFkaXVzOiA4cHg7XHJcbn1cclxuXHJcblxyXG4vKiBmb3IgY2hyb21lL3NhZmFyaSAqL1xyXG4uc2xpZGVyOjotd2Via2l0LXNsaWRlci10aHVtYiB7XHJcbiAgLXdlYmtpdC1hcHBlYXJhbmNlOiBub25lO1xyXG4gIGFwcGVhcmFuY2U6IG5vbmU7XHJcbiAgd2lkdGg6IDIwcHg7XHJcbiAgaGVpZ2h0OiA2MHB4O1xyXG4gIGJhY2tncm91bmQ6IHJnYigyNDgsIDIyNCwgNSk7XHJcbiAgY3Vyc29yOiBwb2ludGVyO1xyXG4gIGJvcmRlcjogNXB4IHNvbGlkIHJnYigyNDgsIDIyNCwgNSk7XHJcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcclxufVxyXG5cclxuLyogZm9yIGZpcmVmb3ggKi9cclxuLnNsaWRlcjo6LW1vei1yYW5nZS10aHVtYiB7XHJcbiAgd2lkdGg6IDIwcHg7XHJcbiAgaGVpZ2h0OiA2MHB4O1xyXG4gIGJhY2tncm91bmQ6IHJnYigyNTUsIDI1NSwgMjU1KTtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgYm9yZGVyOiA1cHggc29saWQgcmdiKDE4OSwgOCwgOCk7XHJcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG59XHJcbi5fX3Jhbmdle1xyXG4gIHdpZHRoOiA4MCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5fX3JhbmdlLXN0ZXB7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG59XHJcbi5fX3JhbmdlLXN0ZXB7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG59XHJcblxyXG4uX19yYW5nZS1tYXh7XHJcbiAgZmxvYXQ6IHJpZ2h0O1xyXG59XHJcblxyXG5cclxuXHJcbi5fX3JhbmdlIGlucHV0OjpyYW5nZS1wcm9ncmVzcyB7XHRiYWNrZ3JvdW5kOiByZ2IoMTg5LCA4LCA4KTtcclxufVxyXG4uc2xpZGVyIGlucHV0W3R5cGU9cmFuZ2VdOjotbW96LXJhbmdlLXByb2dyZXNzIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzY1N2EwO1xyXG59XHJcbi5fX3JhbmdlLXN0ZXAgZGF0YWxpc3Qge1xyXG4gIHBvc2l0aW9uOnJlbGF0aXZlO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gIGhlaWdodDogYXV0bztcclxuICBib3R0b206IDEwcHg7XHJcbiAgLyogZGlzYWJsZSB0ZXh0IHNlbGVjdGlvbiAqL1xyXG4gIC13ZWJraXQtdXNlci1zZWxlY3Q6IG5vbmU7IC8qIFNhZmFyaSAqL1xyXG4gIC1tb3otdXNlci1zZWxlY3Q6IG5vbmU7IC8qIEZpcmVmb3ggKi9cclxuICAtbXMtdXNlci1zZWxlY3Q6IG5vbmU7IC8qIElFMTArL0VkZ2UgKi9cclxuICB1c2VyLXNlbGVjdDogbm9uZTsgLyogU3RhbmRhcmQgKi9cclxuICAvKiBkaXNhYmxlIGNsaWNrIGV2ZW50cyAqL1xyXG4gIHBvaW50ZXItZXZlbnRzOm5vbmU7XHJcbn1cclxuLl9fcmFuZ2Utc3RlcCBkYXRhbGlzdCBvcHRpb24ge1xyXG4gIHdpZHRoOiAxMHB4O1xyXG4gIGhlaWdodDogMTBweDtcclxuICBtaW4taGVpZ2h0OiAxMHB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDEwMHB4O1xyXG4gIC8qIGhpZGUgdGV4dCAqL1xyXG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgcGFkZGluZzowO1xyXG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xyXG59XHJcblxyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDggKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5zbGlkZTh7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGU4IC5pbWFnZXtcclxuICBvcmRlcjogMjtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgd2lkdGg6IDM3JTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlOCAudGVzdHtcclxuICBvcmRlcjogMTtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGZsZXgtZmxleC1zdGFydDtcclxuICB3aWR0aDo0MCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTggLnRlc3QgaDF7XHJcbiAgZm9udC1zaXplOiA1MHB4O1xyXG59XHJcbi5zbGlkZTggLnRlc3Qgc2VsZWN0e1xyXG4gIHdpZHRoOiA4MCU7XHJcbiAgaGVpZ2h0OiA0MHB4O1xyXG4gIGJvcmRlci1jb2xvcjogcmdiKDIxMiwgNSwgNSk7XHJcbn1cclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSA5KioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5zbGlkZTl7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGU5IC5pbWFnZXtcclxuICBvcmRlcjogMTtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgd2lkdGg6IDM3JTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlOSAudGVzdHtcclxuICBvcmRlcjogMjtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGZsZXgtZmxleC1zdGFydDtcclxuICB3aWR0aDo0MCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTkgLnRlc3QgaDF7XHJcbiAgZm9udC1zaXplOiA1MHB4O1xyXG59XHJcbi5zbGlkZTkgLnRlc3QgbWF0LXJhZGlvLWdyb3Vwe1xyXG4gIG1hcmdpbi10b3A6IDUwcHg7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDEwKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5zbGlkZTEwe1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMTAgLmltYWdle1xyXG4gIG9yZGVyOiAyO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICB3aWR0aDogMzclO1xyXG4gIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUxMCAudGVzdHtcclxuICBvcmRlcjogMTtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGZsZXgtZmxleC1zdGFydDtcclxuICB3aWR0aDo0MCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTEwIC50ZXN0IGgxe1xyXG4gIGZvbnQtc2l6ZTogNTBweDtcclxufVxyXG4uc2xpZGUxMCAudGVzdCBtYXQtcmFkaW8tZ3JvdXB7XHJcbiAgbWFyZ2luLXRvcDogNTBweDtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5zbGlkZTEwIC50ZXN0IC5ibG9ja3tcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgbWFyZ2luLXRvcDogMjBweDtcclxufVxyXG5cclxuLnNsaWRlMTAgLnRlc3QgLmJsb2NrIC5zb3VzQmxvY2t7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbn1cclxuLnNsaWRlMTAgLnRlc3QgLmJsb2NrIC5zb3VzQmxvY2sgLnF0eXtcclxuICBvcmRlcjogMjtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbn1cclxuXHJcbi5zbGlkZTEwIC50ZXN0IC5ibG9jayAuc291c0Jsb2NrIC5xdHkgaW5wdXR7XHJcbiAgb3JkZXI6IDI7XHJcbiAgd2lkdGg6IDgwcHg7XHJcbiAgaGVpZ2h0OiA1MHB4O1xyXG4gIGJvcmRlcjogNHB4IHNvbGlkIHJnYigyMTIsIDUsIDUpO1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4uc2xpZGUxMCAudGVzdCAuYmxvY2sgLnNvdXNCbG9jayAgcHtcclxuICBvcmRlcjogMTtcclxuICBtYXJnaW46IDAgNXB4IDAgMDtcclxuICBwYWRkaW5nOiAwO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG59XHJcblxyXG4uc2xpZGUxMCAudGVzdCAuYmxvY2sgLnNvdXNCbG9jayAucXR5IC5xdHlCdG57XHJcbiAgb3JkZXI6IDM7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGp1c3RpZnktY29udGVudDogZmxleC1mbGV4LXN0YXJ0O1xyXG59XHJcbi5zbGlkZTEwIC50ZXN0IC5ibG9jayAuc291c0Jsb2NrIC5xdHkgLnF0eUJ0biBidXR0b257XHJcbiAgYm9yZGVyOiBub25lO1xyXG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCB3aGl0ZTtcclxuICBtYXJnaW4tYm90dG9tOiAxcHg7XHJcbiAgd2lkdGg6IDVweDtcclxuICBib3JkZXItcmFkaXVzOiAwcHg7XHJcbiAgaGVpZ2h0OiAyNXB4O1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMjEyLCA1LCA1KTtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgZm9udC1zaXplOiAyMHB4O1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbn1cclxuXHJcblxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMTEgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5zbGlkZTExe1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMTEgLmltYWdle1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBvcmRlcjogMTtcclxuICB3aWR0aDogMzclO1xyXG4gIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUxMSAudGVzdHtcclxuICBvcmRlcjogMjtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGZsZXgtZmxleC1zdGFydDtcclxuICB3aWR0aDo0MCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTExIC50ZXN0IGgxe1xyXG4gIGZvbnQtc2l6ZTogNTBweDtcclxufVxyXG4uc2xpZGUxMSAudGVzdCBpbnB1dHtcclxuICB3aWR0aDogODAlO1xyXG4gIGhlaWdodDogNDBweDtcclxuICBib3JkZXItY29sb3I6IHJnYigyMTIsIDUsIDUpO1xyXG59XHJcblxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMTIgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5zbGlkZTEye1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTEyIC5pbWFnZXtcclxuICBvcmRlcjogMjtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6Y2VudGVyO1xyXG4gIHdpZHRoOjUzJTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMTIgLmltYWdlIGltZ3tcclxuICBvcmRlcjogMjtcclxuICBoZWlnaHQ6IDE1JTtcclxuICB3aWR0aDogMTUlO1xyXG59XHJcbi5zbGlkZTEyIC5pbWFnZSBoMXtcclxuICBvcmRlcjogMTtcclxuICBmb250LXNpemU6IDUwcHg7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbn1cclxuXHJcblxyXG4uc2xpZGUxMiAudGVzdHtcclxuICBvcmRlcjogMjtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGZsZXgtZmxleC1zdGFydDtcclxuICB3aWR0aDo0MCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcblxyXG4uc2xpZGUxMiAudGVzdCBzZWxlY3R7XHJcbiAgd2lkdGg6IDgwJTtcclxuICBoZWlnaHQ6IDQwcHg7XHJcbiAgYm9yZGVyLWNvbG9yOiByZ2IoMjEyLCA1LCA1KTtcclxufVxyXG5cclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAxMyAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLnNsaWRlMTN7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMTMgLmltYWdle1xyXG4gIG1hcmdpbi1sZWZ0OiA1MHB4O1xyXG4gIG9yZGVyOiAxO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgYWxpZ24taXRlbXM6Y2VudGVyO1xyXG4gIHdpZHRoOiAzNyU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTEzIC50ZXN0e1xyXG4gIG9yZGVyOiAyO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIHdpZHRoOjQwJTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMTMgLnRlc3QgaDF7XHJcbiAgZm9udC1zaXplOiA1MHB4O1xyXG59XHJcbi5zbGlkZTEzIC50ZXN0IGlucHV0e1xyXG4gIHdpZHRoOiA4MCU7XHJcbiAgaGVpZ2h0OiA0MHB4O1xyXG4gIG1hcmdpbjogNTBweCAwIDUwcHggMDtcclxuICBib3JkZXItY29sb3I6IHJnYigyMTIsIDUsIDUpO1xyXG59XHJcblxyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDE0KioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5zbGlkZTE0e1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMTQgLmltYWdle1xyXG4gIG9yZGVyOiAyO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICB3aWR0aDogMzclO1xyXG4gIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUxNCAudGVzdHtcclxuICBvcmRlcjogMTtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGZsZXgtZmxleC1zdGFydDtcclxuICB3aWR0aDo0MCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTE0IC50ZXN0IGgxe1xyXG4gIGZvbnQtc2l6ZTogNTBweDtcclxufVxyXG4uc2xpZGUxNCAudGVzdCBtYXQtcmFkaW8tZ3JvdXB7XHJcbiAgbWFyZ2luLXRvcDogNTBweDtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAxNSAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLnNsaWRlMTV7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMTUgLmltYWdle1xyXG4gIG9yZGVyOiAyO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBhbGlnbi1pdGVtczpjZW50ZXI7XHJcbiAgd2lkdGg6NTMlO1xyXG4gIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUxNSAuaW1hZ2UgcHtcclxuICBvcmRlcjogMjtcclxuXHJcbn1cclxuLnNsaWRlMTUgLmltYWdlIGgxe1xyXG4gIG9yZGVyOiAxO1xyXG4gIGZvbnQtc2l6ZTogNTBweDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxufVxyXG5cclxuXHJcblxyXG4uc2xpZGUxNSAudGVzdHtcclxuICBvcmRlcjogMjtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGZsZXgtZmxleC1zdGFydDtcclxuICB3aWR0aDo0MCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcblxyXG4uc2xpZGUxNSAudGVzdCBzZWxlY3R7XHJcbiAgd2lkdGg6IDgwJTtcclxuICBoZWlnaHQ6IDQwcHg7XHJcbiAgYm9yZGVyLWNvbG9yOiByZ2IoMjEyLCA1LCA1KTtcclxufVxyXG5cclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAxNioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uc2xpZGUxNntcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTE2IC5pbWFnZXtcclxuICBvcmRlcjogMjtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgd2lkdGg6IDM3JTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMTYgLnRlc3R7XHJcbiAgb3JkZXI6IDE7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOiBmbGV4LWZsZXgtc3RhcnQ7XHJcbiAgd2lkdGg6NDAlO1xyXG4gIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUxNiAudGVzdCBoMXtcclxuICBmb250LXNpemU6IDUwcHg7XHJcbn1cclxuLnNsaWRlMTYgLnRlc3QgbWF0LXJhZGlvLWdyb3Vwe1xyXG4gIG1hcmdpbi10b3A6IDUwcHg7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG5cclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAxNyAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLnNsaWRlMTd7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMTcgLmltYWdle1xyXG4gIG9yZGVyOiAyO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBhbGlnbi1pdGVtczpjZW50ZXI7XHJcbiAgd2lkdGg6NTMlO1xyXG4gIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUxNyAuaW1hZ2UgcHtcclxuICBvcmRlcjogMjtcclxuXHJcbn1cclxuLnNsaWRlMTcgLmltYWdlIGgxe1xyXG4gIG9yZGVyOiAxO1xyXG4gIGZvbnQtc2l6ZTogNTBweDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxufVxyXG5cclxuXHJcblxyXG4uc2xpZGUxNyAudGVzdHtcclxuICBvcmRlcjogMjtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGZsZXgtZmxleC1zdGFydDtcclxuICB3aWR0aDo0MCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcblxyXG4uc2xpZGUxNyAudGVzdCBzZWxlY3R7XHJcbiAgd2lkdGg6IDgwJTtcclxuICBoZWlnaHQ6IDQwcHg7XHJcbiAgYm9yZGVyLWNvbG9yOiByZ2IoMjEyLCA1LCA1KTtcclxufVxyXG5cclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAxOCAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLnNsaWRlMTh7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMTggLmltYWdle1xyXG4gIG9yZGVyOiAyO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBhbGlnbi1pdGVtczpjZW50ZXI7XHJcbiAgd2lkdGg6NTMlO1xyXG4gIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUxOCAuaW1hZ2UgaW1ne1xyXG4gIG9yZGVyOiAyO1xyXG4gIGhlaWdodDogMzAlO1xyXG4gIHdpZHRoOiAxNSU7XHJcbn1cclxuLnNsaWRlMTggLmltYWdlIGgxe1xyXG4gIG9yZGVyOiAyO1xyXG4gIGZvbnQtc2l6ZTogNTBweDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxufVxyXG5cclxuXHJcbi5zbGlkZTE4IC50ZXN0e1xyXG4gIG9yZGVyOiAxO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIHdpZHRoOjQwJTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuXHJcbi5zbGlkZTE4IC50ZXN0IHNlbGVjdHtcclxuICB3aWR0aDogODAlO1xyXG4gIGhlaWdodDogNDBweDtcclxuICBib3JkZXItY29sb3I6IHJnYigyMTIsIDUsIDUpO1xyXG59XHJcblxyXG5cclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAxOSAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLnNsaWRlMTl7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMTkgLmltYWdle1xyXG4gIG1hcmdpbi1sZWZ0OiA1MHB4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBvcmRlcjogMTtcclxuICB3aWR0aDogMzclO1xyXG4gIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUxOSAudGVzdHtcclxuICBvcmRlcjogMjtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGZsZXgtZmxleC1zdGFydDtcclxuICB3aWR0aDo0MCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTE5IC50ZXN0IGgxe1xyXG4gIGZvbnQtc2l6ZTogNTBweDtcclxufVxyXG4uc2xpZGUxOSAudGVzdCBpbnB1dHtcclxuICB3aWR0aDogODAlO1xyXG4gIGhlaWdodDogNDBweDtcclxuICBib3JkZXItY29sb3I6IHJnYigyMTIsIDUsIDUpO1xyXG59XHJcblxyXG5cclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAyMCAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLnNsaWRlMjB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMjAgLmltYWdle1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBvcmRlcjogMTtcclxuICB3aWR0aDogNjAlO1xyXG4gIGhlaWdodDogODAlO1xyXG59XHJcbi5zbGlkZTIwIC5pbWFnZSBpbWd7XHJcbiAgaGVpZ2h0OiA4MCU7XHJcbiAgd2lkdGg6IDQwJTtcclxuICBmaWx0ZXI6IGRyb3Atc2hhZG93KDAuNHJlbSAwLjRyZW0gMC40NXJlbSByZ2JhKDAsIDAsIDMwLCAwLjUpKTtcclxufVxyXG4uc2xpZGUyMCAudGVzdHtcclxuICBvcmRlcjogMjtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGZsZXgtZmxleC1zdGFydDtcclxuICB3aWR0aDo2MCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTIwIC50ZXN0IGgxe1xyXG4gIGZvbnQtc2l6ZTogNTBweDtcclxufVxyXG4uc2xpZGUyMCAudGVzdCBpbnB1dHtcclxuICB3aWR0aDogOTAlO1xyXG4gIGhlaWdodDogNDBweDtcclxuICBib3JkZXItY29sb3I6IHJnYigyMTIsIDUsIDUpO1xyXG59XHJcblxyXG4uc2xpZGUyMCAudGVzdCAuYmxvY2sxe1xyXG4gIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICB3aWR0aDogOTAlO1xyXG59XHJcbi5zbGlkZTIwIC50ZXN0IC5ibG9jazIge1xyXG4gIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICB3aWR0aDogOTAlO1xyXG59XHJcblxyXG4uc2xpZGUyMCAudGVzdCAuYmxvY2syIGlucHV0e1xyXG4gIHdpZHRoOiA0MCU7XHJcbn1cclxuLnNsaWRlMjAgLnRlc3QgLmJsb2NrMiBsYWJlbHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgbWFyZ2luOiAwO1xyXG59XHJcbi5zbGlkZTIwIC50ZXN0IC5ibG9jazEgaW5wdXR7XHJcbiAgd2lkdGg6IDkwJTtcclxufVxyXG4uc2xpZGUyMCAudGVzdCAuYmxvY2sxIGxhYmVse1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBtYXJnaW46IDA7XHJcbn1cclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAyMSAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLnNsaWRlMjF7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiA3MCU7XHJcbn1cclxuLnNsaWRlMjEgLmltYWdle1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgYWxpZ24taXRlbXM6IGZsZXgtZmxleC1lbmQ7XHJcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWZsZXgtZW5kO1xyXG4gIG9yZGVyOiAxO1xyXG4gIHdpZHRoOiAyMiU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTIxIC5pbWFnZSBpbWd7XHJcbiAgaGVpZ2h0OiA3MCU7XHJcbiAgd2lkdGg6IDMwJTtcclxufVxyXG4uc2xpZGUyMSAudGVzdHtcclxuICBvcmRlcjogMjtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OmZsZXgtc3RhcnQ7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICB3aWR0aDo3MCU7XHJcbiAgaGVpZ2h0OiA4MCU7XHJcbn1cclxuXHJcbi5zbGlkZTIxIC50ZXN0IGgxe1xyXG4gIGxpbmUtaGVpZ2h0OiBub3JtYWw7XHJcbiAgY29sb3I6IGdyZWVuO1xyXG4gIGZvbnQtc2l6ZTogNTBweDtcclxufVxyXG4uc2xpZGUyMSAudGVzdCBpbWd7XHJcblxyXG4gIGZvbnQtc2l6ZTogMTAwcHg7XHJcbiAgbWFyZ2luOiAxMHB4O1xyXG59XHJcbi5zbGlkZTIxIC50ZXN0IHB7XHJcbiAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcclxuICBjb2xvcjogcmdiKDAsIDAsIDEzMyk7XHJcbiAgZm9udC1zaXplOiAzMHB4O1xyXG59XHJcbi5zbGlkZTIxIC50ZXN0IC5wcml4IHtcclxuICBjb2xvcjogcmVkO1xyXG4gIGxpbmUtaGVpZ2h0OiBub3JtYWw7XHJcbn1cclxuLnNsaWRlMjEgLnRlc3QgLmZhaWxke1xyXG4gIGNvbG9yOiByZWQ7XHJcbiAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcclxuICBmb250LXNpemU6IDYwcHg7XHJcbn1cclxuLnNsaWRlMjEgLnRlc3QgaW1ne1xyXG4gIHdpZHRoOiAxMDBweDtcclxufVxyXG5cclxuXHJcblxyXG4uc2xpZGUyMSAubmV4dEljb257XHJcbiAgb3JkZXI6IDM7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gIGp1c3RpZnktY29udGVudDogZmxleC1mbGV4LXN0YXJ0O1xyXG4gIGFsaWduLWl0ZW1zOiBmbGV4LWZsZXgtZW5kO1xyXG4gIHdpZHRoOiAyMiU7XHJcbiAgaGVpZ2h0OiA5MCU7XHJcbn1cclxuLnNsaWRlMjEgLm5leHRJY29uIGltZ3tcclxuICBjb2xvcjogZ3JlZW47XHJcbiAgd2lkdGg6IDgwcHg7XHJcbn1cclxuXHJcblxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMjQgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5zbGlkZTI0e1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTI0IC5pbWFnZXtcclxuICBtYXJnaW4tbGVmdDogNTBweDtcclxuICBvcmRlcjogMjtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGFsaWduLWl0ZW1zOmZsZXgtZmxleC1lbmQ7XHJcbiAgd2lkdGg6IDQ3JTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbiAgbWFyZ2luLXRvcDogMTBweDtcclxufVxyXG4uc2xpZGUyNCAudGVzdHtcclxuICBvcmRlcjogMTtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICB3aWR0aDo0MCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTI0IC50ZXN0IGgxe1xyXG4gIGZvbnQtc2l6ZTogNTBweDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuLnNsaWRlMjQgLnRlc3QgaW5wdXR7XHJcbiAgd2lkdGg6IDgwJTtcclxuICBoZWlnaHQ6IDQwcHg7XHJcbiAgbWFyZ2luOiA1MHB4IDAgNTBweCAwO1xyXG4gIGJvcmRlci1jb2xvcjogcmdiKDIxMiwgNSwgNSk7XHJcbn1cclxuLnNsaWRlMjQgLnRlc3QgbWF0LXJhZGlvLWdyb3Vwe1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxuICBhbGlnbi1pdGVtczogZmxleC1mbGV4LXN0YXJ0O1xyXG4gIHdpZHRoOiAxMDAlO1xyXG5cclxufVxyXG4uc2xpZGUyNCAudGVzdCAuY2FsZmxleC1lbmR7XHJcbiAgd2lkdGg6IDQ1MHB4O1xyXG4gIGhlaWdodDogNDUwcHg7XHJcbn1cclxuXHJcblxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMjUgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5zbGlkZTI1e1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUyNSAuaW1hZ2V7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBhbGlnbi1pdGVtczogZmxleC1mbGV4LWVuZDtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZmxleC1lbmQ7XHJcbiAgb3JkZXI6IDE7XHJcbiAgd2lkdGg6IDIyJTtcclxuICBoZWlnaHQ6IDIwJTtcclxufVxyXG5cclxuLnNsaWRlMjUgLnRlc3R7XHJcbiAgb3JkZXI6IDI7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGp1c3RpZnktY29udGVudDpmbGV4LWZsZXgtc3RhcnQ7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICB3aWR0aDo3MCU7XHJcbiAgaGVpZ2h0OiA4MCU7XHJcbn1cclxuXHJcbi5zbGlkZTI1IC50ZXN0IGltZ3tcclxuICB3aWR0aDogNTBweDtcclxufVxyXG4uc2xpZGUyNSAudGVzdCBwe1xyXG4gIGNvbG9yOiByZ2IoMCwgMCwgMTMzKTtcclxuICBmb250LXNpemU6IDQwcHg7XHJcbn1cclxuXHJcbi5zbGlkZTI1IC5uZXh0SWNvbntcclxuICBvcmRlcjogMztcclxufVxyXG4uc2xpZGUyNSAubmV4dEljb24gcHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuLnNsaWRlMjUgLm5leHRJY29uIC5zb2NpYWxNZWRpYSB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG59XHJcbi5zbGlkZTI1IC5uZXh0SWNvbiAuc29jaWFsTWVkaWEgaW1ne1xyXG4gIHdpZHRoOiA2MHB4O1xyXG59XHJcblxyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDI2ICoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uc2xpZGUyNntcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUyNiAuaW1hZ2V7XHJcbiAgb3JkZXI6IDI7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOmNlbnRlcjtcclxuICB3aWR0aDo1MyU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTI2IC5pbWFnZSBpbWd7XHJcbiAgb3JkZXI6IDI7XHJcbiAgaGVpZ2h0OiAzMCU7XHJcbiAgd2lkdGg6IDE1JTtcclxufVxyXG4uc2xpZGUyNiAuaW1hZ2UgaDF7XHJcbiAgb3JkZXI6IDI7XHJcbiAgZm9udC1zaXplOiA1MHB4O1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG59XHJcblxyXG4uc2xpZGUyNiAudGVzdHtcclxuICBvcmRlcjogMTtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGZsZXgtZmxleC1zdGFydDtcclxuICB3aWR0aDo2MCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTI2IC50ZXN0IGgxe1xyXG4gIGZvbnQtc2l6ZTogNTBweDtcclxufVxyXG4uc2xpZGUyNiAudGVzdCBpbnB1dHtcclxuICB3aWR0aDogOTAlO1xyXG4gIGhlaWdodDogNDBweDtcclxuICBib3JkZXItY29sb3I6IHJnYigyMTIsIDUsIDUpO1xyXG59XHJcblxyXG4uc2xpZGUyNiAudGVzdCAuYmxvY2sxe1xyXG4gIG1hcmdpbjogMjBweDtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gIGFsaWduLWl0ZW1zOiBmbGV4LWZsZXgtc3RhcnQ7XHJcbiAgd2lkdGg6IDkwJTtcclxufVxyXG4uc2xpZGUyNiAudGVzdCAuYmxvY2syIHtcclxuICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG4uc2xpZGUyNiAudGVzdCAuYmxvY2syIG1hdC1yYWRpby1ncm91cCAge1xyXG4gIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gIHdpZHRoOiA5MCU7XHJcbn1cclxuXHJcblxyXG4uc2xpZGUyNiAudGVzdCAuYmxvY2sxIGlucHV0e1xyXG4gIHdpZHRoOiA5MCU7XHJcbn1cclxuLnNsaWRlMjYgLnRlc3QgLmJsb2NrMSBsYWJlbHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgbWFyZ2luOiAwO1xyXG59XHJcblxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMjcgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5zbGlkZTI3e1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTI3IC5pbWFnZXtcclxuICBvcmRlcjogMTtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6Y2VudGVyO1xyXG4gIHdpZHRoOjUzJTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuXHJcblxyXG5cclxuXHJcbi5zbGlkZTI3IC50ZXN0e1xyXG4gIG9yZGVyOiAyO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZmxleC1zdGFydDtcclxuICB3aWR0aDo0MCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG5cclxufVxyXG4uc2xpZGUyNyAudGVzdCBpbnB1dCB7XHJcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxuICB3aWR0aDogNTAlO1xyXG4gIGJvcmRlci1yYWRpdXM6IDUwcHg7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDEzNCwgMTM0LCAxMzQpO1xyXG4gIGNvbG9yOndoaXRlO1xyXG59XHJcblxyXG4uc2xpZGUyNyAudGVzdCBtYXQtbGlzdCB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcclxuICB3aWR0aDogNzAlO1xyXG59XHJcbi5zbGlkZTI3IC50ZXN0IG1hdC1saXN0IG1hdC1saXN0LWl0ZW0ge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYigyNTUsIDI1NSwgMjU1KTtcclxuICBtYXJnaW4tdG9wOiAwO1xyXG4gIGJvcmRlcjoycHggc29saWQgcmdiKDIwMiwgMywgMyk7XHJcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHJnYigyMDIsIDMsIDMpOztcclxufVxyXG4uc2xpZGUyNyAudGVzdCBtYXQtbGlzdCBtYXQtbGlzdC1pdGVtOmhvdmVye1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYigyMTgsIDk4LCA5OCk7XHJcbn1cclxuLnNsaWRlMjcgLnRlc3QgLmNvbnRlbnRUYWJ7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiA1MDBweDtcclxuICBvdmVyZmxvdy15OiBzY3JvbGw7XHJcbn1cclxuXHJcblxyXG5cclxuXHJcblxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipyZXNwb25zaXZlIGNzcyAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjhweCkge1xyXG5cclxuICAuYm9keXtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgfVxyXG4gIC5mb290ZXJ7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIH1cclxuICAubGVmdHtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxuICAubGVmdCBpbWd7XHJcbiAgICB3aWR0aDogNjBweDtcclxuICB9XHJcbiAgLnBvaW50e1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiA3cHg7XHJcbiAgfVxyXG4gIC5jZW50ZXJ7XHJcbiAgICB3aWR0aDogOTAlO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDIwcHg7XHJcbiAgfVxyXG4gIC5jZW50ZXIgLmxpc3RQe1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIH1cclxuXHJcbiAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDAgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiAgLnNsaWRlMHtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZmxleC1zdGFydDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXIgO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gIH1cclxuICAuc2xpZGUwIC5pbWFnZXtcclxuICAgIG9yZGVyOiAyO1xyXG4gICAgd2lkdGg6IDgwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICB9XHJcblxyXG4gIC5zbGlkZTAgLnRlc3R7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gIH1cclxuXHJcbiAgLnNsaWRlMCAudGVzdCBoMXtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICB9XHJcblxyXG5cclxuICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMSAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuICAuc2xpZGUxe1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgfVxyXG4gIC5zbGlkZTEgLmltYWdle1xyXG4gICAgb3JkZXI6IDI7XHJcbiAgICB3aWR0aDogOTAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gIH1cclxuXHJcbiAgLnNsaWRlMSAudGVzdHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxuICAuc2xpZGUxIC50ZXN0IGgxe1xyXG4gICAgZm9udC1zaXplOiAzMHB4O1xyXG4gIH1cclxuXHJcbiAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDIgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiAgLnNsaWRlMntcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OnNwYWNlLWJldHdlZW47XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICB9XHJcbiAgLnNsaWRlMiAuaW1hZ2V7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6ZmxleC1lbmQ7XHJcbiAgfVxyXG4gIC5zbGlkZTIgLnRlc3R7XHJcbiAgICBvcmRlcjogMTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgd2lkdGg6MTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICB9XHJcbiAgLnNsaWRlMiAudGVzdCBoMXtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB9XHJcblxyXG4gIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAzICoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gIC5zbGlkZTN7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gIH1cclxuICAuc2xpZGUzIC5pbWFnZXtcclxuICAgIG9yZGVyOiAyO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XHJcbiAgfVxyXG4gIC5zbGlkZTMgLnRlc3R7XHJcbiAgICBvcmRlcjogMTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgd2lkdGg6MTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICB9XHJcbiAgLnNsaWRlMyAudGVzdCBoMXtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB9XHJcblxyXG5cclxuICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgNCAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuICAuc2xpZGU0e1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6c3BhY2UtYmV0d2VlbjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICB9XHJcbiAgLnNsaWRlNCAuaW1hZ2V7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIHdpZHRoOiA4MCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDpmbGV4LWVuZDtcclxuICB9XHJcbiAgLnNsaWRlNCAudGVzdHtcclxuICAgIG9yZGVyOiAxO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gIH1cclxuICAuc2xpZGU0IC50ZXN0IGgxe1xyXG4gICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIH1cclxuXHJcblxyXG4gIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSA1ICoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gIC5zbGlkZTV7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuXHJcbiAgfVxyXG4gIC5zbGlkZTUgLmltYWdle1xyXG4gICAgb3JkZXI6IDI7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIHotaW5kZXg6IDE7XHJcblxyXG4gIH1cclxuICAuc2xpZGU1IC50ZXN0e1xyXG4gICAgb3JkZXI6IDE7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gIH1cclxuICAuc2xpZGU1IC50ZXN0IGgxe1xyXG4gICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgbWFyZ2luLWJsb2NrOiBhdXRvO1xyXG4gIH1cclxuICAuc2xpZGU1IC50ZXN0IC5ibG9ja0J0bntcclxuXHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDIwJTtcclxuICB9XHJcbiAgLnNsaWRlNSAudGVzdCAuYmxvY2tCdG4gLnNvdXNCbG9ja3tcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogZmxleC1lbmQ7XHJcbiAgICB3aWR0aDogNzUlO1xyXG4gIH1cclxuXHJcbiAgLnNsaWRlNSAudGVzdCAuYmxvY2tCdG4gLnNvdXNCbG9jayAucXR5e1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIG1hcmdpbjogMTBweDtcclxuICB9XHJcblxyXG4gIC5zbGlkZTUgLnRlc3QgLmJsb2NrQnRuIC5zb3VzQmxvY2sgLnF0eSBpbnB1dHtcclxuICAgIG9yZGVyOiAyO1xyXG4gICAgd2lkdGg6IDgwcHg7XHJcbiAgICBoZWlnaHQ6IDUwLjVweDtcclxuICAgIGJvcmRlcjogNHB4IHNvbGlkIHJnYigyMTIsIDUsIDUpO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIH1cclxuXHJcbiAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDYgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiAgLnNsaWRlNntcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICB9XHJcbiAgLnNsaWRlNiAuaW1hZ2V7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczpjZW50ZXI7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gIH1cclxuICAuc2xpZGU2IC5pbWFnZSBpbWd7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIGhlaWdodDogMTUlO1xyXG4gICAgd2lkdGg6IDE1JTtcclxuICB9XHJcbiAgLnNsaWRlNiAuaW1hZ2UgaDF7XHJcbiAgICBvcmRlcjogMTtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbiAgfVxyXG5cclxuXHJcbiAgLnNsaWRlNiAudGVzdHtcclxuICAgIG9yZGVyOiAyO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZmxleC1zdGFydDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gIH1cclxuXHJcblxyXG4gIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSA3ICoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gIC5zbGlkZTd7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICB9XHJcbiAgLnNsaWRlNyAuY29sdW0xe1xyXG4gICAgb3JkZXI6IDE7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gIH1cclxuICAuc2xpZGU3IC5jb2x1bTJ7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogZmxleC1lbmQ7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICB9XHJcbiAgLnNsaWRlNyAuaW1hZ2V7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIHdpZHRoOiA5MCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgfVxyXG4gIC5zbGlkZTcgLnRlc3R7XHJcbiAgICBvcmRlcjogMTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgbWFyZ2luLXRvcDogMjBweDtcclxuICB9XHJcbiAgLnNsaWRlNyAudGVzdCBoMXtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB9XHJcbiAgLnNsaWRlNyAudGVzdCBpbnB1dHtcclxuICAgIHdpZHRoOiA4MCU7XHJcbiAgICBoZWlnaHQ6IDIwcHg7XHJcbiAgICBib3JkZXItY29sb3I6IHJnYigyMTIsIDUsIDUpO1xyXG4gIH1cclxuICAuc2xpZGU3IC5wcm9ncmVzc3tcclxuICAgIG9yZGVyOiAzO1xyXG4gIH1cclxuXHJcbiAgLnNsaWRlciB7XHJcbiAgICAtd2Via2l0LWFwcGVhcmFuY2U6IG5vbmU7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTVweDtcclxuICAgIGJhY2tncm91bmQ6IHJnYigyNTUsIDI1NSwgMjU1KTtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICBib3JkZXI6IDVweCBzb2xpZCByZ2IoMTg5LCA4LCA4KTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICB9XHJcblxyXG5cclxuICAvKiBmb3IgY2hyb21lL3NhZmFyaSAqL1xyXG4gIC5zbGlkZXI6Oi13ZWJraXQtc2xpZGVyLXRodW1iIHtcclxuICAgIC13ZWJraXQtYXBwZWFyYW5jZTogbm9uZTtcclxuICAgIGFwcGVhcmFuY2U6IG5vbmU7XHJcbiAgICB3aWR0aDogMTBweDtcclxuICAgIGhlaWdodDogNDBweDtcclxuICAgIGJhY2tncm91bmQ6IHJnYigyNDgsIDIyNCwgNSk7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICBib3JkZXI6IDVweCBzb2xpZCByZ2IoMjQ4LCAyMjQsIDUpO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMzBweDtcclxuICB9XHJcblxyXG4gIC8qIGZvciBmaXJlZm94ICovXHJcbiAgLnNsaWRlcjo6LW1vei1yYW5nZS10aHVtYiB7XHJcbiAgICB3aWR0aDogMTBweDtcclxuICAgIGhlaWdodDogMzBweDtcclxuICAgIGJhY2tncm91bmQ6IHJnYigyNTUsIDI1NSwgMjU1KTtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIGJvcmRlcjogNXB4IHNvbGlkIHJnYigxODksIDgsIDgpO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG4gIH1cclxuICAuX19yYW5nZXtcclxuICAgIHdpZHRoOiA4MCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgfVxyXG4gIC5fX3JhbmdlLXN0ZXB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgfVxyXG4gIC5fX3JhbmdlLXN0ZXB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgfVxyXG5cclxuICAuX19yYW5nZS1tYXh7XHJcbiAgICBmbG9hdDogcmlnaHQ7XHJcbiAgfVxyXG5cclxuXHJcblxyXG4gIC5fX3JhbmdlIGlucHV0OjpyYW5nZS1wcm9ncmVzcyB7XHRiYWNrZ3JvdW5kOiByZ2IoMTg5LCA4LCA4KTtcclxuICB9XHJcbiAgLnNsaWRlciBpbnB1dFt0eXBlPXJhbmdlXTo6LW1vei1yYW5nZS1wcm9ncmVzcyB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzY1N2EwO1xyXG4gIH1cclxuICAuX19yYW5nZS1zdGVwIGRhdGFsaXN0IHtcclxuICAgIHBvc2l0aW9uOnJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgIGhlaWdodDogYXV0bztcclxuICAgIGJvdHRvbTogNnB4O1xyXG4gICAgLyogZGlzYWJsZSB0ZXh0IHNlbGVjdGlvbiAqL1xyXG4gICAgLXdlYmtpdC11c2VyLXNlbGVjdDogbm9uZTsgLyogU2FmYXJpICovXHJcbiAgICAtbW96LXVzZXItc2VsZWN0OiBub25lOyAvKiBGaXJlZm94ICovXHJcbiAgICAtbXMtdXNlci1zZWxlY3Q6IG5vbmU7IC8qIElFMTArL0VkZ2UgKi9cclxuICAgIHVzZXItc2VsZWN0OiBub25lOyAvKiBTdGFuZGFyZCAqL1xyXG4gICAgLyogZGlzYWJsZSBjbGljayBldmVudHMgKi9cclxuICAgIHBvaW50ZXItZXZlbnRzOm5vbmU7XHJcbiAgfVxyXG4gIC5fX3JhbmdlLXN0ZXAgZGF0YWxpc3Qgb3B0aW9uIHtcclxuICAgIHdpZHRoOiAxMHB4O1xyXG4gICAgaGVpZ2h0OiAxMHB4O1xyXG4gICAgbWluLWhlaWdodDogMTBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDEwMHB4O1xyXG4gICAgLyogaGlkZSB0ZXh0ICovXHJcbiAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gICAgcGFkZGluZzowO1xyXG4gICAgbGluZS1oZWlnaHQ6IDQwcHg7XHJcbiAgfVxyXG5cclxuXHJcbiAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDggKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiAgLnNsaWRlOHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgfVxyXG4gIC5zbGlkZTggLmltYWdle1xyXG4gICAgb3JkZXI6IDI7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgYWxpZ24taXRlbXM6IGZsZXgtZmxleC1lbmQ7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICB9XHJcbiAgLnNsaWRlOCAudGVzdHtcclxuICAgIG9yZGVyOiAxO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gIH1cclxuICAuc2xpZGU4IC50ZXN0IGgxe1xyXG4gICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIH1cclxuXHJcbiAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDkqKioqKioqKioqKioqKioqKioqKioqKi9cclxuICAuc2xpZGU5e1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogNzUwcHg7XHJcbiAgfVxyXG4gIC5zbGlkZTkgLmltYWdle1xyXG4gICAgb3JkZXI6IDI7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgfVxyXG4gIC5zbGlkZTkgLnRlc3R7XHJcbiAgICBvcmRlcjogMTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgd2lkdGg6MTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICB9XHJcbiAgLnNsaWRlOSAudGVzdCBoMXtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB9XHJcbiAgLnNsaWRlOSAudGVzdCBtYXQtcmFkaW8tZ3JvdXB7XHJcbiAgICBtYXJnaW4tdG9wOiA1MHB4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxuXHJcblxyXG4gIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAxMCoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gIC5zbGlkZTEwe1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogNzUwcHg7XHJcbiAgfVxyXG4gIC5zbGlkZTEwIC5pbWFnZXtcclxuICAgIG9yZGVyOiAyO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICB9XHJcbiAgLnNsaWRlMTAgLnRlc3R7XHJcbiAgICBvcmRlcjogMTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgd2lkdGg6MTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICB9XHJcbiAgLnNsaWRlMTAgLnRlc3QgaDF7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgfVxyXG4gIC5zbGlkZTEwIC50ZXN0IC5ibG9ja3tcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgfVxyXG5cclxuICAuc2xpZGUxMCAudGVzdCAuYmxvY2sgLnNvdXNCbG9ja3tcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgbWFyZ2luLXRvcDogMTBweDtcclxuICB9XHJcblxyXG5cclxuXHJcbiAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDExICoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gIC5zbGlkZTExe1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogNzUwcHg7XHJcbiAgfVxyXG4gIC5zbGlkZTExIC5pbWFnZXtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gIH1cclxuICAuc2xpZGUxMSAudGVzdHtcclxuICAgIG9yZGVyOiAxO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gIH1cclxuICAuc2xpZGUxMSAudGVzdCBoMXtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB9XHJcblxyXG4gIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAxMiAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuICAuc2xpZGUxMntcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OmNlbnRlcjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiA3MDBweDtcclxuICB9XHJcbiAgLnNsaWRlMTIgLmltYWdle1xyXG4gICAgb3JkZXI6IDI7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gICAgYWxpZ24taXRlbXM6Y2VudGVyO1xyXG4gICAgd2lkdGg6MTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICB9XHJcbiAgLnNsaWRlMTIgLmltYWdlIGgxe1xyXG4gICAgb3JkZXI6IDE7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG4gIH1cclxuXHJcblxyXG4gIC5zbGlkZTEyIC50ZXN0e1xyXG4gICAgb3JkZXI6IDI7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHdpZHRoOjEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgfVxyXG5cclxuXHJcbiAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDEzICoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gIC5zbGlkZTEze1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiA3NTBweDtcclxuICB9XHJcbiAgLnNsaWRlMTMgLmltYWdle1xyXG4gICAgb3JkZXI6IDI7XHJcbiAgICB3aWR0aDogODAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gIH1cclxuICAuc2xpZGUxMyAudGVzdHtcclxuICAgIG9yZGVyOiAxO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gIH1cclxuICAuc2xpZGUxMyAudGVzdCBoMXtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB9XHJcblxyXG4gIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAxNCoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gIC5zbGlkZTE0e1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogNzUwcHg7XHJcbiAgfVxyXG4gIC5zbGlkZTE0IC5pbWFnZXtcclxuICAgIG9yZGVyOiAyO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgfVxyXG4gIC5zbGlkZTE0IC50ZXN0e1xyXG4gICAgb3JkZXI6IDE7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHdpZHRoOjEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgfVxyXG4gIC5zbGlkZTE0IC50ZXN0IGgxe1xyXG4gICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIH1cclxuICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMTUgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiAgLnNsaWRlMTV7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0Ojc1MHB4O1xyXG4gIH1cclxuICAuc2xpZGUxNSAuaW1hZ2V7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczpjZW50ZXI7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gIH1cclxuICAuc2xpZGUxNSAuaW1hZ2UgcHtcclxuICAgIG9yZGVyOiAyO1xyXG5cclxuICB9XHJcbiAgLnNsaWRlMTUgLmltYWdlIGgxe1xyXG4gICAgb3JkZXI6IDE7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG4gIH1cclxuXHJcblxyXG5cclxuICAuc2xpZGUxNSAudGVzdHtcclxuICAgIG9yZGVyOiAyO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZmxleC1zdGFydDtcclxuICAgIGFsaWduLWl0ZW1zOmNlbnRlcjtcclxuICAgIHdpZHRoOjEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgfVxyXG5cclxuXHJcblxyXG4gIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAxNioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gIC5zbGlkZTE2e1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogNzUwcHg7XHJcbiAgfVxyXG4gIC5zbGlkZTE2IC5pbWFnZXtcclxuICAgIG9yZGVyOiAyO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgfVxyXG4gIC5zbGlkZTE2IC50ZXN0e1xyXG4gICAgb3JkZXI6IDE7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogZmxleC1mbGV4LXN0YXJ0O1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHdpZHRoOjEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgfVxyXG4gIC5zbGlkZTE2IC50ZXN0IGgxe1xyXG4gICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIH1cclxuXHJcbiAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDE3ICoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gIC5zbGlkZTE3e1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDo3NTBweDtcclxuICB9XHJcbiAgLnNsaWRlMTcgLmltYWdle1xyXG4gICAgb3JkZXI6IDI7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6Y2VudGVyO1xyXG4gICAgd2lkdGg6MTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICB9XHJcbiAgLnNsaWRlMTcgLmltYWdlIGgxe1xyXG4gICAgb3JkZXI6IDE7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG4gIH1cclxuXHJcbiAgLnNsaWRlMTcgLnRlc3R7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWZsZXgtc3RhcnQ7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgd2lkdGg6MTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICB9XHJcblxyXG4gIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAxOCAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuICAuc2xpZGUxOHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICB9XHJcbiAgLnNsaWRlMTggLmltYWdle1xyXG4gICAgb3JkZXI6IDE7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6Y2VudGVyO1xyXG4gICAgd2lkdGg6MTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICB9XHJcbiAgLnNsaWRlMTggLmltYWdlIGltZ3tcclxuICAgIG9yZGVyOiAyO1xyXG4gICAgaGVpZ2h0OiAyMCU7XHJcbiAgICB3aWR0aDogMTUlO1xyXG4gIH1cclxuICAuc2xpZGUxOCAuaW1hZ2UgaDF7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbiAgfVxyXG5cclxuICAuc2xpZGUxOCAudGVzdHtcclxuICAgIG9yZGVyOiAyO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQgO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHdpZHRoOjEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgfVxyXG5cclxuXHJcbiAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDE5ICoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gIC5zbGlkZTE5e1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gIH1cclxuICAuc2xpZGUxOSAuaW1hZ2V7XHJcbiAgICBtYXJnaW4tbGVmdDogNTBweDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIHdpZHRoOiA5MCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgfVxyXG4gIC5zbGlkZTE5IC50ZXN0e1xyXG4gICAgb3JkZXI6IDE7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHdpZHRoOjEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgfVxyXG4gIC5zbGlkZTE5IC50ZXN0IGgxe1xyXG4gICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIH1cclxuXHJcblxyXG4gIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAyMCAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuICAuc2xpZGUyMHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICB9XHJcbiAgLnNsaWRlMjAgLmltYWdle1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIG9yZGVyOiAxO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDgwJTtcclxuICB9XHJcbiAgLnNsaWRlMjAgLmltYWdlIGltZ3tcclxuICAgIGhlaWdodDogNjAlO1xyXG4gICAgd2lkdGg6IDQwJTtcclxuICAgIGZpbHRlcjogZHJvcC1zaGFkb3coMC40cmVtIDAuNHJlbSAwLjQ1cmVtIHJnYmEoMCwgMCwgMzAsIDAuNSkpO1xyXG4gIH1cclxuICAuc2xpZGUyMCAudGVzdHtcclxuICAgIG9yZGVyOiAyO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgd2lkdGg6MTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICB9XHJcbiAgLnNsaWRlMjAgLnRlc3QgaDF7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgfVxyXG4gIC5zbGlkZTIwIC50ZXN0IC5ibG9jazF7XHJcbiAgICBtYXJnaW4tdG9wOiAwcHg7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgd2lkdGg6IDkwJTtcclxuICB9XHJcbiAgLnNsaWRlMjAgLnRlc3QgLmJsb2NrMiB7XHJcbiAgICBtYXJnaW4tdG9wOiAwcHg7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgd2lkdGg6IDkwJTtcclxuICB9XHJcblxyXG4gIC5zbGlkZTIwIC50ZXN0IC5ibG9jazIgbGFiZWx7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgYWxpZ24taXRlbXM6IGZsZXgtZmxleC1zdGFydDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gIH1cclxuXHJcbiAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDIxICoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gIC5zbGlkZTIxe1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICB9XHJcbiAgLnNsaWRlMjEgLmltYWdle1xyXG4gICAgZGlzcGxheTpub25lO1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIG9yZGVyOiAxO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwJTtcclxuICB9XHJcbiAgLnNsaWRlMjEgLnRlc3R7XHJcbiAgICBvcmRlcjogMTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OmNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDo4MCU7XHJcbiAgICBoZWlnaHQ6IDgwJTtcclxuICB9XHJcblxyXG4gIC5zbGlkZTIxIC50ZXN0IGgxe1xyXG4gICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcclxuICAgIGNvbG9yOiBncmVlbjtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB9XHJcbiAgLnNsaWRlMjEgLnRlc3QgaW1ne1xyXG4gICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcclxuICAgIGZvbnQtc2l6ZTogODBweDtcclxuICAgIG1hcmdpbjogMTBweDtcclxuICB9XHJcbiAgLnNsaWRlMjEgLnRlc3QgcHtcclxuICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XHJcbiAgICBjb2xvcjogcmdiKDAsIDAsIDEzMyk7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgfVxyXG4gIC5zbGlkZTIxIC50ZXN0IC5wcml4IHtcclxuICAgIGNvbG9yOiByZWQ7XHJcbiAgfVxyXG4gIC5zbGlkZTIxIC50ZXN0IC5mYWlsZHtcclxuICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XHJcbiAgICB3aWR0aDogMzYwcHg7XHJcbiAgICBjb2xvcjogcmVkO1xyXG4gICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIH1cclxuICAuc2xpZGUyMSAudGVzdCBpbWd7XHJcbiAgICB3aWR0aDogODBweDtcclxuICB9XHJcblxyXG4gIC5zbGlkZTIxIC5uZXh0SWNvbntcclxuICAgIG9yZGVyOiAzO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xyXG4gICAgYWxpZ24taXRlbXM6IGZsZXgtZmxleC1lbmQ7XHJcbiAgICB3aWR0aDo5NyU7XHJcbiAgICBoZWlnaHQ6IDE1JTtcclxuICB9XHJcbiAgLnNsaWRlMjEgLm5leHRJY29uIGltZ3tcclxuICAgIGNvbG9yOiBncmVlbjtcclxuICAgIHdpZHRoOiA4MHB4O1xyXG4gIH1cclxuXHJcblxyXG5cclxuICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMjQgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiAgLnNsaWRlMjR7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgfVxyXG4gIC5zbGlkZTI0IC5pbWFnZXtcclxuICAgIG1hcmdpbi1sZWZ0OiA1MHB4O1xyXG4gICAgb3JkZXI6IDI7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgYWxpZ24taXRlbXM6ZmxleC1mbGV4LWVuZDtcclxuICAgIHdpZHRoOiA4OCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgfVxyXG4gIC5zbGlkZTI0IC50ZXN0e1xyXG4gICAgb3JkZXI6IDE7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgd2lkdGg6MTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgfVxyXG4gIC5zbGlkZTI0IC50ZXN0IGgxe1xyXG4gICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIH1cclxuICAuc2xpZGUyNCAudGVzdCBpbnB1dHtcclxuICAgIHdpZHRoOiA4MCU7XHJcbiAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICBtYXJnaW46IDUwcHggMCA1MHB4IDA7XHJcbiAgICBib3JkZXItY29sb3I6IHJnYigyMTIsIDUsIDUpO1xyXG4gIH1cclxuICAuc2xpZGUyNCAudGVzdCBtYXQtcmFkaW8tZ3JvdXB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gICAgYWxpZ24taXRlbXM6IGZsZXgtZmxleC1zdGFydDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgbWFyZ2luLXRvcDogMjBweDtcclxuXHJcbiAgfVxyXG4gIC5zbGlkZTI0IC50ZXN0IC5jYWxmbGV4LWVuZHtcclxuICAgIHdpZHRoOiAzNTBweDtcclxuICAgIGhlaWdodDogMzUwcHg7XHJcbiAgfVxyXG5cclxuXHJcbiAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDI1ICoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gIC5zbGlkZTI1e1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICB9XHJcbiAgLnNsaWRlMjUgLmltYWdle1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBmbGV4LWZsZXgtZW5kO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWZsZXgtZW5kO1xyXG4gICAgb3JkZXI6IDE7XHJcbiAgICB3aWR0aDogMjIlO1xyXG4gICAgaGVpZ2h0OiAyMCU7XHJcbiAgfVxyXG5cclxuICAuc2xpZGUyNSAudGVzdHtcclxuICAgIG9yZGVyOiAyO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6ZmxleC1zdGFydDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gICAgaGVpZ2h0OiA4MCU7XHJcbiAgfVxyXG5cclxuICAuc2xpZGUyNSAudGVzdCBpbWd7XHJcbiAgICB3aWR0aDogNTBweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDMwcHg7XHJcbiAgfVxyXG4gIC5zbGlkZTI1IC50ZXN0IHB7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBjb2xvcjogcmdiKDAsIDAsIDEzMyk7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xyXG4gIH1cclxuXHJcbiAgLnNsaWRlMjUgLm5leHRJY29ue1xyXG4gICAgb3JkZXI6IDM7XHJcbiAgfVxyXG4gIC5zbGlkZTI1IC5uZXh0SWNvbiBwe1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcclxuICB9XHJcbiAgLnNsaWRlMjUgLm5leHRJY29uIC5zb2NpYWxNZWRpYSB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIH1cclxuICAuc2xpZGUyNSAubmV4dEljb24gLnNvY2lhbE1lZGlhIGltZ3tcclxuICAgIHdpZHRoOiA2MHB4O1xyXG4gIH1cclxuXHJcblxyXG4gIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAyNiAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuICAuc2xpZGUyNntcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gIH1cclxuICAuc2xpZGUyNiAuaW1hZ2V7XHJcbiAgICBvcmRlcjogMTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczpjZW50ZXI7XHJcbiAgICB3aWR0aDo1MyU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgfVxyXG4gIC5zbGlkZTI2IC5pbWFnZSBpbWd7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIGhlaWdodDogMjAlO1xyXG4gICAgd2lkdGg6IDE1JTtcclxuICB9XHJcbiAgLnNsaWRlMjYgLmltYWdlIGgxe1xyXG4gICAgb3JkZXI6IDI7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG4gIH1cclxuXHJcbiAgLnNsaWRlMjYgLnRlc3R7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogZmxleC1mbGV4LXN0YXJ0O1xyXG4gICAgd2lkdGg6MTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICB9XHJcbiAgLnNsaWRlMjYgLnRlc3QgaDF7XHJcbiAgICBmb250LXNpemU6IDUwcHg7XHJcbiAgfVxyXG4gIC5zbGlkZTI2IC50ZXN0IGlucHV0e1xyXG4gICAgd2lkdGg6IDkwJTtcclxuICAgIGhlaWdodDogNDBweDtcclxuICAgIGJvcmRlci1jb2xvcjogcmdiKDIxMiwgNSwgNSk7XHJcbiAgICBtYXJnaW4tbGVmdDogNXB4O1xyXG4gIH1cclxuXHJcbiAgLnNsaWRlMjYgLnRlc3QgLmJsb2NrMXtcclxuICAgIG1hcmdpbjogMjBweDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICBhbGlnbi1pdGVtczogZmxleC1mbGV4LXN0YXJ0O1xyXG4gICAgd2lkdGg6IDkwJTtcclxuICB9XHJcbiAgLnNsaWRlMjYgLnRlc3QgLmJsb2NrMiB7XHJcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxuICAuc2xpZGUyNiAudGVzdCAuYmxvY2syIG1hdC1yYWRpby1ncm91cCAge1xyXG4gICAgbWFyZ2luLXRvcDogMjBweDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICB3aWR0aDogOTAlO1xyXG4gIH1cclxuXHJcblxyXG4gIC5zbGlkZTI2IC50ZXN0IC5ibG9jazEgaW5wdXR7XHJcbiAgICB3aWR0aDogOTAlO1xyXG4gIH1cclxuICAuc2xpZGUyNiAudGVzdCAuYmxvY2sxIGxhYmVse1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgfVxyXG5cclxuXHJcbn1cclxuIl19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TestComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-test',
                templateUrl: './test.component.html',
                styleUrls: ['./test.component.css']
            }]
    }], function () { return [{ type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"] }, { type: _services_crm_test_eligibilite_service__WEBPACK_IMPORTED_MODULE_2__["TestEligibiliteService"] }]; }, null); })();


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\utilisateur\Videos\cpn-crm-angu\resources\frontend\frontCrm\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map